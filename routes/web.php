<?php

use App\Models\Transaction;
use Laravel\Fortify\Features;
use App\Http\Controllers\Admin\Blog;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Http;
use App\Http\Controllers\Admin\Pages;
use App\Http\Controllers\Admin\Users;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\User\Funding;
use App\Http\Controllers\User\Trading;
use App\Http\Controllers\Admin\Setting;
use App\Http\Controllers\User\BtcTrade;
use App\Notifications\TestNotification;
use App\Http\Controllers\Admin\Messages;
use App\Http\Controllers\User\GiftCards;
use App\Http\Controllers\GuestController;
use App\Http\Controllers\Admin\OtherAssets;
use App\Http\Controllers\User\Authorization;
use App\Http\Controllers\User\EtherumWallet;
use App\Http\Controllers\User\LitcoinWallet;
use App\Http\Controllers\User\OtpController;
use App\Http\Controllers\User\PagesController;
use App\Http\Controllers\Admin\CardsController;
use App\Http\Controllers\Admin\CountriesController;
use App\Http\Controllers\User\OtherAssetsController;
use App\Http\Controllers\Admin\TransactionController;
use App\Http\Controllers\Override\RegisterController;
use Laravel\Fortify\Http\Controllers\RegisteredUserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



/*
|------------------------------------------------------------------------
| Guest Route
|------------------------------------------------------------------------
*/

Route::view('/mail/template', 'mail');

Route::get('/', [GuestController::class, 'index'])->name('welcome');
Route::get('/company/about', [GuestController::class, 'about'])->name('about');
Route::get('/company/career', [GuestController::class, 'career'])->name('career');

Route::get('/trade/products', [GuestController::class, 'products'])->name('products.guest');
Route::get('/trade/rates', [GuestController::class, 'rates'])->name('rates');
Route::get('/trade/calculate/rates', [GuestController::class, 'calculator'])->name('calculate');


Route::get('/news/posts/{cat_id?}', [GuestController::class, 'blog'])->name('guest.blog');
Route::get('/news/post/{post_id}', [GuestController::class, 'show_blog'])->name('guest.blog.post');


Route::get('/support/faq', [GuestController::class, 'faq'])->name('faq');
Route::get('/support/contact', [GuestController::class, 'contact'])->name('contact');

Route::get('/legal/terms', [GuestController::class, 'terms'])->name('terms');
Route::get('/legal/policy', [GuestController::class, 'policy'])->name('policy');
Route::get('/legal/policy/anti-money-laundering', [GuestController::class, 'atml'])->name('atml');


//Overide Register Routes
if (Features::enabled(Features::registration())) {
    Route::get('/sign-up', [RegisterController::class, 'create'])
        ->middleware(['guest'])
        ->name('register'); 

    Route::post('/sign-up', [RegisteredUserController::class, 'store'])
        ->middleware(['guest']);
}

//User Routes
Route::get('/wallet/transfer/process', [Funding::class, 'transfer_process'])->name('transfer.proccess');
Route::middleware(['auth:sanctum', 'verified', 'is_active'])->group(function () {
    Route::get('/dashboard', [PagesController::class, 'index'])->name('dashboard');
    Route::get('/settings', [PagesController::class, 'settings'])->name('settings');
    Route::get('/wallet', [PagesController::class, 'wallet'])->name('wallet');
    Route::get('/products', [PagesController::class, 'products'])->name('products');
    Route::get('/transactions', [PagesController::class, 'transactions'])->name('transactions.index');
    Route::get('/cards', [PagesController::class, 'cards'])->name('cards.index');
    Route::get('/rates', [PagesController::class, 'rates'])->name('rates.act');

    // Route::get('/wallet/transfer/process/{transfer_data?}', [Funding::class, 'transfer_process'])->name('wallet.transfer.proccess');
    Route::post('/wallet/tranfer/my-account', [Funding::class, 'my_account'])->name('transfer.my-account')->middleware('twoFa');

    //Ngn Wallet
    Route::get('/wallet/fund/transfer/{to?}', [Funding::class, 'ngn_transfer'])->name('wallet.ngn.transfer');
    Route::post('/wallet/fund/continue', [Funding::class, 'ngn_fund'])->name('wallet.ngn.continue-fund')->middleware('twoFa');
    Route::get('/wallet/fund/process/', [Funding::class, 'proccess_ngn_fund'])->name('wallet.fund.proccess');

    //Etherum Wallet
    Route::get('/wallet/etherum/create', [EtherumWallet::class, 'create'])->name('wallet.etherum.create');
    Route::post('/wallet/etherum/create_new', [EtherumWallet::class, 'save'])->name('wallet.etherum.create.save');
    
    //Litcoin Wallet
    Route::get('/wallet/litcoin/create', [LitcoinWallet::class, 'create'])->name('wallet.litcoin.create');
    Route::post('/wallet/litcoin/create/new', [LitcoinWallet::class, 'save'])->name('wallet.litcoin.create.save');
   


    // Route::get('/wallet/Litcoin/transactions/', [EtherumWallet::class, 'create'])->name('wallet.etherum.index')->middleware('ether_wallet');


    //Transfer
    Route::get('/transfer/to/turvender/user', [Funding::class,'transafer_truvender'])->name('transfer.truvender')->middleware('twoFa');
    Route::get('/transfer/to/other-account/post', [Funding::class,'transfer_other_bank_account'])->name('transfer.other-account.post')->middleware('twoFa');
    Route::get('/transfer/peer2peer', [Funding::class,'peer_2_peer'])->name('transfer.peer-2-peer');
    Route::get('/transfer/peer2peer/perview/{data}', [Funding::class, 'peer_2_peer_preview'])->name('transfer.peer2peer.preview');
    Route::get('/transfer/peer2peer/join/peer', [Funding::class, 'peer_2_peer_post'])->name('transfer.peer2peer.post')->middleware('twoFa');

    
    //Gift Cards
    Route::get('/giftcards/buy', [GiftCards::class, 'buy_cards'])->name('cards.buy');
    Route::get('/giftcards/sell', [GiftCards::class, 'sell_cards'])->name('cards.sell');
    
    Route::get('/card/{card}/buy', [GiftCards::class, 'buy_card'])->name('buy.card');
    Route::get('/card/{card}/sell', [GiftCards::class, 'sell_card'])->name('sell.card');

    Route::get('/trade/preview/{data}', [Trading::class, 'preview'])->name('trading.preview');
    Route::get('/trade/index', [Trading::class, 'index'])->name('trading.index');
    
    Route::get('/trade/giftcard/sell/{trade_id}/images', [GiftCards::class, 'image_upload'])->name('trade.image-upload');
    Route::post('/trade/giftcard/{trade_id}/upload-images', [GiftCards::class, 'submit_images'])->name('trade.submit-images');

    Route::get('/trade/giftcard/buy/submit', [GiftCards::class, 'trade_card_buy'])->name('buy.gift-card.submit')->middleware('twoFa');

    //Trade Airtime
    Route::get('/trade/refill', [Trading::class, 'refill_index'])->name('refill');

    Route::get('/trade/buy/airtime', [Trading::class,'buy_airtime'])->name('airtime.buy');
    Route::get('/trade/buy/data', [Trading::class,'buy_data'])->name('data.buy');

    Route::post('/trade/buy/airtime', [Trading::class,'buy_airtime_'])->name('airtime.buy.post')->middleware('twoFa');
    Route::post('/trade/buy/data', [Trading::class,'buy_data_'])->name('data.buy.post')->middleware('twoFa');

    //Buy And Sell Btc
    Route::get('/btc-trade/btc', [BtcTrade::class, 'trade_btc'])->name('btc.index');
    Route::get('/btc-trade/sell', [BtcTrade::class,'sell_btc'])->name('btc.sell');
    Route::get('/trade/{data}/btc', [BtcTrade::class,'sell_btc_'])->name('btc.sell.post');
    Route::get('/btc-trade/buy', [BtcTrade::class,'buy_btc'])->name('btc.buy');
    Route::get('/btc-trade/{data}/btc', [BtcTrade::class,'buy_btc_'])->name('btc.buy.post')->middleware('twoFa');

    Route::view('/success', 'users.success')->name('transation-success');
    Route::view('/service/unavailable', 'users.services.unavailable')->name('service.unavalable');

    Route::get('/transaction/verify-otp', [OtpController::class,'showVerifyPage'])->name('otp.page');
    Route::post('/transaction/verify-otp/verify-code', [OtpController::class,'verify'])->name('otp.verify-code');


    Route::get('/trade/{asset_id}', [OtherAssetsController::class, 'trade'] )->name('trade.other-asset');
    Route::get('/trade/{data}/next', [OtherAssetsController::class, 'next'] )->name('trade.other-asset.next');
    Route::post('/trade/other-asset/post', [OtherAssetsController::class, '_next_'] )->name('trade.other-asset.post');
    Route::post('/trade/other-asset/cancel', [OtherAssetsController::class, '_cancel_'] )->name('trade.other-asset.cancel');
    
});


Route::middleware(['auth:sanctum', 'ether_wallet'])->group(function () {
    Route::get('/wallet/etherum/index', [EtherumWallet::class, 'index'])->name('wallet.etherum.index');
    Route::get('/wallet/etherum/info', [EtherumWallet::class,'info'])->name('wallet.etherum.info');
    Route::get('/trade/crypto/etherum', [EtherumWallet::class,'trade'])->name('trade.etherum');
    
    Route::get('/trade/crypto/litoin/buy/post', [EtherumWallet::class, 'buy_post'])->name('trade.etherum.buy')->middleware('twoFa');
    Route::get('/trade/crypto/etherum/sell/post', [EtherumWallet::class, 'sell_post'])->name('trade.etherum.sell')->middleware('twoFa');
    
    Route::get('/wallet/transfer/asset/ethereum', [EtherumWallet::class,'send'])->name('wallet.etherum.transfer')->middleware('twoFa');

});


Route::middleware(['auth:sanctum', 'litcoin_wallet'])->group(function () {
    Route::get('/wallet/litcoin/index', [LitcoinWallet::class, 'index'])->name('wallet.litcoin.index');
    Route::get('/wallet/litcoin/info', [LitcoinWallet::class,'info'])->name('wallet.litcoin.info');
    
    Route::get('/trade/crypto/litcoin', [LitcoinWallet::class,'trade'])->name('trade.litcoin');
    Route::get('/trade/crypto/litoin/buy/post', [LitcoinWallet::class, 'buy_post'])->name('trade.litcoin.buy')->middleware('twoFa');
    Route::get('/trade/crypto/litcoin/sell/post', [LitcoinWallet::class, 'sell_post'])->name('trade.litcoin.sell')->middleware('twoFa');
});


Route::get('/user/is-restircted', [PagesController::class,'restrict'])->name('is_restricted'); //Restriction Route


//Admin Routes
Route::middleware(['auth:sanctum', 'verified', 'moderator'])->group(function () {
    Route::get('/admin/index', [Pages::class, 'index'])->name('admin.index');

    /*-----------------------------------------
    |---------------- Admin User Route---------------
    -------------------------------------------*/
    Route::get('/admin/users', [Users::class, 'index'])->name('admin.users');
    Route::get('/admin/user/{user_id}/profile',[Users::class, 'profile'])->name('admin.users.profile');
    Route::get('/admin/user/{user_id}/profile/edit', [Users::class, 'edit_profile'])->name('admin.users.edit-profile');

    //Blacklist User
    Route::get('/admin/{user_id}/blacklist/remove', [Users::class, 'remove_form_blacklist'])->name('admin.users.blacklist.remove');
    Route::post('/admin/user/add-to/blacklist/', [Users::class, 'add_to_blacklist'])->name('admin.users.blacklist.add');


    /*-----------------------------------------
    |---------------- Blog Posts---------------
    -------------------------------------------*/
    Route::get('/admin/blog/{cat_id?}', [Blog::class, 'index'])->name('admin.blog.index');
    Route::get('/admin/post/create', [Blog::class, 'create'])->name('admin.blog.create');
    Route::get('/admin/blog/{blog_id}/show', [Blog::class, 'show'])->name('admin.blog.show');
    Route::post('/admin/blog/create/save', [Blog::class, 'store'])->name('admin.blog.create.save');
    Route::get('/admin/blog/{blog_id}/edit', [Blog::class, 'edit'])->name('admin.blog.edit');
    Route::post('/admin/blog/{blog_id}/edit/save', [Blog::class, 'update'])->name('admin.blog.edit.save');
    Route::get('/admin/blog/{blog_id}/trash', [Blog::class, 'destroy'])->name('admin.blog.delete.trash');


    /*-----------------------------------------
    |---------------- Transactions ---------------
    -------------------------------------------*/
    Route::get('/admin/transactions', [TransactionController::class, 'index'])->name('admin.transactions');
    Route::get('/admin/transaction/{trx_id}/details', [TransactionController::class, 'details'])->name('admin.transaction.detail');


    /*-----------------------------------------
    |---------------- Country ---------------
    -------------------------------------------*/
    Route::get('/admin/countries/', [CountriesController::class, 'index'])->name('admin.countries');
    Route::post('/admin/country/add', [CountriesController::class, 'store'])->name('admin.country.add');


    /*-----------------------------------------
    |---------------- Messages ---------------
    -------------------------------------------*/
    Route::get('/admin/messages', [Messages::class, 'index'])->name('admin.messages');
    Route::get('/admin/message/create', [Messages::class, 'create'])->name('admin.message.create');
    Route::post('/admin/message/send', [Messages::class, 'send'])->name('admin.message.send');

    /*-----------------------------------------
    |---------------- Services ---------------
    -------------------------------------------*/
    Route::get('/admin/sevices/list', [Pages::class, 'services'])->name('admin.services');
    Route::get('/admin/sevices/{service_id}/update', [Pages::class, 'update_services'])->name('admin.services.update');

    /*-----------------------------------------
    |---------------- Bitcoin Sections ---------------
    -------------------------------------------*/
    Route::get('/rates/crypto/btc/{crypto?}', [Pages::class, 'btc_rates'])->name('admin.rates.btc');
    Route::get('/rates/crypto/ltc/{crypto?}', [Pages::class, 'ltc_rates'])->name('admin.rates.ltc');
    Route::get('/rates/crypto/eth/{crypto?}', [Pages::class, 'eth_rates'])->name('admin.rates.eth');


    /*-----------------------------------------
    |---------------- Card ---------------
    -------------------------------------------*/
    Route::get('/admin/giftcards', [CardsController::class, 'index'])->name('admin.cards.index');
    Route::post('/admin/giftcard/save', [CardsController::class, 'save'])->name('admin.cards.save');
    Route::get('/admin/giftcard/{card_id}/rates', [CardsController::class, 'get_rates'])->name('admin.cards.get-rates');
    Route::get('/admin/giftcard/{card_id}/trades', [CardsController::class, 'get_trades'])->name('admin.cards.get-trades');

    Route::get('/admin/giftcard/add-rate/{card_id?}', [CardsController::class, 'add_rate'])->name('admin.card.add-rate');
    Route::post('/admin/giftcard/save-rate', [CardsController::class, 'save_rate'])->name('admin.card.save-rate');

    Route::get('/admin/card/rates', [CardsController::class, 'card_rates'])->name('admin.card-rates');

    Route::get('/admin/giftcard/rate/{rate_id}/edit', [CardsController::class, 'edit_rate'])->name('admin.card.rate.edit');
    Route::post('/admin/giftcard/rate/{rate_id}/update', [CardsController::class, 'update_rate'])->name('admin.card.rate.update');


    Route::get('/admin/card/trades', [CardsController::class, 'card_trades'])->name('admin.card-trades');
    Route::get('/admin/card/{card_id}/images', [CardsController::class, 'card_images'])->name('admin.card.trade.images');

    Route::get('/admin/card/image/{image_id}/approve', [CardsController::class, 'approve_image'])->name('admin.card.image.approve');
    Route::post('/admin/card/image/{image_id}/disapprove', [CardsController::class, 'disapprove_image'])->name('admin.card.image.disapprove');
    
    
    
   
    Route::get('/admin/card/types', [CardsController::class,'card_types'])->name('admin.card.types');
    Route::get('/admin/card/types/add', [CardsController::class, 'add_card_types'])->name('admin.card.types.add');
    Route::post('/admin/card/type/add/post', [CardsController::class, 'save_card_type'])->name('admin.card.types.save');
    
    Route::get('/admin/card/type/{type_id}/delete', [CardsController::class, 'delete_card_type'])->name('admin.card.type.delete');
    
    Route::get('/admin/card/prices', [CardsController::class,'card_prices'])->name('admin.card.prices');
    Route::get('/admin/card/prices/{type_id}/add', [CardsController::class, 'add_card_price'])->name('admin.card.prices.add');
    Route::post('/admin/card/price/add/post/{type_id}', [CardsController::class, 'save_card_price'])->name('admin.card.prices.save');

    /*-----------------------------------------
    |---------------- Previleadges ---------------
    -------------------------------------------*/

    Route::get('/other-assets/trades', [OtherAssets::class,'trades'])->name('admin.other-asset.trades');
    Route::get('/other-assets/{asset_id}/rates', [OtherAssets::class,'rates'])->name('admin.other-asset.rates');
    

    Route::get('/admin/security/password', [Setting::class,'password'])->name('admin.password');
    Route::post('/admin/security/password/update', [Setting::class,'update_password'])->name('admin.password.update');

    
});
      
//Admin Routes
Route::middleware(['auth:sanctum', 'verified', 'admin'])->group(function () {

    /*-----------------------------------------
    |---------------- Previleadges ---------------
    -------------------------------------------*/
    Route::get('/admin/privileadges', [Pages::class, 'priv'])->name('admin.privileadges');
    Route::post('/admin/privileadge/grant', [Pages::class, 'grant'])->name('admin.privileadge');
    Route::get('/admin/privileadged/{user_id}/revoke', [Pages::class, 'revoke'])->name('admin.privileadge.revoke');

    Route::get('/admin/config', [Pages::class, 'config'])->name('admin.config');
    Route::post('/admin/config/tou', [Pages::class, 'tou'])->name('admin.config.term-of-use');
    Route::post('/admin/config/policy', [Pages::class, 'policy'])->name('admin.config.policy');

    Route::post('/admin/config/disclaimer', [Pages::class, 'disclaim'])->name('admin.config.disc');


    Route::get('/admin/card/{card_id}/delete', [CardsController::class, 'delete'])->name('admin.card.delete');

    Route::get('/admin/transactions/peer2peer', [TransactionController::class,'peer2peer'])->name('peer2peer');
    Route::get('/admin/transactions/peer2peer/{id}/approve', [TransactionController::class,'validate_peer'])->name('peer2peer.approve');

    Route::get('/other-assets/transaction/{trx_id}/accept', [OtherAssets::class,'accept'])->name('admin.other-asset.accept');
    Route::get('/other-assets/transaction/{trx_id}/reject', [OtherAssets::class,'reject'])->name('admin.other-asset.reject');

});

// Route::get('/storage/link', function(){
//     File::link(
//         storage_path('app/public'), public_path('storage')
//     );
// });





// Route::view('/dash', 'dashboard')->middleware('auth');

// Route::domain(config('session.domain'))->group(function () {
//     Route::get('user/', function () {
//         return 'User Route';
//     });
// });

