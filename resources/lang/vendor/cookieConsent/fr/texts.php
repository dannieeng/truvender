<?php

return [
    'message' => "Nous utilisons des cookies pour vous offrir une meilleure expérience de navigation, analyser le trafic du site et personnaliser le contenu. Découvrez comment nous utilisons les cookies et comment vous pouvez les contrôler dans notre politique de confidentialité. Si vous continuez à utiliser ce site, vous consentez à notre utilisation des cookies.",
    'agree' => 'Accepter',
];
