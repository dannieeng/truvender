<?php

return [
    'message' => 'Usamos cookies para oferecer a você uma melhor experiência de navegação, analisar o tráfego do site e personalizar o conteúdo. Leia sobre como usamos cookies e como você pode controlá-los em nossa Política de Privacidade. Se continuar a usar este site, você concorda com o uso de cookies.',
    'agree' => 'Aceitar',
];
