<?php

return [
    'message' => 'A sütik segítségével jobb böngészési élményt kínálunk, elemezzük a webhely forgalmát és személyre szabjuk a tartalmat. Az adatvédelmi irányelveinkben olvassa el, hogyan használjuk a sütiket, és hogyan ellenőrizheti őket. Ha továbbra is használja ezt a weboldalt, hozzájárul a sütik használatához.',
    'agree' => 'Sütik engedélyezése',
];
