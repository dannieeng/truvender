<?php

return [
    'message' => 'Vi bruker informasjonskapsler for å gi deg en bedre nettopplevelse, analysere trafikk på nettstedet og tilpasse innholdet. Les om hvordan vi bruker informasjonskapsler og hvordan du kan kontrollere dem i vår personvernpolicy. Hvis du fortsetter å bruke dette nettstedet, samtykker du i vår bruk av informasjonskapsler.',
    'agree' => 'Jeg forstår',
];
