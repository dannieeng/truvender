<?php

return [
    'message' => 'Utilizziamo i cookie per offrirti una migliore esperienza di navigazione, analizzare il traffico del sito e personalizzare i contenuti. Leggi come utilizziamo i cookie e come puoi controllarli nella nostra Informativa sulla privacy. Se continui ad utilizzare questo sito, acconsenti al nostro utilizzo dei cookie.',
    'agree' => 'Consenti i cookies',
];
