<?php

return [
    'message' => 'We use cookies to offer you a better browsing experience, analyze site traffic and personalize content. Read about how we use cookies and how you can control them on our Privacy Policy. If you continue to use this site, you consent to our use of cookies.',
    'agree' => 'Allow cookies',
];
