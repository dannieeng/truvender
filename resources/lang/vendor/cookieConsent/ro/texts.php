<?php

return [
    'message' => 'Folosim cookie-uri pentru a vă oferi o experiență de navigare mai bună, a analiza traficul site-ului și a personaliza conținutul. Citiți despre modul în care utilizăm cookie-urile și despre cum le puteți controla în Politica noastră de confidențialitate. Dacă continuați să utilizați acest site, sunteți de acord cu utilizarea cookie-urilor noastre.',
    'agree' => 'Acceptă cookie',
];
