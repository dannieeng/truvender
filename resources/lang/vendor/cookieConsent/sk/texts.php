<?php

return [
    'message' => 'Používame cookies, aby sme vám ponúkli lepší zážitok z prehliadania, analyzovali prenos stránok a prispôsobili obsah. Prečítajte si o tom, ako používame súbory cookie a ako ich môžete ovládať, v našich zásadách ochrany osobných údajov. Pokiaľ budete naďalej používať túto stránku, dávate súhlas s našim používaním cookies.',
    'agree' => 'Súhlasím',
];
