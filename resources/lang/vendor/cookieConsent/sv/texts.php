<?php

return [
    'message' => 'Vi använder cookies för att erbjuda dig en bättre webbupplevelse, analysera webbplatsens trafik och anpassa innehållet. Läs om hur vi använder cookies och hur du kan kontrollera dem i vår integritetspolicy. Om du fortsätter att använda denna webbplats godkänner du vår användning av cookies.',
    'agree' => 'Jag förstår',
];
