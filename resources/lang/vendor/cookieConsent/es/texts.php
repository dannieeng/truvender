<?php

return [
    'message' => 'Usamos cookies para ofrecerle una mejor experiencia de navegación, analizar el tráfico del sitio y personalizar el contenido. Lea sobre cómo usamos las cookies y cómo puede controlarlas en nuestra Política de privacidad. Si continúa utilizando este sitio, acepta nuestro uso de cookies.',
    'agree' => 'Aceptar',
];
