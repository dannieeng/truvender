<?php

return [
    'message' => 'Używamy plików cookie, aby zapewnić lepsze wrażenia z przeglądania, analizować ruch w witrynie i personalizować zawartość. Przeczytaj o tym, jak używamy plików cookie i jak możesz je kontrolować w naszej Polityce prywatności. Kontynuując korzystanie z tej witryny, wyrażasz zgodę na używanie przez nas plików cookie.',
    'agree' => 'Zezwalaj na cookie',
];
