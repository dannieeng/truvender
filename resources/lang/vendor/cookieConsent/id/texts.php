<?php

return [
    'message' => 'Kami menggunakan cookie untuk menawarkan Anda pengalaman penelusuran yang lebih baik, menganalisis lalu lintas situs, dan mempersonalisasi konten. Baca tentang bagaimana kami menggunakan cookie dan bagaimana Anda dapat mengontrolnya di Kebijakan Privasi kami. Jika Anda terus menggunakan situs ini, Anda menyetujui penggunaan cookie kami.',
    'agree' => 'Izikan Cookies',
];
