<?php

return [
    'message' => 'Size daha iyi bir göz atma deneyimi sunmak, site trafiğini analiz etmek ve içeriği kişiselleştirmek için çerezler kullanıyoruz. Çerezleri nasıl kullandığımız ve bunları nasıl kontrol edebileceğiniz hakkında Gizlilik Politikamızı okuyun. Bu siteyi kullanmaya devam ederseniz, çerez kullanımımıza izin vermiş olursunuz.',
    'agree' => 'Çerezlere izin ver',
];
