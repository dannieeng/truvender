<?php

return [
    'message' => 'Wir verwenden Cookies, um Ihnen ein besseres Surferlebnis zu bieten, den Website-Verkehr zu analysieren und Inhalte zu personalisieren. Lesen Sie in unseren Datenschutzbestimmungen, wie wir Cookies verwenden und wie Sie diese steuern können. Wenn Sie diese Website weiterhin nutzen, stimmen Sie der Verwendung von Cookies zu.',
    'agree' => 'Akzeptieren',
];
