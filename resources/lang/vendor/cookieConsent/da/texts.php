<?php

return [
    'message' => 'Vi bruger cookies til at give dig en bedre browseroplevelse, analysere trafik på webstedet og personalisere indhold. Læs om, hvordan vi bruger cookies, og hvordan du kan kontrollere dem i vores fortrolighedspolitik. Hvis du fortsætter med at bruge dette websted, giver du dit samtykke til vores brug af cookies.',
    'agree' => 'Tillad cookies',
];
