<?php

return [
    'message' => 'Ni uzas kuketojn por proponi al vi pli bonan foliumadon, analizi retejan trafikon kaj personecigi enhavon. Legu pri kiel ni uzas kuketojn kaj kiel vi povas kontroli ilin en nia Privateca Politiko. Se vi daŭre uzas ĉi tiun retejon, vi konsentas pri nia uzo de kuketoj.',
    'agree' => 'Akcepti kuketojn',
];
