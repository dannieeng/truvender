<?php

return [
    'message' => '
Používáme cookies, abychom vám nabídli lepší zážitek z prohlížení, analyzovali provoz stránek a přizpůsobili obsah. Přečtěte si o tom, jak používáme soubory cookie a jak je můžete ovládat, v našich zásadách ochrany osobních údajů. Pokud budete i nadále používat tento web, souhlasíte s naším používáním cookies.',
    'agree' => 'Souhlasím',
];
