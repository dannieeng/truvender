<?php

return [
    'message' => 'Kasutame küpsiseid, et pakkuda teile paremat sirvimiskogemust, analüüsida saidi liiklust ja sisu isikupärastada. Selle kohta, kuidas me küpsiseid kasutame ja kuidas saate neid kontrollida, lugege meie privaatsuseeskirjadest. Kui jätkate selle saidi kasutamist, nõustute meie küpsiste kasutamisega.',
    'agree' => 'Sain aru',
];
