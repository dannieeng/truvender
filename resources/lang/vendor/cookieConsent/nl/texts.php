<?php

return [
    'message' => 'We gebruiken cookies om u een betere browse-ervaring te bieden, siteverkeer te analyseren en inhoud te personaliseren. Lees meer over hoe we cookies gebruiken en hoe u ze kunt beheren in ons privacybeleid. Als u deze site blijft gebruiken, stemt u in met ons gebruik van cookies.',
    'agree' => 'Sta cookies toe',
];
