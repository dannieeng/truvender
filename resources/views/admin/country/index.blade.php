@extends('layouts.dashboard')
@section('styles')
    <style>
        .card{
            padding: 1rem 0;
        }
        .car-img{
            height: 6.2rem;
            width: 5.6rem;
            padding: 0 2rem;
        }
        
    </style>
@endsection

@section('content')

    <div class="d-flex justify-content-end mb-4">
        <a type="button" href="#" class="btn btn-outline-primary mr-3" data-toggle="modal" data-target="#small-modal" > Add Country</a>
    </div>
    @livewire('admin.countries.index', ['countries' => $countries])

    <div class="modal fade" id="small-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md modal-dialog-centered">
            <div class="modal-content">
                
                <div class="modal-body">
                    <h4 class="font-18 text-primary mb-3">Add Country</h4>
                    
                    <div class="mt-4">
                        @livewire('admin.countries.add')
                    </div>
                    <button type="button" class="btn-block btn-sm btn-outline-danger" data-dismiss="modal">Close</button>
                </div>
               
            </div>
        </div>
    </div>
@endsection