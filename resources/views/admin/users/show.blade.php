@extends('layouts.dashboard')

@section('title', 'Settings')
@section('styles')
    <style>
        .profile-pic {
            max-width: 260px;
            max-height: 260px;
            display: block;
        }
        .file-upload {
            display: none;
        }
        .circle {
            border-radius: 20px !important;
            overflow: hidden;
            width: 260px;
            height: 260px;
            border: 3px dotted rgba(96, 102, 187, 0.691);
            position: relative;
            top: 2px;
        }
        img {
            max-width: 100%;
            height: 100%;
        }

    </style>
@endsection
@section('content')
<div class="mb-4">
    <h1 class="font-30">
        Account Settings
    </h1>
</div>
    <div class="row">
        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 mb-30">
            <div class="pd-20 card-box height-100-p">
                <div class="profile-photo">
                    @if ($user->banking == true)
                        <a href="modal" class="edit-avatar text-success"><i class="fa fa-check-circle"></i></a>
                    @endif
                    <img 
                        src="{{$user->profile_photo_path!= null ? $user->getProfilePhotoUrlAttribute() : 'https://ui-avatars.com/api/?name='.urlencode($user->username).'&color=7F9CF5&background=EBF4FF&size=150'}}" 
                        alt="" class="avatar-photo">
                </div>
                <h5 class="text-center h5 mb-2">{{$user->username}}</h5>
                
                <div class="profile-info">
                    <h5 class="mb-20 h5 text-blue">Account Information</h5>
                    <ul>
                        <li>
                            <span>First Name:</span>
                            {{$profile == true ? $profile->firstname : 'N/A'}}
                        </li>

                        <li>
                            <span>Last Name:</span>
                            {{$profile == true ? $profile->lastname : 'N/A'}}
                        </li>
                        <li>
                            <span>Phone Number:</span>
                            {{$profile == true ? $profile->phone : 'N/A'}}
                        </li>
                         <li>
                            <span>Email Address:</span>
                            {{$user->email}}
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 mb-30">
            <div class="card-box height-100-p overflow-hidden">
                <div class="profile-tab height-100-p">
                    <div class="tab height-100-p">
                        <ul class="nav nav-tabs customtab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#banking" role="tab">Banking</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#transact" role="tab">Transactions</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#referral" role="tab">Referrals</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            
                            <!-- Banking Tab start -->
                            <div class="tab-pane fade" id="banking" role="tabpanel">
                                <div class="pd-20 profile-task-wrap">
                                    <div class="container pd-0">
                                        <!-- Open Task start -->
                                        @if ($banking == false)
                                            <div class="task-title row align-items-center">
                                                <div class="col-md-8 col-sm-12">
                                                    <h5 class="font-16">Banking Information</h5>
                                                    <p class="mt-1 mb-2">No Bank account added</p>
                                                </div>
                                            </div>

                                        @else
                                            <div class="task-title row align-items-center">
                                                <div class="col-md-8 col-sm-12">
                                                    <h5 class="font-16">Banking Information</h5>
                                                    <p class="mt-1 mb-2">Bank account</p>
                                                    <div class="mt-2">
                                                        <h5 class="font-16 mb-1">Account Name<span class="font-weight-normal">{{$banking->acc_name}}</span></h5>
                                                        <h5 class="font-16">Account Number<span class="font-weight-normal">{{$banking->acc_number}}</span></h5>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <!-- Banking Tab End -->
                            <!-- transact Tab start -->
                            <div class="tab-pane fade height-100-p" id="transact" role="tabpanel">
                                <div class="profile-setting">
                                    
                                </div>
                            </div>
                            <!-- Notify Tab End -->

                            <!-- Tasks Tab End -->
                            <!-- Referral Tab start -->
                            <div class="tab-pane fade height-100-p" id="referral" role="tabpanel">
                                 <div class="profile-setting">
                                     <div class="mt-3">
                                         @if ($referrals->count() > 0)
                                         @php
                                             $i = 0;
                                         @endphp
                                             <div class="table-responsive">
                                                <table class="table">
                                                    <thead>
                                                        <tr>
                                                            <th scope="col">#</th>
                                                            <th scope="col">Username</th>
                                                            <th scope="col">Date Joined</th>
                                                            <th scope="col">Total Transactions</th>
                                                            <th scope="col">status</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach ($referrals as $referral)
                                                            <tr>
                                                                <th scope="row">{{++$i}}</th>
                                                                <th scope="row">{{$referral->username}}</th>
                                                                <th scope="row">{{now()->parse($referral->created_at)->format('d M Y')}}</th>
                                                                <th scope="row">{{$referral->transactions->count()}}</th>
                                                                <th scope="row">
                                                                    <span class="badge {{$referral->in_blacklist() == true ? 'badge-danger' : 'badge-success'}} py-0 px-1">{{$referral->in_blacklist() == true ? 'suspended' : 'active'}}</span>
                                                                </th>
                                                            </tr> 
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="mt-4 d-flex justify-content-center">
                                                {{$referrals->links()}}
                                            </div>

                                        @else
                                            <div class="mt-4 d-flex justify-content-center align-content-center align-items-center">
                                                <h4 class="font-24">No Referred User</h4>
                                            </div>
                                        @endif
                                     </div>
                                 </div>
                            </div>
                            <!-- Referral Tab End -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="/auth/vendors/scripts/script.min.js"></script>
	<script src="/auth/vendors/scripts/process.js"></script>
	<script src="/auth/vendors/scripts/layout-settings.js"></script>
    
	<script src="/auth/vendors/scripts/layout-settings.js"></script>
	
	{{--  --}}
@endsection