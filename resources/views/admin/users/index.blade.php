@extends('layouts.dashboard')
{{-- @section('title', 'All Users') --}}

@section('content')
    <div class="mb-4 row clearfix">
        <div class="col">
            <h4 class="font-24">Users</h4>
        </div>
    </div>

    <div class="row clearfix">
        <div class="col">
            <div class="card card-box">
                <div class="card-body">
                   <div class="profile-tab height-100-p">
                        <div class="tab height-100-p">
                            <ul class="nav nav-tabs customtab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" data-toggle="tab" href="#all" role="tab">All Users</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#active" role="tab">Active Users</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#suspended" role="tab">Suspended Users</a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <!-- all Tab start -->
                                <div class="tab-pane fade show active" id="all" role="tabpanel">
                                    <div class="pd-20">
                                        <div class="container pd-0">
                                           @livewire('admin.users.all')
                                        </div>
                                    </div>
                                </div>
                                <!-- all Tab End -->
                                <!-- active Tab start -->
                                <div class="tab-pane fade" id="active" role="tabpanel">
                                    <div class="pd-20">
                                        <div class="container pd-0">
                                            @livewire('admin.users.active')
                                        </div>
                                    </div>
                                </div>
                                <!-- Banking Tab End -->
                                <!-- suspended Tab start -->
                                <div class="tab-pane fade height-100-p" id="suspended" role="tabpanel">
                                    <div class="pd-20">
                                        <div class="container pd-0">
                                            @livewire('admin.users.suspended')
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
