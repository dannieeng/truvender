@extends('layouts.dashboard')
@section('content')
    <div class="row">
        <div class="col-md-6 offset-md-2">
            <div class="card card-box">
                <div class="card-body">
                    <h4 class="font-20 text-primary">Change Password</h4>
                   <div class="mt-3">
                        <form method="POST" action="{{route('admin.password.update')}}" class="mb-4" >
                    
                            @csrf
                        
                            <div class="form-group">
                                <label>Old Password</label>
                                <input type="password" name="old_password" class="form-control" autofocus id="old_password">
                                @error('old_password')
                                    <span class="text-danger font-14">{{$message}}</span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label>New Password</label>
                                <input type="password" name="password" class="form-control"  id="">
                                @error('password')
                                    <span class="text-danger font-14">{{$message}}</span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label>Confirm Password</label>
                                <input type="password" name="password_confirmation" class="form-control"  id="">
                            </div>
                            
                            <button type="submit" class="btn btn-rounded btn-sm btn-block btn-primary continue">Update Password</button>
                            <a href="{{url()->previous()}}" class="btn btn-sm btn-outline-danger btn-block btn-rounded">Cancel Edit</a>
                                
                            
                        </form>
                   </div>
                </div>
            </div>
        </div>
    </div>
@endsection