@extends('layouts.dashboard')
@section('styles')
<link rel="stylesheet" href="/css/app.css">
    <style>
        .card{
            padding: 1rem 0;
        }
        .car-img{
            height: 6.2rem;
            width: 5.6rem;
            padding: 0 2rem;
        }
        
    </style>
@endsection


@section('content')

    <div class="d-flex justify-content-between mb-4">
        <h4 class="font-24 font-weight-bold">Card Trades</h4>
        {{--<a href="{{route('admin.card.add-rate')}}" class="btn btn-outline-primary"> Add Rate</a>--}}
    </div>
    <div class="mt-3 row clearfix">
        <div class="col">
            <div class="card card-box">
                <div class="card-body">
                    @if ($trades->count() > 0)
                        <div class="table-responsive mt-4">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">user</th>
                                        <th scope="col">card name</th>
                                        <th scope="col">card type</th>
                                        <th scope="col">card country</th>
                                        <th scope="col">cost</th>
                                        <th scope="col">card amount</th>
                                        <th scope="col">action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $i =0;
                                    @endphp
                                    @foreach ($trades as $trade)
                                    @php
                                        $trade_id = encrypt($trade->id);
                                    @endphp
                                        <tr class=" ">
                                            <th scope="row" class="font-weight-normal font-16">{{++$i}}</th>
                                            <th scope="row" class="font-weight-normal font-16">{{$trade->user = true ? $trade->user->username : ''}}</th>
                                            <th scope="row" class="font-weight-normal font-16">{{$trade->card == true ? $trade->card->name : ''}}</th>
                                            <th scope="row" class="font-weight-normal font-16">{{$trade->trade_type}}</th>
                                            {{-- <th scope="row" class="font-weight-normal font-16">{{$trade->card_rate->country == true ? $trade->card_rate->country->name : ''}}</th> --}}
                                            <th scope="row" class="font-weight-normal font-16"><span class="font-weight-bold">&#8358;</span> {{number_format($trade->price, 2)}}</th>
                                            <th scope="row" class="font-weight-normal font-16">{{$trade->amount}}</th>

                                            <td scope="row">
                                                <div class="dropdown">
                                                    <a class="btn btn-link font-24 p-0 line-height-1 no-arrow dropdown-toggle" href="#" role="button" data-toggle="dropdown">
                                                        <i class="dw dw-more"></i>
                                                    </a>
                                                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
                                                        @if ($trade->trade_type == 'sell')
                                                            <a class="dropdown-item" href="{{route('admin.card.trade.images', $trade_id)}}"><i class="dw dw-eye"></i> view cards</a>
                                                        @endif
                                                        {{-- @if ($card->type == 'buy')
                                                            <a class="dropdown-item" href="{{route('admin.users.edit-profile', $trade_id)}}"><i class="dw dw-edit2"></i> approve </a> 
                                                        @endif --}}

                                                    </div>
                                                </div>
                                            </td>
                                        </tr> 
                                    @endforeach
                                </tbody>
                            </table>
                        </div>

                        <div class="mt-4 d-flex justify-content-center">
                            {{$trades->links()}}
                        </div>
                    @else
                        <div class="flex flex-col justify-items-center align-items-center">
                            <div class="mb-2 mt-4">
                                <img src="/images/bg/card.svg" style="width:30rem;" alt="">
                            </div>
                            <h3 class="font-18 font-weight-normal mt-3">No Rates Available</h3>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>

    
@endsection