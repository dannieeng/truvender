@extends('layouts.dashboard')
@section('styles')
<link rel="stylesheet" href="/css/app.css">
    <style>
        .card{
            padding: 1rem 0;
        }
        .car-img{
            height: 6.2rem;
            width: 5.6rem;
            padding: 0 2rem;
        }
        
    </style>
@endsection


@section('content')

    <div class="d-flex justify-content-between mb-4">
        <h4 class="font-24 font-weight-bold">Card rates</h4>
        <a href="{{route('admin.card.add-rate')}}" class="btn btn-outline-primary"> Add Rate</a>
    </div>
    <div class="mt-3 row clearfix">
        <div class="col">
            <div class="card card-box">
                <div class="card-body">
                    @if ($rates->count() > 0)
                        <div class="table-responsive mt-4">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">card</th>
                                        <th scope="col">card type</th>
                                        <th scope="col">country</th>
                                        <th scope="col">buyer rate</th>
                                        <th scope="col">seller rate</th>
                                        <th scope="col">action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $i =0;
                                    @endphp
                                    @foreach ($rates as $rate)
                                    @php
                                        $rate_id = encrypt($rate->id);
                                    @endphp
                                        <tr class=" ">
                                            <th scope="row" class="font-weight-normal font-16">{{++$i}}</th>
                                            <th scope="row" class="font-weight-normal font-16">{{$rate->card == true ? $rate->card->name : ''}}</th>
                                            <th scope="row" class="font-weight-normal font-16">{{ $rate->type == true ? $rate->type->name : ''}}</th>
                                            <th scope="row" class="font-weight-normal font-16">{{$rate->country->name}}</th>
                                            <th scope="row" class="font-weight-normal font-16"> <span class="font-weight-bold">&#8358;</span> {{number_format($rate->buyer_rate, 2)}}</th>
                                            <th scope="row" class="font-weight-normal font-16"> <span class="font-weight-bold">&#8358;</span> {{number_format($rate->seller_rate, 2)}}</th>
                                            <th scope="row"> 
                                                <a href="{{route('admin.card.rate.edit', $rate_id)}}" class="badge badge-info badge-pill">edit</a> 
                                            </th>
                                        </tr> 
                                    @endforeach
                                </tbody>
                            </table>
                        </div>

                        <div class="mt-4 d-flex justify-content-center">
                            {{$rates->links()}}
                        </div>
                    @else
                        <div class="flex flex-col justify-items-center align-items-center">
                            <div class="mb-2 mt-4">
                                <img src="/images/bg/card.svg" style="width:30rem;" alt="">
                            </div>
                            <h3 class="font-18 font-weight-normal mt-3">No Rates Available</h3>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>

    
@endsection