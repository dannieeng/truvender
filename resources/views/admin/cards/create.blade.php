@extends('layouts.dashboard')
@section('content')
    <div class="row clearfix">
        <div class="col-md-6 offset-md-3">
            <div class="card card-box">
                <div class="card-body">
                    <h4 class="font-20 text-primary mb-4">Add Card</h4>
                    <div class="mt-1">
                        <form action="{{route('admin.cards.save')}}" method="POST">
                            @csrf
                            <div class="form-group">
                                <label>Name</label>
                                <select name="" class="form-control" id="">
    
                                </select>
                                <div class="mt-2">
                                    <input type="text" name="" id="" class="form-control">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection