@extends('layouts.dashboard')
@section('title', 'GitfCards')

@section('styles')
    <style>
        .card{
            padding: 1rem 0;
        }
        .car-img{
            height: 6.2rem;
            width: 5.6rem;
            padding: 0 2rem;
        }
        
    </style>
@endsection
@section('title', 'Available Cards')

@section('content')

    <div class="d-flex justify-content-end mb-4">
        <a href="#" data-toggle="modal" data-target="#add-giftcard" class="btn btn-outline-primary"> Add Card</a>
    </div>

    @livewire('admin.rates.card.index')

    <div class="modal fade" id="add-giftcard" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md modal-dialog-centered">
            <div class="modal-content">
                
                <div class="modal-body">
                    <h4 class="font-18 text-primary mb-3">Add GiftCard</h4>
                    
                    <div class="mt-4">
                       @livewire('admin.rates.card.add')
                    </div>
                    <button type="button" class="btn-block btn-sm btn-outline-danger" data-dismiss="modal">Close</button>
                </div>
               
            </div>
        </div>
    </div>
@endsection