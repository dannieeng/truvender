@extends('layouts.dashboard')
@section('content')
    <div class="row">
        <div class="col-md-6 offset-md-3">
            <div class="card card-box">
                <div class="card-body">
                    <h4 class="text-primary font-18">Edit Card Rate</h4>
                    <div class="mt-4">
                        @php
                            $rate_id = encrypt($rate->id);
                        @endphp
                        <form action="{{route('admin.card.rate.update', $rate_id)}}" method="POST">
                            @csrf
                            <div class="form-group">
                                <label>Card Name</label>
                                <input type="text" class="form-control" readonly value="{{$rate->card->name}}">
                            </div>

                            <div class="form-group">
                                <label>Country Name</label>
                                <input type="text" class="form-control" readonly value="{{$rate->country->name}}">
                            </div>

                            <div class="form-group">
                                <label>Type</label>
                                <input type="text" class="form-control" readonly value="{{$rate->type->name}}">
                            </div>

                            <div class="form-group">
                                <label>Buyer Rate</label>
                                <div class="position-relative">
                                    <span class="position-absolute font-20 font-weight-bold mt-2 ml-2">&#8358;</span>
                                    <input type="text" class="form-control pl-4" value="{{$rate->buyer_rate}}" name="buyer_rate">
                                </div>

                                 @error('buyer_rate')
                                    <p class="text-danger font-16 mt-2">{{$message}}</p>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label>Seller Rate</label>
                                <div class="position-relative">
                                    <span class="position-absolute font-20 font-weight-bold mt-2 ml-2">&#8358;</span>
                                    <input type="text" class="form-control pl-4" value="{{$rate->seller_rate}}" name="seller_rate">
                                </div>

                                @error('seller_rate')
                                    <p class="text-danger font-16 mt-2">{{$message}}</p>
                                @enderror
                            </div>
                            
                            <div class="form-group">
                                <label>Default Rate</label>
                                <div class="position-relative">
                                    <span class="position-absolute font-20 font-weight-bold mt-2 ml-2">&#8358;</span>
                                    <input type="text" name="default_rate" class="form-control pl-4" value="{{$default_rate}}">
                                </div>
                        
                                @error('default_rate')
                                    <p class="text-danger font-16 mt-2">{{$message}}</p>
                                @enderror
                            </div>


                            <div class="mt-4">
                                <button type="submit" class="btn btn-block btn-sm btn-outline-info">Save</button>

                                <a href="{{url()->previous()}}" class="btn btn-block btn-sm btn-outline-danger">cancel</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection