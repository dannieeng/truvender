@extends('layouts.dashboard')
@section('content')
    <div class="row">
        <div class="col-md-6 offset-md-3">
            <div class="card card-box">
                <div class="card-body">
                    <h4 class="text-primary font-18">Add Card Price</h4>
                    <div class="mt-4">
                        @php
                            $card_type_id = encrypt($card_type->id);
                        @endphp
                        <form action="{{route('admin.card.prices.save', $card_type_id)}}" method="POST">
                            @csrf
                            <div class="form-group">
                                <label>Card</label>
                                <input type="text" class="form-control" readonly name="card" value="{{$card_type->card->name}}">
                            </div>

                            <div class="form-group">
                                <label>Card Type</label>
                                <input type="text" class="form-control" readonly name="type" value="{{$card_type->name}}">
                            </div>

                            <div class="my-4">
                                <div class="alert alert-info">
                                    <span class="font-weight-bold">Note:</span> multiple amount should be seperated by coma (,)
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Amount (usd)</label>
                                <div class="position-relative">
                                    <span class="position-absolute font-20 font-weight-bold mt-2 ml-2">$</span>
                                    <input type="text" class="form-control pl-4" name="amount" value="{{old('amount')}}">
                                </div>
                                @error('amount')
                                    <p class="mt-1 text-danger font-16">{{$message}}</p>
                                @enderror
                            </div>


                            <div class="mt-4">
                                <button type="submit" class="btn btn-block btn-sm btn-outline-info">Save</button>

                                <a href="{{url()->previous()}}" class="btn btn-block btn-sm btn-outline-danger">cancel</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('name')
    
@endsection