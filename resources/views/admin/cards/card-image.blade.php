@extends('layouts.dashboard')
@section('styles')
    <style>
        .card{
            padding: 1rem 0;
        }
        .car-img{
            height: 6.2rem;
            width: 5.6rem;
            padding: 0 2rem;
        }
        
    </style>
@endsection
@section('title', 'Available Cards')

@section('content')

<div class="row clearfix mb-1">
    <div class="col-md-6">
        <h3 class="ml-3">Cards</h3>
    </div>
</div>
<div class="row clearfix">
    @foreach ($card_images as $card)
        @php
            $card_id = encrypt($card->id);
        @endphp
        <div class="col-md-4 mt-3">
            <div class="card card-box">
                <div class="card-body">
                    <a href="#" data-toggle="modal" data-target="#image-modal{{$card->id}}">
                        <div class="card-img">
                            <img src="{{Storage::url($card->images)}}" alt="">
                        </div>
                    </a>

                    @if ($card->is_approved == null)
                        <div class="card-footer">
                            <div class="d-flex justify-content-between">
                                <a href="{{route('admin.card.image.approve', $card_id)}}" class="font-18 font-weight-bold text-success">approve</a>
                                <a href="#" data-toggle="modal" data-target="#disapprove-modal{{$card->id}}" class="font-18 font-weight-bold text-danger">disapprove</a>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>

        <div class="modal fade" id="image-modal{{$card->id}}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-md modal-dialog-centered">
                <div class="modal-content">
                    
                    <div class="modal-body">
                        <h4 class="font-18 text-primary mb-3">Card Image</h4>
                        
                        <div class="mt-4 mb-2">
                            <a href="{{Storage::url($card->images)}}" target="_blank">
                                <div class="" style="width: 100%;">
                                    <img src="{{Storage::url($card->images)}}" class="img-fluid" alt="">
                                </div>
                            </a>
                        </div>

                        
                        <button type="button" class="btn-block btn-sm btn-outline-danger" data-dismiss="modal">Close</button>
                    </div>
                
                </div>
            </div>
        </div>

        <div class="modal fade" id="disapprove-modal{{$card->id}}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm modal-dialog-centered">
                <div class="modal-content">
                    
                    <div class="modal-body">
                        
                        <form action="{{route('admin.card.image.disapprove', $card_id)}}" method="POST">
                            @csrf
                            <div class="form-group">
                                <label>Reason</label>
                                <textarea name="note" class="form-control"></textarea>
                                @error('note')
                                    <p class="mt-2 text-danger font-16">{{$message}}</p>
                                @enderror
                            </div>

                            <div class="mt-2">
                                <button type="submit" class="btn-sm btn-block mb-2 btn-outline-info">Submit</button>
                            </div>
                        </form>
                        <button type="button" class="btn-block btn-sm btn-outline-danger" data-dismiss="modal">Close</button>
                    </div>
                
                </div>
            </div>
        </div>
    @endforeach
</div>

    
@endsection