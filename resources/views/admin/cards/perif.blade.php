@extends('layouts.dashboard')
@section('content')
    <div class="mt-3 row">
        <div class="col">
            <div class="card card-box">
                <div class="card-body">
                     <h3 class="mb-2 text-primary font-20">Card {{$title}}</h3>

                    <div class="mt-2 mb-4 d-flex justify-content-end">
                        @if (strtolower($title) == 'rates')
                            @php
                                $id = encrypt($card_id);
                            @endphp
                            <a href="{{route('admin.card.add-rate', $id)}}" class="btn btn-outline-primary btn-sm">Add Rate</a>
                        @endif
                    </div>
                     <div class="mt-2">
                         @if ($collections->count() > 0)
                            @if (strtolower($title) == 'trades')
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th scope="col">#</th>
                                                <th scope="col">user</th>
                                                <th scope="col">trx id</th>
                                                <th scope="col">type</th>
                                                <th scope="col">amount</th>
                                                <th scope="col">rate</th>
                                                <th scope="col">date</th>
                                                <th scope="col">status</th>
                                                <th scope="col">action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php
                                                $i =0;
                                                
                                            @endphp
                                            @foreach ($collections as $trade)
                                            @php
                                                $trx_id = encrypt($trade->id);
                                            @endphp
                                                <tr class=" ">
                                                    <th scope="row" class="font-weight-normal font-16">{{++$i}}</th>
                                                    <th scope="row" class="font-weight-normal font-16">{{$trade->user->username}}</th>
                                                    <th scope="row" class="font-weight-normal font-16">{{$trade->transaction == true ? $trade->transaction->trxn_ref : ''}}</th>
                                                    <th scope="row" class="font-weight-normal font-16">{{$trade->trade_type}}</th>
                                                    <th scope="row" class="font-weight-normal font-16">{{$trade->amount}}</th>
                                                    <th scope="row" class="font-weight-normal font-16"><span>&#8358;</span>{{number_format($trade->price, 2)}}</th>
                                                    <th scope="row" class="font-weight-normal font-16">{{now()->parse($trade->created_at)->format('d/m/Y')}}</th>
                                                    <th scope="row" class=" font-16 {{$trade->status == 'approved' ? 'text-success' : ($trade->status == 'disapproved' ? 'text-danger' : 'text-warning')}}">{{$trade->status}}</th>
                                                    <th scope="row"> 
                                                        <a href="{{route('admin.transaction.detail', $trx_id)}}" class="badge badge-info badge-pill">details</a> 
                                                    </th>
                                                </tr> 
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>

                                <div class="mt-4">
                                    {{$collections->links()}}
                                </div>
                            @else
                                
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th scope="col">#</th>
                                                {{--<th scope="col">card type</th>--}}
                                                <th scope="col">country</th>
                                                <th scope="col">buyer rate</th>
                                                <th scope="col">seller rate</th>
                                                <th scope="col">action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php
                                                $i =0;
                                            @endphp
                                            @foreach ($collections as $trade)
                                            @php
                                                $rate_id = encrypt($trade->id);
                                            @endphp
                                                <tr class=" ">
                                                    <th scope="row" class="font-weight-normal font-16">{{++$i}}</th>
                                                    {{--<th scope="row" class="font-weight-normal font-16">{{$trade->type}}</th>--}}
                                                    <th scope="row" class="font-weight-normal font-16">{{$trade->country->name}}</th>
                                                    <th scope="row" class="font-weight-normal font-16"> <span class="font-weight-bold">&#8358;</span> {{number_format($trade->buyer_rate, 2)}}</th>
                                                    <th scope="row" class="font-weight-normal font-16"> <span class="font-weight-bold">&#8358;</span> {{number_format($trade->seller_rate, 2)}}</th>
                                                    <th scope="row"> 
                                                        <a href="{{route('admin.card.rate.edit', $rate_id)}}" class="btn btn-sm btn-outline-info">edit</a> 
                                                    </th>
                                                </tr> 
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            @endif 

                         @else
                            <div class="text-center">
                                 <div class="mb-2 mt-4">
                                    <img src="/images/bg/collection.svg" style="width:26rem;" alt="">
                                </div>
                                <h3 class="font-18 mt-3">No Collection</h3>
                            </div>
                         @endif
                     </div>
                </div>
            </div>
        </div>
    </div>
@endsection