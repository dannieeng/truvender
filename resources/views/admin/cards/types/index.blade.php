@extends('layouts.dashboard')
@section('styles')
<link rel="stylesheet" href="/css/app.css">
    <style>
        .card{
            padding: 1rem 0;
        }
        .car-img{
            height: 6.2rem;
            width: 5.6rem;
            padding: 0 2rem;
        }
        
    </style>
@endsection


@section('content')

    <div class="d-flex justify-content-between mb-4">
        <h4 class="font-24 font-weight-bold">Card Types</h4>
        <a data-toggle="modal" data-target="#add-card-type" role="button" class="btn btn-outline-primary "> Add Type</a>
    </div>
    <div class="mt-3 row clearfix">
        <div class="col">
            <div class="card card-box">
                <div class="card-body">
                    @if ($types->count() > 0)
                        <div class="table-responsive mt-4">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">type name</th>
                                        <th scope="col">card</th>
                                        {{-- <th scope="col">country</th> --}}
                                        {{-- <th scope="col">buyer rate</th>
                                        <th scope="col">seller rate</th> --}}
                                        <th scope="col">action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $i =0;
                                    @endphp
                                    @foreach ($types as $type)
                                    @php
                                        $type_id = encrypt($type->id);
                                    @endphp
                                        <tr class=" ">
                                            <th scope="row" class="font-weight-normal font-16">{{++$i}}</th>
                                            <th scope="row" class="font-weight-normal font-16">{{ucfirst($type->name) . ' Card'}}</th>
                                            <th scope="row" class="font-weight-normal font-16">{{$type->card == true ? $type->card->name : ''}}</th>
                                            {{-- <th scope="row" class="font-weight-normal font-16">{{$type->country->name}}</th> --}}
                                            <th scope="row"> 
                                                <a href="{{route('admin.card.prices.add', $type_id)}}" class="btn btn-sm btn-outline-info mb-2 mb-lg-0">add price</a> 
                                               {{-- <a href="{{route('admin.card.type.delete', $type_id)}}" class="btn btn-sm btn-outline-danger">delete</a> --}}
                                            </th>
                                        </tr> 
                                    @endforeach
                                </tbody>
                            </table>
                        </div>

                        <div class="mt-4 justify-content-between">
                            {{$types->links()}}
                        </div>
                    @else
                        <div class="flex flex-col justify-items-center align-items-center">
                            <div class="mb-2 mt-4">
                                <img src="/images/bg/card.svg" style="width:30rem;" alt="">
                            </div>
                            <h3 class="font-18 font-weight-normal mt-3">No Rates Available</h3>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>

    
    <div class="modal fade" id="add-card-type" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md modal-dialog-centered">
            <div class="modal-content">
                
                <div class="modal-body">
                    <h4 class="font-18 text-primary mb-3">Add GiftCard</h4>
                    
                    <div class="mt-4">
                       @livewire('admin.card.add-type')
                    </div>
                    <button type="button" class="btn-block btn-sm btn-outline-danger" data-dismiss="modal">Close</button>
                </div>
               
            </div>
        </div>
    </div>
@endsection