@extends('layouts.dashboard')
@section('content')
    <div class="row">
        <div class="col-md-6 offset-md-3">
            <div class="card card-box">
                <div class="card-body">
                    <h4 class="text-primary font-18">Add Card Rate</h4>
                    <div class="mt-4">
                        @livewire('admin.card.add-rate', ['card_id' => $card_id])
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

