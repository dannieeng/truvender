@extends('layouts.dashboard')
@section('content')
    <div class="row">
        <div class="col-md-6 offset-md-2">
            <div class="card card-box">
                <div class="card-body">
                    <h4 class="font-20 text-primary">Crypto Rates</h4>
                    @livewire('admin.rates.crypto.index', ['crypto' => $crypto])
                </div>
            </div>
        </div>
    </div>
@endsection