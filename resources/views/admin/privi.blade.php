@extends('layouts.dashboard')
{{-- @section('title', 'All Users') --}}

@section('content')
    <div class="mb-4 row clearfix">
        <div class="col">
            <h4 class="font-24">Privileadges</h4>
        </div>
    </div>

    <div class="row clearfix">
        <div class="col">
            <div class="card card-box">
                <div class="card-body">
                   <div class="profile-tab height-100-p">
                        <div class="tab height-100-p">
                            <ul class="nav nav-tabs customtab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" data-toggle="tab" href="#all" role="tab">Privileadges</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#active" role="tab">Grant</a>
                                </li>
                               
                            </ul>
                            <div class="tab-content">
                                <!-- all Tab start -->
                                <div class="tab-pane fade show active" id="all" role="tabpanel">
                                    <div class="pd-20">
                                        <div class="container pd-0">
                                           <div class="table-responsive">
                                                <div class="pb-20">
                                                    <table class="data-table-all table stripe hover nowrap">
                                                        <thead>
                                                            <tr>
                                                                <th class="table-plus datatable-nosort">Username</th>
                                                                <th>Date Registered</th>
                                                                <th>Transactions</th>
                                                                <th>Role</th>
                                                                <th>Referrals</th>
                                                                <th class="datatable-nosort">Action</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @foreach ($users as $user)
                                                                @if ($user->role->name != 'user')
                                                                    @php
                                                                        $user_id = Crypt::encrypt($user->id);
                                                                    @endphp
                                                                        <tr>
                                                                            <td class="table-plus">{{$user->username}}</td>
                                                                            <td>{{ now()->parse($user->created_at)->format('d M Y') }}</td>
                                                                            <td>{{ $user->transactions->count() }}</td>
                                                                            <td>{{ $user->role->name }}</td>
                                                                            <td>{{ $user->referrals->count() }}</td>
                                                                             <td>
                                                                                    <div class="dropdown">
                                                                                        <a class="btn btn-link font-24 p-0 line-height-1 no-arrow dropdown-toggle" href="#" role="button" data-toggle="dropdown">
                                                                                            <i class="dw dw-more"></i>
                                                                                        </a>
                                                                                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
                                                                                            <a class="dropdown-item" href="{{route('admin.privileadge.revoke', $user_id)}}"><i class="dw dw-refresh"></i> Revoke</a>
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                        </tr>
                                                                @endif
                                                            @endforeach
                                                            
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <!-- all Tab End -->
                                <!-- active Tab start -->
                                <div class="tab-pane fade" id="active" role="tabpanel">
                                    <div class="pd-20">
                                        <div class="container pd-0">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="mb-4 mt-3">
                                                        <h4 class="font-16 text-primary">Grant Privileadge</h4>
                                                    </div>
                                                    <form action="{{route('admin.privileadge')}}" method="POST">
                                                        <div class="form-group">
                                                            <label>Username</label>
                                                            <input type="text" name="user" class="form-control" placeholder="Username" value="{{old('user')}}">
                                                            @error('user')
                                                                <p class="mt-2 text-danger font-16">{{$message}}</p>
                                                            @enderror
                                                        </div>

                                                         <div class="form-group">
                                                            <label>Privileadge</label>
                                                            <select name="privileadge" class="form-control" value="{{old('user')}}">
                                                                <option>-- Select Privileadge --</option>
                                                                @foreach ($privileadges as $priv)
                                                                    @if ($priv->name != 'user' && $priv->name != 'admin')
                                                                        <option value="{{$priv->name}}">{{$priv->name}}</option>
                                                                    @endif
                                                                @endforeach
                                                            </select>
                                                            @error('privileadges')
                                                                <p class="mt-2 text-danger font-16">{{$message}}</p>
                                                            @enderror
                                                        </div>

                                                        <div class="mt-4 px-2">
                                                            <button class="btn-block btn-sm btn btn-outline-info">Grant Privileadge</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Banking Tab End -->
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
