@extends('layouts.dashboard')
@section('styles')
    <link rel="stylesheet" href="/css/app.css">
@endsection
@section('content')
    <div class="mb-4 row clearfix">
        <div class="col">
            <h4 class="font-24">Transactions</h4>
        </div>
    </div>

    <div class="row clearfix">
        <div class="col">
            <div class="card card-box">
                <div class="card-body">
                    <div class="mb-4 font-18">All Transactions</div>
                   <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">user</th>
                                    <th scope="col">id</th>
                                    <th scope="col">type</th>
                                    <th scope="col">amount</th>
                                    <th scope="col">date</th>
                                    <th scope="col">status</th>
                                    <th scope="col">action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $i =0;
                                    
                                @endphp
                                @foreach ($transactions as $transaction)
                                @php
                                    $trx_id = Crypt::encrypt($transaction->id);
                                @endphp
                                    <tr class=" ">
                                        <th scope="row" class="font-weight-normal font-16">{{++$i}}</th>
                                        <th scope="row" class="font-weight-normal font-16">{{$transaction->user->username}}</th>
                                        <th scope="row" class="font-weight-normal font-16">{{$transaction->trxn_ref}}</th>
                                        <th scope="row" class="font-weight-normal font-16">{{$transaction->type}}</th>
                                        <th scope="row" class="font-weight-normal font-16"><span>&#8358;</span>{{number_format($transaction->amount, 2)}}</th>
                                        <th scope="row" class="font-weight-normal font-16">{{now()->parse($transaction->created_at)->format('d/m/Y')}}</th>
                                        <th scope="row" class=" font-16 {{$transaction->status == 'success' ? 'text-success' : ($transaction->status == 'failed' ? 'text-danger' : 'text-warning')}}">{{$transaction->status}}</th>
                                        <th scope="row"> <a href="{{route('admin.transaction.detail', $trx_id)}}" class="badge badge-info badge-pill">details</a> </th>
                                    </tr> 
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                    <div class="mt-4">
                        {{$transactions->links()}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection