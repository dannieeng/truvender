@extends('layouts.dashboard')
@section('content')

    <div class="row clearfix">
        <div class="col-md-6 offset-md-3">
            <div class="card card-box">
                <div class="card-body">
                    <h4 class="mb-4 font-18 text-primary">Transaction Details</h4>
                    <div class="mb-4">
                        <h4 class="font-16 mb-2">Reference Id: {{$transaction->trxn_ref}}</h4>
                        <h4 class="font-16 mb-2">User: {{$transaction->user->username}}</h4>
                        <h4 class="font-16 mb-2">Type: {{Str::ucfirst($transaction->type)}}</h4>
                        <h4 class="font-16 mb-2">Section: {{ucfirst($transaction->section)}}</h4>
                        <h4 class="font-16 mb-2">Currency: {{strtoupper($transaction->currency)}}</h4>
                        <h4 class="font-16 mb-2">Wallet: {{ucfirst($transaction->wallet)}}</h4>
                        <h4 class="font-16 mb-2">Amount: <span>&#8358;</span> {{number_format($transaction->amount, 2)}}</h4>
                        <h4 class="font-16 mb-2">Date: {{now()->parse($transaction->created_at)->format('d/m/Y')}}</h4>
                        <h4 class="font-16 mb-2 ">Status: 
                            <span class="{{$transaction->status == 'success' ? 'text-success' : ($transaction->status == 'failed' ? 'text-danger' : 'text-warning')}}">
                                {{$transaction->status}}
                            </span> 
                        </h4>
                        
                        @if($transaction->section == 'Transfer Fund')
                            <h4 class="font-16 mb-2">Account Name: {{ucfirst($transaction->acc_name)}}</h4>
                            <h4 class="font-16 mb-2">Account Number: {{$transaction->acc_number}}</h4>
                            <h4 class="font-16 mb-2">Bank: {{ucfirst($transaction->bank)}}</h4>
                        @endif
                    </div>

                    <div class="mb-4">
                        <a href="{{route('transactions.index')}}" class="btn btn-block btn-sm btn-outline-dark"> <i class="fi-arrow-left font-16"></i> Go Back</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection