@extends('layouts.dashboard')
@section('content')
    <div class="row clearfix">
        <div class="col">
            <div class="card car-box">
                <div class="card-body">
                    <h4 class="font-20 text-primary">Services</h4>

                    <div class="row">
                        <div class="col-md-8 offset-md-2">
                            <div class="mt-4 table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th scope="col">Name</th>
                                            <th scope="col">Availability</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($services as $service)
                                        @php
                                            $service_id = encrypt($service->id);
                                        @endphp
                                            <tr>
                                                <td scope="row">{{$service->name}}</td>
                                                <td scope="col">
                                                    <div>
                                                        <a href="{{route('admin.services.update', $service_id)}}" class="btn btn-rounded {{$service->is_available == true ? 'btn-outline-danger' :'btn-outline-info' }} btn-sm"> {{$service->is_available == true ? 'turn off' :'turn on' }}</a>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
            $('#check').change(function ()){
                alert('men');
            }
        });
    </script>
@endsection