@extends('layouts.dashboard')
@section('styles')
    <style>
        .notification-list ul li a{
            display: block;
            position: relative;
            padding: 10px 15px 10px 75px;
            min-height: 75px;
            color: #666;
            font-size: 14px;
            font-weight: 500;
            font-family: 'Inter', sans-serif;
            border-radius: 5px;
        }
    </style>
@endsection
@section('content')
    <div class="mb-4 row clearfix">
        <div class="col">
            <h4 class="font-24">Messages</h4>
        </div>
    </div>
   @livewire('admin.messages.index')
@endsection