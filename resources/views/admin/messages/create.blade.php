@extends('layouts.dashboard')
@section('content')
    

    <form action="{{route('admin.message.send')}}" method="POST">
        @csrf
        <div class="row clearfix">
            <div class="col-md-6 offset-md-2 col-sm-12">

                <div class="card card-box">
                    <div class="card-body">
                        <h4 class="font-20 mb-2">Broadcast Notice To Users</h4>
                        <div class="form-group">
                            <label>Subject</label>
                            <input type="text" name="subject" placeholder="Message Subject" class="form-control">
                            @error('subject')
                                <span class="text-danger font-16">{{$message}}</span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label>Body</label>
                            <textarea name="body" placeholder="Message Body" class="form-control"></textarea>
                            @error('body')
                                <span class="text-danger font-16">{{$message}}</span>
                            @enderror
                        </div>

                        <div class="mt-4">
                            <button type="submit" class="btn btn-block btn-outline-info">Send Message</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

@endsection