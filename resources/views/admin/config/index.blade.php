@extends('layouts.dashboard')
{{-- @section('title', 'All Users') --}}

@section('content')
    <div class="mb-4 row clearfix">
        <div class="col">
            <h4 class="font-24">Configurations</h4>
        </div>
    </div>

    <div class="row clearfix">
        <div class="col">
            <div class="card card-box">
                <div class="card-body">
                   <div class="profile-tab height-100-p">
                        <div class="tab height-100-p">
                            <ul class="nav nav-tabs customtab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" data-toggle="tab" href="#general" role="tab">General</a>
                                </li>
                                
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#policy" role="tab">Policies</a>
                                </li>
                                
                                
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#toc" role="tab">Terms Of Use</a>
                                </li>
                                
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#disc" role="tab">Anti Money Laundery Policy</a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <!-- General Tab start -->
                                <div class="tab-pane fade show active" id="general" role="tabpanel">
                                    <div class="pd-20">
                                        <div class="container pd-0">
                                            <div class="mt-2 mb-4">
                                                <h4 class="font-18 text-primary">General Configuration</h4>
                                            </div>
                                           @livewire('admin.config.general')
                                        </div>
                                    </div>
                                </div>
                                <!-- General Tab End -->

                                

                                 <!-- policies Tab start -->
                                <div class="tab-pane fade" id="policy" role="tabpanel">
                                    <div class="pd-20">
                                        <div class="container pd-0">
                                            <div class="mt-2 mb-4">
                                                <h4 class="font-18 text-primary">Policy</h4>
                                            </div>
                                             <div>
                                                <form method="POST" action="{{route('admin.config.policy')}}">
                                                    @csrf
                                                    <div class="form-group">
                                                        <textarea class="textarea_editorp form-control border-radius-0" readonly name="value" placeholder="Enter text ...">{{$policy->value}}</textarea>
                                                        @error('value')
                                                            <p class="mt-2 text-danger font-16">{{$message}}</p>
                                                        @enderror
                                                    </div>

                                                    <div class="mt-4"><button type="submit" class="btn btn-info" disabled>Save</button></div>
                                                </form>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <!-- policies Tab End -->

                                 <!-- TOU Tab start -->
                                <div class="tab-pane fade" id="toc" role="tabpanel">
                                    <div class="pd-20">
                                        <div class="container pd-0">
                                            <div class="mt-2 mb-4">
                                                <h4 class="font-18 text-primary">Term Of Use</h4>
                                            </div>
                                            <div>
                                                <form method="POST" action="{{route('admin.config.term-of-use')}}">
                                                    @csrf
                                                    <div class="form-group">
                                                        <textarea class="textarea_editort form-control border-radius-0" readonly name="value" placeholder="Enter text ...">{{$terms->value}}</textarea>
                                                        @error('value')
                                                            <p class="mt-2 text-danger font-16">{{$message}}</p>
                                                        @enderror
                                                    </div>

                                                    <div class="mt-4"><button type="submit" class="btn btn-info" disabled>Save</button></div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- TOU Tab End -->

                                <!-- Anti Money Laundery Policy Tab start -->
                                <div class="tab-pane fade" id="disc" role="tabpanel">
                                    <div class="pd-20">
                                        <div class="container pd-0">
                                            <div class="mt-2 mb-4">
                                                <h4 class="font-18 text-primary">Anti Money Laundery Policy </h4>
                                            </div>

                                            <div>
                                                <form method="POST" action="{{route('admin.config.disc')}}">
                                                    @csrf
                                                    <div class="form-group">
                                                        <textarea class="textarea_editord form-control border-radius-0" readonly name="value" placeholder="Enter text ...">{{$disclaim->value}}</textarea>
                                                        @error('value')
                                                            <p class="mt-2 text-danger font-16">{{$message}}</p>
                                                        @enderror
                                                    </div>

                                                    <div class="mt-4"><button type="submit" class="btn btn-info" disabled>Save</button></div>
                                                </form>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <!-- Anti Money Laundery Policy Tab End -->
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        jQuery(window).on("load", function () {
            "use strict";
            $(".textarea_editort").wysihtml5({
                html: !0
            });
            $(".textarea_editorp").wysihtml5({
                html: !0
            });

            $(".textarea_editord").wysihtml5({
                html: !0
            });
        });
    </script>
@endsection
