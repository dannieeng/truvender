@extends('layouts.dashboard')
@php
    $post_id = Crypt::encrypt($post->id);
@endphp

@section('content')
<form action="{{route('admin.blog.edit.save', $post_id)}}" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="row mt-4 mb-4">
            <div class="col-md-9">
                <div class="card card-box">
                    <div class="card-body">
                        <h4 class="font-20 text-primary">Edit Post</h4>
                        <div class="mt-4">
                            <div class="form-group">
                                <label>Title</label>
                                <input type="text" name="title" class="form-control" value="{{$post->title}}" placeholder="Post Title">
                                @error('title')
                                    <span class="text-danger font-16">{{$message}}</span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label>Post Image</label>
                                <div class="custom-file">
                                    <input type="file" name="image" class="custom-file-input">
                                    <label class="custom-file-label">Choose file</label>
                                </div>
                                @error('image')
                                    <span class="text-danger font-16">{{$message}}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label>Slug</label>
                                <input type="text" name="slug" value="{{$post->slug}}" class="form-control" placeholder="Post Slug">
                                @error('slug')
                                    <span class="text-danger font-16">{{$message}}</span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label>Body</label>
                                <textarea name="body" class="textarea_editor form-control border-radius-0" placeholder="Enter Post Body">{!!$post->body!!}</textarea>
                                @error('body')
                                    <span class="text-danger font-16">{{$message}}</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-3 mt-3 mt-lg-0">
                <div class="card card-box">
                    <div class="card-body">
                        <h4 class="font-20 text-primary">Post Category</h4>

                        <div class="mt-4">
                            <div class="form-group">
                                <label>Select Category</label>
                                <select name="category" id="cat" class="form-control">
                                    <option>--</option>
                                    @foreach ($categories as $cat)
                                        <option {{$post->post_category_id == $cat->id ? 'selected' : ''}} value="{{$cat->id}}">{{$cat->name}}</option>
                                    @endforeach
                                    <option value="add">Add Category</option>
                                </select>
                            </div>

                            <div class="form-group" id="add_cat">
                                <input type="text" name="category" id="cat_name" class="form-control" placeholder="Category Name">
                            </div>
                        </div>

                        <div class="mt-4">
                            <button type="submit" class="btn btn-outline-primary btn-sm btn-block">Publish Post</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection

@section('scripts')
    <script>

        $(document).ready(function() {
            $('#add_cat').hide(); 

            $('#cat').change(function(){
                if($('#cat').val() == 'add') {
                    $("#cat").removeAttr("name"); 
                    $('#add_cat').show();
                } else {
                    $("#cat").attr("name", "category"); 
                    $('#add_cat').hide(); 
                } 
            });
        });
    </script>
@endsection