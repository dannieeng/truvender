@extends('layouts.dashboard')
@section('content')
<form action="{{route('admin.blog.create.save')}}" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="row mt-4 mb-4">
            <div class="col-md-9">
                <div class="card card-box">
                    <div class="card-body">
                        <h4 class="font-20 text-primary">Create Post</h4>
                        <div class="mt-4">
                            <div class="form-group">
                                <label>Title</label>
                                <input type="text" name="title" class="form-control" value="{{old('title')}}" placeholder="Post Title">
                                @error('title')
                                    <span class="text-danger font-16">{{$message}}</span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label>Post Image</label>
                                <div class="custom-file">
                                    <input type="file" name="image" class="custom-file-input">
                                    <label class="custom-file-label">Choose file</label>
                                </div>
                                @error('image')
                                    <span class="text-danger font-16">{{$message}}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label>Slug</label>
                                <input type="text" name="slug" class="form-control" value="{{old('slug')}}" placeholder="Post Slug">
                                @error('slug')
                                    <span class="text-danger font-16">{{$message}}</span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label>Body</label>
                                <textarea name="body" class="textarea_editor form-control border-radius-0" placeholder="Enter Post Body">{{old('body')}}</textarea>
                                @error('body')
                                    <span class="text-danger font-16">{{$message}}</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-3 mt-3 mt-lg-0">
                <div class="card card-box">
                    <div class="card-body">
                        <h4 class="font-20 text-primary">Post Category</h4>

                        <div class="mt-4">
                            <div class="form-group">
                                <label>Select Category</label>
                                <select name="category" id="cat" class="form-control">
                                    <option>--</option>
                                    @foreach ($categories as $cat)
                                        <option value="{{$cat->id}}">{{$cat->name}}</option>
                                    @endforeach
                                    <option value="add">Add Category</option>
                                </select>
                            </div>

                            <div class="form-group" id="add_cat">
                                <input type="text" name="category" id="cat_name" class="form-control" placeholder="Category Name">
                            </div>
                        </div>

                        <div class="mt-4">
                            <button type="submit" class="btn btn-outline-primary btn-sm btn-block">Publish Post</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection

@section('scripts')
    <script>
        
        $(document).ready(function() {
            $('#add_cat').hide(); 

            $('#cat').change(function(){
                if($('#cat').val() == 'add') {
                    $("#cat").removeAttr("name"); 
                    $('#add_cat').show();
                } else {
                    $("#cat").attr("name", "category"); 
                    $('#add_cat').hide(); 
                } 
            });
        });
    </script>
@endsection