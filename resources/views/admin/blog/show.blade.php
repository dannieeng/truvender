@extends('layouts.dashboard')
@section('styles')
    <link rel="stylesheet" href="/css/app.css">
@endsection
@php
    $post_id = Crypt::encrypt($post->id);
@endphp
@section('content')
    <div class="row clearfix">
        <h4 class="ml-4 mb-4 font-24 font-weight-bold">Blog</h4>
    </div>
    <div class="row">
        <div class="col-md-8 col-sm-12">
            <div class="blog-detail card-box overflow-hidden mb-30">
                <div class="blog-img">
                    <img src="{{$post->image_path}}" class=" img-fluid" style="height: 26rem; margin: 0 auto;" alt="">
                </div>
                <div class="blog-caption font-14">
                    <div class="mb-4 px-4">
                        <h1 class="font-24 font-weight-bold text-center">{{$post->title}}</h1>
                    </div>
                    {!!$post->body!!}
                </div>
            </div>
        </div>


        <div class="col-md-4 col-sm-12">
            <div class="card-box mb-30">
                <h5 class="pd-20 h5 mb-0">Categories</h5>
                @if ($categories->count() > 0)
                    <div class="list-group">
                        @foreach ($categories as $category)
                            @php
                                $cat_id = Crypt::encrypt($category->id);
                            @endphp
                            <a href="{{route('admin.blog.index', $cat_id)}}}" class="list-group-item d-flex align-items-center justify-content-between">{{$category->name}} <span class="badge badge-primary badge-pill">{{$category->posts->count()}}</span></a>
                        @endforeach
                    </div>
                @else
                    <div class="mb-4 py-6">
                        <h4 class="text-center font-18">No Categories</h4>
                    </div>
                @endif
            </div>
            <div class="card-box mb-30">
                <h5 class="pd-20 h5 mb-0">Related Posts</h5>
                <div class="latest-post">
                    <ul>
                        @foreach ($related_posts as $rpost)
                        @php
                            $blog_id = Crypt::encrypt($rpost->id);
                        @endphp
                            <li>
                                <h4><a href="{{route('admin.blog.show', $blog_id)}}">{{$rpost->title}}</a></h4>
                                <span>{{$rpost->user->username}}</span>
                            </li>    
                        @endforeach
                    </ul>
                </div>
            </div>


            @if ($post->user_id == Auth::user()->id)
                <div class="card-box mb-30">
                    <h5 class="pd-20 h5 mb-0">Action</h5>
                    <div class="latest-post p-2">
                        <a href="{{route('admin.blog.edit', $post_id)}}" class="btn btn-block btn-sm btn-outline-info">Edit Post</a>
                        <a href="{{route('admin.blog.delete.trash', $post_id)}}" class="btn btn-block btn-sm btn-outline-danger">Delete Post</a>
                    </div>
                </div>
            @endif
        </div>
    </div>
 
@endsection