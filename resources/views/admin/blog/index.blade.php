@extends('layouts.dashboard')
@section('styles')
    {{-- <link rel="stylesheet" href="/css/app.css"> --}}
@endsection
@section('content')
    <div class="row clearfix">
        <h4 class="ml-4 mb-4 font-24 font-weight-bold">Blog</h4>
    </div>
    @if ($posts->count(0 > 0))
        <div class="row">
            <div class="col-md-8 col-12">
                <div class="row clearfix mt-2">
                    @foreach ($posts as $post)
                        @php
                            $blog_id = Crypt::encrypt($post->id);
                        @endphp
                        <div class="col-md-6 col-12 mb-4">
                            <a href="{{route('admin.blog.show', $blog_id)}}">
                                <div class="card card-box">
                                    <img class="card-img-top" src="{{$post->image_path}}" alt="Post image cap">
                                    <div class="card-body">
                                        <h4 class="font-16 mb-2 font-weight-bold">{{$post->title}}</h4>
                                        <p class="card-text">
                                            {!!$post->truncate_body(20)!!}
                                        </p>
                                    </div>
                                    <div class="mt-1 pl-4 mb-4">
                                        <span class="font-16 font-weight-bold"> By: {{$post->user->username}}</span>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
            

                <div class="mt-8">
                    {{$posts->links()}}
                </div>
            </div>


            <div class="col-md-4 col-sm-12">
                <div class="card-box mb-30">
                    <h5 class="pd-20 h5 mb-0">Categories</h5>
                    @if ($categories->count() > 0)
                        <div class="list-group">
                            @foreach ($categories as $category)
                                @php
                                    $cat_id = Crypt::encrypt($category->id);
                                @endphp
                                <a href="{{route('admin.blog.index', $cat_id)}}}" class="list-group-item d-flex align-items-center justify-content-between">
                                    {{$category->name}} <span class="badge badge-primary badge-pill">{{$category->posts->count()}}</span>
                                </a>
                            @endforeach
                        </div>
                    @else
                        <div class="mb-4 py-6">
                            <h4 class="text-center font-18">No Categories</h4>
                        </div>
                    @endif
                </div>
                <div class="card-box mb-30">
                    <h5 class="pd-20 h5 mb-0">Latest Post</h5>
                    <div class="latest-post">
                        <ul>
                            @foreach ($latest_posts as $lpost)
                            @php
                                $blog_id = Crypt::encrypt($lpost->id);
                            @endphp
                                <li>
                                    <h4><a href="{{route('admin.blog.show', $blog_id)}}">{{$lpost->title}}</a></h4>
                                    <span>{{$lpost->user->username}}</span>
                                </li>    
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    @else
        <div class="row clearfix">
            <div class="col">
                <div class="card card-box">
                    <div class="card-body">
                        <div class="p-30 d-flex justify-content-center">
                            <div class="mb-4">
                                <img src="/images/bg/blog.svg" style="height: 20rem; width:20rem;" alt="">
                                <h5 class="font-weight-normal text-center text-primary">No Posts</h5>
                                <p class="text-center">Post will be here: <a href="{{route('admin.blog.create')}}" class="badge badge-primary py-1 rounded-md">add post</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
@endsection