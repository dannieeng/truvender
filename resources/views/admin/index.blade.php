@extends('layouts.dashboard')
@section('title', 'Admin Dashboard')

@section('content')
<div class="mb-4 row clearfix">
    <div class="col">
        <h4 class="font-20">Welcome Back! <span class="text-primary">{{auth()->user()->username}}</span></h4>
    </div>
</div>
    <div class="row clearfix">
        <div class="col-md-3 col-6">
            <a href="{{route('admin.users')}}">
                <div class="card card-box">
                    <div class="card-body">
                        <div class="d-flex justify-content-start align-items-center">
                            <i class="font-30 dw dw-user1"></i>
                            <h1 class="ml-2 font-24">{{$users->count()}}</h1>
                        </div>
                        <div class="mt-2">
                            <span class=" font-18 font-weight-bold">Users</span>
                        </div>
                    </div>
                </div>
            </a>
        </div>

        <div class="col-md-3 col-6">
            <a href="{{route('admin.transactions')}}">
                <div class="card card-box">
                    <div class="card-body">
                        <div class="d-flex justify-content-start align-items-center">
                            <i class="font-30 ti-exchange-vertical"></i>
                            <h1 class="ml-2 font-24">{{$transactions->count()}}</h1>
                        </div>
                        <div class="mt-2">
                            <span class=" font-18 font-weight-bold">Transactions</span>
                        </div>
                    </div>
                </div>
            </a>
        </div>

        <div class="col-md-3 col-6 mt-3 mt-lg-0">
            <a href="{{route('admin.blog.index')}}">
                <div class="card card-box">
                    <div class="card-body">
                       <div class="d-flex justify-content-start align-items-center">
                            <i class="font-30 ti-layout-grid2"></i>
                            <h1 class="ml-2 font-24">{{$posts->count()}}</h1>
                        </div>
                        <div class="mt-2">
                            <span class="font-18 font-weight-bold">Blog Posts</span>
                        </div>
                    </div>
                </div>
            </a>
        </div>

        <div class="col-md-3 col-6 mt-3 mt-lg-0">
            <a href="#">
                <div class="card card-box">
                    <div class="card-body">
                        <div class="d-flex justify-content-start align-items-center">
                            <i class="font-30 fi-folder"></i>
                            <h1 class="ml-2 font-24">{{$messages != null ? $messages->count() : 0}}</h1>
                        </div>
                        <div class="mt-2">
                            <span class="font-14 font-weight-bold">New Messages</span>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>



    <div class="row mt-4 clearfix">
        <div class="col">
            <div class="card card-box">
                <h4 class="card-header">Newest Transactions</h4>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">transaction user</th>
                                    <th scope="col">transaction id</th>
                                    <th scope="col">transaction type</th>
                                    <th scope="col">transaction amount</th>
                                    <th scope="col">transaction date</th>
                                    <th scope="col">transaction status</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $i =0;
                                @endphp
                                @foreach ($newest_transactions as $transaction)
                                    <tr class=" ">
                                        <th scope="row" class="font-weight-normal font-16">{{++$i}}</th>
                                        <th scope="row" class="font-weight-normal font-16">{{$transaction->user->username}}</th>
                                        <th scope="row" class="font-weight-normal font-16">{{$transaction->trxn_ref}}</th>
                                        <th scope="row" class="font-weight-normal font-16">{{$transaction->type}}</th>
                                        <th scope="row" class="font-weight-normal font-16"><span>&#8358;</span>{{number_format($transaction->amount, 2)}}</th>
                                        <th scope="row" class="font-weight-normal font-16">{{now()->parse($transaction->created_at)->format('d/m/Y')}}</th>
                                        <th scope="row" class=" font-16 {{$transaction->status == 'success' ? 'text-success' : ($transaction->status == 'failed' ? 'text-danger' : 'text-warning')}}">{{$transaction->status}}</th>
                                    </tr> 
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    
                </div>
                <div class=" card-footer text-center">
                    <a href="" class="btn btn-block btn-sm">View all</a>
                </div>
            </div>
        </div>
    </div>

    <div>
        @if ($posts->count(0 > 0))
        <div class="mb-2 mt-4">
            <h4 class="">Blog Posts</h4>
        </div>
        <div class="row mt-4">
            @foreach ($posts as $post)
            @php
                $blog_id = Crypt::encrypt($post->id);
            @endphp
                <div class="col-lg-3 col-md-6 col-sm-12 mb-4">
                    <a href="{{route('admin.blog.show', $blog_id)}}">
                        <div class="card card-box">
                            <img class="card-img-top" src="{{Storage::url($post->image_path)}}" alt="Post image cap">
                            <div class="card-body">
                                <h4 class="font-16 mb-2">{{$post->title}}</h4>
                            </div>
                        </div>
                    </a>
                </div>
            @endforeach
        </div>
        @endif
    </div>

@endsection