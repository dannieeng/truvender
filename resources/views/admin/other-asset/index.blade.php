@extends('layouts.dashboard')
@section('styles')
    <link rel="stylesheet" href="/css/app.css">
@endsection
@section('content')
    <div class="mb-4 row clearfix">
        <div class="col">
            <h4 class="font-24">Other Asset Trades</h4>
        </div>
    </div>

    <div class="row clearfix">
        <div class="col">
            <div class="card card-box">
                <div class="card-body">
                    
                    @if ($transactions->count() > 0)
                    <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">user</th>
                                        <th scope="col">id</th>
                                        <th scope="col">asset</th>
                                        <th scope="col">amount</th>
                                        <th scope="col">email</th>
                                        <th scope="col">phone</th></th>
                                        <th scope="col">status</th>
                                        <th scope="col">action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $i =0;
                                        
                                    @endphp
                                    @foreach ($transactions as $transaction)
                                    @php
                                        $trx_id = Crypt::encrypt($transaction->id);
                                    @endphp
                                        <tr class=" ">
                                            <th scope="row" class="font-weight-normal font-16">{{++$i}}</th>
                                            <th scope="row" class="font-weight-normal font-16">{{$transaction->user->username}}</th>
                                            <th scope="row" class="font-weight-normal font-16">{{$transaction->transaction->trxn_ref}}</th>
                                            <th scope="row" class="font-weight-normal font-16">{{ucfirst($transaction->asset->name)}}</th>
                                            <th scope="row" class="font-weight-normal font-16"><span>&#8358;</span>{{number_format($transaction->price, 2)}}</th>
                                            <th scope="row" class="font-weight-normal font-16">{{$transaction->account_email}}</th>
                                            <th scope="row" class="font-weight-normal font-16">{{$transaction->account_phone}}</th>
                                            <th scope="row" class=" font-16 {{$transaction->status == true ? 'text-success' : ($transaction->status == null ? 'text-danger' :'text-warning')}}">{{$transaction->status == true ? 'success' : ( $transaction->status == null ?'canceled': 'pending' )}}</th>
                                            <th scope="row"> 
                                            
                                                <div class="dropdown">
                                                    <a class="btn btn-link font-24 p-0 line-height-1 no-arrow dropdown-toggle" href="#" role="button" data-toggle="dropdown">
                                                        <i class="dw dw-more"></i>
                                                    </a>
                                                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
                                                       
                                                        
                                                        @if ($transaction->image_path != null)
                                                            <a href="#" class="dropdown-item" data-toggle="modal" data-target="#trade-{{$transaction->id}}-reciept" ><i class="dw dw-eye"></i> view reciept</a>     
                                                        @endif
                                                        @if (auth()->user()->role->name == 'admin' && $transaction->status == false)
                                                            <a href="{{route('admin.other-asset.accept', $trx_id)}}" class="dropdown-item text-success" > <i class="dw dw-checked"></i>accept trade</a> 
                                                            <a href="{{route('admin.other-asset.reject', $trx_id)}}" class="dropdown-item text-danger" > <i class="dw dw-cancel"></i>reject trade</a> 
                                                        @else
                                                            <a href="#" class="dropdown-item danger text-success" ><i class="dw dw-checked"></i> accept trade</button>
                                                            <a href="#" class="dropdown-item danger text-danger" ><i class="dw dw-cancel"></i> reject trade</button>
                                                        @endif

                                                    </div>
                                                </div>
                                                
                                            </th>
                                        </tr> 

                                         @if ($transaction->image_path != null)
                                            <div class="modal fade" id="trade-{{$transaction->id}}-reciept" tabindex="-1" role="dialog" aria-labelledby="trade-{{$transaction->id}}-reciept" aria-hidden="true">
                                                <div class="modal-dialog modal-sm modal-dialog-centered">
                                                    <div class="modal-content">
                                                    
                                                        <div class="modal-body">
                                                            <div class="d-flex mb-3 justify-content-between align-items-center">
                                                                <h4 class="font-16">Trade Reciept</h4>
                                                                <a href="#" class="text-danger font-weight-normal" data-dismiss="modal"><i class="ion-close-round font-16"></i></a>
                                                            </div>

                                                            <div class="mt-2">
                                                                <img src="{{Storage::url($transaction->image_path)}}" style="height: 36rem: width: 7.6rem;" alt="Reciept">
                                                            </div>
                                                            
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    @endforeach
                                </tbody>
                            </table>
                        </div>

                        <div class="mt-4">
                            {{$transactions->links()}}
                        </div>
                    @else
                        
                        <div class="p-30 d-flex justify-content-center">
                            <div class="mb-4">
                                <img src="/images/bg/box.svg" style="height: 20rem; width:20rem;" alt="">
                                <h5 class="font-weight-normal text-center text-primary">No Trades Yet</h5>
                                {{-- <p class="text-center">Transactions will be here: <a href="" class="text-primary">start transaction</a></p> --}}
                            </div>
                        </div>

                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection