@extends('layouts.dashboard')

@section('content')
    <div class="row">
        <div class="col-md-6 offset-md-2">
            <div class="card card-box">
                <div class="card-body">
                    <h4 class="font-18 mb-3 text-primary text-center">{{ucfirst($asset->name)}} Rates</h4>

                    <div class="d-flex justify-content-center"> 
                            <img src="{{$asset->image_path}}" style="height: 14rem; width: 14rem; margin: 0 auto;" alt="" class="card-img"> 
                        </div>
                        <div class="mt-3">
                            @livewire('admin.rates.other-asset.index', ['asset' => $asset])                           
                        </div>
                </div>
            </div>
        </div>
    </div>
@endsection