
<style>
    .consent{
        position: fixed;
        bottom: 0;
        z-index: 100;
        padding: 10px 40px;
        text-align: left;
        letter-spacing: 1px;
        font-weight: 600;
        color: #2a4365;
        background: azure;
        font-size: smaller;
    }

    .consent button{
        background: rgb(58, 20, 133);
        color: rgb(238, 233, 239);
        padding: 4px 6px;
        border-radius: 4px;
        font-weight: 700;
        cursor: pointer;
    }
    .consent button:hover{
        font-weight: 700;
        background: rgb(92, 44, 189);
    }

</style>


@if($cookieConsentConfig['enabled'] && ! $alreadyConsentedWithCookies)
    <div class="consent">

        @include('cookieConsent::dialogContents')

        <script>

            window.laravelCookieConsent = (function () {

                const COOKIE_VALUE = 1;
                const COOKIE_DOMAIN = '{{ config('session.domain') ?? request()->getHost() }}';

                function consentWithCookies() {
                    setCookie('{{ $cookieConsentConfig['cookie_name'] }}', COOKIE_VALUE, {{ $cookieConsentConfig['cookie_lifetime'] }});
                    hideCookieDialog();
                }

                function cookieExists(name) {
                    return (document.cookie.split('; ').indexOf(name + '=' + COOKIE_VALUE) !== -1);
                }

                function hideCookieDialog() {
                    const dialogs = document.getElementsByClassName('js-cookie-consent');

                    for (let i = 0; i < dialogs.length; ++i) {
                        dialogs[i].style.display = 'none';
                    }
                }

                function setCookie(name, value, expirationInDays) {
                    const date = new Date();
                    date.setTime(date.getTime() + (expirationInDays * 24 * 60 * 60 * 1000));
                    document.cookie = name + '=' + value
                        + ';expires=' + date.toUTCString()
                        + ';domain=' + COOKIE_DOMAIN
                        + ';path=/{{ config('session.secure') ? ';secure' : null }}'
                        + '{{ config('session.same_site') ? ';samesite='.config('session.same_site') : null }}';
                }

                if (cookieExists('{{ $cookieConsentConfig['cookie_name'] }}')) {
                    hideCookieDialog();
                }

                const buttons = document.getElementsByClassName('js-cookie-consent-agree');

                for (let i = 0; i < buttons.length; ++i) {
                    buttons[i].addEventListener('click', consentWithCookies);
                }

                return {
                    consentWithCookies: consentWithCookies,
                    hideCookieDialog: hideCookieDialog
                };
            })();
        </script>

    </div>
@endif
