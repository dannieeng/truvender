@php
	$user = auth()->user();
@endphp
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	<meta charset="utf-8">
	<!-- Mobile Specific Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<!-- ===============================================-->
	<!-- Site Title-->
	<!-- ===============================================-->
	<title>{{config('app.name')}}</title>
	
	<!-- Site favicon -->
	<link rel="apple-touch-icon" sizes="180x180" href="/images/logo/tru-logo.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/images/logo/tru-favicon.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/images/logo/tru-favicon.png">

	<!-- Google Font -->
	<link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700;800&display=swap" rel="stylesheet">
	
	<!-- CSS -->
	<link rel="stylesheet" type="text/css" href="{{asset('auth/vendors/styles/core.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('auth/vendors/styles/icon-font.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('auth/src/plugins/dropzone/src/dropzone.css')}}">
	
	<!-- switchery css -->
	<link rel="stylesheet" type="text/css" href="{{asset('auth/src/plugins/switchery/switchery.min.css')}}">
	<!-- bootstrap-tagsinput css -->
	<link rel="stylesheet" type="text/css" href="{{asset('auth/src/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css')}}">
	<!-- bootstrap-touchspin css -->
	<link rel="stylesheet" type="text/css" href="{{asset('auth/src/plugins/bootstrap-touchspin/jquery.bootstrap-touchspin.css')}}">
	<link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />
	<link rel="stylesheet" href="/css/animate.css">
	
	{{-- //dataTables --}}
	<link rel="stylesheet" type="text/css" href="/auth/src/plugins/datatables/css/dataTables.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="/auth/src/plugins/datatables/css/responsive.bootstrap4.min.css">
	
	<link rel="stylesheet" type="text/css" href="{{asset('auth/vendors/styles/style.css')}}">
	{{--<script src="//code.tidio.co/hktrup1x24wrcn4akxkzvzhu6repbjh7.js" async></script>--}}
	{{--<script src="//code.jivosite.com/widget/a3OBgI5yvs" async></script>--}}
	

	{{-- Font Awesome --}}
	<link rel="stylesheet" href="/fontawesome/css/all.css">
	<link rel="stylesheet" href="/fontawesome/css/fontawesome.css">
	<link rel="stylesheet" href="/fontawesome/css/brands.css">
	<link rel="stylesheet" href="/fontawesome/css/solid.css">
	<style>
		.loader-logo img{
			height: 4.6rem;
			width: 5rem;
		}
	</style>
	<livewire:styles />
	



	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-119386393-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-119386393-1');
	</script>
	
	<!--Start of Tawk.to Script-->
    <script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
    var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
    s1.async=true;
    s1.src='https://embed.tawk.to/5fe5145ddf060f156a8ff6a5/1eqbdf40q';
    s1.charset='UTF-8';
    s1.setAttribute('crossorigin','*');
    s0.parentNode.insertBefore(s1,s0);
    })();
    </script>
    <!--End of Tawk.to Script-->
	
	<style>
        .currency{
            padding: 5px 10px;
            border-radius: 120px;
            font-weight: 800;
            font-size: 20px; 
        }
        .ngn{
            background-color: #28a745;
            color: azure; 
        }
        .btc{
            color: azure;
            background-color: #eb9d48;
        }
    </style>
	@yield('styles')
</head>
<body>

	{{-- <div class="pre-loader">
		<div class="pre-loader-box">
			<div class="loader-logo"><img src="/images/logo/tru-logo-dark.png" alt=""></div>
			<div class='loader-progress' id="progress_div">
				<div class='bar' id='bar1'></div>
			</div>
			<div class='percent' id='percent1'>0%</div>
			<div class="loading-text">
				Loading...
			</div>
		</div>
	</div> --}}

	@include('_inc.auth.header')

	@if ($user->role->name == 'user')
		@include('_inc.auth.user-sidebar')
	@elseif($user->role->name == 'admin')
		@include('_inc.auth.admin-sidebar')
	@endif

	<div class="main-container">
		<div class="pd-ltr-20 xs-pd-20-10">
			<div class="min-height-200px">
				@yield('breadcrumb')
				<div class="pd-5">
					@include('_inc.auth.message')

					@if ($user->banking == false && $user->role->name == "user")
						<div class="alert alert-info alert-dismissible fade show" role="alert">
							<strong>Info! </strong> Banking details have not been set <a href="{{route('settings')}}" class="font-16 font-weight-bold">click here</a> to setup your account
							<button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="font-weight-light" aria-hidden="true">×</span></button>
						</div>
					@endif

					
					@yield('content')
				</div>
			</div>
		</div>
	</div>

	<form action="{{route('logout')}}" style="display: none;" id="logout-form" method="post">
		@csrf
	</form>
	<!-- js -->
	<script src="{{asset('auth/vendors/scripts/core.js')}}"></script>
	<script src="{{asset('auth/vendors/scripts/script.min.js')}}"></script>
	<script src="{{asset('auth/vendors/scripts/process.js')}}"></script>
	<script src="{{asset('auth/vendors/scripts/layout-settings.js')}}"></script>
	<!-- switchery js -->
	<script src="{{asset('auth/src/plugins/switchery/switchery.min.js')}}"></script>
	<!-- bootstrap-tagsinput js -->
	<script src="{{asset('auth/src/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js')}}"></script>
	<!-- bootstrap-touchspin js -->
	<script src="{{asset('auth/src/plugins/bootstrap-touchspin/jquery.bootstrap-touchspin.js')}}"></script>
	<script src="{{asset('auth/vendors/scripts/advanced-components.js')}}"></script>
	<script src="{{asset('auth/src/plugins/dropzone/src/dropzone.js')}}"></script>
	<script src="https://unpkg.com/aos@next/dist/aos.js"></script>

	{{-- //Data Tables --}}
	<script src="/auth/src/plugins/datatables/js/jquery.dataTables.min.js"></script>
	<script src="/auth/src/plugins/datatables/js/dataTables.bootstrap4.min.js"></script>
	<script src="/auth/src/plugins/datatables/js/dataTables.responsive.min.js"></script>
	<script src="/auth/src/plugins/datatables/js/responsive.bootstrap4.min.js"></script>
	<!-- buttons for Export datatable -->
	<script src="/auth/src/plugins/datatables/js/dataTables.buttons.min.js"></script>
	<script src="/auth/src/plugins/datatables/js/buttons.bootstrap4.min.js"></script>
	<script src="/auth/src/plugins/datatables/js/buttons.print.min.js"></script>
	<script src="/auth/src/plugins/datatables/js/buttons.html5.min.js"></script>
	<script src="/auth/src/plugins/datatables/js/buttons.flash.min.js"></script>
	<script src="/auth/src/plugins/datatables/js/pdfmake.min.js"></script>
	<script src="/auth/src/plugins/datatables/js/vfs_fonts.js"></script> 
	<!-- Datatable Setting js -->
	<script src="/auth/vendors/scripts/datatable-setting.js"></script>
	
	@yield('scripts')
	<script>
		var width = 100,
		perfData = window.performance.timing, // The PerformanceTiming interface represents timing-related performance information for the given page.
		EstimatedTime = -(perfData.loadEventEnd - perfData.navigationStart),
		time = parseInt((EstimatedTime/1000)%70)*100;
		// Percentage Increment Animation
		var PercentageID = $("#percent1"),
				start = 0,
				end = 100,
				durataion = time;
				animateValue(PercentageID, start, end, durataion);
				
		function animateValue(id, start, end, duration) {
		
			var range = end - start,
			current = start,
			increment = end > start? 1 : -1,
			stepTime = Math.abs(Math.floor(duration / range)),
			obj = $(id);
			
			var timer = setInterval(function() {
				current += increment;
				$(obj).text(current + "%");
				$("#bar1").css('width', current+"%");
			//obj.innerHTML = current;
				if (current == end) {
					clearInterval(timer);
				}
			}, stepTime);
		}
		// Fading Out Loadbar on Finised
		setTimeout(function(){
			$('.pre-loader').fadeOut(300);
		}, time);
	</script>
	
	<livewire:scripts />
</body>
</html>