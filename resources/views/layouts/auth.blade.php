<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

       <!-- Site favicon -->
        <link rel="apple-touch-icon" sizes="180x180" href="/images/logo/tru-logo.png">
        <link rel="icon" type="image/png" sizes="32x32" href="/images/logo/tru-favicon.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/images/logo/tru-favicon.png">


        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700;800&display=swap" rel="stylesheet">
        <!-- CSS -->
        <link rel="stylesheet" type="text/css" href="{{asset ('auth/vendors/styles/core.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset ('auth/vendors/styles/icon-font.min.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset ('auth/vendors/styles/style.css')}}">

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-119386393-1"></script>
        {{--<script src="//code.tidio.co/hktrup1x24wrcn4akxkzvzhu6repbjh7.js" async></script>--}}
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-119386393-1');
        </script>

        <title>{{ config('app.name') }} - @yield('title')</title>

        <!-- Fonts -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">

        <!-- Styles -->
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">

        @livewireStyles

        <!-- Scripts -->
        {{-- <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.7.0/dist/alpine.js" defer></script> --}}
        
        
        <!--Start of Tawk.to Script-->
        <script type="text/javascript">
        var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
        (function(){
        var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
        s1.async=true;
        s1.src='https://embed.tawk.to/5fe5145ddf060f156a8ff6a5/1eqbdf40q';
        s1.charset='UTF-8';
        s1.setAttribute('crossorigin','*');
        s0.parentNode.insertBefore(s1,s0);
        })();
        </script>
        <!--End of Tawk.to Script-->
    </head>
    <body class="login-page">
        <div class="login-header box-shadow">
            <div class="container-fluid d-flex justify-content-between align-items-center">
                <div class="brand-logo">
                    <a href="{{route('welcome')}}" class="flex align-items-center -ml-5 md:-ml-0">
                        <img src="/images/logo/tru-favicon.png" style="width: 2.6rem; height: 2.6rem;" alt="">
                        <h2 class=" font-weight-bold text-primary mt-2">{{'truvender'}}</h2>
                    </a>
                </div>
                <div class="login-menu">
                    <ul>
                        @if (Route::is('register'))
                                <li><a href="{{ route('login') }}">Sign In</a></li>
                        @elseif(Route::is('login'))
                                <li><a href="{{ route('register') }}">Sign Up</a></li>
                        @else
                            <li><a href="{{route('logout')}}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Sign Out</a></li>
                        @endif
                    </ul>
                </div>
            </div>
        </div>

        @yield('added')

        
        <!-- js -->
        <script src="{{asset ('auth/vendors/scripts/core.js')}}"></script>
        <script src="{{asset ('auth/vendors/scripts/script.min.js')}}"></script>
        <script src="{{asset ('auth/vendors/scripts/process.js')}}"></script>
        <script src="{{asset ('auth/vendors/scripts/layout-settings.js')}}"></script>

        @livewireScripts
    </body>
</html>
