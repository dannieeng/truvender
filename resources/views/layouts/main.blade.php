<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" dir="ltr">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <!-- ===============================================-->
        <!-- Site Title-->
        <!-- ===============================================-->
        <title>@yield('title') - {{config('app.name')}}</title>

    <!-- ===============================================-->
    <!-- Favicons-->
    <!-- ===============================================-->
    <link rel="apple-touch-icon" sizes="180x180" href="/images/logo/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/images/logo/tru-favicon.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/images/logo/tru-favicon.png">
    <link rel="shortcut icon" type="image/x-icon" href="/images/logo/tru-favicon.png">
    <link rel="manifest" href="/guest/img/favicons/manifest.json">
    <meta name="msapplication-TileImage" content="/guest/img/favicons/mstile-150x150.png">
    <meta name="theme-color" content="#ffffff">

    <!-- ===============================================-->
    <!-- Stylesheets-->
    <!-- ===============================================-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,500,600,700%7cPoppins:100,200,300,400,500,600,700,800,900" rel="stylesheet">
    <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />
    <link href="/guest/lib/owl-carousel/owl.carousel.min.css" rel="stylesheet">
    <link href="/guest/lib/plyr/plyr.css" rel="stylesheet">
    <link href="/guest/lib/slick-carousel/slick.css" rel="stylesheet">
    <link href="/guest/lib/slick-carousel/slick-theme.css" rel="stylesheet">
    <link href="/guest/css/theme.css" rel="stylesheet">
    
    {{--<script src="//code.jivosite.com/widget/a3OBgI5yvs" async></script>--}}
    
    {{--<script src="//code.tidio.co/hktrup1x24wrcn4akxkzvzhu6repbjh7.js" async></script>--}}
    
    <!--Start of Tawk.to Script-->
    <script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
    var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
    s1.async=true;
    s1.src='https://embed.tawk.to/5fe5145ddf060f156a8ff6a5/1eqbdf40q';
    s1.charset='UTF-8';
    s1.setAttribute('crossorigin','*');
    s0.parentNode.insertBefore(s1,s0);
    })();
    </script>
    <!--End of Tawk.to Script-->
        
    @livewireStyles
    @livewireScripts
    
</head>

<body>
    <!-- ===============================================-->
    <!-- Main Content-->
    <!-- ===============================================-->
    <main class="main" id="top">
        @include('_inc.guest.header')
        @yield('content')
        
        @include('_inc.guest.footer')
    </main>
    <!-- ===============================================-->
    <!-- End of Main Content-->
    <!-- ===============================================-->



    <!-- ===============================================-->
    <!-- JavaScripts-->
    <!-- ===============================================-->
    <script src="/guest/js/jquery.min.js"></script>
    <script src="/guest/js/popper.min.js"></script>
    <script src="/guest/js/bootstrap.js"></script>
    <script src="/guest/js/plugins.js"></script>
    <script src="/guest/lib/is/is.min.js"></script>
    <script src="/guest/lib/stickyfilljs/stickyfill.min.js"></script>
    <script src="/guest/lib/sticky-kit/sticky-kit.min.js"></script><!-- Global site tag (gtag.js) - Google Analytics-->
    <script src="/guest/lib/owl-carousel/owl.carousel.min.js"></script>
    <script src="/guest/js/polyfill.js"></script>
    <script src="/guest/lib/plyr/plyr.min.js"></script>
    <script src="/guest/lib/slick-carousel/slick.min.js"></script>
    <script src="/guest/js/theme.js"></script>
    <script src="https://unpkg.com/aos@next/dist/aos.js"></script>

    <script>
        AOS.init();
    </script>
</body>

</html>