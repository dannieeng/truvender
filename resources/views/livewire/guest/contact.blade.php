<form class="mt-5 zform" wire:submit.prevent="submit">
    @if (session()->has('msg'))
        <div class="zform-feedback">
            <p class="text-white">{{ session('msg')}}</p>
        </div>
        
    @endif
    <input type="hidden" name="to" wire:model="to" value="support@truvender.com" />
    <div class="form-row">
        <div class="form-group col-md-6">
            <label for="inputName">Your Name</label>
            <input wire:model="fullname" class="form-control bg-extra-1 border-extra-1 text-700" id="inputName" type="text" placeholder="Full Name" />
            @error('fullname')
                <span class="text-white mt-2">{{$message}}</span>
            @enderror
        </div>
        <div class="form-group col-md-6">
            <label for="inputEmail">Your Mail</label>
            <input wire:model="email" class="form-control bg-extra-1 border-extra-1 text-700" id="inputEmail" type="email" placeholder="example@domain.com" />
            @error('email')
                <span class="text-white mt-2">{{$message}}</span>
            @enderror
        </div>
        <div class="form-group col mt-4"> <label for="message">Your Massage</label>
            <textarea wire:model="message" class="form-control bg-extra-1 border-extra-1 text-700" id="message" placeholder="type your message here" rows="10"></textarea>
            @error('message')
                <span class="text-white mt-2">{{$message}}</span>
            @enderror
        </div>
    </div><button class="btn btn-light btn-block mt-4 shadow-white" type="submit">Send Mail</button>
</form>