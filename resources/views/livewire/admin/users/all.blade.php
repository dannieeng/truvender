 <div class="table-responsive">
    <div class="pb-20">
        <table class="data-table-all table stripe hover nowrap">
            <thead>
                <tr>
                    <th class="table-plus datatable-nosort">Username</th>
                    <th>Date Registered</th>
                    <th>Transactions</th>
                    <th>Referrals</th>
                    <th class="datatable-nosort">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($users as $user)
                @php
                    $user_id = Crypt::encrypt($user->id);
                @endphp
                    <tr>
                        <td class="table-plus">{{$user->username}}</td>
                        <td>{{ now()->parse($user->created_at)->format('d/m/Y') }}</td>
                        <td>{{ $user->transactions->count() }}</td>
                        <td>{{ $user->referrals->count() }}</td>
                        <td>
                            <div class="dropdown">
                                <a class="btn btn-link font-24 p-0 line-height-1 no-arrow dropdown-toggle" href="#" role="button" data-toggle="dropdown">
                                    <i class="dw dw-more"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
                                    <a class="dropdown-item" href="{{route('admin.users.profile', $user_id)}}"><i class="dw dw-eye"></i> Profile</a>
                                    <a class="dropdown-item" href="{{route('admin.users.edit-profile', $user_id)}}"><i class="dw dw-edit2"></i> Edit</a>
                                </div>
                            </div>
                        </td>
                    </tr>
                @endforeach
                
            </tbody>
        </table>
    </div>
</div>
