

    <div class="row clearfix mb-1">
        <div class="col-md-6">
            <h3 class="ml-3">Countries</h3>
        </div>

        <div class="col-md-4 offset-md-2 mt-2 mt-md-0">
            <div class="form-group">
                <input type="text" wire:model="search_name" class="form-control" placeholder="Search GiftCard">
            </div>
        </div>
    </div>
    <div class="row clearfix">

        @if ($countries->count() > 0)
            @foreach ($countries as $country)
                <div class="col-md-3 mt-3">
                    <a href="#" wire:click.prevent="edit_country({{$country->id}})">
                        <div class="card card-box">
                            <div class="card-body">
                                <div class="card-img">
                                    <img src="{{Storage::url($country->flag)}}" style="height: 9.4rem; width: 100%;" alt="">
                                </div>
                                <div class="card-name mt-3 ">
                                    <h4 class="text-center font-16">{{$country->name}} ({{$country->currency}})</h4>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            @endforeach
        @else
            <div class="col mt-3">
                <div class="card card-box">
                    <div class="card-body text-center">
                        <div class="mb-2 mt-4">
                            <img src="/images/bg/country.svg" style="width:46rem;" alt="">
                        </div>
                        <h3 class="font-18 font-weight-normal">No Countries Available</h3>
                    </div>
                </div>
            </div>
        @endif
        
        
    </div>