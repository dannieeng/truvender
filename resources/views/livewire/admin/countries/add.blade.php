<form wire:submit.prevent="add">
    <div class="form-group">
        <label> Country Name </label>
        <input type="text" wire:model="name" class="form-control" placeholder="Country Name Here">
        @error('name')
            <p class="text-danger mt-2 font-16">{{$message}}</p>
        @enderror
    </div>
    
    <div class="form-group">
        <label> Currency </label>
        <input type="text" wire:model="currency" class="form-control" placeholder="Currency Here">
        @error('currency')
            <p class="text-danger mt-2 font-16">{{$message}}</p>
        @enderror
    </div>

    <div class="form-group">
        <label> Country flag </label>
        <div class="custom-file">
            <input type="file" wire:model="flag" class="custom-file-input">
            <label class="custom-file-label">Choose file</label>
        </div>
        @error('flag')
             <p class="text-danger mt-2 font-16">{{$message}}</p>
        @enderror
    </div>

    <div class="mt-4 mb-2">
        <button type="submit" class="btn btn-block btn-outline-info btn-sm">Submit</button>
    </div>
</form>