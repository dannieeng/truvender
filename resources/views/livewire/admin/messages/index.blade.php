 <div class="bg-white border-radius-4 box-shadow mb-30">
        <div class="row no-gutters">
            @if ($messages->count() > 0)
                <div class="col-lg-3 col-md-4 col-sm-12">
                    <div class="chat-list bg-light-gray">
                        <div class="chat-search">
                            <span class="ti-search"></span>
                            <input type="text" wire:model="search_title" placeholder="Search title">
                        </div>
                        <div class="notification-list chat-notification-list customscroll">
                            <ul>
                            @foreach ($messages as $message)
                                    <li>
                                        <a href="#" wire:click="read({{$message->id}})" class="clearfix pl-2 font-weight-normal">
                                            <h3>{{$message->title}}</h3>
                                        </a>
                                    </li>
                            @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            @endif
            <div class="col-lg-9 col-md-8 col-sm-12">
                <div class="chat-detail">
                    <div class="chat-profile-header clearfix">
                        <div class="left">
                            <div class="clearfix">
                                <div class="chat-profile-name">
                                    <h3>Read Message</h3>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    <div class="chat-box">
                        <div class="chat-desc customscroll">
                            @if ($message != null)
                                <div class="px-5 mt-5">
                                    <h4 class="font-18 mb-4">{{$message->title}}</h4>
                                    <p>
                                        {{$message->body}}
                                    </p>
                                    <div class="mt-2 d-flex justify-content-between">
                                        <div>Message Date: <span class="font-12 badge-dark text-white-50 badge px-1 py-0">{{now()->parse($message->created_at)->format('Y-m-d H:i:s')}}</span></div>
                                        @if ($message != null && $message->type!= null)
                                            <div>Message Type: <span class="font-12 badge-dark text-white-50 badge px-1 py-0">{{$message->type}}</span></div>
                                        @endif

                                        <div>Message {{$message->from != null ? 'From' : 'To'}}: <span class="font-12 badge-dark text-white-50 badge px-1 py-0">{{$message->from != null ? $message->from : $message->sent_to}}</span></div>
                                    </div>
                                </div>
                            @else
                                <div class="px-5 d-flex justify-content-center align-items-center mt-5">
                                    <h4 class="font-18 mb-4">No messages</h4>
                                </div>
                            @endif
                        </div>
                        {{-- <div class="chat-footer">
                            <div class="file-upload"><a href="#"><i class="fa fa-paperclip"></i></a></div>
                            <div class="chat_text_area">
                                <textarea placeholder="Type your message…"></textarea>
                            </div>
                            <div class="chat_send">
                                <button class="btn btn-link" type="submit"><i class="icon-copy ion-paper-airplane"></i></button>
                            </div>
                        </div> --}}
                    </div>
                </div>
            </div>
        </div>
    </div>