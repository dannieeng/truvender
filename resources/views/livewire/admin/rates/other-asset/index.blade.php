<form wire:submit.prevent="update_rate" class="mb-4" >
{{-- <form method="POST" action="{{route('trade.gift-card.submit')}}" enctype="multipart/form-data"> --}}
    @csrf
  
  

       
    <div class=" form-group">
        <label>Currency </label>
        <select name="currency" class="form-control text-center" id="currency" {{$editing == false ? 'disabled' : ''}} wire:model="currency">
            <option value=""> Select Currency</option>
            <option value="$">(USD) - $</option>
            <option value="£">(GBP) - £</option>
            <option value="€">(EURO) - €</option>
            <option value="¥">(CNY) - ¥</option>
        </select>

        @error('currency')
            <span class="font-14 text-danger">{{$message}}</span>
        @enderror
    </div>

    <div class="form-group">
        <label>NGN Rate/{{$currency != null ? $currency : '$'}}</label>
        <input type="text" class="form-control" wire:model="rate" {{$editing == false ? 'disabled' : ''}} placeholder="NGN0.00"> 

        @error('rate')
            <span class="font-14 text-danger">{{$message}}</span>
        @enderror
    </div>

      <div class="form-group">
        <label>Maximum Amount</label>
        <input type="text" class="form-control" wire:model="max_amount" {{$editing == false ? 'disabled' : ''}} placeholder="{{$currency != null ? $currency : '$'}}0.00"> 

        @error('max_amount')
            <span class="font-14 text-danger">{{$message}}</span>
        @enderror
    </div>


    <div class="form-group">
        <label>Minimum Amount</label>
        <input type="text" class="form-control" wire:model="min_amount" {{$editing == false ? 'disabled' : ''}} placeholder="{{$currency != null ? $currency : '$'}}0.00"> 

        @error('min_amount')
            <span class="font-14 text-danger">{{$message}}</span>
        @enderror
    </div>



    <div class="mt-4">
        @if (session()->has('success') || session()->has('err'))
            <div class="mb-2 text-center">
                <p class="{{session()->has('success') ? 'text-sucess' : 'text-danger' }}">{{session('success')}}</p>
            </div>
        @endif
        
       @if ($editing == false)
           <a href="#" wire:click.prevent="start_editing" class="btn btn-sm btn-outline-info btn-block btn-rounded">Edit Rate</a>
       @endif
        
        @if ($editing == true)
            <a href="{{url()->previous()}}" class="btn btn-sm btn-outline-danger btn-block btn-rounded">Cancel Edit</a>
            <button type="submit" class="btn btn-rounded btn-sm btn-block btn-primary continue">Update</button>
            
        @endif
    </div>

  
</form>