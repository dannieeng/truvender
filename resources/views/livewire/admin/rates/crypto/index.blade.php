
<form wire:submit.prevent="update_rate">
    <div class="mt-4">
        <div class="form-group">
            <label>Currency</label>
            <input type="text" class="form-control" value="{{$crypto->name}}" readonly>
        </div>
    
        <div class="form-group">
            <label>Symbol</label>
            <input type="text" class="form-control" value="{{$crypto->symbol}}" readonly>
        </div>
    
        <div class="form-group">
            <label>Buyer Rate</label>
            <div class="position-relative">
                <span class=" position-absolute font-18 font-weight-bold mt-2 ml-2">&#8358;</span>
                <input type="text" class="form-control pl-4" wire:model="buyer_rate" value="{{$crypto->buyer_rate}}" {{$editing == false ?'readonly' : ''}}>
            </div>
            @error('buyer_rate')
                <span class="text-danger font-16">{{$message}}</span>
            @enderror
        </div>
    
        <div class="form-group">
            <label>Seller Rate</label>
            <div class="position-relative">
                <span class=" position-absolute font-18 font-weight-bold mt-2 ml-2">&#8358;</span>
                <input type="text" class="form-control pl-4" wire:model="seller_rate" value="{{$crypto->seller_rate}}" {{$editing == false ?'readonly' : ''}}>
            </div>
            @error('seller_rate')
                <span class="text-danger font-16">{{$message}}</span>
            @enderror
        </div>
    
            @if (session('success'))
                <div class="mt-2 mb-2">
                    <div class="text-center"><span class="font-18 text-success">{{session('success')}}</span></div>
                </div>
            @endif

        <div class="mt-4">
            @if ($editing == true)
                <button type="submit" class="btn mb-3 btn-sm btn-block btn-outline-info">
                    Update Rate
                </button>
        
                <a href="#" wire:click="stop_editing" class="btn btn-sm btn-block btn-outline-danger">
                    Cancle Edit
                </a>
            @else
                <a href="#" wire:click="start_editing" class="btn mb-3 btn-sm btn-block btn-outline-dark">
                    Edit Rate
                </a>
            @endif
        </div>
    </div>
    
</form>