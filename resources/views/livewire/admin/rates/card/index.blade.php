<div class="row clearfix mb-1">
    <div class="col-md-6">
        <h3 class="ml-3">Cards</h3>
    </div>

     <div class="col-md-4 offset-md-2 mt-2 mt-md-0">
        <div class="form-group position-relative">
            <input type="text" wire:model="search_name" class="form-control" placeholder="Search GiftCard">
            @if ($search_name != null && $search_cards->count() > 0)
                <div class="mt-1 form-control position-absolute box-shadow pb-4" style="z-index: 100; height: 16rem;  overflow-y: hidden;">
                    <p class="mt-1 mb-2">Search Results</p>
                    <div class="my-1 px-1 pb-4" style="height: 100%;  overflow-y: scroll;">
                        <ul class="list-group list-group-flush">
                            @foreach ($search_cards as $card)
                                <li class="list-group-item"><a class="font-16" href="#" data-toggle="modal" data-target="#card-modal{{$card->id}}">{{$card->name}}</a></li>     
                            @endforeach
                        </ul>
                    </div>
                </div>
            @endif

            @if ($search_name != null)
                <div class="mt-1 form-control position-absolute box-shadow py-1" style="z-index: 100;">
                    <p class="my-2 text-center">Search Results</p>
                </div>
            @endif
        </div>
    </div>
</div>
<div class="row clearfix">

    @if ($cards->count() > 0)
        @foreach ($cards as $card)
        @php
            $card_id = encrypt($card->id);
        @endphp
            <div class="col-md-3 mt-3">
                <a href="#" data-toggle="modal" data-target="#card-modal{{$card->id}}">
                    <div class="card card-box">
                        <div class="card-body">
                            <div class="card-img">
                                <img src="{{Storage::url($card->image)}}" alt="">
                            </div>
                            <div class="card-name mt-3 ">
                                <h4 class="text-center font-16">{{$card->name}}</h4>
                            </div>
                        </div>
                    </div>
                </a>
            </div>

            <div class="modal fade" id="card-modal{{$card->id}}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-md modal-dialog-centered">
                    <div class="modal-content">
                        
                        <div class="modal-body">
                            
                            <div class="mb-4">
                                <h4 class="mb-2 text-primary font-16"> Card Info </h4>
                                <ul class="list-group">
                                    
                                    <li class="list-group-item d-flex justify-content-between align-items-center">
                                        Card Rates
                                        <span class="badge badge-primary badge-pill">{{$card->country_cards->count()}}</span>
                                    </li>
                                    <li class="list-group-item d-flex justify-content-between align-items-center">
                                        Card Trades
                                        <span class="badge badge-primary badge-pill">{{$card->trades->count()}}</span>
                                    </li>
                                    <li class="list-group-item d-flex justify-content-between align-items-center">
                                        Card Types
                                        <span class="badge badge-primary badge-pill">{{$card->types->count()}}</span>
                                    </li>
                                </ul>

                                <div class="mt-2">
                                    <a href="{{route('admin.cards.get-rates', $card_id)}}" class="btn btn-block btn-info btn-sm">view rates</a>
                                    <a href="{{route('admin.cards.get-trades', $card_id)}}" class="btn btn-block btn-outline-info btn-sm">view trades</a>
                                </div>

                                <div class="mt-2">
                                    <a href="{{route('admin.card.add-rate', $card_id)}}" class="btn btn-primary btn-block btn-sm">add rate</a>
                                    {{--@if (auth()->user()->role->name = 'admin')
                                        <a href="{{route('admin.card.delete', $card_id)}}" class="btn btn-outline-danger btn-block btn-sm">delete card</a>
                                    @endif--}}
                                </div>
                            </div>

                            <button type="button" class="btn btn-block btn-sm btn-outline-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
        

    @else
        <div class="col mt-3">
            <div class="card card-box">
                <div class="card-body text-center">
                    <div class="mb-2 mt-4">
                            <img src="/images/bg/card.svg" style="width:36rem;" alt="">
                    </div>
                    <h3 class="font-18 font-weight-normal mt-3">No GiftCard Available</h3>
                </div>
            </div>
        </div>
    @endif
</div>