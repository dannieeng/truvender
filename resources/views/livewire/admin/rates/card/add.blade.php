<form wire:submit.prevent="add">
    <div class="form-group">
        <label> Card Name </label>
        <input type="text" wire:model="name" class="form-control" placeholder="Card Name Here">
        @error('name')
            <p class="text-danger mt-2 font-16">{{$message}}</p>
        @enderror
    </div>

    <div class="form-group">
        <label> Card Image </label>
        <div class="custom-file">
            <input type="file" wire:model="image" class="custom-file-input">
            <label class="custom-file-label">Choose file</label>
        </div>
        @error('image')
             <p class="text-danger mt-2 font-16">{{$message}}</p>
        @enderror
    </div>

    <div class="mt-4 mb-2">
        <button type="submit" class="btn btn-block btn-outline-info btn-sm">Submit</button>
    </div>
</form>