<form wire:submit.prevent="submit" method="POST">

    <div class="form-group">
        <label>Card</label>
        <select name="card" wire:model="card" class="form-control">
            <option>-- Select Card --</option>
            @foreach ($cards as $card)
                <option value="{{$card->id}}" {{$card_id != null && $card_id == $card->id ? 'selected' : ''}}>{{$card->name}}</option>
            @endforeach
        </select>
        @error('card')
            <p class="text-danger font-16 mt-2">{{$message}}</p>
        @enderror
    </div>

    <div class="form-group">
        <label>Country</label>
        <select name="country" wire:model="country" class="form-control" {{$card == null ? 'disabled' : ''}}>
            <option>-- Select country --</option>
            @foreach ($countries as $country)
                <option value="{{$country->id}}">{{$country->name}}</option>
            @endforeach
        </select>
        @error('country')
            <p class="text-danger font-16 mt-2">{{$message}}</p>
        @enderror
    </div>

    <div class="form-group">
        <label>Type</label>
        <select name="type" wire:model="type" class="form-control" {{$card == null || $country == null ? 'disabled' : ''}}>
            <option>-- Select type --</option>
            @if($types != null && $types->count() > 0) 
                @foreach ($types as $type)
                    <option value="{{$type->id}}">{{$type->name}}</option>
                @endforeach
            @endif
            
        </select>

        @error('type')
            <p class="text-danger font-16 mt-2">{{$message}}</p>
        @enderror
    </div>


    <div class="form-group">
        <label>Prices</label>
        @if($price != 'add_new' || $price == null)
            <select name="type" id="price" wire:model="price" class="form-control" {{$type == null || $country == null ? 'disabled' : ''}}>
                <option>-- Select price --</option>
                @foreach ($card_prices as $price)
                    <option value="{{$price->id}}">$ {{$price->amount}}</option>
                @endforeach
                <option value="add_new">Add New</option>
            </select>
        @endif
        
        @if($price == 'add_new')
        <div class="position-relative">
            <span class="position-absolute {{$currency != null && Str::length($currency) > 1 ? 'font-16' : 'font-20'}} font-weight-bold mt-2 ml-2">{{$currency != null ? $currency : '$'}}</span>
            <input type="text" wire:model="amount" placeholder="Enter price" class="form-control {{$currency != null && Str::length($currency) > 1 ? 'pl-5' : 'pl-4'}}" value="{{old('amount')}}" name="amount">
        </div>
        @endif

        @error('price')
            <p class="text-danger font-16 mt-2">{{$message}}</p>
        @enderror
    </div>


    
        


    <div class="form-group">
        <label>Buyer Rate</label>
        <div class="position-relative">
            <span class="position-absolute font-20 font-weight-bold mt-2 ml-2">&#8358;</span>
            <input type="text" wire:model="buyer_rate" class="form-control pl-4" value="{{old('buyer_rate')}}" name="buyer_rate">
        </div>

            @error('buyer_rate')
            <p class="text-danger font-16 mt-2">{{$message}}</p>
        @enderror
    </div>

    <div class="form-group">
        <label>Seller Rate</label>
        <div class="position-relative">
            <span class="position-absolute font-20 font-weight-bold mt-2 ml-2">&#8358;</span>
            <input type="text" wire:model="seller_rate" class="form-control pl-4" value="{{old('seller_rate')}}" name="seller_rate">
        </div>

        @error('seller_rate')
            <p class="text-danger font-16 mt-2">{{$message}}</p>
        @enderror
    </div>
    
    <div class="form-group">
        <label>Default Rate</label>
        <div class="position-relative">
            <span class="position-absolute font-20 font-weight-bold mt-2 ml-2">&#8358;</span>
            <input type="text" wire:model="default_rate" class="form-control pl-4" value="{{$default_rate}}" name="seller_rate">
        </div>

        @error('default_rate')
            <p class="text-danger font-16 mt-2">{{$message}}</p>
        @enderror
    </div>


    <div class="mt-4">
        <button type="submit" class="btn btn-block btn-sm btn-outline-info">Save</button>
        <a href="{{url()->previous()}}" class="btn btn-block btn-sm btn-outline-danger">cancel</a>
    </div>
</form>