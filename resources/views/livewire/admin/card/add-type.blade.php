<form wire:submit.prevent="add">
    <div class="form-group">
        <label> Type Name </label>
        <input type="text" wire:model="name" class="form-control" placeholder="Card Name Here">
        @error('name')
            <p class="text-danger mt-2 font-16">{{$message}}</p>
        @enderror
    </div>

    <div class="form-group">
        <label> Card </label>
        <select id="card" name="card" wire:model="card" class="form-control">
            <option>-- Select Card --</option>
            @foreach ($cards as $card)
                <option value="{{$card->id}}">{{$card->name}}</option>
            @endforeach
        </select>
        @error('card')
            <p class="text-danger mt-2 font-16">{{$message}}</p>
        @enderror
    </div>

    <div class="mt-4 mb-2">
        <button type="submit" class="btn btn-block btn-outline-info btn-sm">Submit</button>
    </div>
</form>