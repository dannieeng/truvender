<div class="row">
    <div class="col-md-6">
        <form wire:submit.prevent="save">
            
            <div class="mt-4">
                
                {{--<div class="form-group">
                    <label>Card Rate/<span class="font-16 font-weight-bold">$</span></label>
                    <input type="text" class="form-control" wire:model="card_rate" {{$editing == false ? 'readonly' : ''}}>
                    @error('card_rate')
                        <p class="mt-2 text-danger font-16">{{$message}}</p>
                    @enderror
                </div>--}}
                
                
                <div class="form-group">
                    <label>Referral Commisison <span class="font-16 font-weight-bold">(&#8358;)</span></label>
                    <input type="text" class="form-control" wire:model="ref_bonus" {{$editing == false ? 'readonly' : ''}}>
                    @error('ref_bonus')
                        <p class="mt-2 text-danger font-16">{{$message}}</p>
                    @enderror
                </div>
                <div class="form-group">
                    <label>Transaction Fee  <span class="font-16 font-weight-bold">(%)</span></label>
                    <input type="text" class="form-control" wire:model="trx_fee" {{$editing == false ? 'readonly' : ''}}>
                    @error('trx_fee')
                        <p class="mt-2 text-danger font-16">{{$message}}</p>
                    @enderror
                </div>

                <div class="form-group">
                    <label>Minimum Deposit <span class="font-16 font-weight-bold">($)</span></label>
                    <input type="text" class="form-control" wire:model="min_dep_usd" {{$editing == false ? 'readonly' : ''}}>
                    @error('min_dep_usd')
                        <p class="mt-2 text-danger font-16">{{$message}}</p>
                    @enderror
                </div>

                <div class="form-group">
                    <label>Minimum Deposit <span class="font-16 font-weight-bold">(&#8358;)</span></label>
                    <input type="text" class="form-control" wire:model="min_dep_ngn" {{$editing == false ? 'readonly' : ''}}>
                    @error('min_dep_ngn')
                        <p class="mt-2 text-danger font-16">{{$message}}</p>
                    @enderror
                </div>

                <div class="form-group">
                    <label>Max transaction <span class="font-16 font-weight-bold">(unverified user)</span></label>
                    <input type="text" class="form-control" wire:model="max_trx" {{$editing == false ? 'readonly' : ''}}>
                    @error('max_trx')
                        <p class="mt-2 text-danger font-16">{{$message}}</p>
                    @enderror
                </div>

                <div class="mt-4">
                   @if ($editing == false)
                        <a href="#" wire:click.prevent="start_editing" class="btn btn-block btn-outline-dark btn-sm">Edit</a>
                   @else
                        <p class="{{session('msg') ? '' : d-none}} text-success my-2">{{session('msg')}}</p>
                        <button type="submit" class="btn btn-block btn-outline-info btn-sm">Save</button>
                        <button href="#" wire:click.prevent="stop_editing" class="btn btn-block btn-outline-danger btn-sm">cancel</button>
                   @endif
                </div>
            </div>
        </form>
    </div>
</div>
