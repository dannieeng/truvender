 <div class="row clearfix">

    {{-- Wallet Cards --}}
    <div class="col-md-6">
        <div class="card card-box">
            <div class="card-body d-relate">
                <h5 class="card-title">Naira Wallet</h5>
                <div class="back-icon bg-dark">
                    <span class=" text-success ">&#8358;</span>
                </div>
            
                <h5 class="font-weight-light mt-4">Available Balance 
                    <span class=" font-20 dw dw-question"  data-toggle="tooltip" data-placement="right" title="Balance available for use or withdraw"></span>
                </h5>
                <h1 class="mt-2 mb-2 font-weight-bold"><span class="">&#8358;</span>{{number_format($wallet->ngn_balance, 2)}}</h1>
                <div class="mt-4 d-flex justify-content-start">
                    <a href="#ngn_info" onclick="walletinfo('ngn');" class="btn btn-sm btn-rounded btn-outline-info">View More</a>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6 mt-4 mt-md-0">
        <div class="card card-box">
            <div class="card-body d-relate">
                <h5 class="card-title">Bitcoin Wallet</h5>
                <div class="back-icon bg-success">
                    <i class="fa fa-bitcoin text-warning" aria-hidden="true"></i>
                </div>
                <h5 class="font-weight-light mt-4">Available Balance </h5>
                <h2 class="mt-2 mb-2 font-weight-bold">{{$wallet->btc_balance}}<small>BTC</small></h2>
                <h5 class=" font-weight-normal">0.00USD</h5>
                <div class="mt-3 d-flex justify-content-start">
                    <a href="#btc_info" onclick="walletinfo('btc');" class="btn btn-sm btn-rounded btn-outline-info">View More</a>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="mb-2">

    <div class="row clearfix d-none" id="ngn_info">
        <div class="col-12 mt-3">
            <div class="card card-box">
                <div class="card-body d-relate">
                    <h5 class="card-title">{{__('Naira')}} Wallet Info</h5>
                    <div class="mt-2">
                        <div class="d-flex justify-content-end border-bottom pt-2 pb-2">
                            <a href="#" class="btn btn-outline-info mr-1" data-toggle="modal" data-target="#small-modal">Fund Wallet</a>
                            <a href="{{route('wallet.ngn.transfer')}}" data-toggle="modal" data-target="#transfer-modal" class="btn btn-info">Transafer Fund</a>
                        </div>
                    </div>
    
                    <div class="mt-2">
                            @if ($ngn_transactions && $ngn_transactions->count() > 0)
                            <div class="table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th scope="col">Transaction ID</th>
                                            <th scope="col">Transaction Amount</th>
                                            <th scope="col">Transaction Type</th>
                                            <th scope="col">Transaction Status</th>
                                            <th scope="col">Date</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($ngn_transactions as $transaction)
                                            <tr>
                                                <th scope="row">{{$transaction->trxn_ref}}</th>
                                                <td>{{$transaction->amount}}</td>
                                                <td>{{$transaction->type}}</td>
                                                <td>
                                                    @if ($transaction->status == 'success')
                                                        <span class="text-success font-weight-bold">Success</span>
                                                    @elseif($transaction->status == 'failed')
                                                        <span class="text-danger font-weight-bold">failed</span>
                                                    @else
                                                        <span class="text-warning font-weight-bold">pending</span>
                                                    @endif
                                                </td>
                                                <td>{{now()->parse($transaction->created_at)->diffForHumans()}}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        @else
                            <div class="p-30 d-flex justify-content-center">
                                <div class="mb-4">
                                    <img src="/images/bg/box.svg" style="height: 20rem; width:20rem;" alt="">
                                    <h5 class="font-weight-normal text-center text-primary">No Transactions</h5>
                                    <p class="text-center">Transactions will be here: <a href="" class="text-primary">start transaction</a></p>
                                </div>
                            </div>
                        @endif
                    </div>
    
                </div>
            </div>
        </div>

        <!-- Naira Modal -->    
        <div class="modal fade" id="small-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm modal-dialog-centered">
                <div class="modal-content">
                   
                    <div class="modal-body">
                        <h4 class="font-18">Fund Wallet</h4>
                        <div class="mt-2">
                            <form action="{{route('wallet.ngn.continue-fund')}}" method="POST">

                                @csrf

                                <div class="form-group">
                                <label>How Much Did you want to add to wallet</label>
                                <div class="relative">
                                    <input type="text" class="form-control pl-4" name="amount" placeholder="Amount">
                                    <span style="position: absolute; margin-top: -34px; margin-left: 6px;">&#8358;</span>
                                </div>
                                @error('amount')
                                    <span class="text-danger">{{$message}}</span>
                                @enderror
                            </div>

                            <button type="submit" class="btn-block btn-info btn-sm">Continue</button>
                           </form>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="transfer-modal" tabindex="-1" role="dialog" aria-labelledby="transfer-modal" aria-hidden="true">
        <div class="modal-dialog modal-sm modal-dialog-centered">
            <div class="modal-content">
               
                <div class="modal-body">
                  <div class="d-flex mb-3 justify-content-between align-items-center">
                      <h4 class="font-16">Transfer Funds To</h4>
                      <a href="#" class="text-danger font-weight-normal" data-dismiss="modal"><i class="ion-close-round font-16"></i></a>
                  </div>
                  <div class="mt-3 mb-4">
                      <div class="alert alert-info text-center">
                          <h5 class="font-14 font-weight-bold">Notice!</h5>
                          <p class="font-14 text-left">
                            We advise all users to make withdrawal through B2B withdraw. Payments are safe and secure, as for other methods
                            please not that we will not be responsible for any queries regarding <span class="text-danger font-weight-bold">banks and cbn law enforcement</span>
                          </p>
                      </div>
                  </div>

                  <div class="mt-2">
                      @php
                          $truvender = Crypt::encrypt('truvender');
                          $my_bank = Crypt::encrypt('my_account');
                          $other_bank = Crypt::encrypt('other_account');
                      @endphp
                      <a href="{{route('transfer.peer-2-peer') }}" class="btn btn-block btn-sm btn-outline-secondary mt-2">B2B Withdraw</a>
                      <a href="{{route('wallet.ngn.transfer', $truvender) }}" class="btn btn-block btn-sm btn-outline-secondary mt-2">User on Truvender</a>
                      <a href="{{route('wallet.ngn.transfer', $my_bank) }}" class="btn btn-block btn-sm btn-outline-secondary mt-2">My Bank Account</a>
                      <a href="{{route('wallet.ngn.transfer', $other_bank) }}" class="btn btn-block btn-sm btn-outline-secondary mt-2">Other Bank Account</a>
                  </div>
                </div>
                
            </div>
        </div>
    </div>
    
    
    
    <div class="row clearfix d-none" id="btc_info">
        <div class="col-12 mt-3">
            <div class="card card-box">
                <div class="card-body d-relate">
                    <h5 class="card-title">{{__('Btc')}} Wallet Info</h5>
                    <div class="mt-2">
                        <div class="d-flex flex-column flex-lg-row border-bottom pt-2 pb-2">
                            <div class="d-flex justify-content-end">
                                <a href="#" class="btn btn-outline-info mr-1" data-toggle="modal" data-target="#btc_modal">Send/Recieve BTC</a>
                                
                            </div>

                            <div class="d-flex justify-content-end pt-md-0 pt-2">
                                <a href="{{route('btc.sell')}}" class="btn btn-outline-success mr-1">Sell BTC</a>
                                <a href="{{route('btc.buy')}}" class="btn btn-success mr-1">Buy BTC</a>
                            </div>
                        </div>
                    </div>
    
                    <div class="mt-2">
                            @if ($btc_transactions == true && $btc_transactions->count() > 0)
                            <div class="table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th scope="col">Transaction ID</th>
                                            <th scope="col">Transaction Amount</th>
                                            <th scope="col">Transaction Type</th>
                                            <th scope="col">Transaction Status</th>
                                            <th scope="col">Date</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($btc_transactions as $transaction)
                                            <tr>
                                                <th scope="row">{{$transaction->trxn_ref}}</th>
                                                <td>{{$transaction->amount}}</td>
                                                <td>{{$transaction->type}}</td>
                                                <td>
                                                    @if ($transaction->status == 'success')
                                                        <span class="text-success font-weight-bold">Success</span>
                                                    @elseif($transaction->status == 'failed')
                                                        <span class="text-danger font-weight-bold">failed</span>
                                                    @else
                                                        <span class="text-warning font-weight-bold">pending</span>
                                                    @endif
                                                </td>
                                                <td>{{now()->subDays(4)->diffForHumans()}}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        @else
                            <div class="p-30 d-flex justify-content-center">
                                <div class="mb-4">
                                    <img src="/images/bg/box.svg" style="height: 20rem; width:20rem;" alt="">
                                    <h5 class="font-weight-normal text-center text-primary">No Transactions</h5>
                                    <p class="text-center">Transactions will be here: <a href="" class="text-primary">start transaction</a></p>
                                </div>
                            </div>
                        @endif
                    </div>
    
                </div>
            </div>
        </div>

        <!-- BTC modal -->
        <div class="modal fade" id="btc_modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                   
                    <div class="modal-body">
                       <div class="tab">
                            <ul class="nav nav-tabs customtab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" data-toggle="tab" href="#send" role="tab" aria-selected="false">Send</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#recieve" role="tab" aria-selected="true">Recieve</a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                
                                <div class="tab-pane fade show active" id="send" role="tabpanel">
                                    @livewire('users.transaction.btc.send')
                                </div>

                                <div class="tab-pane fade" id="recieve" role="tabpanel">
                                    <div class="row clearfix mt-4 mb-4">
                                        <div class="col">
                                            <div class="text-center d-none d-lg-block">
                                                <label> Scan the QR code</label>
                                                {{ QrCode::size(400)->generate(auth()->user()->wallet->btc_address)}}   
                                            </div>
                                            <div class="text-center d-lg-none">
                                                <label> Scan the QR code</label>
                                                {{ QrCode::size(300)->generate(auth()->user()->wallet->btc_address)}}   
                                            </div>
                                        </div>
                                        <div class="form-group form-inline row mt-3" style="margin: 0 auto;">
                                            <input type="text" id="address" value="{{auth()->user()->wallet->btc_address}}" readonly class="form-control">
                                            <button class="btn btn-info form-control" onclick="event.preventDefault(); copy_address();">Copy Address</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button type="button" class="btn btn-outline-danger btn-sm btn-block" data-dismiss="modal">Close</button>
                    </div>
                   
                </div>
            </div>
        </div>
    </div>
</div>


<script>

function walletinfo(type){
    var btc_info = document.getElementById('btc_info');
    var ngn_info = document.getElementById('ngn_info');

    if(btc_info.classList.contains('d-none') && type == 'btc'){
        if(!ngn_info.classList.contains('d-none')){
            ngn_info.classList.add('d-none');
        }
        btc_info.classList.remove('d-none');
    }

    if(ngn_info.classList.contains('d-none') && type == 'ngn'){
         if(!btc_info.classList.contains('d-none')){
            btc_info.classList.add('d-none');
        }
        ngn_info.classList.remove('d-none');
    }
}

function copy_address(value){
    var address = document.getElementById('address');
    address.select();
    document.execCommand("copy");
}
</script>




