<div class="row clearfix">
    <div class="col">
        @if (!Route::is('btc.index'))
            <div class="mb-4 d-flex justify-content-end">
                <a class="font-16 font-weight-bold text-primary text-underline" href="{{route('btc.buy')}}" style="text-decoration:underline;">buy btc</a>
            </div>
        @endif
        <div class="mt-2 mb-2 text-center">
            <span class="font-20">Current Rate:</span> <span class="font-weight-bold font-20 ml-3">&#8358;{{$currency->seller_rate}}/$</span>
        </div>
        <form wire:submit.prevent="continue" class="mt-4 mb-4">

            <div class="form-group">
                <label>Sell From</label>
                <div class="form-control">
                    <div class="d-flex justify-content-between align-items-center pb-2">
                        <span class="">
                            <span class="currency btc">&#8383;</span>
                            <span class="font-weight-bold">Btc Wallet</span>
                        </span>
                        
                        <span class="font-weight-normal">{{auth()->user()->wallet->btc_balance . 'BTC'}}</span>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label>Deposit To</label>
                <div class="form-control">
                    <div class="d-flex justify-content-between align-items-center pb-2">
                        <span class="">
                            <span class="currency ngn">&#8358;</span>
                            <span class="font-weight-bold">Ngn Wallet</span>
                        </span>
                        
                        <span class="font-weight-normal">&#8358;{{number_format(auth()->user()->wallet->ngn_balance, 2)}} </span>
                    </div>
                </div>
            </div>


             <div class="form-group row mt-4">
               <div class="col-md-5">
                    <label>Amount <span class="font-18 font-weight-bold">$</span></label>
                    <input type="text" name="" id="" wire:model="usd_amount" value="{{isset($amount_usd) ? $amount_usd : ''}}" class="form-control">
                </div> 
                <div class="col-md-2 d-none d-md-block">
                    <i class="icon-copy ion-arrow-swap"></i>
                </div> 
                <div class="col-md-5">
                    <label>Amount <span class="font-18 font-weight-bold">&#8383;</span></label>
                    <input type="text" name="" id="" wire:model="btc_amount" {{isset($amount_btc) ? $amount_btc : ''}} readonly class="form-control">
                </div>
            </div>

            
                <div class="form-group">
                <label>Rate <span class="font-weight-bold">ngn (&#8358;)</span></label>
                <input type="text" class="form-control" readonly value="&#8358; {{number_format($rate,2)}}" name="rate">
            </div>

            <div class="mt-2 mb-2{{$er_msg == null ? 'd-none' : ''}}">
                <p class="text-danger">{{$er_msg}}</p>
            </div>
            <button type="submit" class="btn btn-block btn-sm btn-info" {{$er_msg != null ? 'disabled' : ($usd_amount == null ? 'disabled' : '')}}>
                <span>Continue <i class="ion-ios-arrow-thin-right"></i></span>
            </button>
            <a href="{{url()->previous()}}" class="btn btn-block btn-sm btn-outline-dark">
                <span><i class="ion-ios-arrow-thin-left"></i> Back </span>
            </a>
        </form>
    </div>
</div>
