<div>
    <div class="mt-2 mb-2">
        @if (count($errors) > 0)
            <ul class="text-danger">
                @foreach ($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
            </ul>
        @endif
    </div>
    <form wire:submit.prevent="continue" class="pt-2 pb-2">
        <div class="form-group">
            <label>Select Data Plan</label>
            <select class="form-control" wire:model="data_plan" data-style="btn-outline-primary" data-size="5">
                    <option value="">-- Select Data Plan --</option>
                    @foreach ($billers as $biller)
                            <option value="{{$biller->id}}">{{$biller->short_name}}</option> 
                    @endforeach
            </select>
        </div>

        <div class="form-group">
            <label>Amount</label>
            <div class="mt-1 position-relative">
                <div class="mt-2 ml-2 align-items-center justify-content-start d-flex position-absolute">
                    <span class="font-18 font-weight-bold text-success">&#8358;</span>
                </div>
                <input type="text" class="form-control" wire:model="amount" {{$data_plan == null ? 'disabled' : ''}} style="padding-left: 28px;" name="amount" >
            </div>
        </div>

        <div class="form-group">
            <label>Mobile Number</label>
            <div class="mt-1 position-relative">
                <div class="mt-1 ml-2 align-items-center justify-content-start d-flex position-absolute">
                    <span class="font-24 text-primary">&#9742;</span>
                </div>
                <input type="text" class="form-control" wire:model="mobile_number" {{$amount == null ? 'disabled' : ''}} style="padding-left: 36px;" name="mobile_number" >
            </div>
            
        </div>

        

        <div class="form-group">
            <p class="text-center">Payment Method</p>
            <select class="form-control" wire:model="pay_opt">
                <option value="naira"><span class="font-20 mr-3">(&#8358;) </span>Naira Wallet</option> 
                <option value="bitcoin"><span class="font-20 mr-3">(&#8383;) </span>Bitcoin Wallet</option> 
            </select>

            {{-- <div class="d-flex justify-content-center align-items-center">
                <div class="text-right">
                    <span class="currency ngn">&#8358;</span><br>
                    <span class="font-weight-bold font-16 mt-1">Naira Wallet</span>
                </div>
                <div class="mr-4 ml-4">
                    <input type="checkbox" value="bitcoin" wire:model="pay_opt" class="switch-wallet" data-size="large" data-color="#f2a654" data-secondary-color="#28a745">
                </div>
                <div class="text-left">
                    <span class="currency btc">&#8383;</span><br>
                    <span class="font-weight-bold font-16 mt-1">Bitcoin Wallet</span>
                </div>
            </div> --}}
        </div>

        <div class="mt-2">
            <button type="submit" class="btn btn-info btn-block" {{($amount == null || $mobile_number == null) ? 'disabled' : ''}}>Continue</button>
        </div>
    </form>
</div>