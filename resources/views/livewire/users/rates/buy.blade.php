<div class="pd-20">
    <form wire:submit.prevent="trade">

        <div class="form-group">
            <label>Digital Asset Type</label>
            <select class="form-control" id="asset_type" wire:model="asset" name="assets">
                 <option> -- Select Asset Type -- </option>
                 <option value="crypto"> Crypto Currency </option>
                 <option value="giftcards"> Giftcards </option>
            </select>
        </div>

        <div class="form-group">
            <label>Digital Assets</label>
            <select class="form-control" id="asset" wire:model="asset" name="assets">
                 <option> -- Select Asset -- </option>
                @foreach ($cards as $card)
                    <option value="{{$card->id}}">{{$card->name}}</option>
                @endforeach
            </select>
        </div>

        <div class="form-group {{ $asset_type == 'crypto' ? 'd-none' : ''}}">
            <label>Country</label>
            <select class="form-control" id="country" wire:model="country" >
                <option>-- Select Card Country --</option>
                @foreach ($countries as $country)
                    <option value="{{$country->id}}">{{$country->name}}</option>
                @endforeach
            </select>
        </div>

        <div class="form-group {{ $asset_type == 'crypto' ? 'd-none' : ''}}">
            <label>Card Type</label>
            <select class="form-control" wire:model="type" id="type">
                <option>-- Select Card Type --</option>
                @foreach($card_types as $type)
                <option value="{{$type->id}}">{{$type->name}}</option>
                @endforeach
            </select>
        </div>

        <div class="form-group">
            <label>Amount <strong>$</strong></label>
            <input class="form-control" wire:model="amount" type="text" placeholder="">
        </div>
        <div class="form-group row">
            <label class="col-sm-12 col-md-2 col-form-label">Calculated Rate</label>
            <div class="col-sm-12 col-md-10 position-relative">
                <span class="position-absolute font-20 font-weight-bold mt-2 ml-2">&#x20a6;</span>
                <input class="form-control pl-4" type="text" readonly value="{{$calc_amount != null ?  number_format($calc_amount,2) : ''}}">
            </div>
        </div>
         @if (session('msg'))
            <p class="mt-1 mb-1 text-danger">{{session('msg')}} <a href="{{route('wallet').'/#ngn'}}" class="text-info">click to fund wallet</a></p> 
        @endif
        <div class="d-flex justify-content-between">
            @if ($calc_amount > 0 )
                <button type="submit" {{session('msg') ? 'disabled' : ''}} class="btn btn-info btn-sm"> Trade Now</button>
               
            @endif
        </div>
    </form>
</div>


