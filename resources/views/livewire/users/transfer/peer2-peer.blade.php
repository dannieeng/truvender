<div>
    <h4 class="font-20 mb-4">
        Bank To Bank Withdraw
    </h4>
   <form wire:submit.prevent="continue">
       @if ($add_account == true)
           <div class="form-group">
                <label>Bank</label>
                <select class="form-control" wire:model="bank">
                    <option>-- Select Bank --</option>
                    @foreach ($banks as $bank)
                            <option value="{{$bank->id}}">{{$bank->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label>Account Number</label>
                <input type="text" class="form-control" wire:model.lazy="account_number">
                @error('account_number')
                        <span class="text-danger font-16">{{$message}}</span>
                @enderror

                <a href="#" wire:click.prevent="user_my_account" class="badge mt-1 badge-primary float-right px-1 py-1 rounded" style="font-size: 10px;"> Use Default Account</a>
            </div>

            <div class="form-group">
                <label>Account Name</label>
                    <input type="text" class="form-control" {{'disabled'}} value="{{$account_name != null ? $account_name : 'loading........'}}" wire:model="account_name">
                @error('account_name')
                        <span class="text-danger font-16">{{$message}}</span>
                @enderror
            </div>
        @else
            <div class="form-group">
                <label>Account Number</label>
                <input type="text" class="form-control" {{'disabled'}} value="{{$account->acc_number}}" >
                <a href="#" wire:click.prevent="add" class="badge mt-1 badge-secondary float-right px-1 py-1 rounded" style="font-size: 10px;"> Use Different Account</a>
            </div>
            
        @endif

       <div class="form-group">
           <label>Amount</label>
           <div class="position-relative mb-1">
               <span class="position-absolute font-weight-bold font-18 mt-2 ml-2">&#8358;</span>
               <input type="text" class="form-control pl-4" wire:model="amount">
            </div>
            @error('amount')
                <span class="text-danger font-16">{{$message}}</span>
           @enderror
       </div>

    
       <div class="mt-4">
            <button type="submit" class="btn btn-sm btn-block btn-outline-info" {{$amount != null ? '' : 'disabled'}}>Continue</button>
            <a href="{{url()->previous()}}" class="btn btn-sm btn-block btn-outline-danger">Cancel</a>
       </div>
   </form>
</div>
