<div>
    <h4 class="font-20 mb-4">
        Transfer to A Truvender
    </h4>
   <form wire:submit.prevent="continue">
       <div class="form-group">
           <label>Truvender Username</label>
           <input type="text" class="form-control" wire:model="username">
           @error('username')
                <span class="text-danger font-16">{{$message}}</span>
           @enderror
       </div>

       <div class="form-group">
           <label>Amount</label>
           <div class="position-relative mb-1">
               <span class="position-absolute font-weight-bold font-18 mt-2 ml-2">&#8358;</span>
               <input type="text" class="form-control pl-4" wire:model="amount">
            </div>
            @error('amount')
                <span class="text-danger font-16">{{$message}}</span>
           @enderror
       </div>
    
       <div class="mt-4">
            <button type="submit" class="btn btn-sm btn-block btn-outline-info">Continue</button>
             <a href="{{url()->previous()}}" class="btn btn-sm btn-block btn-outline-danger">Cancel</a>
       </div>
   </form>
</div>
