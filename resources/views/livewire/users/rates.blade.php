 <div class="col-md-8">
    <div class="tab">
        <ul class="nav nav-tabs customtab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#buy" role="tab" aria-selected="true">Buyer Rate</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#sell" role="tab" aria-selected="false">Seller Rate</a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane fade show active" id="buy" role="tabpanel">
                <div class="pd-20">
                    <form action="">
                        <div class="form-group">
                            <label>Digital Assets</label>
                            <select class="selectpicker form-control" name="assets" data-style="btn-outline-dark" data-size="5">
                                @foreach ($cards as $card)
                                    <option value="{{$card->id}}">{{$card->name}}</option>
                                @endforeach

                                <option value="{{'btc'}}">{{'Bitcoin'}}</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Country</label>
                            <select class="selectpicker form-control" data-style="btn-outline-dark" data-size="5">
                                @foreach ($countries as $country)
                                    <option>{{$country->name}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Card Type</label>
                            <select class="selectpicker form-control" data-style="btn-outline-dark" data-size="5">
                                <option value="new">New</option>
                                <option value="discounted">Discounted</option>
                                
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Amount</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                    </form>
                </div>
            </div>
            <div class="tab-pane fade" id="sell" role="tabpanel">
                <div class="pd-20">
                    <form action="">
                        <div class="form-group">
                            <label>Digital Assets</label>
                            <select class="selectpicker form-control" name="assets" data-style="btn-outline-dark" data-size="5">
                                @foreach ($cards as $card)
                                    <option value="{{$card->id}}">{{$card->name}}</option>
                                @endforeach

                                <option value="{{'btc'}}">{{'Bitcoin'}}</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Country</label>
                            <select class="selectpicker form-control" data-style="btn-outline-dark" data-size="5">
                                @foreach ($countries as $country)
                                    <option>{{$country->name}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Card Type</label>
                            <select class="selectpicker form-control" data-style="btn-outline-dark" data-size="5">
                                <option value="new">New</option>
                                <option value="discounted">Discounted</option>
                                
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Amount</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                    </form>
                </div>
            </div>
        
        </div>
    </div>
</div>