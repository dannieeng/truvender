<form wire:submit.prevent="continue" >
{{-- <form method="POST" action="{{route('trade.gift-card.submit')}}" enctype="multipart/form-data"> --}}
    @csrf
  

    <div class="form-group">
        <label>Email</label>
        <input type="email" class="form-control" wire:model="email" placeholder="example@dmoain.com"> 

        @error('email')
            <span class="font-14 text-danger">{{$message}}</span>
        @enderror
    </div>
    

    <div class="form-group">
        <label>Phone</label>
        <input type="text" class="form-control" wire:model="phone" placeholder="eg: (+234) 913-167-6346"> 

        @error('phone')
            <span class="font-14 text-danger">{{$message}}</span>
        @enderror
    </div>

    <div class="form-group row">
        <div class="col-9">
            <label>Amount </label>
            <input type="text" name="amount" wire:model="amount" class="form-control" placeholder="0.00">
        </div>

        <div class="col-3">
            <label>Currency </label>
            @if ($asset_currency != null)
                <input type="text" name="currency" value="{{$asset_currency}}" readonly class="form-control text-center" placeholder=""> 
                <input type="hidden" name="currency" wire:model="currency">
            @else
                <select name="currency" class="form-control text-center" id="currency" wire:model="currency">
                    <option value="">-- --</option>
                    <option value="$">$</option>
                    <option value="£">£</option>
                    <option value="€">€</option>
                    <option value="¥">¥</option>
                </select>
            @endif
        </div>
    </div>
    


    <div class="mt-4 text-center">
        <div class="mb-2"><p>You Get</p></div>
        <h2 class="font-24 font-weight-normal">
            NGN {{number_format($rate,2)}}
        </h2>
    </div>

    <div class="mt-4">
        @if (session()->has('success') || session()->has('err'))
            <div class="mb-2 text-center">
                <p class="{{session()->has('success') ? 'text-sucess' : 'text-danger' }}">{{session('success')}}</p>
            </div>
        @endif
        
       
       
        <button type="submit" class="btn btn-rounded btn-block btn-primary continue" {{$rate == null ? 'disabled' : ''}}>Continue</button>
    </div>

  
</form>