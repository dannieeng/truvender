<div>
    <form wire:submit.prevent="upload" enctype="multipart/form-data">
        {{-- <div>
            <input type="file"  id="imginput" onchange="readURL(this);" />
            <div class="max-width-200 bg-info" style="margin: 20px auto;">
                <img src="" id="target" alt="">
            </div>
            
        </div> --}}

            <div class="mb-3 d-flex justify-content-center flex-col">
                <div class="circle">
                    <!-- User Profile Image -->
                    <img src="{{$image!=null ? $image->temporaryUrl() : ''}}">
                </div>
            </div>
            <div class=" d-flex justify-content-center">
                <a href="#" class="upload-button btn-sm mt-2 btn btn-rounded btn-primary" >
                    <span>upload</span>
                    <input class="file-upload" type="file" id="file-upload" wire:model="image" onchange="readURL(this);" accept="image/*"/>
                </a>
            </div>
            @error('image')
                <p class="text-danger mb-2 mt-2">{{$message}}</p>
            @enderror
        
            @if (session('msg'))
                <p class="text-info font-18">{{session('msg')}}</p>
            @endif
            <div class="mt-2 d-flex justify-content-between">
                <button class="btn btn-sm btn-info" type="submit">Update Profile Picture</button>
                <button class="btn btn-sm btn-outline-danger" data-dismiss="modal">Cancel</button>
            </div>
    </form>

</div>


