<form wire:submit.prevent="bvn_add">
    <div class="form-group">
        <label> BVN </label>
        <div class="position-relative">
            <input type="password" id="bvn" wire:model="bvn" class="form-control" name="bvn">
            <button class="btn position-absolute" onclick="event.preventDefault(); viewbvn();" role="button" style="position: absolute; right: 0; top: 0;">
                    <i class="fa fa-eye fa-x" aria-hidden="true" id="eye-icon"></i>
            </button>
        </div>

        @error('bvn')
            <span class="text-danger font-16 mt-1">{{$message}}</span>
        @enderror
    </div>
    
    <div class="mt-2">
            <button type="submit" class="btn-info btn-block btn-sm" >Submit</button>
            <button type="button" class="btn btn-outline-danger btn-sm btn-block" data-dismiss="modal">Close</button>
         </div>

</form>