<div class="mt-3 mb-3">
    <form wire:submit.prevent="submit">
        <div class="form-group">
            <label>Select Bank</label>
            <select name="bank" wire:model.debounce.500ms="bank" class="form-control" id="">
                <option>-- Select Bank --</option>
                @foreach ($banks as $bnk)
                    <option value="{{$bnk->id}}">{{$bnk->name}}</option>
                @endforeach
            </select>
            @error('bank')
                <span class="text-danger font-16 mt-1">{{$message}}</span>
            @enderror
        </div>

        <div class="form-group">
            <label> Account Number </label>
            <input type="text" class="form-control" wire:model.lazy="account_number" {{$bank == null ? 'disbaled' : ''}} name="account_number">
             @error('account_number')
                <span class="text-danger font-16 mt-1">{{$message}}</span>
            @enderror
        </div>

        <div class="form-group">
            <label> Account Name </label>
            <input type="text" class="form-control" wire:model="account_name" value="{{$account_name != null ? $account_name : ''}}" disabled name="account_name">
             @error('account_name')
                <span class="text-danger font-16 mt-1">{{$message}}</span>
            @enderror
        </div>
        <div class="mt-2">
            <button type="submit" class="btn-info btn-block btn-sm" {{$account_name == null ? 'disabled' : ''}}>{{$account_number != null && $account_name != null ? 'Submit' : 'Loading......'}}</button>
            <button type="button" class="btn btn-outline-danger btn-sm btn-block" data-dismiss="modal">Close</button>
         </div>
    </form>
</div>