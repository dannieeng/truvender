<form wire:submit.prevent="edit">
    <div class="mt-2">
            <div class="form-group">
                <label>First Name</label>
                <input type="text" wire:model="firstname" value="{{$profile->firstname}}" class="form-control">

                @error('firstname')
                    <span class="text-danger font-14">{{$message}}</span>
                @enderror
            </div>

            <div class="form-group">
                <label>Last Name</label>
                <input type="text" wire:model="lastname" value="{{$profile->lastname}}" class="form-control">
                @error('lastname')
                    <span class="text-danger font-14">{{$message}}</span>
                @enderror
            </div>

            <div class="form-group">
                <label>Phone</label>
                <input type="text" wire:model="phone" value="{{$profile->phone}}" class="form-control">
                @error('phone')
                    <span class="text-danger font-14">{{$message}}</span>
                @enderror
            </div>
        </div>

        <p class="text-info font-16">{{ session('msg') ? session('msg') :''}}</p>
        <button type="submit" class="btn btn-sm btn-block btn-outline-info">Update Profile</button>
        <button class="btn btn-sm btn-block btn-outline-danger" data-dismiss="modal">Close</button>
       
</form>