<div class="profile-setting">
    <div class="profile-timeline">
        <div class="timeline-month">
            <h5>Notification Settings</h5>
        </div>
        
        {{-- <form wire:submit="changeValue">
            
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">Field</th>
                        <th scope="col">Push Notification</th>
                        <th scope="col">Email Notification</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th scope="row">Buy/Sell BTC</th>
                        <th scope="row">

                            <input type="checkbox" class="switch-btn" data-color="#0059b2">
                        </th>
                        <th scope="row">
                            <input type="checkbox" class="switch-btn" data-color="#0059b2">
                        </th>
                    </tr>
        
                    <tr>
                        <th scope="row">Buy Airtime/Data</th>
                        <th scope="row">
                            <input type="checkbox" class="switch-btn" data-color="#0059b2">
                        </th>
                        <th scope="row">
                            <input type="checkbox" class="switch-btn" data-color="#0059b2">
                        </th>
                    </tr>
        
                    <tr>
                        <th scope="row">Buy/Sell GiftCards</th>
                        <th scope="row">
                            <input type="checkbox" class="switch-btn" data-color="#0059b2">
                        </th>
                        <th scope="row">
                            <input type="checkbox" class="switch-btn" data-color="#0059b2">
                        </th>
                    </tr>
        
                </tbody>
            </table>
        </form> --}}
        

        <form wire:submit.prevent="changeValue">

                @if($editting == true)
                    <div class="form-group row">
                        <label class="col-md-2 col-sm-12 col-form-label">Prefer Notification</label>
                        <select wire:model="notify_with" class="form-control col-md-8 col-sm-12" id="">
                            <option> Select Prefered Notification System</option>
                            <option value="push">Push Notification</option>
                            <option value="email">Email</option>
                            <option value="both">Both</option>
                        </select>
                    </div>


                    <div class="form-group row">
                        <label class="col-md-2 col-sm-12 col-form-label">Default Currency</label>
                        <select wire:model="default_currency" class="form-control col-md-8 col-sm-12" id="">
                            <option> Select Currency</option>
                            <option value="naira">Naira (₦)</option>
                            <option value="sedis">Ghana cedis (GH₵)</option>
                        </select>
                    </div>

                @else
                 <div class="form-group row">
                    <label class="col-md-2 col-sm-12 col-form-label">Prefer Notification</label>
                    <input type="text" readonly value="{{$notify == 'both' ? 'Email and Push Notify' : ($notify == 'push' ? 'Push Notify' : 'Email')}}" class="form-control col-sm-12 col-md-8">
                 </div>
                  <div class="form-group row">
                        <label class="col-md-2 col-sm-12 col-form-label">Default Currency</label>
                        <input type="text" readonly value="{{$currency}}" class="form-control col-sm-12 col-md-8">
                  </div>
                @endif

           
            <div class="my-3">
                <p class="text-success"> {{session('msg')}}</p>
            </div>
            <button class="btn btn-sm btn-outline-danger {{$editting == true ? 'd-none' : 'd-block'}}" wire:click.prevent="edit">Edit</button>
            <button class="btn btn-sm btn-outline-info {{$editting == false ? 'd-none' : 'd-block'}}"  wire:loading.attr="disabled">Save<span wire:loading>ing....</span></button>
        </form>
    </div>
</div>

<script>
   
</script>