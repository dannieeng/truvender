 <div class="profile-setting">
    <div class="profile-timeline">
        <div class="timeline-month">
            <h5>Account Verification</h5>
        </div>
        <div class="profile-timeline-list">
            <ul>
                @if ($user->email_verified_at != null)
                    <li>
                        <div class="date">{{now()->parse($user->email_verified_at)->format('M d')}}</div>
                        <div class="task-name"><i class="ion-checkmark-circled font-30 text-success"></i> Email Verification</div>
                        <p>Your Email address was verified. Your one-time and daily transaction limit is N{{number_format(1000, 2)}} and {{number_format($transaction_limit->value, 2)}} respectively.</p>
                    </li>
                @else
                    
                @endif
                
                @if ($banking != null && $banking->bvn != null)
                    <li>
                        <div class="date">{{now()->parse($banking->updated_at)->format('M d')}}</div>
                        <div class="task-name"><i class="ion-ios-chatboxes"></i> Level 1</div>
                        <p>Increase you one-time and daily transaction N{{number_format(500000, 2)}} and N{{number_format($transaction_limit2->value, 2)}} respectively. Update your BVN.</p>
                    </li>
                    @else
                    <button class="btn btn-sm btn-info" data-toggle="modal" data-target="#task-bvn" role="button">Add Verification</button>
                @endif
            </ul>
        </div>
        
    </div>
</div>