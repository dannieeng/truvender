<form wire:submit.prevent="continue" enctype="multipart/form-data">
{{-- <form method="POST" action="{{route('trade.gift-card.submit')}}" enctype="multipart/form-data"> --}}
    @csrf
    <div class="form-group">
        <label>Card</label>
        <input type="text" readonly value="{{$card->name}}" class="form-control">
    </div>

    <div class="form-group">
        <label>Country</label>
        <select name="country" wire:model.lazy="country" class="form-control">
            <option>-- Select Country --</option>
            @foreach ($countries as $country)
                <option value="{{$country->country_id}}">{{$country->country->name}}</option>
            @endforeach      
        </select>
    </div>

    @if ($types != null)
        <div class="form-group">
            <label>Card Type</label>
            <select name="type" wire:model.lazy="type" class="form-control" {{$country == null ? 'disabled' : ''}}>
                <option>-- Select Type --</option>
                @foreach ($types as $type)
                    <option value="{{$type->id}}">{{$type->name. ' Card'}}</option>
                @endforeach
            </select>
        </div>
    @endif


    
    <div class="form-group {{$country == null ? 'd-none' : ''}}">
        <label>Amount</label>
        <select name="price" wire:model="price" class="form-control {{$adding == true ? 'd-none' : ''}}" onchange="check_custom();" id="select_price" {{$type  == null ? 'disabled' : ''}}>
            <option>-- Select amount --</option>
            @foreach ($prices as $price)
                <option value="{{$price->id}}">{{ $currency == null ?  '$ ' . $price->amount : $currency . ' ' . $price->amount}}</option>
            @endforeach
        </select>
        
        @if($trade == 'sell')
                <div class="mt-2 {{$adding != true ? 'd-none' : ''}} form-group" id="custom">
                   <div class="position-relative">
                        <span class="position-absolute {{ $currency != null && Str::length($currency) > 1 ?  'font-16' : 'font-20'}} font-weight-bold mt-2 ml-2">{{ $currency == null ?  '$' : $currency}}</span>
                        <input type="text" name="new_price" wire:model="new_price" class="form-control pl-4">
                    </div>
                </div>
            @if($adding == false)
                <div class"text-center">
                    <a href="#" wire:click.prevent="add_price" class="text-success font-16 font-weight-bold mx-auto">add new amount</a>
                </div>
            @endif
        @endif
        
    </div>
    
    
   
    
    @php
        use App\Models\CardType;
        $large = CardType::where('card_id', $card->id)->where('name', 'Large')->first();
    @endphp
    @if($trade == 'sell' && $large == true)
        <div class="my-3 alert alert-info">
            <span class="font-weight-bold">Note: </span> Any Card Picture above 100$ should be uploaded as a large card
        </div>
    @endif

    <div class="form-group">
        <label>Card Denomination to {{$trade == 'sell' ? 'Upload' : 'recieve'}}</label>
        <input type="number" name="amount" wire:model="amount" class="form-control" placeholder="Eg 50X2=2 or 100X1=1">
    </div>
    
    @if ($trade == 'sell')
        <div class="form-group">
            <label>Card E-code</label>
            <input type="text" name="code" wire:model="code" class="form-control" placeholder="Enter E-code (optional)">
        </div>
    @endif


    <div class="mt-4 text-center">
        <div class="mb-2"><p>{{$trade == 'sell' ? 'You get' : 'Cost'}}</p></div>
        <h2 class="font-24 font-weight-normal">
            NGN {{number_format($rate,2)}}
        </h2>
    </div>

    <div class="mt-4">
        @if (session()->has('success') || session()->has('err'))
            <div class="mb-2 text-center">
                <p class="{{session()->has('success') ? 'text-sucess' : 'text-danger' }}">{{$message}}</p>
            </div>
        @endif
        
        @if ($trade == 'buy')
            <button type="submit" class="btn btn-rounded btn-block btn-primary continue" {{auth()->user()->wallet->ngn_balance < $rate ? 'disabled' : ($prices->count() < 1 || $types->count() < 1 ? 'disabled' : '') }}>Submit</button>
            @if (auth()->user()->wallet->ngn_balance < $rate)
                <p class="mt-2 text-center text-danger">
                    you don't have enough funds for this transaction,
                </p>
            @endif
        @endif
        @if ($trade == 'sell')
             <button type="submit" class="btn btn-rounded btn-block btn-primary continue" {{$prices->count() < 1 || $types->count() < 1 ? 'disabled' : '' }}>Continue</button>
            <p class="mt-2">Average Confirmation Time: {{$confirmation_time}}mins - {{$confirmation_time2}}mins</p>
        @endif
    </div>

  
</form>