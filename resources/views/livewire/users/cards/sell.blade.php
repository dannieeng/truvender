

    <div class="row clearfix mb-1">
        <div class="col-md-6">
            <h3 class="ml-3">Sell Cards</h3>
        </div>

         <div class="col-md-4 offset-md-2 mt-2 mt-md-0">
            <div class="form-group position-relative">
                <input type="text" wire:model="search_name" class="form-control" placeholder="Search GiftCard">
                @if ($search_name != null && $search_cards->count() > 0)
                    <div class="mt-1 form-control position-absolute box-shadow pb-4" style="z-index: 100; height: 16rem;  overflow-y: hidden;">
                        <p class="mt-1 mb-2">Search Results</p>
                        <div class="my-1 px-1 pb-4" style="height: 100%;  overflow-y: scroll;">
                            <ul class="list-group list-group-flush">
                                @foreach ($search_cards as $card)
                                    <li class="list-group-item"><a class="font-16" href="{{route('sell.card', encrypt($card->id))}}">{{$card->name}}</a></li>     
                                @endforeach
                            </ul>
                        </div>
                    </div>
                @endif

                @if ($search_name != null)
                    <div class="mt-1 form-control position-absolute box-shadow py-1" style="z-index: 100;">
                        <p class="my-2 text-center">Search Results</p>
                    </div>
                @endif
            </div>
        </div>
    </div>
    <div class="row clearfix">

        @if ($cards->count() > 0)
            @foreach ($cards as $card)
                <div class="col-md-3 mt-3">
                    <a href="{{route('sell.card', encrypt($card->id))}}" wire:click.prevent="sell_card({{$card->id}})">
                        <div class="card card-box">
                            <div class="card-body">
                                <div class="card-img">
                                    <img src="{{Storage::url($card->image)}}" alt="">
                                </div>
                                <div class="card-name mt-3 ">
                                    <h4 class="text-center font-16">{{$card->name}}</h4>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            @endforeach
        @else
            <div class="col mt-3">
                <div class="card card-box">
                    <div class="card-body text-center">
                        <div class="mb-2 mt-4">
                             <img src="/images/bg/card.svg" style="width:46rem;" alt="">
                        </div>
                        <h3 class="font-18 font-weight-normal mt-3">No GiftCard Available</h3>
                    </div>
                </div>
            </div>
        @endif
        
        
    </div>