<div class="dropdown">
    <a class="dropdown-toggle no-arrow" href="#" role="button" data-toggle="dropdown">
        <i class="icon-copy dw dw-notification"></i>
        @if ($user->unreadNotifications->count() > 0)
            <span class="badge notification-active"></span>
        @endif
    </a>
    <div class="dropdown-menu dropdown-menu-right">
        <h4 class="font-16">Notifications</h4>
        <div class=" pl-3 mt-4 mx-h-350 customscroll">
            <ul>
                @if ($notifications->count() > 0)
                    @foreach($notifications as $notification)
                        @if($notification->unread()->count() > 0)
                            <li>
                                <a href="#">
                                    <h4 class="font-14">{{now()->parse($unread->created_at)->diffForHumans()}}</h4>
                                    <p>{{$notification->data}}</p>
                                </a>
                            </li>
                        @endif
                    @endforeach
                @else
                    <li>
                        <hr>
                    </li>
                @endif
            </ul>
        </div>
    </div>
</div>