<h4 class="font-20 mb-4">
    Ethereum Wallet Info
</h4>

<p class="mb-1">Wallet Info</p>
<ul class="list-group py-3">
    <li class="list-group-item">
        <div class="d-flex justify-content-between flex-column align-items-center">
            <p class=" font-16 mb-2">Address:</h4>
            <p class=" font-16 mb-2 font-weight-bold">{{$address}}</p>
        </div>
    </li>

    <li class="list-group-item">
        <div class="d-flex justify-content-between align-items-center">
            <p class=" font-16 mb-2">Total Received Value:</h4>
            <p class=" font-16 mb-2 font-weight-bold">{{$total_received}}</p>
        </div>
    </li>

    <li class="list-group-item">
        <div class="d-flex justify-content-between align-items-center">
            <p class=" font-16 mb-2">Total Sent Value:</h4>
            <p class=" font-16 mb-2 font-weight-bold">{{$total_sent}}</p>
        </div>
    </li>

    <li class="list-group-item">
        <div class="d-flex justify-content-between align-items-center">
            <p class=" font-16 mb-2">Unconfirmed Balance:</h4>
            <p class=" font-16 mb-2 font-weight-bold">{{$unconfirmed_balance}}</p>
        </div>
    </li>

    <li class="list-group-item">
        <div class="d-flex justify-content-between align-items-center">
            <p class=" font-16 mb-2">Final Balance:</h4>
            <p class=" font-16 mb-2 font-weight-bold">{{$final_balance}}</p>
        </div>
    </li>

    <li class="list-group-item">
        <div class="d-flex justify-content-between align-items-center">
            <p class=" font-16 mb-2">Total Transactions:</h4>
            <p class=" font-16 mb-2 font-weight-bold">{{$transactions}}</p>
        </div>
    </li>

    <li class="list-group-item">
        <div class="d-flex justify-content-between align-items-center">
            <p class=" font-16 mb-2">Total Unconfirmed Transactions:</h4>
            <p class=" font-16 mb-2 font-weight-bold">{{$unconfirmed_transactions}}</p>
        </div>
    </li>

    <li class="list-group-item">
        <div class="d-flex justify-content-between align-items-center">
            <p class=" font-16 mb-2">Total Final Transactions:</h4>
            <p class=" font-16 mb-2 font-weight-bold">{{$final_transactions}}</p>
        </div>
    </li>

    
</ul>