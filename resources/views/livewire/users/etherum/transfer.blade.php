<div class="row clearfix">
    <div class="col">
        <form action="" wire:submit.prevent="send" class="mt-4 mb-4">
            <div class="form-group">
                <label>Sending From</label>
                <div class="form-control">
                    <div class="d-flex justify-content-between align-items-center pb-2">
                        <span class="">
                            <span class="currency"><img src="/images/products/ethereum.svg" style="height: 2rem;" alt=""></span>
                            <span class="font-weight-bold">ETH Wallet</span>
                        </span>
                        
                        <span class="font-weight-normal">{{$wallet->balance . 'ETH'}}</span>
                    </div>
                </div>
            </div>

                <div class="form-group">
                <label>Sending To</label>
                <input type="text" class="form-control" wire:model="reciever" value="{{old('reciever')}}"  name="reciever">
            </div>

            <div class="form-group">
                <label>Note</label>
                <textarea name="note" class="form-control" wire:model="note" cols="30" rows="10" {{$reciever == null ? 'readonly' : ''}}></textarea>
            </div>

            <div class="form-group row">
                
                <div class="col-md-5">
                    <label>Amount <small class="font-weight-bold">USD($)</small></label>
                    <input type="text" name="" id="" wire:model="usd_amount" {{$reciever == null ? 'readonly' : ''}} class="form-control">
                </div>
                <div class="col-md-2 d-none d-md-block">
                    <i class="icon-copy ion-arrow-swap"></i>
                </div>
                <div class="col-md-5">
                    <label>Amount <small class="font-weight-bold">ETH</small></label>
                    <input type="text" name="" id="" wire:model="eth_amount" disabled class="form-control">
                </div>
            </div>

            @if (session()->has('msg'))
                <p class="mt-2 mb-2 text-danger">{{session('msg')}} </p>
            @endif
            <button type="submit" class="btn btn-block btn-sm btn-info" {{$usd_amount == null ? 'disabled' : ''}}>
                <span>Continue <i class="ion-ios-arrow-thin-right"></i></span>
            </button>
        </form>
    </div>
</div>

