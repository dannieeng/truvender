
@extends('layouts.auth')
@section('added')
        <div class="error-page d-flex align-items-center flex-wrap justify-content-center pd-20">
            <div class="pd-10">
                <div class="error-page-wrap text-center">
                    <h1>401</h1>
                    <h3>Error: 401 Unauthorized</h3>
                    <p>Sorry you are Unauthorized to access the content of this page<br>Either check the URL</p>
                    <div class="pt-20 mx-auto max-width-200">
                        <a href="{{route('dashboard')}}" class="btn btn-primary btn-block btn-lg">Back To Home</a>
                    </div>
                </div>
            </div>
        </div>
    
@endsection