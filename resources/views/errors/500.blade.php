@extends('layouts.auth')
@section('added')
    <div class="error-page d-flex align-items-center flex-wrap justify-content-center pd-20">
		<div class="pd-10">
			<div class="error-page-wrap text-center">
				<h1>500</h1>
				<h3>Error: 500 Under Maintainance</h3>
                <p>Sorry, the page undergoing maintainance.<br>
                    Try Sometime Later
                </p>
				<div class="pt-20 mx-auto max-width-200">
					<a href="index.html" class="btn btn-primary btn-block btn-lg">Back To Home</a>
				</div>
			</div>
		</div>
	</div>
@endsection

