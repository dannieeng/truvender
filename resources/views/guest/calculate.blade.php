@extends('layouts.main')
@section('title', 'Rates And Calculator')

@section('content')

    <section class="pt-10 pb-0" style="background-color:#0B0D1B">
        <div class="bg-holder" style="background-image:url(/guest/img/bg-img/big-shape-left.png);background-size:45%;background-position:left top;"></div>
        <!--/.bg-holder-->
        <div class="bg-holder" 
          {{-- style="background-image:url(/guest/img/bg-img/big-shape-right.png);background-size:56%;background-position:right top;" --}}
        >
        </div>
        <!--/.bg-holder-->
        <div class="container">
          <div class="row justify-content-center align-items-center" 
          data-aos="fade-up" data-aos-delay="500" data-aos-duration="1000"
          >
            <div class="col-md-6 col-11" 
            data-aos="fade-up" data-aos-delay="200" data-aos-duration="1000"
            >
              <h1 class="text-capitalize text-white fs-xxl-8 fs-xl-8 fs-sm-6 fs-2">
                <span class="position-relative">Our Rates: 
                <span class="heading-shapes heading-shapes-left d-none d-sm-block" style="background-image: url(/guest/img/illustrations/heading-right-img-banner.png); height: 182px; width: 115px"></span>
                <span class="heading-shapes heading-shapes-right" style="background-image: url(/guest/img/illustrations/heading-side.png); height: 70px; width: 81px"></span>
              </span><br />The Best Every Time.</h1>
              <p class="mt-3 w-lg-75 text-500">With 
                <a class="font-weight-bold text-500" href="{{route('welcome')}}">{{config('app.name')}}</a> 
                simple and secure process you can buy and sell digital assets with USD, 
                GBP, or Euro. Order prices are locked in at the time of placement 
                and our non-custodial approach allows for complete control of your assets.
              </p>
                
            </div>
            <div class="col-md-6 mt-5 mt-lg-0" 
                data-aos="fade-up" data-aos-delay="400" data-aos-duration="1000">
              <div class="card">
              <form>  
                <div class="card-body p-4">
                  <h4 class="card-title">Simple Converter</h4>
                  <div class="row bg-white">
                    <div class="col">
                      <div class="col-12">
                        <div class="form-group">
                          <label for="btc" class=" fs-xl-2"><small>From</small> BTC</label>
                          <input class="form-control font-weight-normal fs-4" id="btc" value="0" type="text" name="btc" />
                        </div>

                        <div class="form-group">
                         <select class="custom-select mb-3 custom-select-lg">
                          <option value="2">USD</option>
                          <option value="3">NGN</option>
                        </select>
                        </div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="row bg-primary card-img-top no-gutters align-items-center">
                  <div class="row pt-4 flex-center text-center">
                    <div class="col">
                      <div class="col-12 justify-content-center">
                        <div class="form-group">
                          <input class="form-control font-weight-normal fs-4" id="result" type="text" name="result" />
                        </div>
                      </div>
                    </div>
                  </div>
              </div>
            </div> 
          </form>
        </div>
         
          
        </div>
      </section><img class="img-fluid bg-white" src="/guest/img/illustrations/cripto-bottom-shape.png" alt="" />


       <!-- ============================================-->
      <!-- <section> begin ============================-->
      <section class="py-0">
        <div class="container">
          <div class="row flex-center text-center min-vh-100">
            <div class="col-auto">
              
            </div>
          </div>
        </div><!-- end of .container-->
      </section><!-- <section> close ============================-->
      <!-- ============================================-->
    
@endsection