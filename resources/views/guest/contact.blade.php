@extends('layouts.main')
@section('title', 'Welcome')

@section('content')
    <section class="bg-extra-1 py-0" data-zanim-timeline="{&quot;delay&quot;:0}" data-zanim-trigger="scroll">
        <div class="bg-holder" style="background-image:url(/images/bg/contact.jpg);"></div>
        <!--/.bg-holder-->
        <div class="bg-holder" style="background-image:url(/guest/img/illustrations/contact-bottom-curve.png);background-position:bottom;background-size:contain;"></div>
        <!--/.bg-holder-->
        <div class="container">
          <div class="row">
            <div class="col-lg-6 col-md-7 col-10 pt-8 pb-9">
              <div class="bc-caret-right bc-light mb-6">
                <nav aria-label="breadcrumb" data-zanim-xs='{"delay":0.2,"duration":1.5}'>
                  <ol class="breadcrumb fs--1">
                    <li class="breadcrumb-item"><a class="text-white" href="index-2.html">Home</a></li>
                    <li class="breadcrumb-item"><a class="text-white" href="#">Pages</a></li>
                    <li class="breadcrumb-item active"><a class="font-weight-bold text-white">Contact</a></li>
                  </ol>
                </nav>
              </div>
              <h1 class="font-weight-medium text-white fs-xl-5 fs-sm-4 fs-3" data-zanim-xs='{"delay":0.4,"duration":1.5}'><span class="position-relative">Get in touch with us<span class="heading-shapes heading-shapes-right" style="background-image: url(/guest/img/illustrations/shapes-19.png); height: 70px; width: 80px"></span></span></h1>
              <div class="overflow-hidden">
                <p class="text-100 mt-4 w-lg-75" data-zanim-xs='{"delay":0.6,"duration":1.5}'>Do you have any Questions?, complains. Fell free to contact us.
                 We’d love to chat.</p>
              </div>
            </div>
          </div>
        </div>
        <div class="down-arrow"><a href="#map" data-fancyscroll="data-fancyscroll" data-offset="96"><span class="fa-stack fa-1x" data-zanim-xs='{"animation":"slide-down","delay":0.9,"duration":1.5}'><span class="fas fa-circle fa-stack-2x text-white" data-fa-transform="grow-4"></span><span class="fas fa-angle-down fa-stack-1x fa-inverse text-800"></span></span></a></div>
      </section>

      <!-- ============================================-->
      <!-- <section> begin ============================-->
      <section class="bg-extra-1 z-index--1 text-center" data-zanim-timeline='{"delay":0.6}' data-zanim-trigger="scroll">
        <div class="container">
          <div class="row justify-content-between align-items-center">
            <div class="col-lg col-md-5" data-zanim-xs='{"delay":0.2,"duration":1.5}'><img src="/guest/img/logos/contact-icon/my-location.svg" alt="" />
              <h6 class="fs-0 font-weight-bold text-white mb-0 mt-2">Address</h6>
              <p class="text-500">343 Nnebisi Road,Opp, Stadium Asaba, Delta State, Nigeria</p>
            </div>
            <div class="border-right border-900 position-relative d-none d-md-block" style="height:50px;width:1px"></div>
            <div class="col-lg col-md-5" data-zanim-xs='{"delay":0.4,"duration":1.5}'><img src="/guest/img/logos/contact-icon/headphone.svg" alt="" />
              <h6 class="fs-0 font-weight-bold text-white mb-0 mt-2">telephone</h6>
              <p class="text-500">(234) 906-254-7323</p>
            </div>
            
            <div class="border-right border-900 position-relative d-none d-lg-block" style="height:50px;width:1px"></div>
            <div class="col-lg col-md-5 mt-md-4 mt-lg-0" data-zanim-xs='{"delay":0.6,"duration":1.5}'><img src="/guest/img/logos/contact-icon/mail.svg" alt="" />
              <h6 class="fs-0 font-weight-bold text-white mb-0 mt-2">email</h6>
              <p class="text-500">support@truvender.com</p>
            </div>
            <div class="border-right border-900 position-relative d-none d-md-block" style="height:50px;width:1px"></div>
                <div class="col-lg col-md-5 mt-md-4 mt-lg-0" data-zanim-xs='{"delay":0.8,"duration":1.5}'>
                     <a href="https://wa.me/2349131676346">
                   
                        <img src="/guest/img/logos/contact-icon/chat.svg" alt="" />
                        <h6 class="fs-0 font-weight-bold text-white mb-0 mt-2">chat on WhatsApp</h6>
                        <p class="text-500">(234) 913-167-6346</p>
                    </a>
                </div>
          </div>
        </div><!-- end of .container-->
      </section><!-- <section> close ============================-->
      <!-- ============================================-->



      <!-- ============================================-->
      <!-- <section> begin ============================-->
      <section class="pt-0 bg-extra-1" data-zanim-timeline='{"delay":0.6}' data-zanim-trigger="scroll">
        <div class="bg-holder" style="background-image:url(/guest/img/bg-img/contact-form-bg.svg);background-position:center 40%;background-size:contain;"></div>
        <!--/.bg-holder-->
        <div class="container">
          <div class="row justify-content-center" data-zanim-timeline="{&quot;delay&quot;:0}" data-zanim-trigger="scroll">
            <div class="col-xl-7 col-lg-8 col-md-10 mt-9" data-zanim-xs='{"delay":0.8,"duration":1.5}'><img class="jhiri-left-top" src="/guest/img/illustrations/contact-b.png" alt="" style="width:13%" /><img class="jhiri-right-top d-none d-md-block" src="/guest/img/illustrations/contact-a.png" alt="" style="width:18%" /><img class="position-absolute d-none d-lg-block b-0" src="/guest/img/illustrations/contact-form-sideshape.png" alt="" style="width:40%;right:-18%;" /><img class="position-absolute d-none d-lg-block" src="/guest/img/illustrations/contact-map-shape.png" alt="" style="width:11%;right:-7%;bottom:-4%" />
              <div class="card bg-secondary">
                <div class="card-body p-md-6 p-4">
                  <div class="text-center">
                    <h1 class="text-white fs-xl-5 fs-lg-4 fs-md-3 fs-2">We’d love to hear from you!</h1>
                    <p class="text-600 w-lg-75 mx-auto mt-3">you’re precisely the person whom we’d love to hear from. Drop us a line and let us know what’s on your mind.</p>
                  </div>
                  @livewire('guest.contact')
                </div>
              </div>
              {{-- <img class="position-absolute d-none d-lg-block" src="/guest/img/illustrations/contact-leaf1.png" alt="" style="width:32%;left:-27%;bottom:-1%" /> --}}
            </div>
          </div>
        </div><!-- end of .container-->
      </section><!-- <section> close ============================-->
      <!-- ============================================-->



      

@endsection