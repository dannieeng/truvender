@extends('layouts.main')
@section('title', 'Welcome')

@section('content')
     {{-- <!-- ============================================-->
      <!-- <section> begin ============================-->
      <section class="py-0">
        <div class="container">
          <div class="row flex-center text-center min-vh-100">
            <div class="col-auto">
              <h1 class="display-2 mb-3">Trade Products <br> with a much Better Rate</h1>
            </div>
          </div>
        </div><!-- end of .container-->
      </section><!-- <section> close ============================-->
      <!-- ============================================--> --}}


      
  

      <!-- ============================================-->
      <!-- <section> begin ============================-->
      <section class="bg-dark overflow-hidden">
        <div class="container">
          <div class="row justify-content-center" data-zanim-timeline="{&quot;delay&quot;:0}" data-zanim-trigger="scroll">
            <div class="col-md-10 col-11 px-md-5 px-3 py-6 jhiri-feature-top-right" data-zanim-xs='{"delay":0.2,"duration":1.5}'><img class="position-absolute b-0 d-none d-sm-block" src="/guest/img/illustrations/side-shape-of-video.png" alt="" style="width:14%;right:-6%;" />
              <div class="bg-holder" style="background-image:url(/guest/img/bg-img/bg-features-image.png);"></div>
              <!--/.bg-holder-->
              <div class="position-relative">
                <h1 class="text-white fs-xl-5 fs-lg-4 fs-3">Our Products and Offers</h1>
                <p class="text-200 w-xxl-25 w-md-50 pt-3">uses the power of blockchain and cryptocurrency to 
                        solve your everyday needs</p>
                        {{-- <a href="{{route('register')}}" class="btn btn-danger mt-3 mr-3" >Start For Free</a>
                        <a href="#products" class="btn btn-light mt-3">Learn more</a> --}}
              </div>
            </div>
          </div>
        </div><!-- end of .container-->
        <div class="bg-holder"
        style="background-image:url(/guest/img/illustrations/about-bottom-curve.png);background-position:bottom;background-size:contain;">
      </div>
      </section><!-- <section> close ============================-->
      <!-- ============================================-->



      <!-- ============================================-->
      <!-- <section> begin ============================-->
      <section class="bg-dark pt-8 mt-8 overflow-hidden" data-zanim-timeline='{"delay":0}' data-zanim-trigger="scroll" id="products">
        <div class="container">
          <div class="row text-center justify-content-center">
            <div class="col-xl-11" data-zanim-xs='{"delay":0.3,"duration":1.5}'><img class="shapes-left-top d-none d-md-block" src="/guest/img/illustrations/shapes-12.png" alt="" style="width:5%" /><img class="shapes-left-bottom d-none d-md-block" src="/guest/img/illustrations/shapes-13.png" alt="" style="width:5%" /><img class="shapes-right-bottom d-none d-xl-block" src="/guest/img/illustrations/shapes-14.png" alt="" style="width:11%" />
              <div class="row justify-content-center" data-zanim-timeline="{&quot;delay&quot;:0.4}" data-zanim-trigger="scroll">
                <div class="col-lg-4 col-md-6 col-sm-10 mb-4"><a class="text-decoration-none" href="#!">
                    <div class="card h-100 hover-cardgroup bg-extra-1">
                      <div class="card-body p-xl-5 p-3"><img class="mb-3" src="/images/bg/bitcoin.svg" alt="logo" width="70" />
                        <h4 class="pb-3 font-weight-bold text-white title">Trade Bitcoin at Best Rates</h4>
                        <p class="subtitle text-600" data-zanim-xs='{"delay":"0.0","duration":1.5}'>
                            Get the best value for your money when you buy or sell BTC with {{config('app.name')}} From our secure wallet system to a BTC debit card.
                        </p>
                      </div>
                    </div>
                  </a></div>
                <div class="col-lg-4 col-md-6 col-sm-10 mb-4"><a class="text-decoration-none" href="#!">
                    <div class="card h-100 hover-cardgroup bg-extra-1">
                      <div class="card-body p-xl-5 p-3"><img class="mb-3" src="/images/bg/network.svg" alt="logo" width="70" />
                        <h4 class="pb-3 font-weight-bold text-white title">Refill on Data and Airtime</h4>
                        <p class="subtitle text-600" data-zanim-xs='{"delay":"0.1","duration":1.5}'>
                            Buy Data or airtime all with Bitcoin. No need for long trips. Enjoy comfort with Truvender.     
                        </p>
                      </div>
                    </div>
                  </a></div>
                <div class="col-lg-4 col-md-6 col-sm-10 mb-4"><a class="text-decoration-none" href="#!">
                    <div class="card h-100 hover-cardgroup bg-extra-1">
                      <div class="card-body p-xl-5 p-3"><img class="mb-3" src="/images/bg/gift.svg" alt="logo" width="70" />
                        <h4 class="pb-3 font-weight-bold text-white title">Giftcard Transaction</h4>
                        <p class="subtitle text-600" data-zanim-xs='{"delay":"0.2","duration":1.5}'>
                            Buy or Sell Giftcards at the best rates in the world while enjoying your comfort.
                        </p>
                      </div>
                    </div>
                  </a></div>
                
              </div>
            </div>
          </div>
        </div><!-- end of .container-->
      </section><!-- <section> close ============================-->
      <!-- ============================================-->

@endsection