@extends('layouts.main')
@section('title', 'News')

@section('content')
          <!-- ============================================-->
    <!-- <section> begin ============================-->
    <section class="pt-10 pb-11" data-zanim-timeline='{"delay":0.5}' data-zanim-trigger="scroll" id="read_more">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-lg col-10 col-12">
              <h1 class="font-weight-medium fs-xl-5 fs-sm-4 fs-3 heading-big-shape-right mb-5"><span
                  class="position-relative">
                  <span class="heading-shapes heading-shapes-right"
                    style="background-image: url(/guest/img/illustrations/shapes-13.png); height: 80px; width: 90px"></span></span><br />  News </h1>
              @foreach($posts as $post)
               <a href="{{route('guest.blog.post', encrypt($post->id) )}}">
                  <div class="col-lg-3 col-md-3 col-12 " data-zanim-xs='{"delay":0.6,"duration":1.5}'>
                    <div class="card border" style="border-radius-top: 20px;">
                        <div class="card-img">
                            <img alt="Post image" src="{{$post->image_path}}" style="width: 100%; height: 12rem;border-radius-top: 20px;"/>
                        </div>
                        <div class="card-body">
                            <div class="">
                                <h6 class="mb-1">
                                    {{$post->title}}
                                </h6>
                                <p class="">
                                    {!!$post->truncate_body(20)!!}
                                </p>
                            </div>
                        </div>
                    </div>
                </a>
                @endforeach
            </div>
          </div>
          
        </div>
      </div><!-- end of .container-->
    </section><!-- <section> close ============================-->
    <!-- ============================================-->
@endsection