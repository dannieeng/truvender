@extends('layouts.main')
@section('title', 'Welcome')

@section('content')
   <!-- Banner -->
      <section class="bg-dark py-xxl-11 py-xl-8 py-lg-6 py-0 position-relative">
        <div class="position-absolute absolute-centered banner"></div>
        <div class="bg-holder banner bg-repeat"></div>
       
        
        <div class="container">
          <div class="row" data-zanim-timeline="{&quot;delay&quot;:0.5}" data-zanim-trigger="scroll">
            <div class="col">
              <div class="position-relative text-center z-index-1 mb-4" data-zanim-xs='{"delay":0.2,"duration":1.5}'>
                <h1 class="display-2 text-white font-weight-medium fs-xl-7 fs-md-6 fs-3 mt-4 mt-md-0"><span class="d-inline-block position-relative">True Trade <span class="heading-shapes heading-shapes-right d-none d-md-block" style="background-image: url(guest/img/illustrations/shapes-13.png); height: 80px; width: 90px"></span></span><br />True Flex</h1>
                <p class="fs-md-2 fs-0 text-capitalize mt-4 mx-auto" data-zanim-xs='{"delay":0.3,"duration":1.5}'>Buy and Sell Gift Cards Around the World At Best Rates <br /></p>
                <form class="mb-6 mailbluster-subscribe" data-zanim-xs='{"delay":0.4,"duration":1.5}'>
                  <div class="form-row justify-content-center">
                    <div class="col-lg-4 col-md-6 col-sm-9">
                      <div class="mailbluster-feedback"></div>
                    </div>
                  </div>
                  <div class="form-row justify-content-center align-items-center">
                    <div class="col-auto mt-md-0"><a class="btn btn-primary mt-3" href="{{route('login')}}">Get Started</a></div>
                  </div>
                </form>
              </div>
             
            </div>
          </div>

          {{-- <!--Partners -->
          <div class="row justify-content-center">
            <div class="col-xxl-10 col-xl-9 col-lg-8 col-10 mb-md-4 mb-lg-0 text-center">
              <h6 class="mb-xxl-6 mb-xl-5 mb-lg-4 mb-md-4 text-800 font-weight-bold fs-md-0 fs--1 position-relative z-index-1">trusted by companies like this</h6>
              
              <div class="owl-carousel owl-carousel-theme owl-theme owl-dot-round owl-theme-primary mb-3" 
                data-options='{"autoplay":true,"loop":true,"dotsEach":true,"margin":50,"autoplayHoverPause":true,"responsive":{"0":{"items":3,"dotsEach":false,"dots":false,"margin":30},"600":{"items":4,"dotsEach":false,"dots":false},"1000":{"items":5,"dotsEach":true}}}'
                >
                
                <img class="img-fluid item" src="guest/img/logos/microsoft-logo.png" alt="" width="191" />
                <img class="img-fluid item" src="guest/img/logos/airbnb-logo.png" alt="" width="156" />
                <img class="img-fluid item" src="guest/img/logos/google-logo.png" alt="" width="162" />
                <img class="img-fluid item" src="guest/img/logos/Spotify_Logo.png" alt="" width="191" />
                <img class="img-fluid item" src="guest/img/logos/Paypal-logo.png" alt="" width="162" />
                <img class="img-fluid item" src="guest/img/logos/microsoft-logo.png" alt="" width="191" />
              </div>
            </div>
          </div>
        </div> --}}

      </section>
      <img class="img-fluid bg-white z-index--1" style="margin-top: -5px;" src="guest/img/illustrations/cripto-bottom-shape.png" alt="" />

     <!-- <section> begin ============================-->
     <section class="overflow-hidden">
       <div class="container">


         <div class="row justify-content-lg-between justify-content-center" data-zanim-timeline="{&quot;delay&quot;:0}"
           data-zanim-trigger="scroll">


           <div class="col-lg-6 col-md-10 align-self-center mt-6 mt-lg-0 order-lg-0"
             data-zanim-lg='{"animation":"slide-right","delay":0.3,"duration":1.5}'
             data-zanim-xs='{"delay":0.4,"duration":1.5}'>
             <div class="jhiri-jhiri-top-right jhiri-jhiri-top-left position-relative">
               <svg class="h-banner"
                 width="100%" height="100%" viewBox="0 0 691 571" fill="none" xmlns="http://www.w3.org/2000/svg"
                 xmlns:xlink="http://www.w3.org/1999/xlink">
                 <path
                   d="M60.3575 457.189L43.4645 404.292C40.9138 396.305 39.5349 387.989 39.3712 379.607L39.0114 361.175C38.5383 336.942 52.8335 314.899 75.0774 305.563C91.6566 298.604 110.506 299.649 126.25 308.399L145.293 318.983C157.121 325.557 164.816 337.723 165.712 351.266L165.809 352.738C166.741 366.837 175.491 379.221 188.435 384.763C197.75 388.751 205.057 396.37 208.677 405.87L211.362 412.916C219.131 433.304 214.226 456.355 198.847 471.729L183.197 487.374C162.469 508.096 131.52 514.594 104.218 503.956C83.326 495.817 67.2032 478.625 60.3575 457.189Z"
                   fill="#E7ECF5"></path>
                 <path
                   d="M328.962 45.0644L503.82 49.3665L527 52.4682C613.926 64.0995 681.308 134.016 689.533 221.115L690.312 229.373C692.773 255.432 688.604 281.69 678.191 305.717C663.432 339.771 636.962 367.446 603.553 383.752L584.394 393.104C558.38 405.8 529.054 410.121 500.472 405.467L489.335 403.653C448.449 396.996 407.022 411.938 379.858 443.14C360.842 464.983 334.512 479.182 305.777 483.091L288.26 485.474C226.969 493.811 166.102 467.556 130.216 417.302L125.737 411.03C114.599 395.433 106.046 378.153 100.406 359.849L98.337 353.137C79.7885 292.95 89.1632 227.643 123.899 175.062L147.132 139.894C187.286 79.1127 256.013 43.2695 328.962 45.0644Z"
                   fill="#5A45FF"></path>
                 <path
                   d="M200.089 499.406L61.8506 358.466L45.5229 337.729C-15.7076 259.961 -15.1117 150.253 46.9601 73.1017L52.845 65.7872C71.4163 42.7043 95.5146 24.6715 122.91 13.3577C161.738 -2.67801 205.019 -4.33085 244.947 8.6971L267.846 16.1685C298.937 26.3129 326.1 45.8586 345.578 72.1023L353.168 82.3281C381.032 119.869 426.379 140.307 473.005 136.337C505.645 133.558 538.177 142.732 564.537 162.147L580.606 173.982C636.83 215.394 665.414 284.546 654.823 353.539L653.502 362.15C650.215 383.562 643.506 404.309 633.63 423.598L630.009 430.671C597.537 494.096 538.406 539.728 468.776 555.096L422.204 565.375C341.715 583.139 257.761 558.205 200.089 499.406Z"
                   fill="#F7F7F8"></path>
                 <path
                   d="M200.089 499.406L61.8506 358.466L45.5229 337.729C-15.7076 259.961 -15.1117 150.253 46.9601 73.1017L52.845 65.7872C71.4163 42.7043 95.5146 24.6715 122.91 13.3577C161.738 -2.67801 205.019 -4.33085 244.947 8.6971L267.846 16.1685C298.937 26.3129 326.1 45.8586 345.578 72.1023L353.168 82.3281C381.032 119.869 426.379 140.307 473.005 136.337C505.645 133.558 538.177 142.732 564.537 162.147L580.606 173.982C636.83 215.394 665.414 284.546 654.823 353.539L653.502 362.15C650.215 383.562 643.506 404.309 633.63 423.598L630.009 430.671C597.537 494.096 538.406 539.728 468.776 555.096L422.204 565.375C341.715 583.139 257.761 558.205 200.089 499.406Z"
                   fill="url(#pattern0)"></path>
                 <defs>
                   <pattern id="pattern0" patternContentUnits="objectBoundingBox" width="1" height="1">
                     <use xlink:href="#image0" transform="translate(-0.160207) scale(0.000817594 0.000940734)"></use>
                   </pattern>
                   <image id="image0" width="1615" height="1063" xlink:href="/guest/img/bg-img/cripto-svg.png"></image>
                 </defs>
               </svg>
              </div>
           </div>


           <div class="col-lg-5 col-md-9 pt-7 mt-5 mt-lg-0"
             data-zanim-lg='{"animation":"slide-left","delay":0.3,"duration":1.5}'
             data-zanim-xs='{"delay":0.7,"duration":1.5}'>
             <div class="bg-holder"
               style="background-image:url(/guest/img/illustrations/shape-bg-text.png);background-size:59%;background-position:right top;">
             </div>
             <!--/.bg-holder-->
             <div class="row">
               <div class="col-xl-10">
                 <h1 class="text-capitalize fs-xl-5 fs-4">
                   <span class="position-relative">Do More
                     <span
                       class="heading-shapes heading-shapes-right"
                       style="background-image: url(/guest/img/illustrations/shapes-15.png); height: 70px; width: 81px">
                      </span>
                    </span><br />With Truvender</h1>
                 <p class="mt-5 text-900">Why worry about selling your various gift cards. We are the best buyers of various US based and other 
                   countries gift cards at the most affordable prices. Use the Live Chat facility on this site 
                   to sepak with one of our staff and find out how we make selling your gift card easy and stress free..</p>
                 
                   <div class="mt-5"><a href="{{route('about')}}" class="btn btn-primary btn-sm mr-3">find out more<span
                       class="fas fa-chevron-right ml-1"></span></a><a href="{{route('login')}}" class="btn btn-extra btn-sm">Get
                      started</a></div>
                 </div>
               </div>
             </div>
             
           </div>
         </div>
       </div><!-- end of .container-->
     </section><!-- <section> close ============================-->
     <!-- ============================================-->


      <!-- <section> begin ============================-->
     <section class="overflow-hidden">
       <div class="container">


         <div class="row justify-content-lg-between justify-content-center" data-zanim-timeline="{&quot;delay&quot;:0}"
           data-zanim-trigger="scroll">

            <div class="col-lg-5 col-md-9 pt-7 mt-5 mt-lg-0"
             data-zanim-lg='{"animation":"slide-right","delay":0.3,"duration":1.5}'
             data-zanim-xs='{"delay":0.7,"duration":1.5}'>
             <div class="bg-holder"
               style="background-image:url(/guest/img/illustrations/shape-bg-text.png);background-size:59%;background-position:right top;">
             </div>
             <!--/.bg-holder-->
             <div class="row">
               <div class="col-xl-10">
                 <h1 class="text-capitalize fs-xl-5 fs-4">
                   <span class="position-relative">Instant Payment
                     <span
                       class="heading-shapes heading-shapes-right"
                       style="background-image: url(/guest/img/illustrations/shapes-15.png); height: 70px; width: 81px">
                      </span>
                    </span><br />With Truvender</h1>
                 <p class="mt-5 text-900">
                   Using state-of-the-art payment procedures, you are guaranteed to get your payment withdraw to your account within minutes.
                 </p>
                 
                   <div class="mt-5"><a href="{{route('about')}}" class="btn btn-primary btn-sm mr-3">find out more<span
                       class="fas fa-chevron-right ml-1"></span></a><a href="{{route('login')}}" class="btn btn-extra btn-sm">Get
                      started</a></div>
                 </div>
               </div>
             </div>
             

           <div class="col-lg-6 col-md-10 align-self-center mt-6 mt-lg-0 order-lg-0"
             data-zanim-lg='{"animation":"slide-left","delay":0.3,"duration":1.5}'
             data-zanim-xs='{"delay":0.4,"duration":1.5}'>
             <div class="jhiri-jhiri-top-right jhiri-jhiri-top-left position-relative">
               <svg class="h-banner"
                 width="100%" height="100%" viewBox="0 0 691 571" fill="none" xmlns="http://www.w3.org/2000/svg"
                 xmlns:xlink="http://www.w3.org/1999/xlink">
                 <path
                   d="M60.3575 457.189L43.4645 404.292C40.9138 396.305 39.5349 387.989 39.3712 379.607L39.0114 361.175C38.5383 336.942 52.8335 314.899 75.0774 305.563C91.6566 298.604 110.506 299.649 126.25 308.399L145.293 318.983C157.121 325.557 164.816 337.723 165.712 351.266L165.809 352.738C166.741 366.837 175.491 379.221 188.435 384.763C197.75 388.751 205.057 396.37 208.677 405.87L211.362 412.916C219.131 433.304 214.226 456.355 198.847 471.729L183.197 487.374C162.469 508.096 131.52 514.594 104.218 503.956C83.326 495.817 67.2032 478.625 60.3575 457.189Z"
                   fill="#E7ECF5"></path>
                 <path
                   d="M328.962 45.0644L503.82 49.3665L527 52.4682C613.926 64.0995 681.308 134.016 689.533 221.115L690.312 229.373C692.773 255.432 688.604 281.69 678.191 305.717C663.432 339.771 636.962 367.446 603.553 383.752L584.394 393.104C558.38 405.8 529.054 410.121 500.472 405.467L489.335 403.653C448.449 396.996 407.022 411.938 379.858 443.14C360.842 464.983 334.512 479.182 305.777 483.091L288.26 485.474C226.969 493.811 166.102 467.556 130.216 417.302L125.737 411.03C114.599 395.433 106.046 378.153 100.406 359.849L98.337 353.137C79.7885 292.95 89.1632 227.643 123.899 175.062L147.132 139.894C187.286 79.1127 256.013 43.2695 328.962 45.0644Z"
                   fill="#5A45FF"></path>
                 <path
                   d="M200.089 499.406L61.8506 358.466L45.5229 337.729C-15.7076 259.961 -15.1117 150.253 46.9601 73.1017L52.845 65.7872C71.4163 42.7043 95.5146 24.6715 122.91 13.3577C161.738 -2.67801 205.019 -4.33085 244.947 8.6971L267.846 16.1685C298.937 26.3129 326.1 45.8586 345.578 72.1023L353.168 82.3281C381.032 119.869 426.379 140.307 473.005 136.337C505.645 133.558 538.177 142.732 564.537 162.147L580.606 173.982C636.83 215.394 665.414 284.546 654.823 353.539L653.502 362.15C650.215 383.562 643.506 404.309 633.63 423.598L630.009 430.671C597.537 494.096 538.406 539.728 468.776 555.096L422.204 565.375C341.715 583.139 257.761 558.205 200.089 499.406Z"
                   fill="#F7F7F8"></path>
                 <path
                   d="M200.089 499.406L61.8506 358.466L45.5229 337.729C-15.7076 259.961 -15.1117 150.253 46.9601 73.1017L52.845 65.7872C71.4163 42.7043 95.5146 24.6715 122.91 13.3577C161.738 -2.67801 205.019 -4.33085 244.947 8.6971L267.846 16.1685C298.937 26.3129 326.1 45.8586 345.578 72.1023L353.168 82.3281C381.032 119.869 426.379 140.307 473.005 136.337C505.645 133.558 538.177 142.732 564.537 162.147L580.606 173.982C636.83 215.394 665.414 284.546 654.823 353.539L653.502 362.15C650.215 383.562 643.506 404.309 633.63 423.598L630.009 430.671C597.537 494.096 538.406 539.728 468.776 555.096L422.204 565.375C341.715 583.139 257.761 558.205 200.089 499.406Z"
                   fill="url(#pattern10_fuck)"></path>
                 <defs>
                   <pattern id="pattern10_fuck" patternContentUnits="objectBoundingBox" width="1" height="1">
                     <use xlink:href="#image10_fuck" transform="translate(-0.160207) scale(0.000817594 0.000940734)"></use>
                   </pattern>
                   <image id="image10_fuck" width="1615" height="1063" xlink:href="/images/bg/cash-flw.jpeg"></image>
                 </defs>
               </svg>
              </div>
           </div>


           </div>
         </div>
       </div><!-- end of .container-->
     </section><!-- <section> close ============================-->
     <!-- ============================================-->

      <!-- ============================================-->
      <!-- <section> begin ============================-->
      <section class="bg-white" data-zanim-timeline='{"delay":0.5}' data-zanim-trigger="scroll">
        <div class="container">
          <div class="container">
          <div class="row flex-center" data-zanim-timeline="{&quot;delay&quot;:0}" data-zanim-trigger="scroll">
            <div class="col-lg col-10 mt-6 mr-3" data-zanim-lg='{"animation":"slide-right","delay":0.5,"duration":1.5}' data-zanim-xs='{"delay":0.7,"duration":1.5}'>
              <img class="position-absolute d-none d-md-block r-0" src="/guest/img/illustrations/home/home-vector-3.png" alt="" style="width:35%;bottom:3%" />
              <img class="position-absolute d-none d-md-block" src="/guest/img/illustrations/home/shapes-4.png" alt="" style="width:10%;right:8%;bottom:3%" />
              <img class="position-absolute d-none d-md-block t-0" src="/guest/img/illustrations/home/shapes-3.png" alt="" style="width:8%;left:15%;" />
              <img class="img-fluid z-index-1 position-relative" src="/images/bg/bitcoin.png" alt="" />
            </div>

            <div class="col-xxl-5 col-lg-6 col-md-10 text-dark mt-4 mt-lg-0" data-zanim-lg='{"animation":"slide-left","delay":0.5,"duration":1.5}' data-zanim-xs='{"delay":0.4,"duration":1.5}'>
              <h1 class="fs-xl-5 fs-lg-4 fs-3">Want To Sell Bitcoins?</h1>
              <p class="mt-4 w-xl-75">
                Exchange Bitcoins For Naira At Best Rates Within Minutes. Also, Get A Free Bitcoin Wallet Address To Receive Bitcoins From Any Part Of The World.
              </p>
              
            </div>
            
          </div>
        </div>
        </div><!-- end of .container-->
      </section><!-- <section> close ============================-->
      <!-- ============================================-->



     <section class="py-xxl-11 py-xl-9 py-lg-11 pb-md-7 pt-10 overflow-hidden">
        <div class="bg-holder" style="background-image:url(/guest/img/bg-img/by-its-default.svg);"></div>
        <!--/.bg-holder-->
        <div class="container">
          <div class="row flex-center" data-zanim-timeline="{&quot;delay&quot;:0}" data-zanim-trigger="scroll">
            <div class="col-xxl-5 col-lg-6 col-md-10" data-zanim-lg='{"animation":"slide-right","delay":0.5,"duration":1.5}' data-zanim-xs='{"delay":0.4,"duration":1.5}'>
              <h1 class="text-white fs-xl-5 fs-lg-4 fs-3">Buy and Sell Gift Cards Around the World At Best Rates.</h1>
              <p class="text-500 mt-4 w-xl-75">It’s more than just trading cards, experience worldclass transaction processes.</p>
              <div class="row mt-6">
                <div class="col-md-5"><span class="fa-stack"><span class="fas fa-circle fa-stack-2x text-white"></span><span class="fas fa-thumbs-up text-primary fa-stack-1x fa-inverse"></span></span>
                  <h5 class="font-weight-bold text-white mt-2">Sweet Rates</h5>
                  <p class="text-500 line-height-2">Buy and sell assets at sweet sweet rates</p>
                </div>
                <div class="col-xl-5 col-lg-6 col-md-5 mt-3 mt-md-0"><span class="fa-stack"><span class="fas fa-circle fa-stack-2x text-white"></span><span class="fas fa-lock text-success fa-stack-1x fa-inverse"></span></span>
                  <h5 class="font-weight-bold text-white mt-2">Easy to use</h5>
                  <p class="text-500 line-height-2">Our platform is easy to use and well secured at the same time</p>
                </div>
              </div>
            </div>
            <div class="col-lg col-10 mt-6" data-zanim-lg='{"animation":"slide-left","delay":0.5,"duration":1.5}' data-zanim-xs='{"delay":0.7,"duration":1.5}'><img class="position-absolute d-none d-md-block r-0" src="/guest/img/illustrations/home/home-vector-3.png" alt="" style="width:35%;bottom:3%" /><img class="position-absolute d-none d-md-block" src="/guest/img/illustrations/home/shapes-4.png" alt="" style="width:10%;right:8%;bottom:3%" /><img class="position-absolute d-none d-md-block t-0" src="/guest/img/illustrations/home/shapes-3.png" alt="" style="width:8%;left:15%;" /><img class="img-fluid z-index-1 position-relative" src="/images/bg/exchange.png" alt="" /></div>
          </div>
        </div>
        <div class="bg-dark b-0 h-50 w-100 position-absolute z-index--1"></div>
      </section>


      
      <!-- ============================================-->
      <!-- <section> begin ============================-->
      <section class="bg-1200 overflow-hidden">
        <div class="container">
          <div class="row flex-center text-center text-lg-left" data-zanim-timeline="{&quot;delay&quot;:0}" data-zanim-trigger="scroll">
            <div class="col-xl-6 col-lg-5 col-10 mb-6 mb-lg-0" data-zanim-lg='{"animation":"slide-right","delay":0.5,"duration":1.5}' data-zanim-xs='{"delay":0.4,"duration":1.5}'><svg class="h-banner" width="100%" height="100%" viewBox="0 0 587 429" fill="none" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                <path d="M360.547 427.465L250.513 411.354C237.546 409.456 224.927 405.665 213.059 400.103L186.015 387.428C145.323 368.356 122.323 324.508 129.763 280.189C134.557 251.635 151.477 226.554 176.158 211.417L221.275 183.745C242.047 171.004 267.654 168.949 290.192 178.212C315.033 188.422 343.754 184.568 365.261 168.482C380.637 156.981 399.967 151.414 419.088 153.176L427.519 153.952C469.544 157.824 504.568 187.759 514.938 228.668L521.106 253.003C534.055 304.083 515.862 358.01 474.618 390.809L463.363 399.759C434.389 422.8 397.175 432.828 360.547 427.465Z" fill="#FEB850"></path>
                <mask id="mask1" style="mask-type: alpha;" maskUnits="userSpaceOnUse" x="0" y="0" width="557" height="420">
                  <path d="M384.472 47.4725L463.425 59.7989C489.345 63.8457 512.953 77.0582 529.958 97.0352C565.208 138.446 564.75 199.446 528.883 240.323L475.87 300.739L442.843 343.654C429.759 360.656 413.905 375.333 395.946 387.07C365.81 406.764 330.724 417.544 294.729 418.169L215.266 419.548C144.409 420.778 78.5768 383.075 43.7816 321.338L24.8651 287.774C-12.1444 222.107 -7.53693 140.903 36.6622 79.8434C84.404 13.89 168.401 -15.0098 246.615 7.60794L384.472 47.4725Z" fill="#FE3162"></path>
                </mask>
                <g mask="url(#mask1)">
                  <path d="M-8 0H562V429H-8V0Z" fill="url(#pattern1)"></path>
                </g>
                <path d="M520.02 66.3228L519.844 66.3126C518.805 66.2523 517.866 65.6733 517.346 64.7718C516.902 64.0026 516.81 63.0794 517.094 62.2377L517.427 61.2512C517.585 60.7816 517.635 60.2823 517.573 59.7907L517.553 59.6241C517.417 58.5434 517.877 57.474 518.755 56.8294C519.867 56.0134 521.391 56.0592 522.452 56.9403L523.176 57.5419C524.183 58.3789 524.743 59.6373 524.691 60.9461L524.66 61.7252C524.595 63.3611 523.696 64.8494 522.278 65.668L521.927 65.8706C521.349 66.2042 520.686 66.3614 520.02 66.3228Z" fill="#5A45FF"></path>
                <path d="M579.782 77.9682L579.879 77.9526C580.457 77.8607 581.041 78.0653 581.435 78.4971C581.771 78.8656 581.933 79.3609 581.878 79.8567L581.814 80.4378C581.784 80.7144 581.817 80.9943 581.91 81.2565L581.941 81.3454C582.145 81.9218 582.022 82.5637 581.618 83.0228C581.106 83.6041 580.265 83.7624 579.577 83.4067L579.107 83.1638C578.454 82.8259 577.996 82.2031 577.867 81.4791L577.79 81.048C577.629 80.1429 577.943 79.2184 578.622 78.5989L578.79 78.4456C579.067 78.1931 579.412 78.0271 579.782 77.9682Z" fill="#5A45FF"></path>
                <path d="M555.922 147.319L555.7 147.236C554.386 146.741 553.409 145.618 553.101 144.248C552.838 143.08 553.09 141.854 553.794 140.884L554.618 139.747C555.011 139.206 555.276 138.583 555.394 137.926L555.434 137.703C555.693 136.257 556.715 135.064 558.104 134.587C559.863 133.982 561.807 134.653 562.819 136.213L563.51 137.279C564.472 138.761 564.688 140.606 564.095 142.271L563.743 143.262C563.002 145.342 561.247 146.897 559.092 147.382L558.559 147.502C557.681 147.7 556.764 147.636 555.922 147.319Z" fill="white"></path>
                <path d="M564.443 105.12L564.307 105.315C563.502 106.465 562.172 107.133 560.769 107.093C559.571 107.058 558.446 106.51 557.681 105.588L556.784 104.507C556.357 103.993 555.819 103.582 555.211 103.304L555.005 103.211C553.668 102.601 552.766 101.315 552.648 99.8511C552.498 97.9973 553.63 96.2797 555.393 95.686L556.596 95.2806C558.27 94.7166 560.112 94.9643 561.577 95.9508L562.45 96.5381C564.282 97.7711 565.354 99.8572 565.29 102.065L565.274 102.611C565.248 103.511 564.959 104.383 564.443 105.12Z" fill="#5A45FF"></path>
                <path d="M500.605 115.48L500.814 115.592C502.052 116.255 502.873 117.496 502.999 118.894C503.106 120.087 502.695 121.269 501.871 122.138L500.904 123.157C500.444 123.642 500.1 124.225 499.896 124.861L499.827 125.077C499.381 126.477 498.211 127.525 496.771 127.816C494.948 128.184 493.109 127.264 492.31 125.585L491.765 124.438C491.006 122.842 491.034 120.984 491.84 119.412L492.319 118.476C493.326 116.511 495.271 115.199 497.47 115.001L498.014 114.952C498.911 114.871 499.811 115.055 500.605 115.48Z" fill="white"></path>
                <path d="M579.234 66.3228L579.058 66.3126C578.019 66.2523 577.08 65.6733 576.56 64.7718C576.116 64.0026 576.024 63.0794 576.308 62.2377L576.64 61.2512C576.799 60.7816 576.849 60.2823 576.787 59.7907L576.766 59.6241C576.631 58.5434 577.091 57.474 577.969 56.8294C579.081 56.0134 580.605 56.0592 581.665 56.9403L582.39 57.5419C583.397 58.3789 583.957 59.6373 583.905 60.9461L583.874 61.7252C583.809 63.3611 582.91 64.8494 581.492 65.668L581.141 65.8706C580.563 66.2042 579.9 66.3614 579.234 66.3228Z" fill="#5A45FF"></path>
                <path d="M554.586 63.9962L554.488 63.8498C553.91 62.9841 553.807 61.8857 554.213 60.9273C554.56 60.1094 555.241 59.48 556.084 59.1996L557.072 58.8709C557.542 58.7145 557.971 58.4538 558.326 58.1084L558.447 57.9914C559.228 57.2322 560.358 56.9553 561.402 57.2679C562.723 57.6636 563.604 58.9079 563.54 60.2852L563.496 61.2256C563.435 62.534 562.768 63.7392 561.692 64.486L561.051 64.9306C559.706 65.864 557.976 66.0427 556.469 65.4041L556.096 65.2461C555.481 64.9857 554.956 64.5513 554.586 63.9962Z" fill="#5A45FF"></path>
                <path d="M564.01 35.652L564.083 35.8121C564.515 36.7592 564.441 37.86 563.887 38.741C563.414 39.4929 562.64 40.0051 561.763 40.1469L560.735 40.3132C560.246 40.3923 559.781 40.581 559.375 40.8651L559.238 40.9613C558.345 41.5857 557.185 41.678 556.205 41.2024C554.964 40.6003 554.293 39.231 554.577 37.8817L554.771 36.9605C555.041 35.6788 555.893 34.5959 557.074 34.031L557.778 33.6947C559.255 32.9886 560.991 33.0891 562.377 33.9608L562.72 34.1765C563.285 34.5319 563.733 35.0448 564.01 35.652Z" fill="#5A45FF"></path>
                <path d="M542.087 63.1864L542.02 63.3493C541.627 64.3131 540.775 65.0142 539.754 65.215C538.882 65.3863 537.979 65.1745 537.274 64.6336L536.449 63.9995C536.055 63.6977 535.599 63.4886 535.114 63.3879L534.95 63.3538C533.883 63.1324 533.02 62.3509 532.695 61.3116C532.282 59.9959 532.819 58.5684 533.995 57.8499L534.799 57.3594C535.917 56.677 537.289 56.554 538.51 57.0268L539.238 57.3083C540.765 57.8992 541.882 59.2316 542.198 60.8381L542.276 61.2356C542.404 61.8904 542.339 62.5685 542.087 63.1864Z" fill="white"></path>
                <path d="M539.628 102.814L539.563 102.902C539.177 103.422 538.555 103.714 537.907 103.677C537.355 103.646 536.843 103.378 536.502 102.943L536.101 102.433C535.911 102.19 535.668 101.993 535.391 101.858L535.297 101.812C534.688 101.513 534.288 100.908 534.252 100.231C534.207 99.3737 534.751 98.5955 535.572 98.344L536.133 98.1723C536.912 97.9334 537.759 98.0713 538.423 98.5453L538.818 98.8274C539.648 99.4199 540.116 100.396 540.058 101.414L540.044 101.666C540.02 102.081 539.876 102.48 539.628 102.814Z" fill="white"></path>
                <path d="M536.713 124.983L536.604 124.977C535.957 124.939 535.372 124.578 535.048 124.017C534.771 123.538 534.714 122.963 534.891 122.439L535.098 121.825C535.197 121.532 535.228 121.221 535.19 120.915L535.177 120.811C535.092 120.138 535.379 119.472 535.925 119.071C536.618 118.563 537.567 118.591 538.227 119.14L538.678 119.515C539.306 120.036 539.655 120.82 539.622 121.635L539.603 122.12C539.562 123.139 539.002 124.065 538.119 124.575L537.901 124.701C537.541 124.909 537.128 125.007 536.713 124.983Z" fill="white"></path>
                <path d="M521.926 123.806L521.861 123.894C521.475 124.414 520.852 124.706 520.205 124.669C519.653 124.638 519.141 124.37 518.799 123.935L518.399 123.425C518.209 123.182 517.966 122.986 517.689 122.85L517.595 122.804C516.986 122.505 516.586 121.901 516.55 121.223C516.505 120.366 517.049 119.588 517.87 119.336L518.43 119.164C519.21 118.926 520.057 119.063 520.721 119.537L521.116 119.82C521.946 120.412 522.414 121.389 522.356 122.406L522.342 122.658C522.318 123.073 522.174 123.472 521.926 123.806Z" fill="white"></path>
                <path d="M542.36 139.068L542.425 139.197C542.808 139.96 542.782 140.865 542.355 141.605C541.99 142.236 541.372 142.681 540.657 142.824L539.819 142.993C539.42 143.073 539.045 143.242 538.721 143.488L538.611 143.571C537.898 144.111 536.949 144.222 536.131 143.863C535.095 143.408 534.502 142.305 534.693 141.19L534.823 140.428C535.005 139.369 535.669 138.454 536.621 137.954L537.188 137.656C538.377 137.031 539.804 137.059 540.968 137.731L541.256 137.898C541.73 138.171 542.114 138.578 542.36 139.068Z" fill="white"></path>
                <path d="M561.833 123.806L561.768 123.894C561.382 124.414 560.76 124.706 560.112 124.669C559.56 124.638 559.048 124.37 558.707 123.935L558.307 123.425C558.116 123.182 557.873 122.986 557.596 122.85L557.502 122.804C556.893 122.505 556.493 121.901 556.457 121.223C556.412 120.366 556.956 119.588 557.777 119.336L558.338 119.164C559.118 118.926 559.964 119.063 560.628 119.537L561.023 119.82C561.853 120.412 562.321 121.389 562.263 122.406L562.249 122.658C562.225 123.073 562.081 123.472 561.833 123.806Z" fill="#5A45FF"></path>
                <path d="M534.051 162.949L534.002 162.851C533.711 162.272 533.731 161.585 534.055 161.024C534.332 160.545 534.801 160.208 535.343 160.099L535.979 159.971C536.282 159.91 536.566 159.782 536.812 159.596L536.896 159.533C537.436 159.123 538.156 159.038 538.777 159.311C539.563 159.656 540.013 160.493 539.868 161.339L539.769 161.917C539.632 162.721 539.128 163.415 538.406 163.794L537.976 164.02C537.073 164.494 535.991 164.473 535.108 163.963L534.889 163.837C534.529 163.629 534.238 163.32 534.051 162.949Z" fill="white"></path>
                <path d="M561.46 184.938L561.394 185.026C561.008 185.546 560.386 185.838 559.739 185.801C559.187 185.77 558.675 185.502 558.333 185.067L557.933 184.557C557.743 184.314 557.5 184.117 557.223 183.982L557.129 183.936C556.52 183.637 556.12 183.032 556.084 182.355C556.038 181.498 556.583 180.72 557.404 180.468L557.964 180.296C558.744 180.057 559.591 180.195 560.255 180.669L560.65 180.951C561.479 181.544 561.947 182.52 561.89 183.538L561.875 183.79C561.852 184.205 561.707 184.604 561.46 184.938Z" fill="white"></path>
                <path d="M561.895 200.612L561.83 200.7C561.444 201.22 560.822 201.511 560.174 201.475C559.622 201.443 559.11 201.176 558.769 200.741L558.369 200.231C558.178 199.988 557.935 199.791 557.658 199.655L557.564 199.61C556.955 199.311 556.555 198.706 556.519 198.029C556.474 197.172 557.018 196.393 557.839 196.142L558.4 195.97C559.18 195.731 560.026 195.869 560.69 196.343L561.085 196.625C561.915 197.218 562.383 198.194 562.325 199.212L562.311 199.464C562.287 199.879 562.143 200.278 561.895 200.612Z" fill="white"></path>
                <path d="M534.965 83.9718L534.878 83.9061C534.359 83.5175 534.07 82.8939 534.11 82.2469C534.144 81.6948 534.414 81.184 534.851 80.8446L535.363 80.4469C535.607 80.2576 535.804 80.0157 535.941 79.7392L535.988 79.6456C536.289 79.0379 536.896 78.6409 537.573 78.6082C538.431 78.5669 539.207 79.1148 539.454 79.937L539.623 80.4983C539.858 81.2793 539.716 82.1253 539.239 82.7869L538.955 83.1807C538.359 84.0076 537.38 84.471 536.363 84.4084L536.111 84.3929C535.696 84.3674 535.298 84.221 534.965 83.9718Z" fill="#5A45FF"></path>
                <path d="M561.845 83.382L561.779 83.4701C561.393 83.9906 560.771 84.282 560.124 84.2452C559.571 84.2139 559.059 83.9465 558.718 83.5113L558.318 83.0012C558.127 82.7584 557.884 82.5617 557.607 82.426L557.513 82.38C556.904 82.0816 556.504 81.4768 556.469 80.7995C556.423 79.9421 556.967 79.1638 557.788 78.9124L558.349 78.7406C559.129 78.5017 559.975 78.6396 560.639 79.1136L561.034 79.3958C561.864 79.9883 562.332 80.9647 562.274 81.9826L562.26 82.2345C562.237 82.6494 562.092 83.0483 561.845 83.382Z" fill="#5A45FF"></path>
                <defs>
                  <pattern id="pattern1" patternContentUnits="objectBoundingBox" width="1" height="1">
                    <use xlink:href="#image1" transform="translate(-0.117371 -0.0296406) scale(0.000315709 0.000419474)"></use>
                  </pattern>
                  <image id="image1" width="3600" height="2542" xlink:href="/guest/img/gallery/home-svg-bg.png"></image>
                </defs>
              </svg></div>
            <div class="col-xl col-lg-7 col-10" data-zanim-lg='{"animation":"slide-left","delay":0.5,"duration":1.5}' data-zanim-xs='{"delay":0.5,"duration":1.5}'>
              <h1 class="text-white fs-xl-5 fs-lg-4 fs-md-3 fs-2"> All for amazing naira rates and payment is instant.</h1>
              {{-- <p class="text-500 w-md-75 mx-auto mx-lg-0"> All for amazing naira rates and payment is instant.</p> --}}
              <div class="row mt-5 justify-content-center justify-content-lg-start">
                <div class="col-md-5 col-10 hover-cardgroup bg-extra-1 mb-xl-4 mb-3 mr-xl-4 mr-md-3">
                  <div class="media align-items-center">
                    <div class="card-body text-left"><a class="stretched-link" href="#!"></a>
                      <p class="mb-0 font-weight-bold title text-white fs-xl-0 fs-lg--1 fs-0">iTunes</p>
                      <p class="text-500 mb-0 subtitle fs-xl-0 fs-lg--1 fs-0">giftcards</p>
                    </div>
                  </div>
                </div>
                <div class="col-md-5 col-10 hover-cardgroup bg-extra-1 mb-xl-4 mb-3">
                  <div class="media align-items-center">
                    <div class="card-body text-left"><a class="stretched-link" href="#!"></a>
                      <p class="mb-0 font-weight-bold title text-white fs-xl-0 fs-lg--1 fs-0">Amazon</p>
                      <p class="text-500 mb-0 subtitle fs-xl-0 fs-lg--1 fs-0">giftcards</p>
                    </div>
                  </div>
                </div>
                <div class="col-md-5 col-10 hover-cardgroup bg-extra-1 mr-xl-4 mr-md-3 mb-3 mb-md-0">
                  <div class="media align-items-center">
                    <div class="card-body text-left"><a class="stretched-link" href="#!"></a>
                      <p class="mb-0 font-weight-bold title text-white fs-xl-0 fs-lg--1 fs-0">Bitcoin</p>
                      <p class="text-500 mb-0 subtitle fs-xl-0 fs-lg--1 fs-0">crypto currency</p>
                    </div>
                  </div>
                </div>
                <div class="col-md-5 col-10 hover-cardgroup bg-extra-1">
                  <div class="media align-items-center">
                    <div class="card-body text-left"><a class="stretched-link" href="#!"></a>
                      <p class="mb-0 font-weight-bold title text-white fs-xl-0 fs-lg--1 fs-0">Many more</p>
                    </div>
                  </div>
                </div>
              </div><a class="text-decoration-none" href="{{route('products')}}">
                <h6 class="font-weight-bold text-info mt-4 ml-1">view all products<span class="fas fa-arrow-right" data-fa-transform="down-2 right-4"></span></h6>
              </a>
            </div>
          </div>
        </div><!-- end of .container-->
      </section><!-- <section> close ============================-->
      <!-- ============================================-->



      <!-- ============================================-->
      <!-- <section> begin ============================-->
      <section class=" overflow-hidden">
        <div class="container">
          <div class="row justify-content-center" data-zanim-timeline="{&quot;delay&quot;:0}" data-zanim-trigger="scroll">
            <div class="col-md-10 col-11 px-md-5 px-3 py-6 jhiri-feature-top-right" data-zanim-xs='{"delay":0.2,"duration":1.5}'><img class="position-absolute b-0 d-none d-sm-block" src="/guest/img/illustrations/side-shape-of-video.png" alt="" style="width:14%;right:-6%;" />
              <div class="bg-holder" style="background-image:url(/guest/img/bg-img/bg-features-image.png);"></div>
              <!--/.bg-holder-->
              <div class="position-relative">
                <h1 class="text-white fs-xl-5 fs-lg-4 fs-3"> Buy Airtime and Purchase Data with Truvender.</h1>
                <p class="text-200 w-xxl-25 w-md-50 pt-3">

                </p>
              </div>
            </div>
          </div>
        </div><!-- end of .container-->
        
      </section><!-- <section> close ============================-->
      <!-- ============================================-->



      

@endsection