@extends('layouts.main')
@section('title', 'About')

@section('content')
     <section class="pb-11">
      <div class="bg-holder" style="background-image:url(/images/bg/faltlay1.jpg);"></div>
      <!--/.bg-holder-->
      <div class="bg-holder"
        style="background-image:url(/guest/img/illustrations/about-bottom-curve.png);background-position:bottom;background-size:contain;">
      </div>
      <!--/.bg-holder-->
      <div class="container" 
        data-aos="fade-up"
        data-aos-delay="500"
        data-aos-duration="1000"
        >
        <div class="row">
          <div class="col-md-6 col-10 pt-6 pb-6" 
            data-aos="fade-up"
            data-aos-delay="200"
            data-aos-duration="1000"
            >
            <div class="bc-caret-right bc-light mb-6">
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb indigo lighten-4">
                  <li class="breadcrumb-item"><a class="text-white fs--1" href="{{route('welcome')}}">Home</a></li>
                  <li class="breadcrumb-item"><a class="text-white fs--1" href="#">Pages</a></li>
                  <li class="breadcrumb-item active"><a class="font-weight-bold text-white fs--1">About</a></li>
                </ol>
              </nav>
            </div>
            <h1 class="font-weight-medium text-white fs-xl-5 fs-4"><span class="position-relative">About us<span
                  class="heading-shapes heading-shapes-right"
                  style="background-image: url(/guest/img/illustrations/shapes-13.png); height: 70px; width: 80px"></span></span>
            </h1>
            <p class="text-100 mt-4 w-lg-75">Things you need to know about Truvender Enterprises <a class="font-weight-bold text-white"
                href="#read_more">read more</a> </p>
          </div>
        </div>
      </div>
    </section>

      <!-- ============================================-->
    <!-- <section> begin ============================-->
    <section class="pt-10 pb-11" data-zanim-timeline='{"delay":0.5}' data-zanim-trigger="scroll" id="read_more">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-lg col-10 order-lg-1 order-md-0" data-zanim-xs='{"delay":0.6,"duration":1.5}'>
            <div class="position-relative">
              <h1 class="font-weight-medium fs-xl-5 fs-sm-4 fs-3 heading-big-shape-right"><span
                  class="position-relative">Who is, What is,
                  <span class="heading-shapes heading-shapes-right"
                    style="background-image: url(/guest/img/illustrations/shapes-13.png); height: 80px; width: 90px"></span></span><br />  Why Truvender</h1>
              <p class="text-900 mt-4 w-lg-75">
                Things you need to know about Truvender Enterprises
                we buy Itunes, steam, google play, amazon, bitcoin. <br> <br>
                Truvender has been a house hold name for 10years , providing Nigerians
                 with a comfortable and convenient means for converting cryptocurrencies
                  into cash.  <br>
                  With more than 5000 users served and more than 3 billion naira
                   in transactions completed you cannot be wrong with conducting a business
                    transaction with us today, Amazing rates and fast payment , Wether you
                     are looking for how to sell itunes, steam, amazon, google play or bitcoin
                      in Nigeria, Truvender Enterprises has got you covered. <br> <br>
                      Our rates are competitive 
                      and second to none, we offer you the best gift card exchange service in 
                      the industry. 
                      <br> <br> To start a trade click on sign up and register your account 
                      input you account number and current name of the account , check rate on 
                      our website , upload a clear picture of your card and your receipt if needed, 
                      wait few minutes and boom! you get credited into your Truvender account , 
                      Honesty is our Watch word, we look forward to transacting with you!
              </p>
              
            </div>
          </div>
          <div class="col-lg col-10 mt-4 mt-lg-0">
            <div class="row justify-content-end">
              <div class="col-lg-11 col-6" data-zanim-xs='{"delay":0.2,"duration":1}'><img class="jhiri-left-top"
                  src="/guest/img/illustrations/home/jhiri-1.png" alt="" style="width:20%" />
                <img class="img-fluid rounded-soft" src="/guest/img/gallery/about-second-sec.png" alt="" /></div>
              
            </div>
          </div>
        </div>
      </div><!-- end of .container-->
    </section><!-- <section> close ============================-->
    <!-- ============================================-->


@endsection

