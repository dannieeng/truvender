@extends('layouts.main')
@section('title', $title)

@section('content')
        <!-- ============================================-->
    <!-- <section> begin ============================-->
    <section class="pt-10 pb-11" data-zanim-timeline='{"delay":0.5}' data-zanim-trigger="scroll" id="read_more">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-lg col-10 order-lg-1 order-md-0" data-zanim-xs='{"delay":0.6,"duration":1.5}'>
            <div class="position-relative">
              <h1 class="font-weight-medium fs-xl-5 fs-sm-4 fs-3 heading-big-shape-right"><span
                  class="position-relative">
                  <span class="heading-shapes heading-shapes-right"
                    style="background-image: url(/guest/img/illustrations/shapes-13.png); height: 80px; width: 90px"></span></span><br />  {{$title}}</h1>
                <p class="text-900 mt-4 w-lg-75">
                    {!!$text!!}
                </p>

                <p class="text-900 mt-4 w-lg-75">
                    {!!isset($atml) ? $atml : ''!!}
                </p>
            </div>
          </div>
        </div>
      </div><!-- end of .container-->
    </section><!-- <section> close ========================== -->
@endsection