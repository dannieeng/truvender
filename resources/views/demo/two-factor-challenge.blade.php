@extends('layouts.auth')
@section('title', 'Sign Up')

@section('added')
    <div class="login-wrap d-flex align-items-center flex-wrap justify-content-center">
		<div class="container">
			<div class="row align-items-center">
				<div class="col-md-6 col-lg-7">
					<img src="{{asset ('auth/vendors/images/register-page-img.png')}}" style="-webkit-transform: scaleX(-1); transform: scaleX(-1);" alt="">
				</div>
				<div class="col-md-6 col-lg-5">
					<div class="login-box bg-white box-shadow border-radius-10">
						<div class="login-title">
                            <h2 class="text-center text-primary"><small>{{config('app.name')}}</small> Sign Up</h2>
						</div>
						<x-jet-validation-errors class="mb-2 mt-2" />
						<form method="POST" action="{{ route('register') }}">
							@csrf
							<div class="input-group custom">
								<input type="text" class="form-control form-control-lg" name="username" placeholder="Username" value="{{old('username')}}" required autofocus>
								<div class="input-group-append custom">
									<span class="input-group-text"><i class="icon-copy dw dw-user1"></i></span>
								</div>
                            </div>
                            
                            <div class="input-group custom">
								<input type="email" class="form-control form-control-lg" name="email" placeholder="Email" value="{{old('email')}}" required autofocus>
								<div class="input-group-append custom">
									<span class="input-group-text"><i class="icon-copy dw dw-email1"></i></span>
								</div>
                            </div>
                        
                            <div class="input-group custom">
								<input type="password" class="form-control form-control-lg" name="password" required autocomplete="new-password" placeholder="Password">
								<div class="input-group-append custom">
									<span class="input-group-text"><i class="dw dw-padlock1"></i></span>
								</div>
                            </div>
                            
                            <div class="input-group custom">
								<input type="password" class="form-control form-control-lg" name="password_confirmation" required autocomplete="new-password" placeholder="Confirm Password">
								<div class="input-group-append custom">
									<span class="input-group-text"><i class="dw dw-padlock1"></i></span>
								</div>
                            </div>
                            
                            <div class="input-group custom">
								<input type="text" class="form-control form-control-lg" name="referrer_code" placeholder="Referrer Code">
								<div class="input-group-append custom">
									<span class="input-group-text"><i class="icon-copy dw dw-user3"></i></span>
								</div>
                            </div>

							
							<div class="row">
								<div class="col-sm-12">
									<div class="input-group mb-0">
										<button type="submit" class="btn btn-primary btn-lg btn-block" href="index.html">Sign Up</button>
									</div>
									<div class="font-16 weight-600 pt-2 pb-2 text-center" data-color="#707373">OR</div>
									<div class="input-group mb-0">
										<a class="btn btn-outline-primary btn-lg btn-block" href="{{route('login')}}">Sign In</a>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection