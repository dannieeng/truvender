<nav class="navbar bg-dark navbar-dark sticky-kit navbar-expand-lg py-4 sticky-kit">
    <div class="container">
        <a class="navbar-brand" href="{{route('welcome')}}">
            <div class="d-flex align-items-center">
                <img src="/images/logo/tru-logo.png" style="height: 2.6rem;" alt="logo"/>
                <h5 class=" font-weight-bold text-primary mt-3 text-white text-lowercase">{{'truvender'}}</h5>
            </div>
        </a>
        <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarStandard" aria-controls="navbarStandard" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
        <div class="collapse navbar-collapse ml-xl-7 ml-xxl-9" id="navbarStandard">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item dropdown dropdown-menu-on-hover">
            <a class="nav-link" id="companyNavbarDropdown" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Company</a>
            <div class="dropdown-menu" aria-labelledby="companyNavbarDropdown">
                <a class="dropdown-item" href="{{route('about')}}">About</a>
                <a class="dropdown-item" href="{{route('career')}}">Career</a>
                
            </div>
            </li>
            
            <li class="nav-item dropdown dropdown-menu-on-hover">
            <a class="nav-link" id="tradeNavbarDropdown" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Trade</a>
            <div class="dropdown-menu" aria-labelledby="tradeNavbarDropdown">
                <a class="dropdown-item" href="{{route('products.guest')}}">Products To Trade</a>
                <a class="dropdown-item" href="{{route('btc.index')}}">Trade Bitcoin</a>
                <a class="dropdown-item" href="{{route('cards.index')}}">Trade Gift Cards</a>
                <a class="dropdown-item" href="{{route('products')}}">Trade Airtime</a>
            </div>
            </li>

            {{-- <li class="nav-item">
            <a class="nav-link" href="{{route('calculate')}}">Rates</a>
            </li> --}}
            {{-- <li class="nav-item">
            <a class="nav-link" href="{{route('faq')}}">Faq</a>
            </li> --}}
            <li class="nav-item">
            <a class="nav-link" href="{{route('contact')}}">Contact</a>
            </li>
        </ul>
        <div class="ml-auto mt-3 mt-lg-0">
           
                <a target="_blank" class="btn btn-sm btn-secondary mr-xl-3 mr-lg-2 mr-3" href="{{route('login')}}">Sign In</a>
                <a target="_blank" class="btn-primary btn btn-sm" href="{{route('register')}}">Sign Up</a>
        </div>
        </div>
    </div>
    </nav>