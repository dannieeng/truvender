
    <!-- ============================================-->
      <!-- <section> begin ============================-->
      <section class="py-0 bg-dark" data-zanim-timeline='{"delay":0}' data-zanim-trigger="scroll">
        <div class="bg-holder" style="background-image:url(/guest/img/bg-img/CTA-primary.png);" ></div>
        <!--/.bg-holder-->
        <div class="container">
          <div class="row">
            <div class="col text-center py-5">
              <h3 class="text-white fs-lg-3 fs-2" data-zanim-xs='{"delay":0.3,"duration":1.5}'>
                <span class="position-relative">Create a free Account now
                  <span class="heading-shapes heading-shapes-right d-none d-md-block" 
                  style="background-image: url(/guest/img/illustrations/shapes-19.png); height: 70px; width: 81px">
                  </span>
                </span>
              </h3>
              <a href="{{route('register')}}" class="btn mt-3 btn-light" data-zanim-xs='{"delay":0.5,"duration":1.5}'>Sign Up</a>
            </div>
          </div>
        </div><!-- end of .container-->
      </section><!-- <section> close ============================-->
      <!-- ============================================-->

    <!-- ============================================-->
      <!-- <section> begin ============================-->
      <section class="bg-dark overflow-hidden">
        <div class="container">
          <div class="row justify-content-center justify-content-lg-between"  data-zanim-timeline="{&quot;delay&quot;:0}" data-zanim-trigger="scroll">
            <div class="col-lg-3 col-sm-6 mb-4 mb-md-0">
              <h4 class="text-white">Products</h4>
              <div class="nav flex-column mt-3">
                  <a class="nav-item nav-link px-2 pl-0 text-800" href="#{{route('btc.index')}}">Bitcoin</a><a
                  class="nav-item nav-link px-2 pl-0 text-800" href="{{route('cards.index')}}">Gifcards</a>
                  <a class="nav-item nav-link px-2 pl-0 text-800" href="{{route('airtime.buy')}}">Airtime</a>
              </div>
            </div>

            <div class="col-lg-3 col-sm-6 mr-auto">
              <h4 class="text-white">Company</h4>
              <div class="nav flex-column mt-3">
                <a class="nav-item nav-link px-2 pl-0 text-800" href="{{route('about')}}">About</a><a
                  class="nav-item nav-link px-2 pl-0 text-800" href="{{route('career')}}">Career</a>
                <a class="nav-item nav-link px-2 pl-0 text-800" href="{{route('guest.blog')}}">News</a>
                <a class="nav-item nav-link px-2 pl-0 text-800" href="{{route('rates')}}">Rates</a>
              </div>
            </div>
            
             <div class="col-lg-3 col-sm-6 mr-auto">
              <h4 class="text-white">Legal</h4>
              <div class="nav flex-column mt-3">
                  <a class="nav-item nav-link px-2 pl-0 text-800" href="{{route('terms')}}">Terms</a><a
                  class="nav-item nav-link px-2 pl-0 text-800" href="{{route('policy')}}">Policies</a>
                  <a class="nav-item nav-link px-2 pl-0 text-800" href="{{route('atml')}}">Anti-Money Laundering Policy</a>
              </div>
            </div>

            <div class="col-lg-3 col-sm-6 mr-auto">
              <h4 class="text-white">Contact</h4>
              <div class="media mt-1"><span class="fas fa-envelope mr-3 mt-1 text-warning" data-fa-transform="down-2"></span>
                <div class="media-body">
                  <p class="text-600 mb-0"> {{ 'info@'. strtolower(config('app.name')) . '.com'}}</p>
                  <p class="text-600">343 Nnebisi Road,Opp, Stadium Asaba, Delta State, Nigeria.</p>
                </div>
              </div>
              
              <div class="media mt-1">
                <div class="media-body">
                  <p class="text-600 mb-0"> 
                        <a href="{{route('welcome')}}">{{config('app.name')}}.com</a> is a property of TRUVENDER ENTERPRISE, Information Technologies Solutions Online Trading , a company duly registered with CAC with BN Number: 3241249. A company duly registered under the Laws of the Federal Republic of Nigeria. 
                        Licensed Small and Medium Enterprises Development Agency of Nigeria with 
                        SMEDAN Number: SUIN57186811
                  </p>
                </div>
              </div>
            </div>

           
          </div>
        </div><!-- end of .container-->
      </section>
      <!-- <section> close ============================-->
      <!-- ============================================-->

    <!-- ============================================-->
    <!-- <section> begin ============================-->
    <section class="bg-dark py-2">
    <div class="container">
        <div class="row text-center justify-content-center">
        <div class="col-auto">
            <div class="nav justify-content-center"><a class="nav-item nav-link px-2 social-icon-hover-primary" target="_blank" href="https://twitter.com/truvender"><span class="fab fa-twitter social-icon"></span></a>
            <a class="nav-item nav-link px-2 social-icon-hover-primary" target="_blank" href="https://www.facebook.com/Truvender"><span class="fab fa-facebook-f social-icon"></span></a>
            <!--<a class="nav-item nav-link px-2 social-icon-hover-danger" href="#!"><span class="fab fa-youtube social-icon"></span></a>-->
            <a class="nav-item nav-link px-2 social-icon-hover-warning" target="_blank" href="https://www.instagram.com/officialtruvender/"><span class="fab fa-instagram social-icon"></span></a>
            <!--<a class="nav-item nav-link px-2 social-icon-hover-info" href="#!"><span class="fab fa-linkedin-in social-icon"></span></a>-->
            </div>
            <p>&copy; {{now()->format('M Y')}} <a class="text-decoration-none font-weight-bold text-white" href="{{route('welcome')}}" target="_blank">{{config('app.name')}}</a></p>
        </div>
        </div>
    </div><!-- end of .container-->
    </section><!-- <section> close ============================-->
    <!-- ============================================-->
