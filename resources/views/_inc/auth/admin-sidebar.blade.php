	@php
		$other_assets = App\Models\OtherAsset::all();
	@endphp
	
	<div class="left-side-bar">
		<div class="brand-logo mb-2">
			<a href="{{route('admin.index')}}">
				<img src="/images/logo/tru-logo-dark.png" alt="" class="dark-logo" style="height:2.4rem; ">
				<img src="/images/logo/tru-logo.png" alt="" class="light-logo" style="height:2.4rem;">
				<h4 class="text-white mt-2">truvender</h4>
			</a>
			<div class="close-sidebar" data-toggle="left-sidebar-close">
				<i class="ion-close-round"></i>
			</div>
		</div>
		<div class="menu-block customscroll">
			<div class="sidebar-menu">
				<ul id="accordion-menu">
					<li>
						<a href="{{route('admin.index')}}" class="dropdown-toggle no-arrow {{Route::is('admin.index') ? 'active' : ''}}">
							<span class="micon dw dw-home"></span>
							<span class="mtext">Dashboard </span>
						</a>
					</li>
                    
                    <li>
						<a href="{{route('admin.users')}}" class="dropdown-toggle no-arrow {{Route::is('admin.users') ? 'active' : ''}}">
                            <span class="micon  ti-user"></span>
							<span class="mtext">Users </span>
						</a>
					</li>

                    
                    
					<li class="dropdown">
						<a href="javascript:;" class="dropdown-toggle">
							<span class="micon fi-list"></span><span class="mtext">Blog</span>
						</a>
						<ul class="submenu">
							<li><a href="{{route('admin.blog.index')}}">All Post</a></li>
							<li><a href="{{route('admin.blog.create')}}">Add New Post</a></li>
						</ul>
					</li>

					<li class="dropdown">
						<a href="javascript:;" class="dropdown-toggle">
							<span class="micon fi-credit-card"></span><span class="mtext">GiftCards</span>
						</a>
						<ul class="submenu">
							<li><a href="{{route('admin.cards.index')}}">All GiftCards</a></li>
							<li><a href="{{route('admin.card-rates')}}">Card Rates</a></li>
							<li><a href="{{route('admin.card-trades')}}">Card Trades</a></li>
							<li><a href="{{route('admin.card.types')}}">Card Types</a></li>
						</ul>
					</li>


					<li class="dropdown">
						<a href="javascript:;" class="dropdown-toggle">
							<span class="micon">
								<img src="/images/products/bitcoin.svg" style="height: 2rem;" alt="">
							</span>
							<span class="mtext">Crypto</span>
						</a>
						<ul class="submenu">
							<li><a href="{{route('admin.rates.btc', encrypt('btc'))}}">Bitcoin Rate</a></li>
							<li><a href="{{route('admin.rates.ltc', encrypt('ltc'))}}">Litecoin Rate</a></li>
							<li><a href="{{route('admin.rates.eth', encrypt('eth'))}}">Ethereum Rate</a></li>
						</ul>
					</li>


					<li class="dropdown">
						<a href="javascript:;" class="dropdown-toggle">
							<span class="micon fi-align-justify"></span><span class="mtext">Other Assets</span>
						</a>
						<ul class="submenu">
							<li><a href="{{route('admin.other-asset.trades')}}">Trades</a></li>

							@foreach ($other_assets as $item)
								<li><a href="{{route('admin.other-asset.rates', encrypt($item->id))}}">{{ucfirst($item->name)}} Rate</a></li>
								@endforeach
							
						</ul>
					</li>

										
					<li>
						<a href="{{route('admin.transactions')}}" class="dropdown-toggle no-arrow {{Route::is('admin.transactions') ? 'active' : ''}}">
                            <span class="micon ti-exchange-vertical"></span>
							<span class="mtext">Transactions</span>
						</a>
					</li>

					@if (auth()->user()->role->name == 'admin')
						<li>
							<a href="{{route('peer2peer')}}" class="dropdown-toggle no-arrow {{Route::is('peer2peer') ? 'active' : ''}}">
								<span class="micon fi-arrows-expand"></span>
								<span class="mtext">Peer2Peer</span>
							</a>
						</li>
					@endif

					@if (auth()->user()->role->name == 'admin')
						<li>
							<a href="{{route('admin.config')}}" class="dropdown-toggle no-arrow {{Route::is('admin.config') ? 'active' : ''}}">
								<span class="micon fi-widget"></span>
								<span class="mtext">Configurations</span>
							</a>
						</li>
					@endif

					<li class="dropdown">
						<a href="javascript:;" class="dropdown-toggle">
							<span class="micon fi-wrench"></span><span class="mtext">Toolkits</span>
						</a>
						<ul class="submenu">
							<li><a href="{{route('admin.services')}}">Services</a></li>
							<li><a href="{{route('admin.countries')}}">Country</a></li>
							<li><a href="{{route('admin.message.create')}}">Users Notice</a></li>
							<li><a href="{{route('admin.password')}}">Change Password</a></li>
							@if (auth()->user()->role->name == 'admin')
								<li><a href="{{route('admin.privileadges')}}">Previleadges</a></li>
							@endif
						</ul>
					</li>

				

                     <li>
						<a href="{{route('logout')}}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="dropdown-toggle no-arrow">
                            <i class="micon dw dw-logout1"></i>
							<span class="mtext">Logout</span>
						</a>
                    </li>
					
				</ul>
			</div>
		</div>
	</div>
	<div class="mobile-menu-overlay"></div>