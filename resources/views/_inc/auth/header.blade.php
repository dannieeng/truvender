<div class="header">
	<div class="header-left d-flex justify-content-start align-items-center">
		<div class="menu-icon ion-navicon-round ml-3"></div>
		<div class="brand-logo ml-0" style="">
            <a href="{{route('dashboard')}}" class="flex align-items-center d-lg-none">
                <img src="/images/logo/tru-favicon.png" style="width: 2.6rem; height: 2.6rem;" alt="">
                {{-- <h2 class=" font-weight-bold text-primary mt-2">{{'truvender'}}</h2> --}}
            </a>
        </div>
	</div>
	<div class="header-right">
		<div class="user-notification">
			{{-- @livewire('users.notifications') --}}
		</div>
		<div class="user-info-dropdown">
			<div class="dropdown">
				<a class="dropdown-toggle" href="#" role="button" data-toggle="dropdown">
					<span class="user-icon">
					    @php $user = auth()->user(); @endphp
						<img src="{{$user->profile_photo_path != null ? Storage::url($user->profile_photo_path)  : $user->getProfilePhotoUrlAttribute()}}" alt="">
					</span>
					<span class="user-name">{{$user->username}}</span>
				</a>
				<div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
					<a class="dropdown-item" href="{{route('settings')}}"><i class="dw dw-settings2"></i> Setting</a>
					<a class="dropdown-item" href="{{route('logout')}}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
						<i class="dw dw-logout"></i> Log Out
					</a>
				</div>
			</div>
		</div>
	</div>
</div>
