	<div class="left-side-bar">
		<div class="brand-logo mb-2">
			<a href="{{route('dashboard')}}">
				<img src="/images/logo/tru-logo-dark.png" alt="" class="dark-logo" style="height:2.4rem; ">
				<img src="/images/logo/tru-logo.png" alt="" class="light-logo" style="height:2.4rem;">
				<h4 class="text-white mt-2">truvender</h4>
			</a>
			<div class="close-sidebar" data-toggle="left-sidebar-close">
				<i class="ion-close-round"></i>
			</div>
		</div>
		<div class="menu-block customscroll">
			<div class="sidebar-menu">
				<ul id="accordion-menu">
					<li>
						<a href="{{route('dashboard')}}" class="dropdown-toggle no-arrow {{Route::is('dashboard') ? 'active' : ''}}">
							<span class="micon dw dw-home"></span>
							<span class="mtext">Dashboard </span>
						</a>
					</li>
					
					<li class="dropdown">
						<a href="javascript:;" class="dropdown-toggle">
							<span class="micon ti-wallet"></span><span class="mtext">Wallets</span>
						</a>
						<ul class="submenu">
							<li><a href="{{route('wallet')}}">Major Wallet</a></li>
							<li><a href="{{route('wallet.etherum.index')}}">Ethereum Wallet</a></li>
							<li><a href="{{route('wallet.litcoin.index')}}">Litecoin Wallet</a></li>
						</ul>
					</li>

                    {{-- <li>
						<a href="{{route('wallet')}}" class="dropdown-toggle no-arrow {{Route::is('wallet') ? 'active' : ''}}">
                            <span class="micon ti-wallet"></span>
							<span class="mtext">Wallet </span>
						</a>
                    </li> --}}
                    
                    <li>
						<a href="{{route('products')}}" class="dropdown-toggle no-arrow {{Route::is('products') ? 'active' : ''}}">
                            <span class="micon ti-layout-list-thumb-alt"></span>
							<span class="mtext">Products</span>
						</a>
                    </li>
					
					<li>
						<a href="{{route('transactions.index')}}" class="dropdown-toggle no-arrow {{Route::is('transactions.index') ? 'active' : ''}}">
                            <span class="micon ti-exchange-vertical"></span>
							<span class="mtext">Transactions</span>
						</a>
                    </li>

                    <li>
						<a href="{{route('rates.act')}}" class="dropdown-toggle no-arrow {{Route::is('rates.act') ? 'active' : ''}}">
                           <span class="micon ti-bar-chart-alt"></span>
							<span class="mtext">Rates</span>
						</a>
                    </li>

                    <li>
						<a href="{{route('cards.buy')}}" class="dropdown-toggle no-arrow {{Route::is('cards.index') ? 'active' : ''}}">
                            <span class="micon ti-layout-grid2-alt"></span>
							<span class="mtext">Cards</span>
						</a>
                    </li>

                    <li>
						<a href="{{route('settings')}}" class="dropdown-toggle no-arrow {{Route::is('settings') ? 'active' : ''}}">
                            <span class="micon ti-settings"></span>
							<span class="mtext">Settings</span>
						</a>
                    </li>

                     <li>
						<a href="{{route('logout')}}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="dropdown-toggle no-arrow">
                            <i class="micon dw dw-logout1"></i>
							<span class="mtext">Logout</span>
						</a>
                    </li>
					
				</ul>
			</div>
		</div>
	</div>
	<div class="mobile-menu-overlay"></div>