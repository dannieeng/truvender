@component('mail::message')


    Use the code below to verify your transaction.

    Code: {{ $data['code'] }} 

    Note the above code expires in {{$data['minutes']}} minutes.


Thanks,<br>
{{ config('app.name') }}
@endcomponent
