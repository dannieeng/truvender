@component('mail::message')
# Hello there, 

Thank you for contacting {{config('app.name')}},
we will get back to your through this email

Regards,<br>
{{ config('app.name') }}
@endcomponent
