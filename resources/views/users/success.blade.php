@extends('layouts.dashboard')
@section('title', 'Successful Transaction')

@section('content')
    <div class="row clearfix">
        <div class="col-md-6 offset-md-3">
            <div class="card card-box">
                <div class="card-body">
                    <div class="mt-2 mb-2 text-center">
                        <i class="ion-checkmark-circled text-success animate__animated animate__tada animate__delay-1s animate__repeat-3" style="font-size: 10rem;"></i>
                        <h4 class="font-20">Transaction Success</h4>
                    </div>
                    <div class="mt-3 text-center">
                        
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
@endsection