@extends('layouts.dashboard')
@section('title', 'Trade Card')
@section('styles')
    <style>
        #preview_img img{
            height: 8rem;
        }
    </style>
@endsection
@section('content')

    <div class="row clearfix">
        <div class="col-md-9 offset-md-1">
            <div class="card card-box">
                <div class="card-body">
                    <h4 class="font-20 text-primary">Upload Card Images</h4>
                    
                    <div class="mt-3" >
                      @if (count($errors) > 0)
                            <div class="error mb-2 text-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li class="text-danger">{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <form  method="POST" action="{{route('trade.submit-images', encrypt($card_trade->id))}}" id="my-awesome-dropzone" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                            <div class="col-md-4"></div>
                            <div class="form-group col-md-6 offset-md-3">
                                <div class="custom-file">
                                    <input type="file" id="profileImage" name="images[]" class="custom-file-input" multiple>
                                    <label class="custom-file-label">Choose Images</label>
                                </div>
                                {{-- <input type="file"   class="form-control" multiple=""> --}}
                            </div>
                            </div>
                            <div class="row px-4 mt-4" id="preview_img">
                               
                            </div>
                        </form>
                        <div class="mt-4">
                            <a onclick="event.preventDefault(); document.getElementById('my-awesome-dropzone').submit();" class="btn btn-block btn-outline-info btn-sm" type="submit">Submit</a>
                            <a href="{{url()->previous()}}" class="mt-2 btn btn-sm btn-block btn-outline-danger">Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')

	<script>

        $(document).ready(function(){
         $('#profileImage').on('change', function(){ //on file input change
            if (window.File && window.FileReader && window.FileList && window.Blob) //check File API supported browser
            {
        
                var data = $(this)[0].files; //this file data
                 
                $.each(data, function(index, file){ //loop though each file
                    if(/(\.|\/)(gif|jpe?g|png)$/i.test(file.type)){ //check supported file type
                        var fRead = new FileReader(); //new filereader
                        fRead.onload = (function(file){ //trigger function on successful read
                        return function(e) {
                            var img = $('<img/>').addClass('thumb col-md-4 col-6 c mb-2').attr('src', e.target.result); //create image element 
                            $('#preview_img').append(img); //append image to output element
                        };
                        })(file);
                        fRead.readAsDataURL(file); //URL representing the file's data.
                    }
                });
                 
            }else{
                alert("Your browser doesn't support File API!"); //if File API is absent
            }
         });
        });
    </script>
    

@endsection