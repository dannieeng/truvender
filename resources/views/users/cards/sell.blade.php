@extends('layouts.dashboard')
@section('title', 'GiftCards')
@section('styles')
    <style>
        .card{
            padding: 1rem 0;
        }
        .car-img{
            height: 6.2rem;
            width: 5.6rem;
            padding: 0 2rem;
        }
    </style>
@endsection
@section('title', 'Available Cards')

@section('content')

    <div class="d-flex justify-content-start mb-4">
        <a href="{{route('cards.buy')}}" class="btn {{Route::is('cards.buy') ? 'btn-primary' : 'btn-outline-primary'}} mr-3" > Buy GiftCards</a>
        <a href="{{route('cards.sell')}}" class="btn {{Route::is('cards.sell') ? 'btn-primary' : 'btn-outline-primary'}}"> Sell GiftCards</a>
    </div>

    @livewire('users.cards.sell')
@endsection