@extends('layouts.dashboard')
@section('title', 'Trade Card')
@section('styles')
    
@endsection
@section('content')

    <div class="row clearfix">
        <div class="col-md-6 offset-md-2">
            <div class="card card-box">
                <div class="card-body">
                    <h4 class="mb-3">Trade Card</h4>
                    <div class="img"> 
                        <img src="{{Storage::url($card->image)}}" alt="" class="card-img"> 
                    </div>
                    <div class="mt-3">
                       
                        @livewire('users.cards.trade', ['card' => $card, 'type' => $type])
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')


    <script>
        function check_custom(){
            var value_selected = $("#select_price").val();
            if(value_selected == 'other'){
                $("#custom").removeClass("d-none");
            }else{
                $("#custom").addClass("d-none");
            }
        }
        Dropzone.autoDiscover = false;
		$(".dropzone").dropzone({
			addRemoveLinks: true,
            maxFilesize: 2,
            dictDefaultMessage: "Drop Images or Reciepts here",
			removedfile: function(file) {
				var name = file.name;
				var _ref;
				return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
			}
		});
    </script>
@endsection