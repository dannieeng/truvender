@extends('layouts.dashboard')
@section('title', 'Peer to Peer Withdrawal')

@section('content')
    <div class="row clearfix">
        <div class="col-md-6 offset-md-2">
            <div class="card card-box">
                <div class="card-body">
                    
                    @livewire('users.transfer.peer2-peer')
                    
                </div>
            </div>
        </div>
    </div>
@endsection