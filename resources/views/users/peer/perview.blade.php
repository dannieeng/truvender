@extends('layouts.dashboard')
@section('title', 'Peer to Peer Withdrawal')

@section('content')
    <div class="row clearfix">
        <div class="col-md-6 offset-md-2">
            <div class="card card-box">
                <div class="card-body">
                    
                    <div>
                        <h4 class="font-20 mb-4">
                            Peer to Peer Withdraw
                        </h4>

                        <div class="mb-2 py-3 rounded">
                            <p class="mb-0">Amount: </p>
                            <h1>&#8358; {{number_format($data['amount'], 2)}}</h1>
                        </div>

                        <p class="mb-1">Transaction info</p>
                        <ul class="list-group py-3">
                            <li class="list-group-item">
                                <div class="d-flex justify-content-between align-items-center">
                                    <p class=" font-16 mb-0">Account Number:</h4>
                                    <p class=" font-16 mb-0 font-weight-bold">{{$data['account_number']}}</p>
                                </div>
                            </li>

                            <li class="list-group-item">
                                <div class="d-flex justify-content-between align-items-center">
                                    <p class=" font-16 mb-0">Account Name:</h4>
                                    <p class=" font-16 mb-0 font-weight-bold">{{$data['account_name']}}</p>
                                </div>
                            </li>

                            <li class="list-group-item">
                                <div class="d-flex justify-content-between align-items-center">
                                    <p class=" font-16 mb-0">Bank:</h4>
                                    <p class=" font-16 mb-0 font-weight-bold">{{$data['bank']->name}}</p>
                                </div>
                            </li>

                            <li class="list-group-item">
                                <div class="d-flex justify-content-between align-items-center">
                                    <p class=" font-16 mb-0">Transaction Fee:</h4>
                                    <p class=" font-16 mb-0 font-weight-bold">{{number_format($fee,2)}}</p>
                                </div>
                            </li>

                            
                        </ul>
                        
                        @livewire('users.transfer.peer', ['data' => $data])
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
@endsection