@extends('layouts.dashboard')
@section('title', 'Trading Rates')
@section('styles')
    <style>
        .nav-item{
            font-weight: 500;
            font-size: 1.4rem;
        }
    </style>
@endsection
@section('content')
 
<div class="row clearfix">
    <div class="col-md-9 offset-md-2 mb-30">
        <div class="pd-20 card-box">
            <h5 class="h4 text-blue mb-20">Rates Calculator</h5>
            <div class="row clearfix">
                <div class="col-md-8">
                    <div class="tab">
                        <ul class="nav nav-tabs customtab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#buy" role="tab" aria-selected="true">Buyer Rate</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#sell" role="tab" aria-selected="false">Seller Rate</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane fade show active" id="buy" role="tabpanel">
                                @livewire('users.rates.buy')
                            </div>
                            <div class="tab-pane fade" id="sell" role="tabpanel">
                                @livewire('users.rates.sell')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>    
</div>
@endsection

