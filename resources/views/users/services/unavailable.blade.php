@extends('layouts.dashboard')
@section('title', 'Service Unavailable')

@section('content')
    <div class="row clearfix">
        <div class="col-md-6 offset-md-3">
            <div class="card card-box">
                <div class="card-body">
                    <div class="mt-2 mb-2 text-center">
                        <i class="ion-close-circled text-danger animate__animated animate__tada animate__delay-1s animate__repeat-3" style="font-size: 10rem;"></i>
                        <h4 class="font-20">Service is Not Available</h4>
                        <p class="mt-2">Sorry this service is currently unavailable, we do apologise for the inconvinience</p>
                    </div>
                   
                </div>
            </div>
        </div>
    </div>
@endsection