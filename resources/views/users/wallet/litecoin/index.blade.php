@extends('layouts.dashboard')
@section('styles')
    <style>
        .d-relate{
            position: relative;
        }

        .back-icon{
            position: absolute;
            opacity: 0.5;
            right: 0;
            margin-right: 3rem;
            font-size: 3.6rem;
            border-radius: 50%;
            padding: 0 20px;
        
        }
        .back-icoin img{
            height: 3.8rem;
            width: 3.8rem;
        }

        .nav-item{
            font-weight: 500;
            font-size: 1.2rem;
        }
        .currency{
            padding: 5px 10px;
            border-radius: 120px;
            font-weight: 800;
            font-size: 20px; 
        }
        .ltc{
            background-color: #94d05e;
            color: azure; 
        }
        .btc{
            color: azure;
            background-color: #eb9d48;
        }
    </style>

@endsection
@section('title', 'My Litcoin Wallet')

@section('content')
    <div class="row clearfix">
        {{-- Wallet Cards --}}
        <div class="col-md-4 col-sm-12 col-lg-3">

            <div class="card card-box">
                <a href="{{route('wallet.litcoin.info')}}">
                    <div class="card-body d-relate">
                        <h5 class="card-title">Litecoin Wallet</h5>
                        <div class="back-icon">
                            <img src="/images/products/litecoin.svg" style="height: 4rem;" alt="">
                        </div>
                    
                        <h5 class="font-weight-light font-16 mt-4">Available Balance 
                            <span class=" font-20 dw dw-question"  data-toggle="tooltip" data-placement="right" title="Balance available for use or withdraw"></span>
                        </h5>
                        <h1 class="mt-2 mb-2 font-weight-bold font-30">{{$wallet->balance}}<span class=" font-14">LTC</span></h1>
                    </div>
                </a>
            </div>

            <div class="card card-box mt-4 ">
                <div class="card-body">
                    <div class="">
                        <a href="{{route('trade.litcoin')}}" class="btn btn-block btn-outline-info btn-sm"> Trade </a>
                        <a href="#" data-toggle="modal" data-target="#info_modal" class="btn btn-block btn-outline-info btn-sm"> Info </a>
                        <a href="#" data-toggle="modal" data-target="#ltc_modal" class="btn btn-block btn-outline-info btn-sm"> Send/Recieve </a>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-8 col-lg-9 col-sm-12 mt-4 mt-lg-0">
            <div class="card car-box">
                <div class="card-body">
                    <div class="clearfix mb-20">
                        <div class="pull-left">
                            <h4 class="text-blue h4">Transactions</h4>
                        </div>
                    </div>

                     @if ($transactions->count() > 0)
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th scope="col">Transaction ID</th>
                                        <th scope="col">Transaction Amount</th>
                                        <th scope="col">Transaction Type</th>
                                        <th scope="col">Transaction Status</th>
                                        <th scope="col">Date</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($transactions as $transaction)
                                        <tr>
                                            <th scope="row">{{$transaction->trxn_ref}}</th>
                                            <td>{{ 'LTC' . number_format($transaction->amount, 2)}}</td>
                                            <td>{{$transaction->type}}</td>
                                            <td>
                                                @if ($transaction->status == 'success')
                                                    <span class="text-success font-weight-bold">Success</span>
                                                @elseif($transaction->status == 'failed')
                                                    <span class="text-danger font-weight-bold">failed</span>
                                                @else
                                                    <span class="text-warning font-weight-bold">pending</span>
                                                @endif
                                            </td>
                                            <td>{{now()->parse($transaction->created_at)->diffForHumans()}}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    @else
                        <div class="p-30 d-flex justify-content-center">
                            <div class="mb-4">
                                <img src="/images/bg/empty.svg" style="height: 20rem; width:20rem;" alt="">
                                <h5 class="font-weight-normal text-center text-primary">No Transactions</h5>
                                {{-- <p class="text-center">Transactions will be here: <a href="" class="text-primary">start transaction</a></p> --}}
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>

            <!-- LTC modal -->
            <div class="modal fade" id="ltc_modal" tabindex="-1" role="dialog" aria-labelledby="sendandreceive" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content">
                        
                        <div class="modal-body">
                            <div class="tab">
                                <ul class="nav nav-tabs customtab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" data-toggle="tab" href="#send" role="tab" aria-selected="false">Send</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#recieve" role="tab" aria-selected="true">Recieve</a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    
                                    <div class="tab-pane fade show active" id="send" role="tabpanel">
                                        @livewire('users.litecoin.transfer')
                                    </div>
        
                                    <div class="tab-pane fade" id="recieve" role="tabpanel">
                                        <div class="row clearfix mt-4 mb-4">
                                            <div class="col">
                                                <div class="text-center d-none d-lg-block">
                                                    <label> Scan the QR code</label>
                                                    {{ QrCode::size(400)->generate($wallet->address)}}   
                                                </div>
                                                <div class="text-center d-lg-none">
                                                    <label> Scan the QR code</label>
                                                    {{ QrCode::size(300)->generate($wallet->address)}}   
                                                </div>
                                            </div>
                                            <div class="form-group form-inline row mt-3" style="margin: 0 auto;">
                                                <input type="text" id="address" value="{{$wallet->address}}" readonly class="form-control">
                                                <button class="btn btn-info form-control" onclick="event.preventDefault(); copy_address();">Copy Address</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <button type="button" class="btn btn-outline-danger btn-sm btn-block" data-dismiss="modal">Close</button>
                        </div>
                        
                    </div>
                </div>
            </div>

            {{-- LTC Info Modal --}}

            <div class="modal fade" id="info_modal" tabindex="-1" role="dialog" aria-labelledby="walletInfo" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content">
                        
                        <div class="modal-body">
                            @livewire('users.litecoin.info')
                            <button type="button" class="btn btn-outline-danger btn-sm btn-block" data-dismiss="modal">Close</button>
                        </div>
                        
                    </div>
                </div>
            </div>
    </div>


    <script>

        function copy_address(value){
            var address = document.getElementById('address');
            address.select();
            document.execCommand("copy");
        }
    </script>


@endsection