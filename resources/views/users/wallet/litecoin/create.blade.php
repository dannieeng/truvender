@extends('layouts.dashboard')
{{-- @section('styles')
    <style>
        .currency{
            padding: 5px 10px;
            border-radius: 120px;
            font-weight: 800;
            font-size: 20px; 
        }
        .ngn{
            background-color: #94d05e;
            color: azure; 
        }
        .btc{
            color: azure;
            background-color: #eb9d48;
        }
    </style>
@endsection --}}

@section('title', 'Create Litcoin Wallet')

@section('content')
     <!-- Responsive tables Start -->
    <div class="pd-20 card-box mb-30">
        <div class="clearfix mb-20">
            <div class="pull-left">
                <h4 class="text-blue h4">Litcoin Wallet</h4>
            </div>
        </div>

        <div class="p-30 d-flex justify-content-center">
            <div class="mb-4">
                <img src="/images/bg/empty.svg" style="height: 20rem; width:20rem;" alt="">
                <h5 class="font-weight-normal text-center text-primary">Sorry No Litcoin Wallet Found</h5>
                <p class="text-center">Click to <a href="#" onclick="event.preventDefault(); document.getElementById('litcoin-wallet').submit();" class="text-primary">create litcoin wallet</a></p>

                <form method="POST" action="{{route('wallet.litcoin.create.save')}}" style="display: none;" id="litcoin-wallet">
                    @csrf
                </form>
            </div>
        </div>
    </div>
    <!-- Responsive tables End -->
@endsection