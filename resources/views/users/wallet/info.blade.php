@extends('layouts.dashboard')
@section('styles')
    <style>
        .d-relate{
            position: relative;
        }

        .back-icon{
            position: absolute;
            opacity: 0.5;
            right: 0;
            margin-right: 3rem;
            font-size: 3.6rem;
            border-radius: 50%;
            padding: 0 20px;
        
        }
    </style>
@endsection
@section('title', 'My Wallet')

@section('content')
    <div class="row clearfix">
        <div class="col-md-6">
            <div class="card card-box">
                <div class="card-body d-relate">
                    <h5 class="card-title">Naira Wallet</h5>
                    <div class="back-icon bg-dark">
                        <span class=" text-success ">&#8358;</span>
                    </div>
                
                    <h5 class="font-weight-light mt-4">Available Balance 
                        <span class=" font-20 dw dw-question"  data-toggle="tooltip" data-placement="right" title="Balance available for use or withdraw"></span>
                    </h5>
                    <h1 class="mt-2 mb-2 font-weight-bold"><span class="">&#8358;</span>0.00</h1>
                    <div class="mt-4 d-flex justify-content-start">
                        <a href="#" class="btn btn-sm btn-rounded btn-outline-info">View More</a>
                    </div>
                </div>
            </div>
        </div>

         <div class="col-md-6 mt-4 mt-md-0">
            <div class="card card-box">
                <div class="card-body d-relate">
                    <h5 class="card-title">Bitcoin Wallet</h5>
                    <div class="back-icon bg-success">
                        <i class="fa fa-bitcoin text-warning" aria-hidden="true"></i>
                    </div>
                    <h5 class="font-weight-light mt-4">Available Balance </h5>
                    <h2 class="mt-2 mb-2 font-weight-bold">0.00</h2>
                    <h5 class=" font-weight-normal">0.00USD</h5>
                    <div class="mt-3 d-flex justify-content-start">
                        <a href="#" class="btn btn-sm btn-rounded btn-outline-info">View More</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection