@extends('layouts.dashboard')
@section('title', 'Tranfer')
@section('content')
    <div class="row clearfix">
        <div class="col-md-6 offset-md-3">
            <div class="card card-box">
                <div class="card-body">
                    <h4 class="font-18 mb-4">Transfer to My Bank Account</h4>
                    <div class="mt-2">
                        <form action="{{route('transfer.my-account')}}" method="POST">
                            @csrf
                            <div class="form-group">
                                <label>Amount</label>
                                <div class="position-relative mb-1">
                                    <span class="position-absolute font-weight-bold font-18 mt-2 ml-2">&#8358;</span>
                                        <input type="text" class="form-control pl-4" name="amount">
                                    </div>
                                    @error('amount')
                                        <span class="text-danger font-16">{{$message}}</span>
                                    @enderror
                            </div>

                            <div class="mt-4">
                                <button type="submit" class="btn btn-sm btn-block btn-outline-info">Continue</button>
                                <a href="{{url()->previous()}}" class="btn btn-sm btn-block btn-outline-danger">Cancel</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection