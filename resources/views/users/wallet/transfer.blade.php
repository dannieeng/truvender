@extends('layouts.dashboard')
@section('title', 'Transfer Fund')

@section('content')
    <div class="row clearfix">
        <div class="col-md-6 offset-md-2">
            <div class="card card-box">
                <div class="card-body">
                    @if ($transfer_to == 'truvender')
                        @livewire('users.transfer.truvender')
                    @elseif($transfer_to == 'other_account')
                        @livewire('users.transfer.other-account')
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection