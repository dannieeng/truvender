@extends('layouts.dashboard')
@section('styles')
    <style>
        .d-relate{
            position: relative;
        }

        .back-icon{
            position: absolute;
            opacity: 0.5;
            right: 0;
            margin-right: 3rem;
            font-size: 3.6rem;
            border-radius: 50%;
            padding: 0 20px;
        
        }

        .nav-item{
            font-weight: 500;
            font-size: 1.2rem;
        }
        .currency{
            padding: 5px 10px;
            border-radius: 120px;
            font-weight: 800;
            font-size: 20px; 
        }
        .ngn{
            background-color: #94d05e;
            color: azure; 
        }
        .btc{
            color: azure;
            background-color: #eb9d48;
        }
    </style>

@endsection
@section('title', 'My Wallet')

@section('content')
    @livewire('users.wallet')
@endsection