@extends('layouts.dashboard')

@section('title', 'Settings')
@section('styles')
    <style>
        .profile-pic {
            max-width: 260px;
            max-height: 260px;
            display: block;
        }
        .file-upload {
            display: none;
        }
        .circle {
            border-radius: 20px !important;
            overflow: hidden;
            width: 260px;
            height: 260px;
            border: 3px dotted rgba(96, 102, 187, 0.691);
            position: relative;
            top: 2px;
        }
        img {
            max-width: 100%;
            height: 100%;
        }

    </style>
@endsection
@section('content')
<div class="mb-4">
    <h1 class="font-30">
        Account Settings
    </h1>
</div>
    <div class="row">
        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 mb-30">
            <div class="pd-20 card-box height-100-p">
                <div class="profile-photo">
                    <a href="modal" data-toggle="modal" data-target="#modal" class="edit-avatar"><i class="fa fa-pencil"></i></a>
                    <img 
                        src="{{$user->profile_photo_path != null ? Storage::url($user->profile_photo_path)  : $user->getProfilePhotoUrlAttribute()}}" 
                        alt="" class="avatar-photo">
                    <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-body">
                                    <h4 class="font-18 font-weight-bold">Update Profile Picture</h4>
                                    <div class="mt-1 mb-1">
                                        @livewire('users.settings.profile-upload', ['user' => $user])
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
                <h5 class="text-center h5 mb-2">{{$user->username}}</h5>
                
                <div class="profile-info">
                    <h5 class="mb-20 h5 text-blue">Account Information</h5>
                    <ul>
                        <li>
                            <span>First Name: </span>
                            <h4 class="font-18 font-weight-normal font-16">{{$profile == true ? $profile->firstname : 'N/A'}}</h4>
                        </li>

                        <li>
                            <span>Last Name: </span>
                            <h4 class="font-18 font-weight-normal font-16">{{$profile == true ? $profile->lastname : 'N/A'}}</h4>
                        </li>
                        <li>
                            <span>Phone Number: </span>
                            <h4 class="font-18 font-weight-normal font-16">{{$profile == true ? $profile->phone : 'N/A'}}</h4>
                        </li>
                         <li>
                            <span>Email Address: </span>
                            <h4 class="font-18 font-weight-normal font-16">{{$user->email}}</h4>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 mb-30">
            <div class="card-box height-100-p overflow-hidden">
                <div class="profile-tab height-100-p">
                    <div class="tab height-100-p">
                        <ul class="nav nav-tabs customtab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#profile" role="tab">Profile</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#banking" role="tab">Banking</a>
                            </li>
                            
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#verify" role="tab">Verification</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#notify" role="tab">Advance Settings</a>
                            </li>

                            
                        </ul>
                        <div class="tab-content">
                            <!-- Profile Tab start -->
                            <div class="tab-pane fade show active" id="profile" role="tabpanel">
                                <div class="pd-20">
                                    <div class="container pd-0">
                                        <h5 class="font-16">Profile Setting</h5>
                                        <div class="mt-2">
                                            <div class="row">
                                                <div class="col-md-6 form-group">
                                                    <label>First Name: </label>
                                                    <input type="text" value="{{$profile->firstname}}" readonly class="form-control">
                                                </div>

                                                <div class="col-md-6 form-group">
                                                    <label>Last Name: </label>
                                                    <input type="text" value="{{$profile->lastname}}" readonly class="form-control">
                                                </div>

                                                <div class="col-md-6 form-group">
                                                    <label>Phone: </label>
                                                    <input type="text" value="{{$profile->phone}}" readonly class="form-control">
                                                </div>
                                            </div>

                                            <div class="d-flex justify-content-end">
                                                <button class="btn btn-sm btn-info"  data-toggle="modal" data-target="#edit-profile ">Edit Profile</button>
                                            </div>

                                            <div class="mt-4">
                                                <h4 class="font-16">Referral: </h4>
                                                <div class="row mt-2">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label>Referral link</label>
                                                                <input type="text" value="{{route('register').'/?ref='.$user->referral_token}}" id="referral-link" disabled class="form-control">
                                                            <a href="#" class="btn btn-info btn-sm mt-2 btn-block" onclick=" event.preventDefault(); copyToClipboard();" data-toggle="tooltip" title="Copy To Clipboard">Copy</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                         <!-- add edit profile popup start -->
                                            <div class="modal fade customscroll" id="edit-profile" tabindex="-1" role="dialog">
                                                <div class="modal-dialog modal-dialog-centered" role="document">
                                                    <div class="modal-content">
                                                        
                                                        <div class="modal-body">
                                                            <h5 class="font-16">Edit Profile</h5>
                                                            @livewire('users.settings.profile')
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        <!-- add edit profile popup End -->
                                    </div>
                                </div>
                            </div>
                            <!-- Profile Tab End -->
                            <!-- Banking Tab start -->
                            <div class="tab-pane fade" id="banking" role="tabpanel">
                                <div class="pd-20 profile-task-wrap">
                                    <div class="container pd-0">
                                        <!-- Open Task start -->
                                        @if ($banking == false)
                                            <div class="task-title row align-items-center">
                                                <div class="col-md-8 col-sm-12">
                                                    <h5 class="font-16 font-weight-bold">Banking Information</h5>
                                                    <p class="mt-1 mb-2">No Bank account added</p>
                                                </div>
                                                <div class="col-md-4 col-sm-12 text-right">
                                                    <a href="task-add" data-toggle="modal" data-target="#task-add" class="bg-light-blue btn text-blue weight-500"><i class="ion-plus-round"></i> Add</a>
                                                </div>
                                            </div>

                                        @else
                                            <div class="task-title row align-items-center">
                                                <div class="col-md-8 col-sm-12">
                                                    <h5 class="font-16">Banking Information</h5>
                                                    <p class="mt-1 mb-2">Bank account</p>
                                                    <div class="mt-2">
                                                        <h5 class="font-16 mb-1">Account Name: <span class="font-weight-normal">{{$banking->acc_name}}</span></h5>
                                                        <h5 class="font-16">Account Number: <span class="font-weight-normal">{{$banking->acc_number}}</span></h5>
                                                        <h5 class="font-16">Account Bank: <span class="font-weight-normal">{{$banking->bank->name}}</span></h5>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif


                                       
                                        <!-- add task popup start -->
                                        <div class="modal fade customscroll" id="task-add" tabindex="-1" role="dialog">
                                            <div class="modal-dialog modal-dialog-centered" role="document">
                                                <div class="modal-content">
                                                   
                                                    <div class="modal-body">
                                                        <h5 class="font-16">Add Banking Information</h5>
                                                        @livewire('users.settings.banking', ['user' => $user])
                                                    </div>
                                                   
                                                </div>
                                            </div>
                                        </div>
                                        <!-- add task popup End -->
                                    </div>
                                </div>
                            </div>
                            <!-- Banking Tab End -->
                            <!-- Notify Tab start -->
                            <div class="tab-pane fade height-100-p" id="notify" role="tabpanel">
                                <div class="profile-setting">
                                    @livewire('users.settings.notify', ['user' => $user, 'setting' => $settings])
                                </div>
                            </div>
                            <!-- Notify Tab End -->

                            <!-- Tasks Tab End -->
                            <!-- Verify Tab start -->
                            <div class="tab-pane fade height-100-p" id="verify" role="tabpanel">
                                @livewire('users.settings.verify', ['user' => $user, 'banking' => $banking])

                                <!-- verify modal start -->
                                <div class="modal fade customscroll" id="task-bvn" tabindex="-1" role="dialog">
                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                        <div class="modal-content">
                                            
                                            <div class="modal-body">
                                                <h5 class="font-16">Add Banking Information</h5>
                                                <div class="mt-3">
                                                    @livewire('users.settings.verify-bvn')
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Verify Tab End -->

                             <!-- Verify Tab start -->
                            <div class="tab-pane fade height-100-p" id="security" role="tabpanel">
                                {{-- @livewire('users.settings.verify', ['user' => $user, 'banking' => $banking]) --}}

                                <!-- verify modal start -->
                                <div class="modal fade customscroll" id="task-bvn" tabindex="-1" role="dialog">
                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                        <div class="modal-content">
                                            
                                            <div class="modal-body">
                                                <h5 class="font-16">Add Banking Information</h5>
                                                <div class="mt-3">
                                                    {{-- @livewire('users.settings.verify-bvn') --}}
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Verify Tab End -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    {{-- <script src="/auth/vendors/scripts/script.min.js"></script>
	<script src="/auth/vendors/scripts/process.js"></script>
	<script src="/auth/vendors/scripts/layout-settings.js"></script> --}}
    
	{{-- <script src="/auth/vendors/scripts/layout-settings.js"></script> --}}
	{{-- <script src="/auth/src/plugins/dropzone/src/dropzone.js"></script> --}}
	<script>
        $(".upload-button").on('click', function() {
            document.getElementById('file-upload').click();
        });
        // $(".file-upload").on('change', function(){
        //     readURL(this);
        // });
        
        // var readURL = function(input) {
        //     if (input.files && input.files[0]) {
        //         var reader = new FileReader();

        //         reader.onload = function (e) {
        //             $('.profile-pic').attr('src', e.target.result);
        //         }
        
        //         reader.readAsDataURL(input.files[0]);
        //     }
        // }
    
        
        function viewbvn() {
            var x = document.getElementById("bvn");
            var icon = document.getElementById("eye-icon");
            if (x.type === "password") {
                icon.classList.add("fa-eye-slash");
                icon.classList.remove("fa-eye");
                x.type = "text";
            } else {

                icon.classList.remove("fa-eye-slash");
                icon.classList.add("fa-eye");
                x.type = "password";
            }
        }

        
        const copyToClipboard = () => {
            const el = document.createElement('textarea');
            el.value = "{{config('app.url')}}/register/?ref={{Auth::user()->referral_token}}";
            el.setAttribute('readonly', '');
            el.style.position = 'absolute';
            el.style.left = '-9999px';
            document.body.appendChild(el);
            el.select();
            document.execCommand('copy');
            document.body.removeChild(el);
        };
    </script>
    
	{{--  --}}
@endsection