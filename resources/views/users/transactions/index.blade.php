@extends('layouts.dashboard')
@section('styles')
    <style>
        .currency{
            padding: 5px 10px;
            border-radius: 120px;
            font-weight: 800;
            font-size: 20px; 
        }
        .ngn{
            background-color: #94d05e;
            color: azure; 
        }
        .btc{
            color: azure;
            background-color: #eb9d48;
        }
    </style>
@endsection

@section('title', 'Transactions')

@section('content')
     <!-- Responsive tables Start -->
    <div class="pd-20 card-box mb-30">
        <div class="clearfix mb-20">
            <div class="pull-left">
                <h4 class="text-blue h4">Transactions</h4>
            </div>
        </div>
        @if ($transactions->count() > 0)
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col">Transaction ID</th>
                            <th scope="col">Transaction Amount</th>
                            <th scope="col">Transaction Type</th>
                            <th scope="col">Transaction Status</th>
                            <th scope="col">Date</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($transactions as $transaction)
                             <tr>
                                <th scope="row">{{$transaction->trxn_ref}}</th>
                                <td>{{ 'NGN' . number_format($transaction->amount, 2)}}</td>
                                <td>{{$transaction->type}}</td>
                                <td>
                                    @if ($transaction->status == 'success')
                                        <span class="text-success font-weight-bold">Success</span>
                                    @elseif($transaction->status == 'failed')
                                        <span class="text-danger font-weight-bold">failed</span>
                                    @else
                                        <span class="text-warning font-weight-bold">pending</span>
                                    @endif
                                </td>
                                <td>{{now()->parse($transaction->created_at)->diffForHumans()}}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        @else
            <div class="p-30 d-flex justify-content-center">
                <div class="mb-4">
                    <img src="/images/bg/box.svg" style="height: 20rem; width:20rem;" alt="">
                    <h5 class="font-weight-normal text-center text-primary">No Transactions</h5>
                    <p class="text-center">Transactions will be here: <a href="" class="text-primary">start transaction</a></p>
                </div>
            </div>
        @endif
    </div>
    <!-- Responsive tables End -->
@endsection