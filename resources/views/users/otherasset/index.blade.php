@extends('layouts.dashboard')
@section('title', 'Trade ' . ucfirst($asset->name))
@section('content')
    
    <div class="row clearfix">
        <div class="col-md-6 offset-md-3">
            <div class="card card-box">
                <div class="card-body">
                    <div class="mb-4">
                        <h3 class=" font-18 text-center mb-3">{{ucfirst($asset->name)}}</h3>

                        <div class="d-flex justify-content-center"> 
                            <img src="{{$asset->image_path}}" style="height: 14rem; width: 14rem; margin: 0 auto;" alt="" class="card-img"> 
                        </div>
                        <div class="mt-3">
                        
                            @livewire('users.other-asset.index', ['asset' => $asset])
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection