@extends('layouts.dashboard')
@section('title', ucfirst($asset->name).' Funds → Naira NGN')

@section('content')
    
    <div class="row clearfix">
        <div class="col-md-6 offset-md-3">
            <div class="card card-box">
                <div class="card-body">
                    <div class="mb-4">
                        <h3 class=" font-18 text-center mb-3">
                            {{ucfirst($asset->name)}} Funds → Naira NGN
                        </h3>

                        <div class="text-right mb-3">
                            <span class="font-18 text-white bg-primary px-2 py-2 rounded font-weight-bold" id="display"></span>
                        </div>
                        <div class="d-flex justify-content-between align-items-center border rounded py-3 px-lg-4 px-2"> 
                            <img src="{{$asset->image_path}}" style="height: 6rem; width: 6rem; border-radius:10px;" alt="" class="card-img"> 
                            <div class="ml-1 class-name text-right">
                                <h3 class=" font-16">{{ucfirst($asset->name)}}</h3>
                                <h3 class="font-20">&#8358;{{number_format($rate, 2)}}</h3>
                                <h4 class="font-18 font-weight-normal">{{$data['currency'].number_format($amount, 2)}}</h4>
                            </div>
                        </div>

                        
                        @switch($asset->name)
                            @case('cashapp')
                                <div class="mt-3">
                                    <div class="mb-2 alert alert-warning">
                                        <i class="icon-copy ion-information-circled"></i> <span class="font-16 font-weight-bold">This page is not to be exited and exchange process takes 25 minutes.</span>
                                    </div>

                                    <div class="alert alert-info">
                                        <h3 class="font-16">Instructions:</h3>
                                        <ul>
                                            <li><span class="font-18 font-weight-bold">*</span> Make sure the balance ( money ) is available in your cashapp before opening this trade ❗️</li>
                                            <li><span class="font-18 font-weight-bold">*</span> No bank Or card payment is allowed so follow the first step because most bank might get account locked </li>
                                            <li><span class="font-18 font-weight-bold">*</span> Once you make payment, upload screenshot for proof</li>
                                            <li><span class="font-18 font-weight-bold">*</span> Wait for  confirmation </li>
                                            <li><span class="font-18 font-weight-bold">*</span> Once payment is completed, you get your Naira Wallet Credited very easy .Thank you ✅</li>
                                        </ul>
                                    </div> 
                                </div>
                                @break
                            @case('paypal')
                                <div class="mt-3">
                                    <div class="mb-2 alert alert-warning">
                                        <i class="icon-copy ion-information-circled"></i> <span class="font-16 font-weight-bold">This page is not to be exited and exchange process takes 25 minutes.</span>
                                    </div>

                                    <div class="alert alert-info">
                                        <h3 class="font-16">Instructions:</h3>
                                        <ul>
                                            <li><span class="font-18 font-weight-bold">*</span> Upload the payment receipt and click paid «Bid status» page, where the status of your transfer will be shown.</li>
                                            <li><span class="font-18 font-weight-bold">*</span> If your funds is on hold kindly know that you may have to wait for 24 hours once the payment is available on the PayPal you get your Naira Wallet credited. </li>
                                            <li><span class="font-18 font-weight-bold">*</span> Please do send the payment as family and friends only.</li>
                                            <li><span class="font-18 font-weight-bold">*</span> Avoid echeque  </li>
                                            <li><span class="font-18 font-weight-bold">*</span> No Goods and service are not acceptable</li>
                                        </ul>
                                    </div> 
                                </div>
                                @break

                            @case('perfect money')
                                <div class="mt-3">
                                    <div class="mb-2 alert alert-warning">
                                        <i class="icon-copy ion-information-circled"></i> <span class="font-16 font-weight-bold">This page is not to be exited and exchange process takes 25 minutes.</span>
                                    </div>

                                    <div class="alert alert-info">
                                        <h3 class="font-16">Instructions:</h3>
                                        <ul>
                                            <li><span class="font-18 font-weight-bold">*</span> PERFECT MONEY TO PERFECT MONEY ONLY</li>
                                            <li><span class="font-18 font-weight-bold">*</span> FEES ON YOUR YOU SIDE </li>
                                            <li><span class="font-18 font-weight-bold">*</span> AVOID SENDING GIFT CARD OR E-VOUCHER</li>
                                            <li><span class="font-18 font-weight-bold">*</span> Once you make payment, Upload screenshot for proof please do wait for  confirmation  </li>
                                            <li><span class="font-18 font-weight-bold">*</span> once payment is completed, you get your Naira Wallet Credited very easy </li>
                                        </ul>
                                    </div> 
                                </div>
                                @break

                            @case('wechat')
                                
                                <div class="mt-3">
                                    <div class="mb-2 alert alert-warning">
                                        <i class="icon-copy ion-information-circled"></i> <span class="font-16 font-weight-bold">This page is not to be exited and exchange process takes 25 minutes.</span>
                                    </div>

                                    <div class="alert alert-info">
                                        <h3 class="font-16">Instructions:</h3>
                                        <ul>
                                            <li><span class="font-18 font-weight-bold">*</span>  Once you make payment, Upload screenshot for proof</li>
                                            <li><span class="font-18 font-weight-bold">*</span> wait for  confirmation  </li>
                                            <li><span class="font-18 font-weight-bold">*</span> Please do send the payment as family and friends only.</li>
                                            <li><span class="font-18 font-weight-bold">*</span> once payment is completed, you get your Naira Wallet Credited very easy .Thank you ✅  </li>
                                        </ul>
                                    </div> 
                                </div>

                                @break
                            @default
                                
                        @endswitch
                        


                        <div class="mt-3">
                            <form action="{{route('trade.other-asset.post')}}" enctype="multipart/form-data" method="POST">
                                @csrf
                                    <div class="row">
                                        <div class="form-group col-12">
                                            <label>Upload a Reciept or Screenshot</label>
                                            <div class="custom-file">
                                                <input type="file" id="profileImage" name="reciept" class="custom-file-input">
                                                <label class="custom-file-label">Upload Transaction Reciept</label>
                                            </div>
                                            @error('reciept')
                                                <span class="text-danger font-14">{{$message}}</span>
                                            @enderror
                                        </div>
                                        
                                        <input type="hidden" name="data" value="{{encrypt($data)}}">

                                    </div>
                                <div class="mt-4">
                                    <button class="btn btn-block btn-outline-info btn-sm" type="submit">Submit</button>
                                    <a href="#" onclick="event.preventDefault(); document.getElementById('cancel-process').submit();" class="mt-2 btn btn-sm btn-block btn-outline-danger">Cancel</a>
                                </div>
                            </form>


                            <form action="{{route('trade.other-asset.cancel')}}" method="POST" id="cancel-process" class=" d-none">
                                @csrf

                                <input type="hidden" name="data" value="{{encrypt($data)}}">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

    <script>
        var div = document.getElementById('display');
            var submitted = document.getElementById('submitted');

              function CountDown(duration, display) {

                    var timer = duration, minutes, seconds;

                      var interVal=  setInterval(function () {
                            minutes = parseInt(timer / 60, 10);
                            seconds = parseInt(timer % 60, 10);

                            minutes = minutes < 10 ? "0" + minutes : minutes;
                            seconds = seconds < 10 ? "0" + seconds : seconds;
                            display.innerHTML = + minutes + ":" + seconds;
                            if (timer > 0) {
                               --timer;
                            }else{
                            clearInterval(interVal)
                                SubmitFunction();
                             }

                       },1000);

                }

              function SubmitFunction(){
                console.log("Time is up. Transaction was canceled and deleted!");
                document.getElementById('cancel-process').submit();

               }
               var duration = 60 * 25;
               CountDown(duration,div);
    </script>

@endsection