@extends('layouts.dashboard')
@section('styles')
    <style>
        .product-img {
            margin: 10px auto;
            height: 6.5rem;
            width: 6.5rem;
        }

        .product-img2 {
            margin: 10px auto;
            height: 6.5rem;
            border-radius: 10px;
        }
    </style>
@endsection
@section('title', 'Products')

@section('content')

<div class="my-4">
    <div class="mr-2">
        <h3>Products Available</h3>
    </div>
</div>
<div style="margin: 0 auto;">
    <div class="row clearfix">
        
        
        <div class="col-lg-3 col-md-6 col-sm-12 mb-30">
            <div class="card card-box">
                <img src="/images/bg/gift.svg" class="product-img mt-4" alt="Card image cap">
                <div class="card-body text-center">
                    <h5 class="mt-15 mb-15">GiftCards</h5>
                    <a href="{{route('cards.buy')}}" class="btn btn-sm btn-rounded btn-info">Trade Now</a>
                </div>
            </div>
        </div>

        <div class="col-lg-3 col-md-6 col-sm-12 mb-30">
            <div class="card card-box">
                <img src="/images/bg/bitcoin.svg" class="product-img mt-4" alt="Card image cap">
                <div class="card-body text-center">
                    <h5 class="mt-15 mb-15">Bitcoin</h5>
                    <a href="{{route('btc.index')}}" class="btn btn-sm btn-rounded btn-info">Trade Now</a>
                </div>
            </div>
        </div>


        <div class="col-lg-3 col-md-6 col-sm-12 mb-30">
            <div class="card card-box">
                <img src="/images/bg/etherum.png" class="product-img mt-4" alt="Card image cap">
                <div class="card-body text-center">
                    <h5 class="mt-15 mb-15">Etherum</h5>
                    <a href="{{route('trade.etherum')}}" class="btn btn-sm btn-rounded btn-info">Trade Now</a>
                </div>
            </div>
        </div>


        <div class="col-lg-3 col-md-6 col-sm-12 mb-30">
            <div class="card card-box">
                <img src="/images/bg/litcoin.png" class="product-img mt-4" alt="Card image cap">
                <div class="card-body text-center">
                    <h5 class="mt-15 mb-15">Litcoin</h5>
                    <a href="{{route('trade.litcoin')}}" class="btn btn-sm btn-rounded btn-info">Trade Now</a>
                </div>
            </div>
        </div>
        
        <div class="col-lg-3 col-md-6 col-sm-12 mb-30">
            <div class="card card-box">
                <img  src="/images/products/airtime.jpeg" class="product-img mt-4" alt="Card image cap">
                <div class="card-body text-center">
                    <h5 class="mt-15 mb-15">Airtime</h5>
                    <a href="{{route('airtime.buy')}}" class="btn btn-sm btn-rounded btn-info">Buy Now</a>
                </div>
            </div>
        </div>

        <div class="col-lg-3 col-md-6 col-sm-12 mb-30">
            <div class="card card-box">
                <img src="/images/bg/network.svg" class="product-img mt-4" alt="Card image cap">
                <div class="card-body text-center">
                    <h5 class="mt-15 mb-15">Data Refill</h5>
                    <a href="{{route('data.buy')}}" class="btn btn-sm btn-rounded btn-info">Refill Now</a>
                </div>
            </div>
        </div>

       @foreach ($otherassets as $item)
            <div class="col-lg-3 col-md-6 col-sm-12 mb-30">
                <div class="card card-box">
                    <img  src="{{$item->image_path}}" class="product-img2 mt-3" alt="Card image cap">
                    <div class="card-body text-center">
                    <h5 class="mt-15 mb-15">{{ucfirst($item->name)}}</h5>
                        <a href="#" data-toggle="modal" data-target="#instructon-{{$item->id}}" class="btn btn-sm btn-rounded btn-info">Trade Now</a>
                    </div>
                </div>
            </div>


            <div class="modal fade" id="instructon-{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="instructon-{{$item->id}}" aria-hidden="true">
                    <div class="modal-dialog modal-sm modal-dialog-centered">
                        <div class="modal-content">
                        
                            <div class="modal-body">
                                <div class="d-flex mb-3 justify-content-between align-items-center">
                                    <h4 class="font-16">EXCHANGE DIRECTION</h4>
                                    <a href="#" class="text-danger font-weight-normal" data-dismiss="modal"><i class="ion-close-round font-16"></i></a>
                                </div>
                            

                                @switch($item->name)
                                    @case('paypal')
                                        <div class="mt-2">
                                            <p>For exchange you need to follow a few steps:</p>
                                            <p>
                                                Complete all the fields in the form be low with your correct information 
                                                And request for PayPal account which will be sent to your provided email address <br>
                                                Send on the PayPal account the same  amount  you open on trade to the following  
                                                PayPal account provide to you. and click on the «Exchange» button. You will receive 
                                                an email which  contains PayPal account details which you’re going to send funds.
                                            </p>
                                        </div>
                                        @break
                                    @case('cash app')
                                        <div class="mt-2">
                                            <p>For exchange you need to follow a few steps:</p>
                                            <p>
                                                Complete all the fields in the form be low with your correct information 
                                                And request for CashApp Tag which will be sent to your provided email address <br>
                                                Send on the CashApp the same  amount  you open on trade to the following  
                                                CashApp Account provide to you. and click on the «Exchange» button. You will receive an email 
                                                which  contains CashApp Tag which you’re going to send funds.
                                            </p>
                                        </div>
                                        @break

                                    @case('perfect money')
                                        <div class="mt-2">
                                            <p>For exchange you need to follow a few steps:</p>
                                            <p>
                                                Complete all the fields in the form be low with your correct information 
                                                And request for Perfect Money which will be sent to your provided email address <br>
                                                Send on the Perfect Money the same  amount  you open on trade to the following  Perfect Money  
                                                provide to you. and click on the «Exchange» button. You will receive an email which  contains 
                                                Perfect Money which you’re going to send funds.

                                            </p>
                                        </div>
                                        @break


                                    @case('wechat')
                                        <div class="mt-2">
                                            <p>For exchange you need to follow a few steps:</p>
                                            <p>
                                                Complete all the fields in the form be low with your correct information 
                                                And request for WeChat QR which will be sent to your provided email address <br>
                                                Send on the WeChat the same  amount  you open on trade to the following  
                                                WeChat QR provide to you. and click on the «Exchange» button. You will receive an email which 
                                                contains WeChat QR which you’re going to send funds.
                                            </p>
                                        </div>
                                        @break
                                    @default
                                        
                                @endswitch


                                <a href="{{route('trade.other-asset', encrypt($item->id))}}" class="btn btn-sm btn-block btn-rounded btn-outline-info">Continue Trade</a>
                            </div>
                            
                        </div>
                    </div>
                </div>
        @endforeach
        

    </div>
</div>
@endsection