@extends('layouts.dashboard')
@section('title', 'Buy Airtime')
@section('styles')
    
@endsection
@section('content')

    <div class="my-4">
        <div class="mr-2">
            <h3>Buy Airtime</h3>
        </div>
    </div>
    <div class="row clearfix">
        <div class="col-md-6 offset-md-3">
            <div class="card card-box">
                <div class="card-body">
                    @livewire('users.trade.refill.airtime')
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

@endsection