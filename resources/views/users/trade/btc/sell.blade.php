@extends('layouts.dashboard')
@section('title', 'Sell BTC')
@section('styles')
     <style>
        .currency{
            padding: 5px 10px;
            border-radius: 120px;
            font-weight: 800;
            font-size: 20px; 
        }
        .ngn{
            background-color: #94d05e;
            color: azure; 
        }
        .btc{
            color: azure;
            background-color: #eb9d48;
        }
    </style>
@endsection
@section('content')
    <div class="my-4">
        <div class="mr-2">
            <h3>Sell BTC</h3>
        </div>
    </div>
    <div class="row clearfix">
        <div class="col-md-6 offset-md-3">
            <div class="card card-box">
                <div class="card-body">
                    @livewire('users.trade.btc.sell')
                </div>
            </div>
        </div>
    </div>
@endsection