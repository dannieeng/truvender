@extends('layouts.dashboard')
@section('title', 'Trade Litecoin')
@section('styles')
    <style>
        .nav-item{
            font-weight: 500;
            font-size: 1.2rem;
        }
        .currency{
            padding: 5px 10px;
            border-radius: 120px;
            font-weight: 800;
            font-size: 20px; 
        }
        .ngn{
            background-color: #94d05e;
            color: azure; 
        }
        .btc{
            color: azure;
            background-color: #eb9d48;
        }
    </style>
@endsection
@section('content')
    <div class="my-4">
        <div class="mr-2">
            <h3>Trade Litecoin</h3>
        </div>
    </div>
    <div class="row clearfix">
        <div class="col-md-6 offset-md-3">
            <div class="card card-box">
                <div class="card-body">
                    <div class="tab">
                        <ul class="nav nav-tabs customtab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#buy" role="tab" aria-selected="false">Buy Litecoin</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#sell" role="tab" aria-selected="true">Sell Litecoin</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            
                            <div class="tab-pane fade show active pt-4" id="buy" role="tabpanel">
                                @livewire('users.trade.ltc.buy')
                            </div>

                            <div class="tab-pane fade pt-4" id="sell" role="tabpanel">
                                @livewire('users.trade.ltc.sell')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection