@extends('layouts.dashboard')
@section('styles')
    <style>
        .product-img {
            margin: 10px auto;
            height: 6.5rem;
            width: 6.5rem;
        }


        .product-img2 {
            margin: 10px auto;
            height: 6.5rem;
            /* width: 6.5rem; */
            border-radius: 10px;
        }
    </style>
@endsection
@section('title', 'Account Dashboard')
@section('content')
    <div class="card-box pd-20 mb-20">
        <div class="row align-items-center">
            <div class="col-md-8">
                <h4 class="font-20 weight-500 mb-10 text-capitalize">
                    Welcome! <span class="weight-600 font-18 text-blue">{{auth()->user()->username}}</span>
                </h4>

                <div class="mt-1">
                    <h4 class="font-20 font-weight-normal">
                        Balance: <span>&#8358;</span>{{number_format(auth()->user()->wallet->ngn_balance,2)}}
                    </h4>
                </div>
            </div>

            <div class="col-md-4">
                <div class="d-flex justify-content-lg-end justify-content-start align-items-center">
                    <a href="{{route('products')}}" class="btn btn-outline-info btn-sm btn-rounded mr-2">Products</a>
                    <a href="{{route('rates.act')}}" class="btn btn-outline-info btn-sm btn-rounded">Rates</a>
                </div>
            </div>
        </div>
    </div>

    <div class="row clearfix">
        <div class="col-lg-3 col-md-6 col-sm-12 mb-30">
            <div class="card card-box">
                <img  src="/images/bg/gift.svg" class="product-img mt-3" alt="Card image cap">
                <div class="card-body text-center">
                   <h5 class="mt-15 mb-15">GiftCards</h5>
                    <a href="{{route('cards.buy')}}" class="btn btn-sm btn-rounded btn-info">Trade Now</a>
                </div>
            </div>
        </div>

        <div class="col-lg-3 col-md-6 col-sm-12 mb-30">
            <div class="card card-box">
                <img  src="/images/bg/bitcoin.svg" class="product-img mt-3" alt="Card image cap">
                <div class="card-body text-center">
                   <h5 class="mt-15 mb-15">Bitcoin</h5>
                    <a href="{{route('btc.index')}}" class="btn btn-sm btn-rounded btn-info">Trade Now</a>
                </div>
            </div>
        </div>

        <div class="col-lg-3 col-md-6 col-sm-12 mb-30">
            <div class="card card-box">
                <img src="/images/bg/etherum.png" class="product-img mt-4" alt="Card image cap">
                <div class="card-body text-center">
                    <h5 class="mt-15 mb-15">Etherum</h5>
                    <a href="{{route('trade.etherum')}}" class="btn btn-sm btn-rounded btn-info">Trade Now</a>
                </div>
            </div>
        </div>


        <div class="col-lg-3 col-md-6 col-sm-12 mb-30">
            <div class="card card-box">
                <img src="/images/bg/litcoin.png" class="product-img mt-4" alt="Card image cap">
                <div class="card-body text-center">
                    <h5 class="mt-15 mb-15">Litcoin</h5>
                    <a href="{{route('trade.litcoin')}}" class="btn btn-sm btn-rounded btn-info">Trade Now</a>
                </div>
            </div>
        </div>
        
        
        <div class="col-lg-3 col-md-6 col-sm-12 mb-30">
            <div class="card card-box">
                <img  src="/images/products/airtime.jpeg" class="product-img mt-3" alt="Card image cap">
                <div class="card-body text-center">
                   <h5 class="mt-15 mb-15">Airtime</h5>
                    <a href="{{route('airtime.buy')}}" class="btn btn-sm btn-rounded btn-info">Trade Now</a>
                </div>
            </div>
        </div>

        <div class="col-lg-3 col-md-6 col-sm-12 mb-30">
            <div class="card card-box">
                <img  src="/images/bg/network.svg" class="product-img mt-3" alt="Card image cap">
                <div class="card-body text-center">
                   <h5 class="mt-15 mb-15">Data Refill</h5>
                    <a href="{{route('data.buy')}}" class="btn btn-sm btn-rounded btn-info">Refill Now</a>
                </div>
            </div>
        </div>

        @foreach ($otherassets as $item)
            <div class="col-lg-3 col-md-6 col-sm-12 mb-30">
                <div class="card card-box">
                    <img  src="{{$item->image_path}}" class="product-img2 mt-3" alt="Card image cap">
                    <div class="card-body text-center">
                    <h5 class="mt-15 mb-15">{{ucfirst($item->name)}}</h5>
                        <a href="#" data-toggle="modal" data-target="#instructon-{{$item->id}}" class="btn btn-sm btn-rounded btn-info">Trade Now</a>
                    </div>
                </div>
            </div>


            <div class="modal fade" id="instructon-{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="instructon-{{$item->id}}" aria-hidden="true">
                    <div class="modal-dialog modal-sm modal-dialog-centered">
                        <div class="modal-content">
                        
                            <div class="modal-body">
                                <div class="d-flex mb-3 justify-content-between align-items-center">
                                    <h4 class="font-16">EXCHANGE DIRECTION</h4>
                                    <a href="#" class="text-danger font-weight-normal" data-dismiss="modal"><i class="ion-close-round font-16"></i></a>
                                </div>
                            

                                @switch($item->name)
                                    @case('paypal')
                                        <div class="mt-2">
                                            <p>For exchange you need to follow a few steps:</p>
                                            <p>
                                                Complete all the fields in the form be low with your correct information 
                                                And request for PayPal account which will be sent to your provided email address <br>
                                                Send on the PayPal account the same  amount  you open on trade to the following  
                                                PayPal account provide to you. and click on the «Exchange» button. You will receive 
                                                an email which  contains PayPal account details which you’re going to send funds.
                                            </p>
                                        </div>
                                        @break
                                    @case('cash app')
                                        <div class="mt-2">
                                            <p>For exchange you need to follow a few steps:</p>
                                            <p>
                                                Complete all the fields in the form be low with your correct information 
                                                And request for CashApp Tag which will be sent to your provided email address <br>
                                                Send on the CashApp the same  amount  you open on trade to the following  
                                                CashApp Account provide to you. and click on the «Exchange» button. You will receive an email 
                                                which  contains CashApp Tag which you’re going to send funds.
                                            </p>
                                        </div>
                                        @break

                                    @case('perfect money')
                                        <div class="mt-2">
                                            <p>For exchange you need to follow a few steps:</p>
                                            <p>
                                                Complete all the fields in the form be low with your correct information 
                                                And request for Perfect Money which will be sent to your provided email address <br>
                                                Send on the Perfect Money the same  amount  you open on trade to the following  Perfect Money  
                                                provide to you. and click on the «Exchange» button. You will receive an email which  contains 
                                                Perfect Money which you’re going to send funds.

                                            </p>
                                        </div>
                                        @break


                                    @case('wechat')
                                        <div class="mt-2">
                                            <p>For exchange you need to follow a few steps:</p>
                                            <p>
                                                Complete all the fields in the form be low with your correct information 
                                                And request for WeChat QR which will be sent to your provided email address <br>
                                                Send on the WeChat the same  amount  you open on trade to the following  
                                                WeChat QR provide to you. and click on the «Exchange» button. You will receive an email which 
                                                contains WeChat QR which you’re going to send funds.
                                            </p>
                                        </div>
                                        @break
                                    @default
                                        
                                @endswitch


                                <a href="{{route('trade.other-asset', encrypt($item->id))}}" class="btn btn-sm btn-block btn-rounded btn-outline-info">Continue Trade</a>
                            </div>
                            
                        </div>
                    </div>
                </div>
        @endforeach
        
    </div>

    <div class="row">
        <div class="col-lg-3 col-md-6 col-6 mb-30">
            <div class="card-box height-100-p widget-style1">
                <div class="d-flex flex-wrap align-items-center">
                    <div class="widget-data">
                        <div class="h4 mb-0">{{$transactions->count()}}</div>
                        <div class="weight-600 font-12 mt-2">Total Transactions</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-6 mb-30">
            <div class="card-box height-100-p widget-style1">
                <div class="d-flex flex-wrap align-items-center">
                    <div class="widget-data">
                        <div class="h4 mb-0">{{$transactions->where('status', 'failed')->count()}}</div>
                        <div class="weight-600 font-12 text-danger mt-2">Failed Transactions</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6 mb-30">
            <div class="card-box height-100-p widget-style1">
                <div class="d-flex flex-wrap align-items-center">
                    <div class="widget-data">
                        <div class="h4 mb-0">{{$transactions->where('status', 'pending')->count()}}</div>
                        <div class="weight-600 font-12 text-info">Pending Transactions</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6 mb-30">
            <div class="card-box height-100-p widget-style1">
                <div class="d-flex flex-wrap align-items-center">
                    <div class="widget-data">
                        <div class="h4 mb-0">&#x20a6;{{number_format($wallet->ngn_balance, 2)}}</div>
                        <div class="weight-600 font-12">Wallet Balance</div>
                        <a href="{{route('wallet')}}" class="text-info">Go to wallet <i class=" dw dw-right-arrow1"></i></a>
                    </div>
                </div>
            </div>
        </div>


        
    </div>
    <!-- Responsive tables Start -->
    <div class="pd-20 card-box mb-30">
        <div class="clearfix mb-20">
            <div class="pull-left">
                <h4 class="text-blue h4">Recent Transactions</h4>
            </div>
        </div>
         @if ($transactions->count() > 0)
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col">Transaction ID</th>
                            <th scope="col">Transaction Amount</th>
                            <th scope="col">Transaction Type</th>
                            <th scope="col">Transaction Status</th>
                            <th scope="col">Date</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($transactions->take(30) as $transaction)
                             <tr>
                                <th scope="row">{{$transaction->trxn_ref}}</th>
                                <td>{{'NGN '.number_format($transaction->amount,2)}}</td>
                                <td>{{$transaction->type}}</td>
                                <td>
                                    @if ($transaction->status == 'success')
                                        <span class="text-success font-weight-bold">Success</span>
                                    @elseif($transaction->status == 'failed')
                                        <span class="text-danger font-weight-bold">failed</span>
                                    @else
                                        <span class="text-warning font-weight-bold">pending</span>
                                    @endif
                                </td>
                                <td>{{now()->parse($transaction->created_at)->diffForHumans()}}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        @else
            <div class="p-30 d-flex justify-content-center">
                <div class="mb-4">
                    <img src="/images/bg/box.svg" style="height: 20rem; width:20rem;" alt="">
                    <h5 class="font-weight-normal text-center text-primary">No Transactions</h5>
                    <p class="text-center">Transactions will be here: <a href="" class="text-primary">start transaction</a></p>
                </div>
            </div>
        @endif
    </div>
    <!-- Responsive tables End -->
    
@endsection