
@extends('layouts.auth')
@section('title', 'Forgot Password')

@section('added')
    <div class="login-wrap d-flex align-items-center flex-wrap justify-content-center">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-md-6">
                        <img src="/images/bg/forgot-password.svg" alt="">
                    </div>
                    <div class="col-md-6">
                        <div class="login-box bg-white box-shadow border-radius-10">
                            <div class="login-title">
                                <h2 class="text-center text-primary"> <small>{{config('app.name')}}</small> Forgot Password</h2>
                            </div>
                            @if (session('status'))
                                <div class="my-2 font-medium text-sm text-green-600">
                                    {{ session('status') }}
                                </div>
                            @endif
                            <x-jet-validation-errors class="my-2" />
                            <h6 class="mb-10">Enter your email address to reset your password</h6>
                            
                            <form method="POST" action="{{ route('password.email') }}">
                                @csrf

                                <div class="input-group custom">
                                    <input type="text" class="form-control form-control-lg" name="email" value="{{old('email')}}" placeholder="Email" required autofocus>
                                    <div class="input-group-append custom">
                                        <span class="input-group-text"><i class="fa fa-envelope-o" aria-hidden="true"></i></span>
                                    </div>
                                </div>
                                <div class="row align-items-center">
                                    <div class="col-5">
                                        <div class="input-group mb-0">
                                            <button type="submit" class="btn btn-primary btn-lg btn-block">Submit</a>
                                        </div>
                                    </div>
                                    <div class="col-2">
                                        <div class="font-16 weight-600 text-center" data-color="#707373">OR</div>
                                    </div>
                                    <div class="col-5">
                                        <div class="input-group mb-0">
                                            <a class="btn btn-outline-primary btn-lg btn-block" href="{{route('login')}}">Sign In</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
