@extends('layouts.auth')
@section('title', 'Reset Password')

@section('added')
   <div class="login-wrap d-flex align-items-center flex-wrap justify-content-center">
		<div class="container">
			<div class="row align-items-center">
				
				<div class="col-md-8 mx-auto">
					<div class="login-box border-radius-10">
						<div class="login-title">
							<h2 class="text-center text-primary">Account Restricted</h2>
							{{-- <h4 class="mt-2 font-18 text-center text-primary"></h4> --}}
						</div>

						<h6 class="mb-6 tracking-wider text-lg text-wrap">
                            Sorry {{Auth::user()->username}}! Your account was restricted for {{Auth::user()->in_blacklist->cause_of_restriction}}
						</h6>
						<h6 class="mb-10 tracking-wider text-lg text-wrap">
                            Please do contact the support team by sending a mail to <a href="#" class="text-success">support@truvender.com</a> for account reactivation
                        </h6>
						
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection