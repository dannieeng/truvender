


@extends('layouts.auth')
@section('title', 'Verify Transaction')

@section('added')
   <div class="login-wrap d-flex align-items-center flex-wrap justify-content-center">
		<div class="container">
			<div class="row align-items-center">
				<div class="col-md-6">
					<img src="/images/bg/my-password.svg" alt="">
				</div>
				<div class="col-md-6">
					<div class="login-box bg-white box-shadow border-radius-10">
						<div class="login-title">
							<h2 class="text-center text-primary">Verify Transaction</h2>
						</div>
						

                        <h6 class="mb-10">A code was sent to your email ({{auth()->user()->email}}), please check your email and enter the code below to continue</h6>
						<form method="POST" action="{{route('otp.verify-code')}}">
							@csrf
							<div class="input-group custom">
								<input type="text" class="form-control form-control-lg" placeholder="Enter Verification Code" name="code" required autofocus >
								<div class="input-group-append custom">
									<span class="input-group-text"><i class="dw dw-key"></i></span>
								</div>
								@error('code')
									<p class="text-danger mt-2">{{$message}}</p>   
								@enderror
								@if(session('error'))
									<p class="text-danger mt-2">{{session('error')}}</p>   
								@endif
							</div>

							<div class="row align-items-center">
								<div class="col-5">
									<div class="input-group mb-0">
										<button type="submit" class="btn btn-primary btn-lg btn-block">Submit</button>
									</div>

								</div>
								<div class="col-7">
									@livewire('users.code')
								</div>

								
							</div>
							
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection