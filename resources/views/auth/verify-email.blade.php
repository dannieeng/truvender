@extends('layouts.auth')
@section('title', 'Reset Password')

@section('added')
   <div class="login-wrap d-flex align-items-center flex-wrap justify-content-center">
		<div class="container">
			<div class="row align-items-center">
				
				<div class="col-md-8 mx-auto">
					<div class="login-box border-radius-10">
						<div class="login-title">
							<h2 class="text-center text-primary"> <small>{{config('app.name')}}</small> Account Verification</h2>
						</div>

						<x-jet-validation-errors class="my-2" />

						<h6 class="mb-10 tracking-wider text-lg text-wrap">Thanks for signing up! Before getting started, could you verify your email address by clicking on the link we
							 just emailed to you? If you didn\'t receive the email, we will gladly send you another.</h6>

							 <form method="POST" action="{{ route('verification.send') }}" class="mb-4">
								@csrf

								<button type="submit" class="btn btn-primary btn-lg btn-block">Resend Mail</button>
							</form>
							<form method="POST" action="{{ route('logout') }}">
								@csrf

								<button type="submit" class="btn  btn-outline-primary btn-lg btn-block">Sign Out</button>
							</form>
						
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection