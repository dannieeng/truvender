@extends('layouts.auth')
@section('title', 'Sign In')

@section('added')
    <div class="login-wrap d-flex align-items-center flex-wrap justify-content-center">
		<div class="container">
			<div class="row align-items-center">
				<div class="col-md-6 col-lg-7">
					<img src="{{asset ('/images/bg/login-secure.svg')}}" alt="">
				</div>
				<div class="col-md-6 col-lg-5">
					<div class="login-box bg-white box-shadow border-radius-10">
						<div class="login-title">
							<h2 class="text-center text-primary"><small>{{config('app.name')}}</small> Sign In</h2>
						</div>
						@if (session('status'))
							<div class="my-2 font-medium text-sm text-green-600">
								{{ session('status') }}
							</div>
						@endif

						@if (session('success'))
							<div class="my-2 font-medium text-sm text-success text-green-600">
								{{ session('success') }}
							</div>
						@endif
						<x-jet-validation-errors class="mb-2 mt-2" />
						<form method="POST" action="{{ route('login') }}">
							@csrf
							<div class="input-group custom">
								<input type="text" class="form-control form-control-lg" name="username" placeholder="Username" value="{{old('username')}}" required autofocus>
								<div class="input-group-append custom">
									<span class="input-group-text"><i class="icon-copy dw dw-user1"></i></span>
								</div>
							</div>
							<div class="input-group custom">
								<input type="password" class="form-control form-control-lg" name="password" required autocomplete="current-password" placeholder="**********">
								<div class="input-group-append custom">
									<span class="input-group-text"><i class="dw dw-padlock1"></i></span>
								</div>
							</div>
							<div class="row pb-30">
								<div class="col-6">
									<div class="custom-control custom-checkbox">
										<input type="checkbox" id="remember_me" class="custom-control-input" id="remember_me">
										<label class="custom-control-label" for="remember_me">Remember</label>
									</div>
								</div>
								<div class="col-6">
									@if (Route::has('password.request'))
										<div class="forgot-password"><a href="{{route('password.request')}}">Forgot Password</a></div>
									@endif
								</div>
							</div>
							<div class="row">
								<div class="col-sm-12">
									<div class="input-group mb-0">
										<button type="submit" class="btn btn-primary btn-lg btn-block" href="index.html">Sign In</button>
									</div>
									<div class="font-16 weight-600 pt-2 pb-2 text-center" data-color="#707373">OR</div>
									<div class="input-group mb-0">
										<a class="btn btn-outline-primary btn-lg btn-block" href="{{route('register')}}">Sign Up</a>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection