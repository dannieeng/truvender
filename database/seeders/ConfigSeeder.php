<?php

namespace Database\Seeders;

use App\Models\Configuration;
use Illuminate\Database\Seeder;

class ConfigSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Configuration::create([
            'key' => 'company name',
            'value' => \env('APP_NAME'),
        ]);

        Configuration::create([
            'key' => 'contact no',
            'value' => null,
        ]);
        Configuration::create([
            'key' => 'contact email',
            'value' => 'support@truvender.com',
        ]);

        Configuration::create([
            'key' => 'url',
            'value' => \env('APP_URL'),
        ]);

        Configuration::create([
            'key' => 'referral_comission',
            'value' => 500,
        ]);

        Configuration::create([
            'key' => 'transaction_fee',
            'value' => 2,
        ]);

        Configuration::create([
            'key' => 'minimum_usd_deposit',
            'value' => 10,
        ]);

        Configuration::create([
            'key' => 'minimum_ngn_deposit',
            'value' => 1000,
        ]);


        Configuration::create([
            'key' => 'term of use',
            'value' => null,
        ]);

        Configuration::create([
            'key' => 'policy',
            'value' => null,
        ]);

        Configuration::create([
            'key' => 'disclaimer',
            'value' => null,
        ]);
        Configuration::create([
            'key' => 'card_confimation_time',
            'value' => 20,
        ]);
        Configuration::create([
            'key' => 'card_confimation_time2',
            'value' => 30,
        ]);

        Configuration::create([
            'key' => 'btc_wallet',
            'value' => null
        ]);
        Configuration::create([
            'key' => 'btc_wallet2',
            'value' => null
        ]);

        
    }
}
