<?php

namespace Database\Seeders;

use App\Models\BillerCategory;
use Illuminate\Database\Seeder;

class BillerCatergorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        BillerCategory::create([
            'name' => 'Airtime'
        ]);

        BillerCategory::create([
            'name' => 'Data'
        ]);

        BillerCategory::create([
            'name' => 'Cable'
        ]);

        BillerCategory::create([
            'name' => 'Tax'
        ]);

        BillerCategory::create([
            'name' => 'Utility Bill'
        ]);
    }
}
