<?php

namespace Database\Seeders;

use App\Models\Biller;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Http;

class BillerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $flutterwave_endpoint = 'https://api.flutterwave.com/v3/bill-categories';
        $flutterwave_key_secrete = \config('services.flutterwave-test.secrete_key');
        $api_call = Http::withHeaders(['Authorization' => $flutterwave_key_secrete])->get($flutterwave_endpoint)->json();
        
        $collection = $api_call;
        foreach($api_call['data'] as $biller){
            $biller = Biller::create([
                'biller_code' => $biller['biller_code'],
                'name' => $biller['name'],
                'date_added' => now()->parse($biller['date_added'])->format('Y-m-d H:i:s'),
                'country' => $biller['country'],
                'is_airtime' => $biller['is_airtime'],
                'biller_name' => $biller['biller_name'],
                'item_code' => $biller['item_code'],
                'short_name' => $biller['short_name'],
                'fee' => $biller['fee'],
                'commission_on_fee' => $biller['commission_on_fee'],
                'label_name' => $biller['label_name'],
                'amount' => $biller['amount'],
            ]);
        }
    }
}
