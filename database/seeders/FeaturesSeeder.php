<?php

namespace Database\Seeders;

use App\Models\Feature;
use Illuminate\Database\Seeder;

class FeaturesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Feature::create([
            'name' => 'Buy Bitcoin',
            'is_available' => false,
        ]);

        Feature::create([
            'name' => 'Sell Bitcoin',
            'is_available' => true,
        ]);

        Feature::create([
            'name' => 'Buy GiftCards',
            'is_available' => false,
        ]);


        Feature::create([
            'name' => 'Sell GiftCards',
            'is_available' => true,
        ]);

        Feature::create([
            'name' => 'Buy Airtime',
            'is_available' => false,
        ]);

        Feature::create([
            'name' => 'Buy Data',
            'is_available' => false,
        ]);
    }
}
