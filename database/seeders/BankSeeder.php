<?php

namespace Database\Seeders;

use App\Models\Bank;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Http;

class BankSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $flutter_endpoint = 'https://api.flutterwave.com/v3/banks/NG';
        $flutter_secrete_key = \config('services.flutterwave-test.secrete_key');
        $api_call = Http::withHeaders(['Authorization' => $flutter_secrete_key])->get($flutter_endpoint)->json();
        $banks = $api_call['data'];

        foreach($banks as $bank){
            Bank::create([
                'name' => $bank['name'],
                'code' => $bank['code'],
            ]);
        }
    }
}
