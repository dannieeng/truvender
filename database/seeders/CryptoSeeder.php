<?php

namespace Database\Seeders;

use App\Models\CryptoRates;
use Illuminate\Database\Seeder;

class CryptoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CryptoRates::create([
            'name' => 'Bitcoin',
            'image' => '/images/bg/bitcoin.jpg',
            'symbol' => 'BTC',
            'buyer_rate' => 497.96,
            'seller_rate' => 474.96,
        ]);
    }
}
