<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $this->call([
            // RolesTableSeeder::class,
            // ConfigSeeder::class,
            // CountrySeeder::class,
            // CryptoSeeder::class,
            // CardSeeder::class,
            // BillerSeeder::class,
            // BillerCatergorySeeder::class,
            // BankSeeder::class,
            // FeaturesSeeder::class,
            OtherAssets::class,
        ]);
    }
}
