<?php

namespace Database\Seeders;

use App\Models\Country;
use Illuminate\Database\Seeder;

class CountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Country::create([
            'name' =>'USA',
            'flag' => '/images/flags/usa.png'
        ]);
        Country::create([
            'name' =>'UK',
            'flag' => '/images/flags/uk.png'
        ]);
    }
}
