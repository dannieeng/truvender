<?php

namespace Database\Seeders;

use App\Models\OtherAsset;
use Illuminate\Database\Seeder;

class OtherAssets extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        OtherAsset::create([
            'name' => 'paypal',
            'description' => 'Funds from paypal account',
            'image_path' => '/images/products/paypal.png',
        ]);

        OtherAsset::create([
            'name' => 'cash app',
            'description' => 'funds from cash app account',
            'image_path' => '/images/products/cashapp.png',
        ]);

        OtherAsset::create([
            'name' => 'wechat',
            'description' => 'funds from wechat account',
            'country_id' => 13,
            'image_path' => '/images/products/wechat.png',
        ]);

        OtherAsset::create([
            'name' => 'perfect money',
            'description' => 'funds from perfect money account',
            'country_id' => 1,
            'image_path' => '/images/products/perfectmoney.jpeg',
        ]);
    }
}
