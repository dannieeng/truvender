<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->string('trxn_ref');
            $table->string('type');
            $table->string('section');
            $table->string('wallet')->nullable();
            $table->string('currency');
            $table->double('amount');
            $table->string('account_number')->nulable();
            $table->string('account_name')->nulable();
            $table->string('bank')->nulable();
            $table->string('status')->default('pending');
            $table->double('fee')->default(0);
            $table->string('tx_ref')->nulable();
            $table->string('recipient_code')->nulable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
