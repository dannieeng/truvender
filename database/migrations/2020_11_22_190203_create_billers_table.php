<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBillersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('billers', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('country');
            $table->boolean('is_airtime');
            $table->string('biller_name');
            $table->string('biller_code');
            $table->string('item_code');
            $table->string('short_name');
            $table->double('fee')->default(0);
            $table->double('commission_on_fee')->default(0);
            $table->string('label_name');
            $table->double('amount');
            $table->timestamp('date_added');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('billers');
    }
}
