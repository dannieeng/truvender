<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOtherAssetTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('other_asset_transactions', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->integer('transaction_id');
            $table->integer('other_asset_id');
            $table->double('rate');
            $table->string('currency')->nullable();
            $table->double('amount')->default(0);
            $table->string('account_email');
            $table->string('account_phone');
            $table->string('image_path')->nullable();
            $table->double('price')->default(0);
            $table->boolean('status')->deafult(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('other_asset_transactions');
    }
}
