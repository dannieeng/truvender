<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCardTradesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('card_trades', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->integer('transaction_id');
            $table->integer('card_id');
            $table->integer('card_country_id');
            $table->string('status');
            $table->integer('amount');
            $table->integer('amount_accepted')->nullable();
            $table->integer('amount_rejected')->nullable();
            $table->double('price');
            $table->double('accepted_price')->nullable();
            $table->double('rate');
            $table->string('trade_type');
            $table->string('type');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('card_trades');
    }
}
