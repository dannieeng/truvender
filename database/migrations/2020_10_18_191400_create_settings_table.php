<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->string('notify_buy_sell_btc')->default('both')->nullable();
            $table->string('notify_buy_sell_giftcards')->default('both')->nullable();
            $table->string('notify_buy_airtime_data')->default('both')->nullable();
            $table->string('default_wallet')->nullable();
            $table->boolean('gift_decription')->default(true);
            $table->boolean('twoFA')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
