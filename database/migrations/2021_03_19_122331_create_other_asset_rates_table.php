<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOtherAssetRatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('other_asset_rates', function (Blueprint $table) {
            $table->id();
            $table->integer('other_asset_id');
            $table->double('rate_usd')->nullable();
            $table->double('rate_gbp')->nullable();
            $table->double('rate_euro')->nullable();
            $table->double('rate_cny')->nullable();
            $table->double('min_trade_amount')->nullable();
            $table->double('max_trade_amount')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('other_asset_rates');
    }
}
