<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCardCountriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('card_countries', function (Blueprint $table) {
            $table->id();
            $table->integer('card_id');
            $table->integer('country_id');
            $table->integer('card_type_id');
            $table->double('buyer_rate');
            $table->double('seller_rate');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('card_countries');
    }
}
