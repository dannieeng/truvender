<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEtherumTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('etherum_transactions', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->integer('ether_wallet_id');
            $table->text('tx_hash');
            $table->string('block_height');
            $table->string('tx_input_n');
            $table->string('tx_output_n');
            $table->double('value');
            $table->double("ref_balance");
            $table->integer("confirmation");
            $table->timestamp("confirmed");
            $table->boolean("double_spend");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('etherum_transactions');
    }
}
