<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],
    'block-io' => [
        'root_url' => 'https://block.io/api/v2/',
        'btc-key' => env('BLOCK_IO_KEY_BTC'),
        'ltc-key' => env('BLOCK_IO_KEY_LTC'),
        'dgc-key' => env('BLOCK_IO_KEY_DGC'),
    ],

    'test-block-io' => [
        'root_url' => 'https://block.io/api/v2/',
        'btc-key' => env('TEST_BLOCK_IO_KEY_BTC'),
        'ltc-key' => env('TEST_BLOCK_IO_KEY_LTC'),
        'dgc-key' => env('TEST_BLOCK_IO_KEY_DGC'),
    ],

    'flutterwave' => [
        'public_key' => env('FLUTTERWAVE_PUBLIC_KEY'),
        'secrete_key' => env('FLUTTERWAVE_SECRETE_KEY'),
        'encyt_key' => env('FLUTTERWAVE_ENCRYPTION_KEY'),
        'root-url' => env('FLUTTERWAVE_ROOT_URL'),
    ],

    'flutterwave-test' => [
        'public_key' => env('FLUTTERWAVE_TEST_PUBLIC_KEY'),
        'secrete_key' => env('FLUTTERWAVE_TEST_SECRETE_KEY'),
        'encyt_key' => env('FLUTTERWAVE_TEST_ENCRYPTION_KEY'),
        'root-url' => env('FLUTTERWAVE_ROOT_URL'),
    ],

    'reloadly' => [
        'id' => env('RELOADLY_ID'),
        'secrete' => env('RELOADLY_SECRETE'),
        'root-url' => env('RELOADLY_ROOT'),
    ],

    'block_cypher' => [
        'token' => env('BLOCK_CYPHER_TOKEN'),
        'url' => env('BLOCK_CYPHER_URL'),
    ],

    'paystack_test' => [
        'pk' => env('PAYSTACK_PRIMARY_KEY'),
        'sk' => env('PAYSTACK_SECRET_KEY'),
        'url' => env('PAYSTACK_URL'),
    ],

    'paystack' => [
        'pk' => env('PAYSTACK_TEST_PRIMARY_KEY'),
        'sk' => env('PAYSTACK_TEST_SECRET_KEY'),
        'url' => env('PAYSTACK_URL'),
    ],

    'crypto-api' => [
        'key' => env('CRYPTOAPI_KEY'),
        'base' => env('CRYPTOAPI_BASE'),
        'net' => env('CRYPTOAPI_NETWORK'),
    ]


];
