<?php

namespace App\Http\Livewire\Guest;


use App\Models\Message;
use Livewire\Component;
use App\Mail\ContactResponse;
use Illuminate\Support\Facades\Mail;

class Contact extends Component
{
    public $email, $message, $fullname, $to;

    public $rules = [
        'email' => 'required|email',
        'message' => 'required|string',
        'fullname' => 'required|string'
    ];

    public function submit()
    {
        $this->validate();

        $message = Message::create([
            'type' => 'contact',
            'title' => 'New Contact Email',
            'body' => $this->message,
            'from' => $this->fullname,
            'visitor_email' => $this->email
        ]);

        // $data = [
        //     'name' => $this->fullname
        // ];
        session()->flash('msg', 'Thank you for messaging truvender, you will contact you via the email you submitted');

        Mail::to($message->visitor_email)->send(new ContactResponse());
    }
    public function render()
    {
        return view('livewire.guest.contact');
    }
}
