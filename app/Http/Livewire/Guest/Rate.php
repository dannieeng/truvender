<?php

namespace App\Http\Livewire\Guest;

use Livewire\Component;

class Rate extends Component
{
    public function render()
    {
        return view('livewire.guest.rate');
    }
}
