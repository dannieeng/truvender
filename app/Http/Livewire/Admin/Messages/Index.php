<?php

namespace App\Http\Livewire\Admin\Messages;

use App\Models\Message;
use Livewire\Component;

class Index extends Component
{
    public $messages, $message, $search_title;

    public function read($id)
    {
        $this->message = Message::where('id', $id)->first();
        $message = $this->message;
        $message->update([
            'read' => true
        ]);
    }

    public function updated($fields)
    {
         $this->messages = $this->search_title != null ? 
                Message::where('title', 'like', $this->search_title)->get() : 
                    Message::orderBy('created_at', 'desc')->get();
    }

    public function render()
    {
        $this->messages = $this->search_title != null ? 
                Message::where('title', 'like', $this->search_title)->get() : 
                    Message::orderBy('created_at', 'desc')->get();

        return view('livewire.admin.messages.index');
    }
}
