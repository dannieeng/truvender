<?php

namespace App\Http\Livewire\Admin\User;

use Livewire\Component;

class Bank extends Component
{
    public function render()
    {
        return view('livewire.admin.user.bank');
    }
}
