<?php

namespace App\Http\Livewire\Admin\Countries;

use Livewire\Component;
use Illuminate\Support\Str;
use Livewire\WithFileUploads;
use App\Models\Country;

class Add extends Component
{
    use WithFileUploads;   

    public $name, $flag, $currency;

    public $rules = [
        'name' => 'required|string|max:100',
        'flag' => 'required|image|mimes:png,jpg,jpeg',
        'currency' => 'required|string'
    ];

    public function updated($fields)
    {
        $this->validateOnly($fields);
    }


    public function add()
    {
        $this->validate();

        $file = $this->flag;
        // Generate a file name with extension
        $flag_name = $file->getClientOriginalName();
        $flag_extension = $file->getClientOriginalExtension();
        $flag = Str::random(30) .'-'. time() .'-'.'.'.$flag_extension ;

        // Save the file
        $save_path =  $flag_path = $file->storeAs('/public/country-flags/',$flag);
        $flag_path = '/country-flags/' . $flag;


        $country = Country::create([
            'name' => $this->name,
            'flag' => $flag_path,
            'currency' => $this->currency,
        ]);

        session()->flash('success', 'Country was added successfully');
        return redirect()->route('admin.countries');
    }
    public function render()
    {
        return view('livewire.admin.countries.add');
    }
}
