<?php

namespace App\Http\Livewire\Admin\Countries;

use App\Models\Country;
use Livewire\Component;

class Index extends Component
{
    public $countries, $search_name;

    public function mount($countries)
    {
        $this->countries = $countries;
    }

    public function updated($fields)
    {
        $name = '%' . $this->search_name . '%';
        $this->countries = Country::where('name', 'like', $name)->get();
    }

    
    public function render()
    {
       
        return view('livewire.admin.countries.index');
    }
}
