<?php

namespace App\Http\Livewire\Admin\Card;

use App\Models\Card;
use Livewire\Component;

class AddPrice extends Component
{
    public $cards, $amount, $card;

    public $rules = [
        'amount' => 'required|numeric',
        'card' => 'required'
    ];

    public function mount()
    {
        $this->cards = Card::orderBy('name', 'asc')->get();
    }
    

    public function updated($fields)
    {
        $this->validateOnly($fields);
    }

    public function render()
    {
        return view('livewire.admin.card.add-price');
    }
}
