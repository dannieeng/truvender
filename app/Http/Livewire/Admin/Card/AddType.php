<?php

namespace App\Http\Livewire\Admin\Card;

use App\Models\Card;
use Livewire\Component;
use App\Models\CardType;

class AddType extends Component
{
    public $cards, $name, $card;

    public $rules = [
        'name' => 'required|string',
        'card' => 'required'
    ];

    public function mount()
    {
        $this->cards = Card::orderBy('name', 'asc')->get();
    }

    public function updated($fields)
    {
        $this->validateOnly($fields);
    }

    public function add()
    {
        $this->validate();
        $card = Card::where('id', $this->card)->first();

        $types = explode(',', $this->name);
        foreach($types as $key => $type){
            $type = CardType::create([
                'name' => $type,
                'card_id' => $card->id
            ]);
        }

        session()->flash('success', ' card type add successfully');
        return \redirect()->route('admin.card.types');
    }
    public function render()
    {
        return view('livewire.admin.card.add-type');
    }
}
