<?php

namespace App\Http\Livewire\Admin\Card;

use App\Models\Card;
use App\Models\Country;
use Livewire\Component;
use App\Models\CardType;
use App\Models\CardPrice;
use App\Models\CardCountry;
use App\Models\CardDefaultRate;

class AddRate extends Component
{
    public $cards, $types, $countries, 
    $card, $card_id, $card_prices, 
    $price, $country, $type,$amount, 
    $buyer_rate, $seller_rate, 
    $default_rate, $currency;
    public $rules = [
        'card' => 'required|numeric',
        'country' => 'required|numeric',
        'buyer_rate' => 'required|numeric',
        'seller_rate' => 'required|numeric',
        'type' => 'required|string', 
        'amount' => 'nullable|numeric',
        'price' => 'nullable',
        'default_rate' => 'required|numeric',
    ];

    public function mount($card_id)
    {
        $country = Country::orderBy('name' , 'asc')->get();
        $this->countries = $country;
        // $type = CardType::orderBy('created_at', 'desc')->get();
        // $this->types = $type;
        $cards = Card::orderBy('name', 'asc')->get();
        $this->cards = $cards;
        $this->card_id = $card_id;

        $card_prices = CardPrice::orderBy('created_at', 'desc')->get();
        $this->card_prices = $card_prices;
        
        $default_rate = CardDefaultRate::where('card_id', $card_id)->first();
        $this->default_rate = $default_rate != null ? $default_rate->seller_rate : null;


    }

    public function updated($fields)
    {
        $this->validateOnly($fields);

        if($this->card != null && $this->country != null){
            $card =  Card::where('id', $this->card)->first();
            $this->types = $card == true ? CardType::where('card_id', $card->id)->orderBy('created_at', 'desc')->get() : null;
            $country = $this->country != null ? Country::where('id', $this->country)->first() : null;
            $this->currency = $country != null ? $country->currency : null;

            if($this->type != null && $card == true){
                $card_type = CardType::where('id', $this->type)->first();
                $card_prices = CardPrice::where('card_id', $card->id)->where('card_type_id', $card_type->id)->get();
                $this->card_prices = $card_prices == true ? $card_prices : null;
            }
        }


    }

    public function submit()
    {
        $this->validate();

        $card = Card::where('id', $this->card)->first();
        $country = Country::where('id', $this->country)->first();
        $card_type = CardType::where('id', $this->type)->first();

        $card_price = $card == true ? 
            CardPrice::where('card_id', $card->id)->where('card_type_id', $card_type->id)->where('amount', $this->amount)->first()
            : null ;

        if($card_price != true && $this->amount != null){
            $card_price = CardPrice::create([
                'card_id' => $card->id,
                'card_type_id' => $card_type->id,
                'amount' => $this->amount
            ]);
        }

        $cardCountry = CardCountry::create([
            'card_id' => $card->id,
            'country_id' => $country->id,
            'card_price_id' => $card_price->id,
            'card_type_id' => $card_type->id,
            'buyer_rate' => $this->buyer_rate,
            'seller_rate' => $this->seller_rate,
        ]);
        
        $default_rate = CardDefaultRate::where('card_id', $card->id)->first();
        if($default_rate == true){
            $default_rate->update([
                'seller_rate' => $this->default_rate
            ]);
        }else{
            $default_rate = CardDefaultRate::create([
                'card_id' => $card->id,
                'buyer_rate' => 0,
                'seller_rate' => $this->default_rate,
            ]);
        }

        session()->flash('success', 'Card rate successfully added');
        return redirect()->route('admin.cards.index');
    }


    public function render()
    {
        return view('livewire.admin.card.add-rate');
    }
}
