<?php

namespace App\Http\Livewire\Admin\Rates\Crypto;

use Livewire\Component;

class Index extends Component
{
    public $crypto, $buyer_rate, $seller_rate, $editing;

    public $rules = [
        'buyer_rate' => 'required|numeric',
        'seller_rate' => 'required|numeric'
    ];

    public function mount($crypto)
    {
        $this->editing = false;
        $this->crypto = $crypto;
        $this->buyer_rate = $crypto->buyer_rate;
        $this->seller_rate = $crypto->seller_rate;
    }

    public function start_editing()
    {
        $this->editing = true;
    }

    public function update_rate()
    {
        $this->validate();

        $crypto = $this->crypto;
        $crypto->update([
            'buyer_rate' => $this->buyer_rate,
            'seller_rate' => $this->seller_rate
        ]);
        
        $this->editing = false;
        session()->flash('success', 'Rate updated successfully');

    }

    public function stop_editing()
    {
        $this->editing = false;
    }
    public function render()
    {
        return view('livewire.admin.rates.crypto.index');
    }
}
