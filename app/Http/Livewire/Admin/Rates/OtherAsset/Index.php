<?php

namespace App\Http\Livewire\Admin\Rates\OtherAsset;

use Livewire\Component;

class Index extends Component
{

    public $asset, $currency, $rate, $editing, $min_amount, $max_amount;

    public $rules = [
        'currency' => 'required',
        'rate' => 'required|numeric',
        'min_amount' => 'required',
    ];

    public function mount($asset)
    {
        $this->editing = false;
        $this->asset = $asset;

        $asset_rate = $this->asset->rate;
        
        $rate = null;

        if($asset_rate == true && $asset_rate->rate_usd != null){
            $rate = $asset_rate->rate_usd;
            $this->currency = '$';
        }

        if($asset_rate == true && $asset_rate->rate_gbp != null){
            $rate = $asset_rate->rate_gbp;
             $this->currency = '£';
        }

        if($asset_rate == true && $asset_rate->rate_euro != null){
            $rate = $asset_rate->rate_euro;
            $this->currency = '€';
        }

        if($asset_rate == true && $asset_rate->rate_cny != null){
            $rate = $asset_rate->rate_cny;
            $this->currency = '¥';
        }
        
        $this->min_amount = $asset_rate == true ? $asset_rate->min_trade_amount : null;
        $this->max_amount = $asset_rate == true ? $asset_rate->max_trade_amount : null;

        $this->rate = $rate;
    }

    public function updated($fields)
    {
        $this->validateOnly($fields);

    }

    public function start_editing()
    {
        $this->editing = true;
    }

    public function update_rate()
    {
        $this->validate();

        $asset = $this->asset;
        $asset_rate = $asset->rate;

        $min_amount = $this->min_amount;
        $max_amount = $this->max_amount;
        $currency = $this->currency;

        $rate = $this->rate;
       
        if($asset_rate == true){
            $asset_rate->update([
               'min_trade_amount' => $min_amount,
               'max_trade_amount' => $max_amount,
               'rate_usd' => $currency == '$' ? $rate : $asset_rate->rate_usd,
               'rate_gbp' => $currency == '£' ? $rate : $asset_rate->rate_gbp,
               'rate_euro' => $currency == '€' ? $rate : $asset_rate->rate_euro,
               'rate_cny' => $currency == '¥' ? $rate : $asset_rate->rate_cyn,
            ]);

        }else{
            $asset->rate()->create([
                'min_trade_amount' => $min_amount,
                'max_trade_amount' => $max_amount,
                'rate_usd' => $currency == '$' ? $rate : null,
                'rate_gbp' => $currency == '£' ? $rate : null,
                'rate_euro' => $currency == '€' ? $rate : null,
                'rate_cny' => $currency == '¥' ? $rate : null,
            ]);
        }
        
        $this->editing = false;
        session()->flash('success', 'Rate updated successfully');

        

    }


    public function render()
    {
        return view('livewire.admin.rates.other-asset.index');
    }
}
