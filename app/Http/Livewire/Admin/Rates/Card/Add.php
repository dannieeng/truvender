<?php

namespace App\Http\Livewire\Admin\Rates\Card;

use App\Models\Card;
use Livewire\Component;
use Illuminate\Support\Str;
use Livewire\WithFileUploads;

class Add extends Component
{
    use WithFileUploads;   

    public $name, $image;

    public $rules = [
        'name' => 'required|string|max:100',
        'image' => 'required|image|mimes:png,jpg,jpeg'
    ];

    public function updated($fields)
    {
        $this->validateOnly($fields);
    }


    public function add()
    {
        $this->validate();

        $file = $this->image;
        // Generate a file name with extension
        $image_name = $file->getClientOriginalName();
        $image_extension = $file->getClientOriginalExtension();
        $image = Str::random(30) .'-'. time() .'.'.$image_extension ;

        // Save the file
        $save_path =  $image_path = $file->storeAs('/public/cards', $image);
        $image_path = 'cards/' . $image;


        $country = Card::create([
            'name' => $this->name,
            'image' => $image_path
        ]);

        session()->flash('success', 'Card was added successfully');
        return redirect()->route('admin.cards.index');
    }

    public function render()
    {
        return view('livewire.admin.rates.card.add');
    }
}
