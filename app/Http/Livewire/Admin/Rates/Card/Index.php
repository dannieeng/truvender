<?php

namespace App\Http\Livewire\Admin\Rates\Card;

use App\Models\Card;
use Livewire\Component;
use Illuminate\Support\Facades\Crypt;

class Index extends Component
{
    public $cards, $search_name, $search_cards;

    public function mount()
    {
        $this->cards = Card::orderBy('name', 'asc')->get();
    }

     public function updated($fields)
    {
        $name = '%' . $this->search_name . '%';
        $this->search_cards = Card::where('name', 'LIKE', $name)->get();
    }

    
    public function render()
    {
        return view('livewire.admin.rates.card.index');
    }
}
