<?php

namespace App\Http\Livewire\Admin\Transaction;

use Livewire\Component;

class Index extends Component
{
    public function render()
    {
        return view('livewire.admin.transaction.index');
    }
}
