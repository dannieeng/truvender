<?php

namespace App\Http\Livewire\Admin\Config;

use Livewire\Component;
use App\Models\Configuration;

class General extends Component
{
    public $min_dep_ngn, $min_dep_usd, $max_trx, $ref_bonus,$trx_fee, $editing, $card_rate;

    public $rules = [
        'min_dep_usd' => 'required|numeric',
        'min_dep_ngn' => 'required|numeric',
        'ref_bonus' => 'required|numeric',
        'max_trx' => 'required|numeric',
        'trx_fee' => 'required|numeric', 
        // 'card_rate' => 'required|numeric'
    ];

    public function mount()
    {
        $this->editing = false;
        $this->min_dep_usd = Configuration::where('key', 'min_usd_deposit')->first()->value;
        $this->min_dep_ngn = Configuration::where('key', 'min_ngn_deposit')->first()->value;
        $this->ref_bonus = Configuration::where('key', 'referral_commision')->first()->value;
        $this->max_trx = Configuration::where('key', 'max_transaction')->first()->value;
        $this->trx_fee = Configuration::where('key', 'transaction fee %')->first()->value;
        // $this->card_rate = Configuration::where('key', 'other_card_rate')->first()->value;
    }

    public function updated($fields)
    {
        $this->validateOnly($fields);
    }

    public function save()
    {
        $this->validate();
        $min_dep_usd = Configuration::where('key', 'min_usd_deposit')->first();
        $min_dep_ngn = Configuration::where('key', 'min_ngn_deposit')->first();
        $min_dep_ngn->update([
            'value' => $this->min_dep_ngn
        ]);
        

        $min_dep_usd->update([
            'value' => $this->min_dep_usd
        ]);

        $ref_bonus = Configuration::where('key', 'referral_commision')->first();

        $ref_bonus->update([
            'value' => $this->ref_bonus
        ]);


        $max_trx = Configuration::where('key', 'max_transaction')->first();

        $max_trx->update([
            'value' =>$this->max_trx
        ]);

        $trx_fee = Configuration::where('key', 'transaction fee %')->first();
        $trx_fee->update([
            'value' => $this->trx_fee
        ]);
        
        // $card_rate = Configuration::where('key', 'other_card_rate')->first();
        // $card_rate->update([
        //     'value' => $this->card_rate
        // ]);
        
         $this->editing = false;
         session()->flash('msg', 'Configuration updated');
    }

    public function start_editing()
    {
        $this->editing = true;
    }

    public function stop_editing()
    {
        $this->editing = false;
    }


    public function render()
    {
        return view('livewire.admin.config.general');
    }
}
