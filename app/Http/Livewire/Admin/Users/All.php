<?php

namespace App\Http\Livewire\Admin\Users;

use App\Models\User;
use Livewire\Component;
use Illuminate\Support\Facades\Auth;

class All extends Component
{
    public $users;

    public function mount()
    {
        $this->users = User::where('id', '!=', Auth::user()->id)->get();
    }

    public function render()
    {
        return view('livewire.admin.users.all');
    }
}
