<?php

namespace App\Http\Livewire\Users\Trade\Refill;

use App\Models\Biller;
use App\Models\Feature;
use Livewire\Component;
use App\Models\Transaction;
use Illuminate\Support\Str;
use App\Models\BillerCategory;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;

class Airtime extends Component
{
    public $biller_group, $billers, $service_carrier, $amount, $pay_opt, $mobile_number;

    public $rules = [
        'amount' => 'required|numeric',
        'mobile_number' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10',
        'service_carrier' => 'required|numeric',
    ];

    public function mount()
    {
        $this->billers = Biller::where('biller_name', 'Airtime')->where('country', 'NG')->get();
        $this->pay_opt = 'naira';
    }

    public function updated($fields)
    {
        $this->validateOnly($fields);

        // dd($this->pay_opt);
    }

    public function continue()
    {

        $this->validate();

        $user = Auth::user();
        $wallet = $user->wallet;

        $service = Feature::where('name', 'Buy Airtime')->first();
        if($service->is_available == false){
                return \redirect()->route('service.unavalable');
        }

        // $biller = $this->service_carrier;
        $amount = $this->amount;
        $mobile = $this->mobile_number;
        $pay_opt = $this->pay_opt;

        $biller = Biller::where('id', $this->service_carrier)->first();
        
        
        if($pay_opt == 'naira'){

            if($wallet->ngn_balance < $amount){
                $this->addError('pay_opt', 'insufficient wllet fund');
                session()->flash('msg', 'YOu don\'t have suficient fund for this transaction');
            }

        }else{
            session()->flash('error', ' Sorry Bitcoin airtime sale is unavailable.');
            return redirect()->route('airtime.buy');
        }

        $transaction = Transaction::create([
            'user_id' => $user->id,
            'trxn_ref' => 'tvtx'.time().Str::random(5),
            'type' => 'debit',
            'section' => 'Buy Airtime',
            'wallet' => $pay_opt == 'naira wallet' ?  'ngn' : 'bitcoin wallet',
            'currency' => $pay_opt == 'naira' ?  'ngn' : 'btc',
            'status' => 'unverified',
            'amount' => $amount,
        ]);


        $flutterwave_secret_key = config('services.flutterwave.secrete_key');
        $fluttwerwave_endpoint = 'https://api.flutterwave.com/v3/bills';
        $bill_payment = Http::withHeaders(['Authorization' => $flutterwave_secret_key])->post($fluttwerwave_endpoint, [
            'country' => $biller->country,
            'customer' => $mobile,
            'amount' => $amount,
            'type' => $biller->biller_name,
            'reference' => 'txtv'.time().Str::random(5)
        ])->json();

        $status = $bill_payment['status'];
        $data = $bill_payment['data'];
        

        if($status == 'success' && $data != null){
            if($pay_opt == 'naira'){
                $wallet->update([
                    'ngn_balance' => $wallet->ngn_balance - $amount
                ]);
            }else{
                //Btc Calculations

            }

            $transaction->update([
                'status' => 'success',
            ]);
            
            session()->flash('success', ' Transaction was successful');
            return redirect()->route('transactions.index');
        }else{
            
            $transaction->update([
                'status' => 'failed',
            ]);
            
            session()->flash('error', ' Transaction failed');
            return redirect()->route('airtime.buy');
        }
    }
    public function render()
    {
       
        return view('livewire.users.trade.refill.airtime');
    }
}
