<?php

namespace App\Http\Livewire\Users\Trade\Ltc;

use Livewire\Component;
use App\Helper\Conversion;
use App\Models\CryptoRates;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;

class Sell extends Component
{
    public $ltc_amount, $usd_amount, $rate, $currency, $er_msg, $wallet;

    public $rules = [
        'ltc_amount' => 'required|numeric',
        'usd_amount' => 'required|numeric',
    ];

    public function mount()
    {
        $user = Auth::user();
        $currency_symbol = 'eth';
        $this->wallet = $user->lithcoin_wallet;
        $this->currency = CryptoRates::where('symbol', \strtoupper($currency_symbol))->first();
    }

    public function updated($fields)
    {
        $this->validateOnly($fields);
        
        $conversion = Conversion::convert_usd_to_eth($this->usd_amount);
        $this->ltc_amount = $conversion;
        $this->rate = $this->usd_amount * $this->currency->seller_rate;

        $user = Auth::user();
        $wallet = $this->wallet;
      
        if($wallet->balance < $this->ltc_amount){
            $this->er_msg = 'You don\'t have sufficient fund to carry out this transaction';
        }else{
            $this->er_msg =null;
        }
        
    }

    public function continue()
    {
        $this->validate();

        $ltc_amount = $this->ltc_amount;
        $usd_amount = $this->usd_amount;
        $rate = $this->rate;
        
        $buy_with = [
            'ltc' => $ltc_amount,
            'usd' => $usd_amount,
            'ngn' => $rate
        ];

        $data = Crypt::encrypt($buy_with);

        return \redirect()->route('trade.litcoin.sell', $data);

    }
    
    public function render()
    {
        return view('livewire.users.trade.ltc.sell');
    }
}
