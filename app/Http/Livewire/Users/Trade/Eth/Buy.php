<?php

namespace App\Http\Livewire\Users\Trade\Eth;

use Livewire\Component;
use App\Helper\Conversion;
use App\Models\CryptoRates;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;

class Buy extends Component
{

    public $eth_amount, $usd_amount, $rate, $currency, $er_msg, $wallet;

    public $rules = [
        'eth_amount' => 'required|numeric',
        'usd_amount' => 'required|numeric',
    ];

    public function mount()
    {
        $user = Auth::user();
        $this->wallet = $user->etherum_wallet;
        $currency_symbol = 'eth';
        $this->currency = CryptoRates::where('symbol', \strtoupper($currency_symbol))->first();
    }
    
    public function updated($fields)
    {
        $this->validateOnly($fields);

        $conversion = Conversion::convert_usd_to_eth($this->usd_amount);
        $this->eth_amount = $conversion;
        $this->rate = $this->usd_amount * $this->currency->buyer_rate;

        $user = Auth::user();
        $wallet = $user->wallet;

        if($wallet->ngn_balance < $this->rate){
            $this->er_msg = 'You don\'t have sufficient fund to carry out this transaction';
        }else{
            $this->er_msg =null;
        }
    }
    
    public function continue()
    {
        $this->validate();

        $eth_amount = $this->eth_amount;
        $usd_amount = $this->usd_amount;
        $rate = $this->rate;
        $buy_with = [
            'eth' => $eth_amount,
            'usd' => $usd_amount,
            'ngn' => $rate
        ];

        $user = Auth::user();

        $user->reset_otp_verification();
        $user->create_otp();

        $data = Crypt::encrypt($buy_with);

        return \redirect()->route('trade.etherum.buy',[
            'data' => $data
        ]);

    }

    public function render()
    {
        return view('livewire.users.trade.eth.buy');
    }
}
