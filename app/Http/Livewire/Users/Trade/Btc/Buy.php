<?php

namespace App\Http\Livewire\Users\Trade\Btc;

use Livewire\Component;
use App\Models\CryptoRates;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Crypt;

class Buy extends Component
{
    public $btc_amount, $usd_amount, $rate, $currency, $er_msg;

    public $rules = [
        'btc_amount' => 'required|numeric',
        'usd_amount' => 'required|numeric',
    ];

    public function mount()
    {
        $user = Auth::user();
        $currency_symbol = 'btc';
        $this->currency = CryptoRates::where('symbol', \strtoupper($currency_symbol))->first();
    }

    public function updated($fields)
    {
        $this->validateOnly($fields);

        $api_call = Http::get('https://blockchain.info/tobtc?currency=USD&value=' . $this->usd_amount)->json();
        $currency = $this->currency;
        $this->btc_amount = $api_call;
        $this->rate = $this->usd_amount * $currency->buyer_rate;

        $user = Auth::user();
        $wallet = $user->wallet;
        if($wallet->ngn_balance < $this->rate){
            $this->er_msg = 'You don\'t have sufficient fund to carry out this transaction';
        }else{
            $this->er_msg =null;
        }
    }

    public function continue()
    {
        $this->validate();

        $btc_amount = $this->btc_amount;
        $usd_amount = $this->usd_amount;
        $rate = $this->rate;
        $buy_with = [
            'btc' => $btc_amount,
            'usd' => $usd_amount,
            'ngn' => $rate
        ];

        $user = Auth::user();
        $user->reset_otp_verification();
        $user->create_otp();

        $data = Crypt::encrypt($buy_with);

        return \redirect()->route('btc.buy.post', [
            'data' => $data
        ]);

    }

    public function render()
    {
        return view('livewire.users.trade.btc.buy');
    }
}
