<?php

namespace App\Http\Livewire\Users\Etherum;

use Livewire\Component;
use App\Helper\Conversion;
use App\Models\Configuration;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;

class Transfer extends Component
{
    public $usd_amount, $wallet, $eth_amount, $reciever, $note;

    public $rules = [
        'reciever' => 'required|string',
        'note' => 'nullable|string|max:250',
        'usd_amount' => 'required|numeric'
    ];

    public function mount()
    {
        $user = Auth::user();
        $this->wallet = $user->etherum_wallet;
    }

    public function updated($fields)
    {
        $this->validateOnly($fields);
        $usd_amount = $this->usd_amount != null ? $this->usd_amount : 0;
        $api_call = Conversion::convert_usd_to_eth($this->usd_amount);;
        $this->eth_amount = $api_call;   
    }

    public function send()
    {
        $user = Auth::user();
        $wallet = $user->etherum_wallet;
        
        $setting = Configuration::where('key', 'max_transaction')->first();
        $trx_limit = $setting->value;
        $ngn = Conversion::convert_usd_to_ngn($this->usd_amount);

        $ethereum_value = $this->eth_amount;

        if($ngn > $trx_limit){
            \session()->flash('error', 'Unable to continue trasaction. Max Limit of $'. $setting->value.' exceeded');
            return redirect()->route('wallet.etherum.index');
        }

        if($wallet->balance >= $ethereum_value){

            $data = [
                'address' => $this->reciever,
                'value' => $this->eth_amount,
                'amount_usd' => $this->usd_amount,
                'amount_ngn' => $ngn,
                'note' => $this->note,
            ];

            $user->reset_otp_verification();
            $user->create_otp();

            session()->flash('data' , $data);
            
            return redirect()->route('wallet.etherum.transfer');
        }else{
            session()->flash('msg', 'Insufficient Btc balance');
            return redirect()->route('wallet.etherum.index');
        }
        

    }

    
    public function render()
    {
        return view('livewire.users.etherum.transfer');
    }
}
