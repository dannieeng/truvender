<?php

namespace App\Http\Livewire\Users\Litecoin;

use Livewire\Component;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Cache;

class Info extends Component
{

    public $user, 
        $wallet, $address, $total_received, 
            $total_sent, $unconfirmed_balance, $final_balance, 
                $transactions, $unconfirmed_transactions, $final_transactions;

    public function mount()
    {
        $user = Auth::user();
        $wallet = $user->lithcoin_wallet;
        $litcoin_transactions = $wallet->transactions;

        $cached_info = Cache::get('litcoin_info');

        if($cached_info == false){
            $url = config('services.block_cypher.url').'/ltc/main/addrs/'.$wallet->address;
            $api_call = Http::get($url)->json();

        
            $this->address = $api_call['address'];
            $this->total_received = $api_call['total_received'];
            $this->total_sent = $api_call['total_sent'];
            $this->unconfirmed_balance = $api_call['unconfirmed_balance'];
            $this->final_balance = $api_call['final_balance'];
            $this->transactions = $api_call['n_tx'];
            $this->unconfirmed_transactions = $api_call['unconfirmed_n_tx'];
            $this->final_transactions = $api_call['final_n_tx'];

            $new_litcoin = [
                'address' => $api_call['address'],
                'total_received' => $api_call['total_received'],
                'total_sent' => $api_call['total_sent'],
                'unconfirmed_balance' => $api_call['unconfirmed_balance'],
                'final_balance' => $api_call['final_balance'],
                'transactions' =>  $api_call['n_tx'],
                'unconfirmed_transactions' => $api_call['unconfirmed_n_tx'],
                'final_transactions' =>  $api_call['final_n_tx'],
            ];

            $add_cache = Cache::put('key', 'litcoin_info', now()->addDays(20));
        }

        else{
            $this->address = $cached_info['address'];
            $this->total_received = $cached_info['total_received'];
            $this->total_sent = $cached_info['total_sent'];
            $this->unconfirmed_balance = $cached_info['unconfirmed_balance'];
            $this->final_balance = $cached_info['final_balance'];
            $this->transactions = $cached_info['n_tx'];
            $this->unconfirmed_transactions = $cached_info['unconfirmed_n_tx'];
            $this->final_transactions = $cached_info['final_n_tx'];
        }
        
    
        
        
    }

    
    public function render()
    {
        return view('livewire.users.litecoin.info');
    }
}
