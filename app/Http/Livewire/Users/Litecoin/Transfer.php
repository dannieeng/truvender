<?php

namespace App\Http\Livewire\Users\Litecoin;

use Livewire\Component;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;

class Transfer extends Component
{
    public $usd_amount, $wallet, $ltc_amount, $reciever, $note;

    public $rules = [
        'reciever' => 'required|string',
        'note' => 'nullable|string|max:250',
    ];

    public function mount()
    {
        $user = Auth::user();
        $this->wallet = $user->lithcoin_wallet;
    }

    public function updated($fields)
    {
        $this->validateOnly($fields);
        $api_call = Http::get('https://api.exchangerate.host/convert?from=USD&to=LTC&amount='.$this->usd_amount .'&source=crypto')->json();
        
        $this->ltc_amount = $api_call['result'];   
    }

    public function send()
    {
        
    }

    public function render()
    {
        return view('livewire.users.litecoin.transfer');
    }
}
