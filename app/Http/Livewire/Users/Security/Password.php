<?php

namespace App\Http\Livewire\Users\Security;

use Livewire\Component;

class Password extends Component
{
    public function render()
    {
        return view('livewire.users.security.password');
    }
}
