<?php

namespace App\Http\Livewire\Users;

use Livewire\Component;
use Illuminate\Support\Facades\Auth;

class Notifications extends Component
{
    public  $user, $notifications;
    public function mount()
    {
        $this->user = Auth::user();   
        $user = $this->user;
        $this->notifications = $user->notifications;
        $this->unreadNotifications = $user->notifications;
    }
    public function render()
    {
        return view('livewire.users.notifications');
    }
}
