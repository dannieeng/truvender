<?php

namespace App\Http\Livewire\Users\Transfer;

use Livewire\Component;
use Illuminate\Support\Facades\Auth;

class Peer extends Component
{

    public $data;

    public function mount($data)
    {
        $this->data = $data;
    }

    public function continue()
    {
        $user = Auth::user();
        $user->reset_otp_verification();
        $user->create_otp();

        $data = $this->data;

        session()->flash('data', $data);
        return redirect()->route('transfer.peer2peer.post');
        
    }

    public function cancel()
    {
        return redirect()->route('wallet');
    }


    public function render()
    {
        return view('livewire.users.transfer.peer');
    }
}
