<?php

namespace App\Http\Livewire\Users\Transfer;

use App\Models\Bank;
use Livewire\Component;
use Illuminate\Support\Str;
use App\Models\Configuration;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use App\Notifications\NewTransaction;
use Illuminate\Support\Facades\Crypt;

class Peer2Peer extends Component
{
    public $account_name, $account_number, $account_verified, $bank, $banks, $amount, $account;
    public $add_account = false;

    public $rules = [
        'amount' => 'required|numeric',
        'account_number' => 'required|numeric',
        'account_name' => 'required|string',
        'bank' => 'required',
    ];
    


    public function mount()
    {
        $user = Auth::user();
        $this->account = $user->banking;
        $this->account_number = $this->account->acc_number;
        $this->account_name = $this->account->acc_name;
        $this->bank = $this->account->bank_id;
        $this->account_verified = true;
        $this->banks = Bank::orderBy('name', 'asc')->get();
    }
     public function updated($fields)
    {
        $this->validateOnly($fields);

        //Resolve Account
        if($this->bank != null && $this->account_number != null){
            $bank = Bank::where('id', $this->bank)->first();
  
            $bank_code =  $bank->code ;

            $flutterwave_endpoint = 'https://api.flutterwave.com/v3/accounts/resolve';
            $flutterwave_secrete_key = \config('services.flutterwave.secrete_key');
            $resolve_account = Http::withHeaders(['Authorization' => $flutterwave_secrete_key])->post($flutterwave_endpoint, [
                'account_number' => $this->account_number,
                'account_bank' => $bank_code,
            ])->json();

            $data = $resolve_account['data'];
           
            if($resolve_account['status'] == 'success'){
                $this->account_name = $data['account_name'];
                $this->account_verified == true;
                
            }else{
                $this->addError('account_number', 'account number is not valid');
            }
        }
           
    }

    public function add()
    {
        $this->account_number = null;
        $this->account_name = null;
        $this->bank = null;
        $this->account_verified = null;
        $this->add_account = true;
    }

    public function user_my_account()
    {
        $user = Auth::user();
        $this->account = $user->banking;
        $this->account_number = $this->account->acc_number;
        $this->account_name = $this->account->acc_name;
        $this->bank = $this->account->bank_id;
        $this->account_verified = true;

        $this->add_account = false;
    }


    public function continue()
    {
        $this->validate(); 
        $user = Auth::user();
        $wallet = $user->wallet;
        $trx_limit = Configuration::where('key', 'max_transaction')->first();
        $bank = Bank::where('id', $this->bank)->first();

        if($this->amount >= $trx_limit){
            session()->flash('error', 'Transaction Limit Exceeded');
            return redirect()->route('wallet');
        }else if($this->amount < $wallet->ngn_balance){
            
            if($this->account_verified == true){
                
                $peer_data = [
                    'amount' => $this->amount,
                    'bank' => $bank,
                    'account_name' => $this->account_name,
                    'account_number' => $this->account_number,
                ];

                $data = encrypt($peer_data);

                return redirect()->route('transfer.peer2peer.preview', $data);   

            }else{
                $this->addError('account_number', 'Account number could not be resolved');
            }
            if($this->amount > $wallet->ngn_balance){
                session()->flash('error', 'insufficient fund in wallet');
                return redirect()->route('wallet');
            }
        }

    }

    public function render()
    {
        return view('livewire.users.transfer.peer2-peer');
    }
}
