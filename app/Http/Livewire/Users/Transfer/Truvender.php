<?php

namespace App\Http\Livewire\Users\Transfer;

use App\Models\User;
use Livewire\Component;
use App\Models\Transaction;
use Illuminate\Support\Str;
use App\Models\Configuration;
use Illuminate\Support\Facades\Auth;
use App\Notifications\NewTransaction;

class Truvender extends Component
{
    public $username, $amount, $user, $wallet;

    public $rules = [
        'username' => 'required|string',
        'amount' => 'required|numeric',
    ];
    public function updated($fields)
    {
        $this->validateOnly($fields);

        $user = User::where('username', $this->username)->first();

        if($user!=true){
            $this->addError('username', 'The username does not exist');
        }
    }

    public function continue()
    {
        $this->validate();

        $user = Auth::user();
        $wallet = $user->wallet;
        $trx_limit = Configuration::where('key', 'max_transaction')->first();
        $truvender = User::where('username', $this->username)->first();
        $fee = 0;

        if($truvender!=true){
            $this->addError('username', 'The username does not exist');
        }else{
            if($this->amount >= $trx_limit){
                session()->flash('error', 'Transaction Limit Exceeded');
                return redirect()->route('wallet');
            }else if($this->amount < $wallet->ngn_balance){

                $truvenderWallet = $truvender->wallet;
                
                $user->reset_otp_verification();
                $user->create_otp();

                $data = [
                    'amount' => $this->amount,
                    'username' => $this->username
                ];

                session()->flash('data', $data);
                return redirect()->route('transfer.truvender');

                
            }else if($this->amount > $wallet->ngn_balance){
                session()->flash('error', 'insufficient fund in wallet');
                return redirect()->route('wallet');
            }
        }
    }
    public function render()
    {
        return view('livewire.users.transfer.truvender');
    }
}
