<?php

namespace App\Http\Livewire\Users\Settings;

use App\Models\Bank;
use Livewire\Component;
use App\Models\BankDetail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;

class Banking extends Component
{
    public $user, $banks, $bank, $account_name, $account_number;

    public  $verified = false;
    public $rules = [
        'account_name' => 'required|string',
        'account_number' => 'required|numeric',
        'bank' => 'required|string',
    ];
    
    public function mount($user)
    {
        $this->user = $user;
        $banks = Bank::orderBy('name', 'asc')->get();
        $this->banks = $banks;
    }

    public function updated($fields)
    {
        $this->validateOnly($fields);
        $user = Auth::user();

        //Resolve Account
        if($this->bank != null && $this->account_number != null){
            $bank = Bank::where('id', $this->bank)->first();
            $bank_code = $bank->code;


            $endpoint = config('services.paystack.url').'bank/resolve?account_number='.$this->account_number.'&bank_code='.$bank_code ;
            $paystack_sk = \config('services.paystack.sk');
            $resolve_account = Http::withHeaders(['Authorization' => 'Bearer ' . $paystack_sk])->get($endpoint)->json();
            
            if($resolve_account['status'] == true){

                
                $data = $resolve_account['data'];
                
                $this->account_number = $data['account_number'];
                $this->account_name = $data['account_name'];
                
                $this->verified = true;
                
            }else{
                $this->addError('account_number', 'account number is not valid');
            }
        }
    }


    public function submit()
    {
        $this->validate();
        $user = Auth::user();

        $account_verified = $this->verified;
      
        if($account_verified == true || $this->account_name != null){
            $account_number = $this->account_number;
            $account_name = $this->account_name;

            $banking = BankDetail::create([
                'user_id' => $user->id,
                'bank_id' => $this->bank,
                'acc_name' => $account_name,
                'acc_number' => $account_number,
            ]);

            \session()->flash('success', 'Bank account added successfully');
            return \redirect()->route('settings');
                    
        }else{
            $this->addError('account_number', 'account number is not valid');
        }
      
    }
    public function render()
    {
        return view('livewire.users.settings.banking');
    }
}
