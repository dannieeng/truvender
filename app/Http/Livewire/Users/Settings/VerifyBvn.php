<?php

namespace App\Http\Livewire\Users\Settings;

use Livewire\Component;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;

class VerifyBvn extends Component
{
    public $bvn, $user, $banking;

    public $rules = [
        'bvn' => 'required|numeric'
    ];

    public function mount()
    {
        $this->user = Auth::user();
        $banking = $this->banking;
    }

    public function render()
    {
        return view('livewire.users.settings.verify-bvn');
    }

    public function bvn_add()
    {
        $this->validate();

        $banking = $this->user->banking;

        //Resolve Bvn
        if($this->bvn != null){
            // $flutterwave_endpoint = 'https://api.flutterwave.com/v3/kyc/bvns/'. $this->bvn;
            // $flutterwave_secrete_key = \config('services.flutterwave.secrete_key');

            // $resolve_bvn = Http::withHeaders(['Authorization' => $flutterwave_secrete_key])
            // ->get($flutterwave_endpoint)->json();

            $endpoint_url = config('services.paystack.url').'/identity/bvn/resolve/:'. $this->bvn;
            $paystack_sk = \config('services.paystack.sk');


            $resolve_bvn = Http::withHeaders(['Authorization' => 'Bearer ' . $paystack_sk])
            ->get($endpoint_url)->json();
           
            if($resolve_bvn['status'] == true){
                if($banking == true){
                    $banking->update([
                        'bvn' => $this->bvn
                    ]);
                }else{
                    session()->flash('error', 'Account details have not been added');
                }
            }else{
                $this->addError('bvn', 'bvn is not valid');
            }
        }
    }
}
