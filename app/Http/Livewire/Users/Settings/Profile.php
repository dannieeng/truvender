<?php

namespace App\Http\Livewire\Users\Settings;


use Livewire\Component;
use Illuminate\Support\Facades\Auth;

class Profile extends Component
{
    public $user, $profile, $firstname, $lastname, $phone;
    public $rules = [
        'firstname' => 'required|string|max:100',
        'lastname' => 'required|string|max:100',
        'phone' => 'required|numeric'
    ];

    public function mount()
    {
        $this->user = Auth::user();
        $this->profile = $this->user->profile;
        $profile = $this->profile;
        $this->firstname = $profile->firstname;
        $this->lastname = $profile->lastname;
        $this->phone = $profile->phone;
    }

    public function updated($fields)
    {
        $this->validateOnly($fields);
    }

    public function edit()
    {
        $user = $this->user;
        $profile = $user->profile;
       
        $profile->update([
            'firstname' => $this->firstname,
            'lastname' => $this->lastname,
            'phone' => $this->phone,
        ]);
        

        session()->flash('msg', 'Updated successfully');
    }
    public function render()
    {
        return view('livewire.users.settings.profile');
    }
}
