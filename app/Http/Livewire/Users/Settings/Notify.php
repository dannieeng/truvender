<?php

namespace App\Http\Livewire\Users\Settings;

use Livewire\Component;
use Illuminate\Support\Facades\Auth;

class Notify extends Component
{

    public $notify_through, $default_currency, $notify_with, $setting, $user;
    public $currency, $notify;
    public $editting = false;
    
    
    public function mount()
    {
        //Get User settings
        $this->user = Auth::user();
        $this->setting = $this->user->setting;
        $this->currency = $this->setting->default_currency;
        $this->notify = $this->setting->notify_with;
        
    }


    public function changeValue()
    {
        $setting = $this->setting;

        $notify_with = $this->notify_with;
        $default_currency = $this->default_currency;


        $setting->update([
            'notify_with' => $notify_with != null ? $notify_with : $setting->notify_with,
            'default_currency' => $default_currency != null ? $default_currency : $setting->default_currency
        ]);

        $this->currency = $setting->default_currency;
        $this->notify = $setting->notify_with;
        $this->editting = false;
        \session()->flash('msg','updated');
    }

    public function edit()
    {
        $this->editting = true;
    }

    public function render()
    {
        return view('livewire.users.settings.notify');
    }
}
