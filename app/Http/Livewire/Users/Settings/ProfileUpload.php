<?php

namespace App\Http\Livewire\Users\Settings;


use Livewire\Component;
use Illuminate\Support\Str;
use Livewire\WithFileUploads;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;


class ProfileUpload extends Component
{
    use WithFileUploads;

    public $image, $user;

    public $rules = [
        'image' => 'required|image|mimes:jpeg,png,jpg|max:2048'
    ];

    public function updated($fields)
    {
        $this->validateOnly($fields);
    }
    
    public function mount()
    {
        $this->user = Auth::user();
    }

    public function upload()
    {
        $this->validate();
        
        $file = $this->image;
        $user = Auth::user();
        $user->updateProfilePhoto($file);

       session()->flash('msg', 'Profile photo update');
    }
    public function render()
    {
        return view('livewire.users.settings.profile-upload');
    }
}
