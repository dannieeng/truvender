<?php

namespace App\Http\Livewire\Users\Settings;

use Livewire\Component;
use App\Models\Configuration;
use Illuminate\Support\Facades\Http;

class Verify extends Component
{
    public $user, $banking, $bvn, $transaction_limit, $transaction_limit2;

    
    public function mount($user, $banking)
    {
        $this->user = $user;
        $this->banking = $banking;
        $this->transaction_limit = Configuration::where('key', 'max_transaction')->first();
        $this->transaction_limit2 = Configuration::where('key', 'max_transaction2')->first();

    }


    public function render()
    {
        return view('livewire.users.settings.verify');
    }


}
