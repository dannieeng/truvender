<?php

namespace App\Http\Livewire\Users\OtherAsset;

use App\Models\User;
use Livewire\Component;
use App\Models\Transaction;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use App\Models\OtherAssetTransaction;
use App\Notifications\OtherAssetFund;

class Index extends Component
{
    public  $asset, $asset_currency, $amount, $currency, $email, $phone, $rate, $rates;

    public $rules = [
        'amount' => 'required',
        'currency' => 'required',
        'email' => 'required',
        'phone' => 'required'
    ];


    public function mount($asset)
    {
        $this->asset = $asset;

        $this->asset_currency = $this->asset->currency ? $this->asset->currency->currency : null;

        $this->currency = $this->asset_currency;
    }


    public function updated($fields)
    {
        // $this->validateOnly($fields);

        $asset = $this->asset;
        
        
        
        $asset_rate = $asset->rate;
        
        $rates = $asset_rate->rate_usd;
        $currency = $this->currency;

        if($currency == '€'){
            $rates = $asset_rate->rate_euro;
        }

        if($currency == '£'){
            $rates = $asset_rate->rate_gbp;
        }

        if($currency == '¥'){
            $rates = $asset_rate->rate_cny;
        }
        
        if($currency == '$'){
            $rates = $asset_rate->rate_usd;
        }
        

        $this->rate = $this->amount * $rates;

        
    }

    public function continue()
    {
        $this->validate();

        $user = Auth::user();
        //Create Transaction
        $transaction = Transaction::create([
            'user_id' => $user->id,
            'trxn_ref' => 'tvtx'.time() . Str::random(5),
            'type' => 'credit',
            'section' => 'convert ' . $this->asset->name . ' fund',
            'wallet' => 'ngn_wallet',
            'currency' => 'ngn',
            'amount' => $this->rate,
            'status' =>'pending',
        ]);

        $asset_transaction = OtherAssetTransaction::create([
            'user_id' => $user->id,
            'other_asset_id' => $this->asset->id,
            'account_email' => $this->email,
            'account_phone' => $this->phone,
            'transaction_id' => $transaction->id,
            'rate' => $this->rate,
            'currency' => $this->currency,
            'amount' => $this->amount,
            // 'image_path' => $saved_to,
            'price' => $this->rate,
            'status' => false
        ]);

        $data = [
            'asset' => $this->asset,
            'currency' => $this->currency,
            'amount' => $this->amount,
            'rate' => $this->rate,
            'phone' => $this->phone,
            'email' => $this->email,
            'transaction_id' => $transaction->id,
            'asset_transaction_id' => $asset_transaction->id,
        ];

        $admin_email = 'info@truvender.com';

        $admin = User::where('role_id', 1)->first();
        
        $admin->notify(new OtherAssetFund($asset_transaction));

        return  redirect()->route('trade.other-asset.next', encrypt($data));
    }

    
    public function render()
    {
        return view('livewire.users.other-asset.index');
    }
}
