<?php

namespace App\Http\Livewire\Users\Cards;

use App\Models\Card;
use Livewire\Component;
use Illuminate\Support\Facades\Crypt;

class Sell extends Component
{
    public $cards, $search_name, $search_cards;

    public function sell_card($card)
    {
        $card_id = Crypt::encrypt($card);
        return \redirect()->route('buy.card', $card_id);
    }

     public function updated($fields)
    {
        $name = '%' . $this->search_name . '%';
        $this->search_cards = Card::where('name', 'LIKE', $name)->get();
    }
    
    public function render()
    {
        $this->cards = Card::orderBy('name', 'Asc')->get();
        return view('livewire.users.cards.sell');
    }
}
