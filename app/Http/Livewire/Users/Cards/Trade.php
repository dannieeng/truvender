<?php

namespace App\Http\Livewire\Users\Cards;

use App\Models\Country;
use App\Models\Feature;
use App\Models\CardPrice;
use Livewire\Component;
use App\Models\CardType;
use App\Models\CardTrade;
use App\Models\CardCountry;
use App\Models\Transaction;
use Illuminate\Support\Str;
use App\Models\Configuration;
use Livewire\WithFileUploads;
use App\Mail\GiftCardPurchase;
use App\Models\CardTradeImage;
use App\Models\CardDefaultRate;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use App\Notifications\TransactionSuccess;
use Illuminate\Notifications\Notification;
use Illuminate\Routing\Redirector;

class Trade extends Component
{
    use WithFileUploads;
    public $card, $type, 
    $types, $prices, $price, $new_price, 
    $countries, $country, 
    $rate, $trade,
    $code, $currency,
    $confirmation_time,
     $confirmation_time2, $images, $card_rate, $amount;
     
     public $adding = false;

    public $rules = [
        'type' => 'required|numeric',
        'price' => 'required|numeric',
        'country' => 'required|string',
        'code' => 'nullable|string',
        'new_price' => 'nullable|numeric'
    ];

    public function user()
    {
        return Auth::user();
    }

    public function mount($card, $type)
    {
        $this->card = $card;


        //Get Card Countries
        $countries = CardCountry::groupBy('country_id')->where('card_id', $this->card->id)->get();
        
        $this->countries = $countries;


        //Set card types
        $this->types = $card->types;

        $this->prices = $this->card->prices;
        //Define Type of trade
        $this->trade = $type;

        $settings = Configuration::all();

        //Get configurations
        $this->confirmation_time = Configuration::where('key', 'card_confimation_time')->first()->value;
        $this->confirmation_time2 = Configuration::where('key', 'card_confimation_time2')->first()->value;
    }

    public function updated($fields)
    {
        $this->validateOnly($fields);

        $country = Country::whereName($this->country)->first();// get country
        $this->currency =  $country == true ? $country->currency : null;
        $type = CardType::whereName($this->type)->first(); // get card type


        // assign rate
        if ($this->country != null && $this->type != null) {
            $this->prices = $this->card->prices()->where('card_type_id', $this->type)->get();
       
            $this->card_rate = $this->price != null ? CardCountry::whereCard_id($this->card->id)
                ->where('card_type_id', $this->type)->where('card_price_id', $this->price)->first() : null;
                
        }
        
        $this->price = $this->adding == true ? null : $this->price;
        $price = $this->adding == true ? null : $this->price;
        if($price == null && $this->new_price != null){
            $default_rate = CardDefaultRate::where('card_id', $this->card->id)->first();
            $rate = $this->amount > 0 ? ($default_rate->seller_rate * $this->new_price) * $this->amount : 0;
            $this->rate =  $default_rate == true ? $rate : 0;
        }

        // Calculate rates
        if($this->card_rate != null){
            // get rates
            if($this->trade == 'buy'){
                $rate = $this->amount > 0 ? $this->card_rate->buyer_rate * $this->amount : 0;
            }else{
                $rate = $this->amount > 0 ?  $this->card_rate->seller_rate * $this->amount : 0;
            }
            $this->rate = $rate;
        }

    }
    
    public function add_price()
    {
        if($this->adding == false){
            $this->adding = true;
        }
    }

    public function red()
    {
        $user = Auth::user();
        $this->validate();
        
    }

    public function continue()
    {
        $user = Auth::user();
        $this->validate();

        $amount = $this->amount;

        $max_transation_unverified = Configuration::where('key', 'max_transaction')->first();
        
        $country = Country::where( 'id',$this->country)->first();// get country
        
        if($user->profile->verified != true && $this->rate > (int)$max_transation_unverified->value){
            \session()->flash('err', 'To make transactions above ' . $max_transation_unverified->value . ', do provide your Bank Verification Code/Number');
        }else{
            if($this->trade == 'sell'){
                
                $service = Feature::where('name', 'Sell GiftCards')->first();
                if($service->is_available == false){
                        return \redirect()->route('service.unavalable');
                }

                
                $card_trade = CardTrade::create([
                    'user_id' => $user->id,
                    'card_id' => $this->card->id,
                    // 'transaction_id' => $transaction->id,
                    'card_country_id' => $this->country,
                    'status' => 'unreviewed',
                    'amount' => $this->amount,
                    'price' => $this->rate,
                    'trade_type' => $this->trade,
                    'rate' => $this->card_rate->seller_rate,
                    'code' => $this->code, 
                ]);
                
                
                $trade_id = encrypt($card_trade->id);

                return \redirect()->route('trade.image-upload', $trade_id);
    
            }elseif($this->trade == 'buy' && ($user->wallet->ngn_balance > $this->rate)){

                $service = Feature::where('name', 'Buy GiftCards')->first();
                if($service->is_available == false){
                        return \redirect()->route('service.unavalable');
                }

                $user->reset_otp_verification();
                $user->create_otp();

                $data = [
                    'amount' => $this->amount,
                    'card' => $this->card,
                    'rate' => $this->rate,
                    'buyer_rate' => $this->card_rate->buyer_rate,
                    'trade' => $this->trade,
                    'country' => $this->country,
                    'card_country' => $this->card_rate->id
                ];
                session()->flash('data', $data);
                return redirect()->route('buy.gift-card.submit');
            }
        
            
        }
    }


    public function render()
    {
        return view('livewire.users.cards.trade');
    }
}
