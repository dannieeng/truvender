<?php

namespace App\Http\Livewire\Users;

use Livewire\Component;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use App\Notifications\SendAccessCode;
use Illuminate\Notifications\Notification;

class Code extends Component
{
    public $code, $msg;

    public $rules = [
        'code' => 'required'
    ];
    


    public function submit_code()
    {
        $this->validate();
        $user = Auth::user();
        $status = $user->validate_otp($code);
        if($status == true){
            return response(null, 201);
        }else{
            $this->addError(['code' => 'Invalid verification code!']);
        }
    }

    public function resend()
    {
        $user = Auth::user();
        $user->create_otp();
        $this->msg == 'Sent!';
    }

    public function render()
    {
        return view('livewire.users.code');
    }
}
