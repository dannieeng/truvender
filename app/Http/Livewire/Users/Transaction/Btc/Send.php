<?php

namespace App\Http\Livewire\Users\Transaction\Btc;

use Livewire\Component;
use App\Helper\Conversion;
use App\Models\Transaction;
use Illuminate\Support\Str;
use App\Models\Configuration;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;

class Send extends Component
{
    public $usd_amount, $wallet, $btc_amount, $reciever, $note;

    public $rules = [
        'reciever' => 'required|string',
        'note' => 'nullable|string|max:250',
    ];
    public function mount()
    {
        $user = Auth::user();
        $this->wallet = $user->wallet;
    }

    public function updated($fields)
    {
        $this->validateOnly($fields);
        $api_call = Conversion::convert_usd_to_btc($this->usd_amount);;
        
        $this->btc_amount = $api_call;
        
    }


    public function send()
    {
        $wallet = $this->wallet;

        $reciever = $this->reciever;
        $btc_amount = $this->btc_amount;
        $usd_amount = $this->usd_amount;

        $wallet->update_btc_address();
       
        $banking = Auth::user()->banking;

        $setting = Configuration::where('key', 'max_transaction')->first();

        if($wallet->btc_balance >= $btc_amount){
            if($btc_amount > $setting->value && $banking->bvn == null){
                \session()->flash('error', 'Unable to continue trasaction. Max Limit of $'. $setting->value.' exceeded');
                return \redirect()->route('transactions.index');
            }

            $transfer = $wallet->transfer_btc($reciever, $btc_amount);
            if($transfer['status'] == true && isset($transfer['data'])){
                
                $get_fee = Configuration::where('key', 'crypto_tx_fee')->first()->value; 
                $get_fee2 = Configuration::where('key', 'crypto_tx_fee2')->first()->value; 

                $fee = $usd_amount >= 600 ?
                    ($usd_amount / 100) * $get_fee2 : 
                        ($usd_amount / 100) * $get_fee;

                $transaction_fee = Conversion::convert_usd_to_btc($fee);

                $wallet->update_btc_address();


                $wallet->update([
                    'btc_balance' => $wallet->btc_balance - $transaction_fee
                ]);

                $transaction = Transaction::create([
                    'user_id' => $user->id,
                    'trxn_ref' => 'tvtx'.time().Str::random(5),
                    'wallet' => 'Btc wallet',
                    'section' => 'Send Btc',
                    'type' => 'debit',
                    'currency' => 'Btc',
                    'amount' => $this->btc_amount,
                ]);

                
                
                \session()->flash('success', 'Transaction was successful');
                return \redirect()->route('transactions.index');
            }else{

                \session()->flash('error', 'Transaction failed');
                return \redirect()->route('transactions.index');
            }
        }else{
            session()->flash('msg', 'Insufficient Btc balance');
        }
    }
    public function render()
    {
        return view('livewire.users.transaction.btc.send');
    }
}
