<?php

namespace App\Http\Livewire\Users\Rates;

use App\Models\Card;
use App\Models\Country;
use App\Models\CardType;
use App\Models\CardCountry;
use Livewire\Component;
use App\Models\CryptoRates;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;

class Sell extends Component
{


    public $countries, $cards,$card_types, $asset, $country, $type, $amount, $calc_amount, $asset_type;

    public $rules = [
        'type' => 'nullable|string',
        'asset' => 'required|string', 
        'country' => 'required|numeric',
        'amount' => 'required|numeric',
        'asset_type' => 'required'
    ];
    
    public function mount()
    {
        $countries = Country::orderBy('created_at', 'desc')->get();
        $cards = Card::orderBy('name', 'Asc')->get();
        
        $this->card_types = CardType::all();
        $this->cards = $cards;
        $this->countries = $countries;
        // $this->asset = 'btc';
    }

    public function updated($fields)
    {
        $this->validateOnly($fields);
        $user = Auth::user();
        $wallet = $user->wallet;

        if($this->asset_type == 'crypto'){
            $this->cards = CryptoRates::orderBy('name', 'Asc')->get();
        }else{
             $this->cards = Card::orderBy('name', 'Asc')->get();
        }

        if($this->asset_type == 'crypto'){
            $crypto = CryptoRates::where('id', $this->asset)->first();
            $this->calc_amount = $this->amount !=null ? $btc->seller_rate * $this->amount : 0;
        }else{
            $card_rate = CardCountry::where('card_id', $this->asset)
            ->where('country_id', $this->country)->where('card_type_id', $this->type)->first();

            if($card_rate != null){
                $this->calc_amount = $card_rate->seller_rate * $this->amount;
            }else{
                $this->calc_amount = null;
            }
        }
       
        if($this->calc_amount > $wallet->ngn_balance){
            session()->flash('msg', 'You don\'t have sufficient fund on your wallet to continue');
        }
    }


    public function getRate()
    {
        $this->validate();
        if($this->asset_type != 'crypto'){
            $card_rate = CardCountry::where('card_id', $this->asset)
            ->where('country_id', $this->country)->where('type', $this->type)->first();
            $calc_amount = $this->amount!=null? $card_rate->seller_rate * $this->amount:0;
        }else{
            $btc = CryptoRates::where('symbol', $this->asset)->first();
            $calc_amount = $this->amount!=null?$btc->seller_rate * $this->amount : 0;
        }
    }

    public function trade()
    {

        if($this->calc_amount != null){
            if($this->asset_type != 'crypto'){
                $data = [
                    'current' => 'sell',
                    'amount' => $this->amount,
                    'asset' => $this->asset,
                    'country' => $this->country,
                    'type' => $this->type,
                    'rate' => $this->calc_amount
                ];
            }else{
                $data = [
                    'current' => 'sell',
                    'amount' => $this->amount,
                    'asset' => $this->asset,
                    'rate' => $this->calc_amount
                ];
            }
        }

        $trade_with = Crypt::encrypt($data);

        return \redirect()->route('trading.preview', $trade_with);
        
    }
    public function render()
    {
        return view('livewire.users.rates.sell');
    }
}
