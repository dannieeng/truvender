<?php

namespace App\Http\Livewire\Users;

use App\Models\Country;
use Livewire\Component;
use App\Models\Card;

class Rates extends Component
{
    public $countries, $cards;
    
    public function mount()
    {
        $countries = Country::orderBy('created_at', 'desc')->get();
        $cards = Card::orderBy('name', 'Asc')->get();

        $this->cards = $cards;
        $this->countries = $countries;
    }
    public function render()
    {
        return view('livewire.users.rates');
    }
}
