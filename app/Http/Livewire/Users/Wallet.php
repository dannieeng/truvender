<?php

namespace App\Http\Livewire\Users;

use Livewire\Component;
use Illuminate\Support\Facades\Auth;

class Wallet extends Component
{
    public $user, $type, $btc_transactions, $ngn_transactions, $wallet;

    public function mount()
    {
        $this->user = Auth::user();
        $user = $this->user;
        $btc_transactions = $user->transactions()->orderBy('created_at', 'desc')->where('wallet', 'btc wallet')->get();
        $ngn_transactions = $user->transactions()->orderBy('created_at', 'desc')->where('wallet', 'ngn_wallet')->get();

        $this->ngn_transactions = $ngn_transactions;
        $this->btc_transactions = $btc_transactions;
        $this->wallet = $user->wallet;  
    }


    public function render()
    {
        return view('livewire.users.wallet');
    }
}
