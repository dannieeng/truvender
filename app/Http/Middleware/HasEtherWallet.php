<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HasEtherWallet
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $user = Auth::user();
        $ether_wallet = $user->etherum_wallet;
        if($user == true && $ether_wallet == true){
            return $next($request);
        }else{
            return redirect()->route('wallet.etherum.create');
        }
    }
}
