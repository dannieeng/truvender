<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HasLitcoinWallet
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $user = Auth::user();
        $litcoin_wallet = $user->lithcoin_wallet;
        if($user == true && $litcoin_wallet == true){
            return $next($request);
        }else{
            return redirect()->route('wallet.litcoin.create');
        }
    }
}
