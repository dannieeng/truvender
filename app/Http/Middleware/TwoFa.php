<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\AccessCode;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Notifications\SendAccessCode;
use Illuminate\Support\Facades\Cache;
use Illuminate\Notifications\Notification;

class TwoFa
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $user = Auth::user();
        
        
        
        if($user->has_verify_otp)
        {  
            return $next($request);
        }

        $what_next = encrypt($request->url());
        $data = session('data');
        Cache::put('uri', $what_next, now()->addMinutes('10'));
        Cache::put('data', $data, now()->addMinutes('10'));

        return redirect('/transaction/verify-otp');
        
    }


    
}
