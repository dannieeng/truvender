<?php

namespace App\Http\Controllers\Override;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Laravel\Fortify\Contracts\RegisterViewResponse;

class RegisterController extends Controller
{
    public function create(Request $request): RegisterViewResponse
    {
        if ($request->has('ref')) {
            session(['referrer' => $request->query('ref')]);
        }
        return app(RegisterViewResponse::class);
    }
}
