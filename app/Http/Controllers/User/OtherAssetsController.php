<?php

namespace App\Http\Controllers\User;

use App\Models\User;
use App\Models\OtherAsset;
use App\Models\Transaction;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\OtherAssetTransaction;
use App\Notifications\OtherAssetFund;

class OtherAssetsController extends Controller
{
    public function trade($asset_id)
    {
        $id = decrypt($asset_id);
        $asset = OtherAsset::where('id', $id)->first();

        return view('users.otherasset.index', [
            'asset' => $asset
        ]);
    }

    public function next($data)
    {
        $trade_data = decrypt($data);

        $asset = $trade_data['asset'];
        $currency = $trade_data['currency'];
        $rate = $trade_data['rate'];
        $amount = $trade_data['amount'];

        return view('users.otherasset.pre-submit', [
            'asset' => $asset,
            'currency' => $currency,
            'amount' => $amount,
            'rate' => $rate,
            'data' => $trade_data,
        ]);
    }

    public function _next_(Request $request)
    {
        $this->validate($request, [
            'data' => 'required',
            'reciept' => 'required|image|mimes:png,jpg,svg,jpeg'
        ]);

        $data = decrypt($request->data);
        $user = Auth::user();
        $asset = $data['asset'];
        $asset_rate = $asset->rate;

        $asset_transaction = OtherAssetTransaction::where('id', $data['asset_transaction_id'])->first();
        $transaction = Transaction::where('id', $data['transaction_id'])->first();

        $currency = $data['currency'];
        $rate = $asset->rate_usd;

        if($currency == '€'){
            $rate = $asset_rate->rate_euro;
        }

        if($currency == '£'){
            $rate = $asset_rate->rate_gbp;
        }

        if($currency == '¥'){
            $rate = $asset_rate->rate_cny;
        }
        
        if($currency == '$'){
            $rate = $asset_rate->rate_usd;
        }
        

        if ($request->hasFile('reciept')) :
            $reciept = $request->file('reciept');
            $fileName = $reciept->getClientOriginalName();
            $fileExt = $reciept->getClientOriginalExtension();
            $imageName = 'OA-reciept'.Str::random(5) .'-'. time().'.'.$fileExt ;

            // Save the file
            $path = $reciept->storeAs('/public/uploads/other-assets/reciepts/', $imageName);

            $saved_to = '/uploads/other-assets/reciepts/'.$imageName;
        else :
             \session()->flash('error', 'Please uplaod a reciept of your transaction');
            return \redirect()->back();
        endif;


        $asset_transaction->update([
            'image_path' => $saved_to,
        ]);
        


        session()->flash('success', 'Your reciept was sent successfully and transaction is under confirmation');
        return redirect()->route('products');
    }

    public function _cancel_(Request $request)
    {
        $data = decrypt($request->data);
        $asset_transaction = OtherAssetTransaction::where('id', $data['asset_transaction_id'])->first();
        $transaction = Transaction::where('id', $data['transaction_id'])->first();

        $transaction->update([
            'status' => 'failed'
        ]);

        session()->flash('error', 'Process time exceded! process was cancled and transaction was deleted');
        return redirect()->route('dashboard');
    }
}
