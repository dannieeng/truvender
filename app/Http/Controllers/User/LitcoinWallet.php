<?php

namespace App\Http\Controllers\User;

use App\Models\Feature;
use App\Helper\Conversion;
use App\Models\Transaction;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Models\Configuration;
use App\Models\LitcoinTransaction;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use App\Notifications\NewTransaction;

class LitcoinWallet extends Controller
{
    public function index()
    {
        $user = Auth::user();
        $wallet = $user->lithcoin_wallet;
        $litcoin_transactions = $wallet->transactions;

        return view('users.wallet.litecoin.index', [
            'wallet' => $wallet,
            'transactions' => $litcoin_transactions
        ]);
    }


    public function create()
    {
        $user = Auth::user();
        $wallet = $user->lithcoin_wallet;
        if($wallet){
            return redirect()->route('wallet.litecoin.index');
        }else{
            return view('users.wallet.litecoin.create');
        }
    }


    public function save()
    {
        $user  = Auth::user();
        $ltc_wallet = $user->create_litcoin_wallet();
        if($ltc_wallet['status'] == true){
            return redirect()->route('wallet.litecoin.index');
        }else{
            return redirect()->route('service.unavalable');
        }

    }


    public function info()
    {
        $user = Auth::user();
        $wallet = $user->lithcoin_wallet;
        $litcoin_transactions = $wallet->transactions;
        $url = config('services.block_cypher.url').'/btc/main/addrs/'.$wallet->address;
        $api_call = Http::get($url)->json();

        $address = $api_call['address'];
        $total_received = $api_call['total_received'];
        $total_sent = $api_call['total_sent'];
        $unconfirmed_balance = $api_call['unconfirmed_balance'];
        $final_balance = $api_call['final_balance'];
        $transactions = $api_call['n_tx'];
        $unconfirmed_transactions = $api_call['unconfirmed_n_tx'];
        $final_transactions = $api_call['final_n_tx'];


        return view('users.wallet.litecoin.info', [
            'address' => $address,
            'total_received' => $total_received,
            'total_sent' => $total_sent,
            'unconfirmed_balance' => $unconfirmed_balance,
            'final_balance' => $final_balance,
            'transaction_total' => $transactions,
            'unconfirmed_transaction_total' => $unconfirmed_transactions,
            'final_transaction_total' => $final_transactions

        ]);
        
    }


    public function send()
    {
        $req = session('data');
        $user = Auth::user();
        $wallet = $user->lithcoin_wallet;
        $value = $req['value'];
        $address = $req['address'];
        $ngn_amount = $req['amount_ngn'];
        $usd_amount = $req['amount_usd'];
        $note = $req['note'];
        
        $transfer = $wallet->transfer($value, $address);
        if(isset($transfer['data'])){
            $data = $transfer['data'];

            $get_fee = Configuration::where('key', 'crypto_tx_fee')->first()->value; 
            $get_fee2 = Configuration::where('key', 'crypto_tx_fee2')->first()->value; 

            $fee = $usd_amount >= 600 ?
                    ($usd_amount / 100) * $get_fee2 : 
                        ($usd_amount / 100) * $get_fee;

            $wallet->update_address_balance();

            $transaction_fee = Conversion::convert_usd_to_LTC($fee);

            $wallet->update([
                'balance' => $wallet->balance - $transaction_fee
            ]);


            $transaction = Transaction::create([
                'user_id' => $user->id,
                'trxn_ref' => 'tvtx'.time().Str::random(5),
                'wallet' => 'Ltc wallet',
                'section' => 'Send Ltc',
                'type' => 'debit',
                'currency' => 'Ltc',
                'fee' => $fee,
                'amount' => $value,
                'status' => 'success'
            ]);

            $litecoin_transaction = LitcoinTransaction::create([
                'transaction_id' => $transaction->id,
                'ether_wallet_id' => $wallet->id,
                'user_id' => $user->id,
                'tx_hash' => $data['hash'],
                'block_height' => $data['block_height'],
                'value' => $value,
                'double_spend' => $data['double_spend'],
                'confirmation' => $data['confirmation'],
                'created_at' => now()->parse($data['confirmed'])->format('Y-m-d H:i:s'),
                'updated_at' => now()->parse($data['received'])->format('Y-m-d H:i:s'),
            ]);

            $info = [
                'username' => $user->username,
                'status' => 'successfully',
                'date' => $litecoin_transaction->created_at,
                'type' => 'debit',
                'amount' => $value,
                'currency' => 'LTC',
                'fee' => $data['fees'],
                'wallet' => 'Litecoin Wallet',
                'balance' => $wallet->balance,
            ];


            $user->notify(new NewTransaction($info));

            session()->flash('success', 'Transaction was successfull');

            return redirect()->route('wallet.etherum.index');

        }else{
            session()->flash('success', 'Transaction Failed');
            return redirect()->route('wallet.etherum.index');
        }

    }


    public function trade()
    {
        return view('users.trade.ltc.index');
    }

    public function buy_post($data)
    {
        $tradeWith =  decrypt($data);
        $user = Auth::user();
        $wallet = $user->lithcoin_wallet;
        $main_wallet = $user->wallet;
        $settings =  $user->settings;

        $service = Feature::where('name', 'Buy Ethereum')->first();
        if($service->is_available == false){
                return \redirect()->route('service.unavalable');
        }

        $max_transaction =  Configuration::where('key', 'max_transaction')->first();

        //Get Admin Wallets
        $config_wallet = Configuration::where('key', 'ltc_wallet')->first();
        $config_wallet2 = Configuration::where('key', 'ltc_wallet2')->first();

        $wallet->update_address_balance();

        if($wallet->balance <= $tradeWith['ltc']){
            session()->flash('error', 'Could not Continue Transaction due to Insufficient Ngn Balance');
            return \redirect()->back();
        }else{
            //Create Transactions
            $transaction = Transaction::create([
                'user_id' => $user->id,
                'trxn_ref' => 'tvtx'.time().Str::random(5),
                'type' => 'debit',
                'section' => 'buy ltc',
                'wallet' => 'ngn_wallet',
                'currency' => 'ngn',
                'amount' => $tradeWith['rate'],
                'status' => 'unverified',
            ]);


            $transaction2 = Transaction::create([
                'user_id' => $user->id,
                'trxn_ref' => 'tvtx'.time().Str::random(5),
                'type' => 'credit',
                'section' => 'buy_ltc',
                'wallet' => 'litecoin_wallet',
                'amount' => $tradeWith['ltc'],
                'status' => 'unverified', 
            ]);

            $transfer = $wallet->receive($config_wallet, $tradeWith['ltc']);

            if(isset($transfer['data']))
            {
                $data = $transfer['data'];
                $value = $data['value'];

                $main_wallet->update([
                    'ngn_balance' => $main_wallet->ngn_balance - $tradeWith['ngn']
                ]);

                $transaction2->update([
                    'status' => 'success'
                ]);

                $transaction->update([
                    'status' => 'success'
                ]);

                $litecoin_transaction = LitcoinTransaction::create([
                    'transaction_id' => $transaction->id,
                    'ether_wallet_id' => $wallet->id,
                    'user_id' => $user->id,
                    'tx_hash' => $data['hash'],
                    'block_height' => $data['block_height'],
                    'value' => $value,
                    'double_spend' => $data['double_spend'],
                    'confirmation' => $data['confirmation'],
                    'created_at' => now()->parse($data['confirmed'])->format('Y-m-d H:i:s'),
                    'updated_at' => now()->parse($data['received'])->format('Y-m-d H:i:s'),
                ]);

                $info = [
                    'username' => $user->username,
                    'status' => 'successful',
                    'date' => $litecoin_transaction->created_at,
                    'type' => 'credit',
                    'amount' => $value,
                    'currency' => 'Ltc',
                    'fee' => $data['fees'],
                    'wallet' => 'Litecoin Wallet',
                    'balance' => $wallet->balance,
                ];


                $user->notify(new NewTransaction($info));


                $data2 = [
                    'username' => $user->username,
                    'status' => 'successful',
                    'date' => $transaction->updated_at,
                    'type' => 'debit',
                    'amount' => $tradeWith['ngn'],
                    'wallet' => 'Ngn wallet',
                    'currency' => 'NGN',
                    'balance' => $main_wallet->ngn_balance,
                ];

                $user->notify(new NewTransaction($data2));

                session()->flash('success', 'Transaction was successful');
                return \redirect()->route('wallet.etherum.index');

            }else{
                session()->flash('error', 'Transaction failed');
                return \redirect()->back();
            }

        }

    }

    public function sell_post($data)
    {
        $tradeWith =  decrypt($data);
        $user = Auth::user();
        $wallet = $user->lithcoin_wallet;
        $main_wallet = $user->wallet;
        $settings =  $user->settings;

        $service = Feature::where('name', 'Buy Ethereum')->first();
        if($service->is_available == false){
                return \redirect()->route('service.unavalable');
        }

        $max_transaction =  Configuration::where('key', 'max_transaction')->first();

        //Get Admin Wallets
        $config_wallet = Configuration::where('key', 'ltc_wallet')->first();
        $config_wallet2 = Configuration::where('key', 'ltc_wallet2')->first();

        $wallet->update_address_balance();

        if($wallet->balance <= $tradeWith['ltc']){
            session()->flash('error', 'Could not Continue Transaction due to Insufficient Ngn Balance');
            return \redirect()->back();
        }else{
            //Create Transactions
            $transaction = Transaction::create([
                'user_id' => $user->id,
                'trxn_ref' => 'tvtx'.time().Str::random(5),
                'type' => 'debit',
                'section' => 'sell ltc',
                'wallet' => 'litecoin_wallet',
                'currency' => 'ngn',
                'amount' => $tradeWith['rate'],
                'status' => 'unverified',
            ]);


            $transaction2 = Transaction::create([
                'user_id' => $user->id,
                'trxn_ref' => 'tvtx'.time().Str::random(5),
                'type' => 'credit',
                'section' => 'sell ltc',
                'wallet' => 'ngn_wallet',
                'amount' => $tradeWith['ltc'],
                'status' => 'unverified', 
            ]);

            $transfer = $wallet->transfer($config_wallet, $tradeWith['ltc']);
            if(isset($transfer['data']))
            {
                $data = $transfer['data'];
                $value = $data['value'];

                $main_wallet->update([
                    'ngn_balance' => $main_wallet->ngn_balance + $tradeWith['ngn']
                ]);

                $transaction2->update([
                    'status' => 'success'
                ]);

                $transaction->update([
                    'status' => 'success'
                ]);

                $litecoin_transaction = LitcoinTransaction::create([
                    'transaction_id' => $transaction->id,
                    'ether_wallet_id' => $wallet->id,
                    'user_id' => $user->id,
                    'tx_hash' => $data['hash'],
                    'block_height' => $data['block_height'],
                    'value' => $value,
                    'double_spend' => $data['double_spend'],
                    'confirmation' => $data['confirmation'],
                    'created_at' => now()->parse($data['confirmed'])->format('Y-m-d H:i:s'),
                    'updated_at' => now()->parse($data['received'])->format('Y-m-d H:i:s'),
                ]);

                $info = [
                    'username' => $user->username,
                    'status' => 'successful',
                    'date' => $litecoin_transaction->created_at,
                    'type' => 'debit',
                    'amount' => $value,
                    'currency' => 'Ltc',
                    'fee' => $data['fees'],
                    'wallet' => 'Litecoin Wallet',
                    'balance' => $wallet->balance,
                ];


                $user->notify(new NewTransaction($info));


                $data2 = [
                    'username' => $user->username,
                    'status' => 'successful',
                    'date' => $transaction->updated_at,
                    'type' => 'credit',
                    'amount' => $tradeWith['ngn'],
                    'wallet' => 'Ngn wallet',
                    'currency' => 'NGN',
                    'balance' => $main_wallet->ngn_balance,
                ];

                $user->notify(new NewTransaction($data2));

                session()->flash('success', 'Transaction was successful');
                return \redirect()->route('wallet.etherum.index');

            }else{
                session()->flash('error', 'Transaction failed');
                return \redirect()->back();
            }
            
        }
    }
}
