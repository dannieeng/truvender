<?php

namespace App\Http\Controllers\User;

use App\Models\Biller;
use App\Models\CardCountry;
use App\Models\CryptoRates;
use App\Models\Transaction;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Models\Configuration;
use App\Models\BillerCategory;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Crypt;

class Trading extends Controller
{
    public function index()
    {
        
    }

    public function preview($data = null)
    {
        $trading_with = decrypt($data);

        
        if($trading_with['asset'] == 'btc'){
            $amount = $trading_with['amount'];

            $currency = CryptoRates::where('symbol', 'btc')->first();
            $api_call = Http::get('https://blockchain.info/tobtc?currency=USD&value=' . $amount)->json();

            $amount_btc = $api_call;

            $rate = $trading_with['current'] == 'selling' ? $amount * $currency->seller_rate :$amount * $currency->buyer_rate;

            if($trading_with['current'] == 'sell'){
                return \redirect()->route('btc.sell', [
                    'amount_btc' => $amount_btc,
                    'amount_usd' => $amount,
                ]);

            }else{
                return \redirect()->route('btc.buy', [
                    'amount_btc' => $amount_btc,
                    'amount_usd' => $amount,
                ]);
            }
        }else{
            $amount = $trading_with['amount'];

            $asset = CardCountry::where('card_id', $this->asset)
            ->where('country_id', $trading_with['country'])->where('type', $trading_with['type'])->first();

            $rate = $trading_with['rate'];
            $type = $trading_with['type'];
            $country = $trading_with['country'];

            if($trading_with['current'] == 'sell'){
                 return \redirect()->route('cards.sell', [
                    'card' => $asset,
                    'rate' => $rate,
                    'amount' => $amount,
                ]);
            }else{
                 return \redirect()->route('cards.buy', [
                    'card' => $asset,
                    'rate' => $rate,
                    'amount' => $amount,
                ]);
            }

        }
        return \view('users.transactions.preview', [
            'data' => $trading_with
        ]);
    }

    // public function proceed(Request $request)
    // {
    //     $data = decrypt($request->data);
        
    // }

    

    // public function refill()
    // {
        
    //     return \view('users.trade.airtime.index');
    // }

    public function buy_airtime()
    {
        $user = Auth::user();
        $user->reset_otp_verification();
        $user->create_otp();

        return view('users.trade.airtime.airtime');
    }

    public function buy_airtime_(Request $request)
    {
        $this->validate($request, [
            // 'phone' => 'regex:^[0]\d{10}$'
        ]);
    }


    public function buy_data()
    {
        $user = Auth::user();
        $user->reset_otp_verification();
        $user->create_otp();

        $biller_category = BillerCategory::whereName('Data')->first();
        $billers = $biller_category->billers;
        return view('users.trade.airtime.data');   
    }

    public function buy_data_(Request $request)
    {
        $this->validate($request, [

        ]);
    }



}
