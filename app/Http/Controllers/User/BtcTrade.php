<?php

namespace App\Http\Controllers\User;

use App\Models\Feature;
use App\Models\Transaction;
use Illuminate\Http\Request;
use App\Models\Configuration;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use App\Notifications\NewTransaction;
use Illuminate\Support\Facades\Crypt;
use App\Notifications\NewBtcTransaction;
use Illuminate\Notifications\Notification;

class BtcTrade extends Controller
{
    public function trade_btc()
    {
        return \view('users.trade.btc.index');
    }


    /*------------------------------------
    |
    |Sell bitcoin
    |
    -------------------------------------*/

    public function sell_btc()
    {
        return \view('users.trade.btc.sell');
    }

    public function sell_btc_($data)
    {
       $tradeWith =  decrypt($data);
       $user = Auth::user();
       $wallet = $user->wallet;
       $settings =  $user->settings;
       
       $service = Feature::where('name', 'Sell Bitcoin')->first();
       if($service->is_available == false){
            return \redirect()->route('service.unavalable');
       }
      
       $max_transaction =  Configuration::where('key', 'max_transaction')->first();

       //Check Verification
       if(($user->banking == true && $user->banking->bvn == null ) && $tradeWith['btc'] >= $max_transaction){
            session()->flash('error', 'Apply verification to make transaction above $' . $max_transaction );
            return redirect()->back();
       }
 
       //Get Admin Wallets
       $config_wallet = Configuration::where('key', 'btc_wallet')->first();
       $config_wallet2 = Configuration::where('key', 'btc_wallet2')->first();


       $api_key = config('services.block-io.btc-key');
       $checkUser_wallet = 'https://block.io/api/v2/get_address_balance/?api_key='.$api_key.'&addresses'.$wallet->btc_address;
    //    $end_point2 = 'https://block.io/api/v2/get_address_balance/?api_key='.$api_key.'&addresses'.$config_wallet2;

        if($checkUser_wallet['data']['available_balance'] > $tradeWith['btc']){

            $user_wallet_btc = $wallet->btc_address;

            $transaction = Transaction::create([
                'user_id' => $user->id,
                'trxn_ref' => 'tvtx'.time().Str::random(5),
                'type' => 'credit',
                'section' => 'buy_btc',
                'wallet' => 'ngn_wallet',
                'currency' => 'ngn',
                'amount' => $tradeWith['rate'],
                'status' => 'unverified',
            ]);

            $transaction2 = Transaction::create([
                'user_id' => $user->id,
                'trxn_ref' => 'tvtx'.time().Str::random(5),
                'type' => 'debit',
                'section' => 'buy_btc',
                'wallet' => 'btc_wallet',
                'amount' => $tradeWith['btc'],
                'status' => 'unverified', 
            ]);


            $transfer_endpoint = 'https://block.io/api/v2/withdraw_from_addresses/?api_key='.$api_key.'&from_address'.$user_wallet_btc.'&to_address'.$config_wallet;
            $get_response = Http::get($withdraw_endpoint)->json();

            
            //Check response
            if($get_response['status'] == 'success'){

                $transaction->update(['status' => 'success']);
                $transaction2->update(['status' => 'success']);

                //Get User Wallet Balance
                $getBalance_btc = Http::get('https://block.io/api/v2/get_address_balance/?api_key='.$api_key.'&addresses' . $user_wallet_btc)->json();

                $wallet->update([
                    'ngn_balance' => $wallet->ngn_balance + $tradeWith['ngn'],
                    'btc_balance' => $getBalance_btc['data']['available_balance'],
                ]);

                
                $data = [
                    'username' => $user->username,
                    'status' => 'Successful',
                    'date' => $transaction2->updated_at,
                    'type' => 'debit',
                    'amount' => $tradeWith['btc'],
                    'wallet' => 'Btc wallet',
                    'currency' => 'BTC',
                    'balance' => $wallet->btc_balance,
                ];
                $user->notify(new NewBtcTransaction($data));

                $data2 = [
                    'username' => $user->username,
                    'status' => 'Successful',
                    'date' => $transaction->updated_at,
                    'type' => 'credit',
                    'amount' => $tradeWith['ngn'],
                    'wallet' => 'Ngn wallet',
                    'currency' => 'NGN',
                    'balance' => $wallet->ngn_balance,
                ];

                $user->notify(new NewTransaction($data2));

                session()->flash('success', 'Transaction was successful');
                return \redirect()->route('transactions.index');

            }else{
                session()->flash('error', 'Transaction failed');
                return \redirect()->back();
            }
            
        }else{
            session()->flash('error', 'Could not continue Transaction due to Insufficient BTC Balance');
            return \redirect()->back();
        }

       
    }

    public function buy_btc()
    {
        return \view('users.trade.btc.buy');   
    }


    /*------------------------------------
    |
    |Buy bitcoin
    |
    -------------------------------------*/
    public function buy_btc_($data)
    {
       $tradeWith =  decrypt($data);
       $user = Auth::user();
       $wallet = $user->wallet;
       $settings =  $user->settings;

       $service = Feature::where('name', 'Buy Bitcoin')->first();
       if($service->is_available == false){
            return \redirect()->route('service.unavalable');
       }

       $max_transaction =  Configuration::where('key', 'max_transaction')->first();

       //Check Verification
       if(($user->banking == true && $user->banking->bvn == null ) && $tradeWith['btc'] >= $max_transaction){
            session()->flash('error', 'Apply verification to make transaction above $' . $max_transaction );
            return redirect()->back();
       }

       //Get Admin Wallets
       $config_wallet = Configuration::where('key', 'btc_wallet')->first();
       $config_wallet2 = Configuration::where('key', 'btc_wallet2')->first();

       $api_key = config('services.block-io.btc-key');
       $end_point = 'https://block.io/api/v2/get_address_balance/?api_key='.$api_key.'&addresses'.$config_wallet;
       $end_point2 = 'https://block.io/api/v2/get_address_balance/?api_key='.$api_key.'&addresses'.$config_wallet2;

       //Check Admin wallet Balance
       $api_call = Http::get($end_point)->json();
       $block_balance = $api_call['data']['available_balance'];

       if($block_balance <= $tradeWith['btc']){

           //Check Second Admin wallet Balance
            $api_call2 = Http::get($end_point2)->json();
            $block_balance2 = $api_call['data']['available_balance'];

            if($block_balance2 <= $tradeWith['btc']){
                \session()->flash('error', 'Sorry this Service is currenctly unavailable');
                return \redirect()->back();
            }elseif($block_balance2 > $tradeWith['btc']){
                if($wallet->ngn_balance > $tradeWith['ngn']){
                    //Create Transactions
                    $transaction = Transactio::create([
                        'user_id' => $user->id,
                        'trxn_ref' => 'tvtx'.time().Str::random(5),
                        'type' => 'debit',
                        'section' => 'buy_btc',
                        'wallet' => 'ngn_wallet',
                        'currency' => 'ngn',
                        'amount' => $tradeWith['rate'],
                        'status' => 'unverified',
                    ]);

                    $transaction2 = Transaction::create([
                        'user_id' => $user->id,
                        'trxn_ref' => 'tvtx'.time().Str::random(5),
                        'type' => 'credit',
                        'section' => 'buy_btc',
                        'wallet' => 'btc_wallet',
                        'amount' => $tradeWith['btc'],
                        'status' => 'unverified', 
                    ]);

                    $withdraw_endpoint = 'https://block.io/api/v2/withdraw_from_addresses/?api_key='.$api_key.'&from_address'.$config_wallet2.'&to_address'.$wallet->btc_address;
                    $get_response = Http::get($withdraw_endpoint)->json();
                    if($get_response['status'] == 'success'){
                        $transaction->update(['status' => 'success']);
                        $transaction2->update(['status' => 'success']);

                        $getBalance_btc = Http::get('https://block.io/api/v2/get_address_balance/?api_key='.$api_key.'&addresses'.$wallet->btc_address)->json();
                        $wallet->update([
                            'ngn_balance' => $wallet->ngn_balance - $tradeWith['ngn'],
                            'btc_balance' => $getBalance_btc['data']['available_balance']
                        ]);

                        session()->flash('success', 'Transaction was successful');
                        return \redirect()->route('transactions.index');
                    }else{
                        session()->flash('error', 'Transaction failed');
                        return \redirect()->back();
                    }


                }else{
                    session()->flash('error', 'Could not Continue Transaction due to Insufficient Ngn Balance');
                    return \redirect()->back();
                }
            } 
       }elseif($block_balance > $tradeWith['btc']){
            if($wallet->ngn_balance > $tradeWith['ngn']){

                //Create Transactions
                $transaction = Transactio::create([
                    'user_id' => $user->id,
                    'trxn_ref' => 'tvtx'.time().Str::random(5),
                    'type' => 'debit',
                    'section' => 'buy_btc',
                    'wallet' => 'ngn_wallet',
                    'currency' => 'ngn',
                    'amount' => $tradeWith['rate'],
                    'status' => 'unverified',
                ]);

                $transaction2 = Transaction::create([
                    'user_id' => $user->id,
                    'trxn_ref' => 'tvtx'.time().Str::random(5),
                    'type' => 'credit',
                    'section' => 'buy_btc',
                    'wallet' => 'btc_wallet',
                    'amount' => $tradeWith['btc'],
                    'status' => 'unverified', 
                ]);

                $withdraw_endpoint = 'https://block.io/api/v2/withdraw_from_addresses/?api_key='.$api_key.'&from_address'.$config_wallet.'&to_address'.$wallet->btc_address;
                $get_response = Http::get($withdraw_endpoint)->json();
                if($get_response['status'] == 'success'){
                    $transaction->update(['status' => 'success']);
                    $transaction2->update(['status' => 'success']);

                    $getBalance_btc = Http::get('https://block.io/api/v2/get_address_balance/?api_key='.$api_key.'&'.$wallet->btc_address)->json();
                    $wallet->update([
                        'ngn_balance' => $wallet->ngn_balance - $tradeWith['ngn'],
                        'btc_balance' => $getBalance_btc['data']['available_balance'],
                    ]);

                    $data2 = [
                        'username' => $user->username,
                        'status' => 'Successful',
                        'date' => $transaction->updated_at,
                        'type' => 'credit',
                        'amount' => $tradeWith['ngn'],
                        'wallet' => 'Ngn wallet',
                        'currency' => 'NGN',
                        'balance' => $wallet->ngn_balance,
                    ];

                    $user->notify(new NewTransaction($data2));

                    $data = [
                        'username' => $user->username,
                        'status' => 'Successful',
                        'date' => $transaction2->updated_at,
                        'type' => 'debit',
                        'amount' => $tradeWith['btc'],
                        'wallet' => 'Btc wallet',
                        'currency' => 'BTC',
                        'balance' => $wallet->btc_balance,
                    ];
                    $user->notify(new NewBtcTransaction($data));

                    session()->flash('success', 'Transaction was successful');
                    return \redirect()->route('transactions.index');
                }else{

                    session()->flash('error', 'Transaction failed');
                    return \redirect()->back();
                }


            }else{
                session()->flash('error', 'Could not Continue Transaction due to Insufficient Ngn Balance');
                return \redirect()->back();
            }
        }
       

    }
}
