<?php

namespace App\Http\Controllers\User;

use App\Models\Card;
use App\Models\Country;
use App\Models\Feature;
use App\Models\CardTrade;
use App\Models\Transaction;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Models\Configuration;
use App\Mail\GiftCardPurchase;
use App\Models\CardTradeImage;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Crypt;
use App\Notifications\TransactionSuccess;
use Illuminate\Notifications\Notification;

class GiftCards extends Controller
{
    public function get_user()
    {
        return Auth::user();
    }

    public function buy_cards()
    {
        $cards = Card::orderBy('name', 'asc');
        return view('users.cards.buy', [
            'cards' => $cards
        ]);
    }

    public function sell_cards()
    {
        $cards = Card::orderBy('name', 'asc');
        return view('users.cards.sell', [
            'cards' => $cards
        ]);
    }

    public function buy_card($card)
    {
        // $user = Auth::user();
        // $user->reset_otp_verification();
        // $user->create_otp();
        
        $card_id = decrypt($card);
        $current_card = Card::whereId($card_id)->first();
        $type = 'buy';
        return \view('users.cards.trade', [
            'type' => $type,
            'card' => $current_card
        ]);
    }

    public function sell_card($card)
    {
        // $user = Auth::user();
        // $user->reset_otp_verification();
        // $user->create_otp();

        $card_id = Crypt::decrypt($card);
        $current_card = Card::whereId($card_id)->first();
        $type = 'sell';
        return \view('users.cards.trade', [
            'type' => $type,
            'card' => $current_card
        ]);
    }
    
    
    
    public function image_upload($trade_id)
    {
        $id = decrypt($trade_id);
        $card_trade = CardTrade::where('id', $id)->first();
        return \view('users.cards.images', [
            'card_trade' => $card_trade
        ]);
    }


    public function submit_images(Request $request, $trade_id)
    {
        $this->validate($request, [
            'images.*' => 'nullable|image|mimes:png,jpg,svg,jpeg'
        ]);

        $user = Auth::user();

        $id = decrypt($trade_id);
        $card_trade = CardTrade::where('id', $id)->first();

        $cards = array();
        if ($request->hasFile('images')) :
            foreach($request->file('images') as $image){
                $file = $image;
                // Generate a file name with extension
                $fileName = $file->getClientOriginalName();
                $fileExt = $file->getClientOriginalExtension();
                $imageName = Str::random(30) .'-'. time() .'-'.'.'.$fileExt ;

                // Save the file
                $path = $file->storeAs('/public/uploads/cards/', $imageName);

                $cards[] = '/uploads/cards/'.$imageName;
            }

            foreach($cards as $key => $c_image){
                $card_trade_image = CardTradeImage::create([
                    'user_id' => $user->id,
                    'card_trade_id' => $card_trade->id,
                    'images' => $c_image,
                ]);
            }

            //Create Transaction
            $transaction = Transaction::create([
                'user_id' => $user->id,
                'trxn_ref' => 'tvtx'.time() . Str::random(5),
                'type' => 'credit',
                'section' => 'sell giftcard',
                'wallet' => 'ngn_wallet',
                'currency' => 'ngn',
                'amount' => $card_trade->price,
                'status' =>'pending',
            ]);

            $card_trade->update([
                'transaction_id' => $transaction->id
            ]);

            \session()->flash('success', ' Card images uploaded successfully');
            return \redirect()->route('products');

        else:
            \session()->flash('error', 'Please uplaod one or more images');
            return \redirect()->back();
        endif;

        
    }



    public function trade_card_buy()
    {
        $user = Auth::user();
        //Deduct from wallet
        $req = session('data');        
        $wallet = $user->wallet;
        $ngn_balance = $wallet->ngn_balance;
        $wallet->update([
            'ngn_balance' => $ngn_balance - $req['rate']
        ]);


        $rate = $req['rate'];
        $amount = $req['amount'];
        $trade = $req['trade'];
        $card = $req['card'];
        $country = Country::where('id', $req['country'])->first();
        $card_country = $req['card_country'];
        
        $buyer_rate = $req['buyer_rate'];

        //create Transaction
        $transaction = Transaction::create([
            'user_id' => $user->id,
            'trxn_ref' => 'tvtx'.time() . Str::random(5),
            'type' => 'debit',
            'section' => 'giftcard',
            'wallet' => 'ngn_wallet',
            'currency' => 'ngn',
            'amount' => $rate,
            'status' =>'success',
        ]);

        //add trading record
        $card_trade = CardTrade::create([
            'user_id' => $user->id,
            'card_id' => $card->id,
            'transaction_id' => $transaction->id,
            'card_country_id' => $card_country,
            'status' => 'unreviewed',
            'amount' => $amount,
            'price' => $rate,
            'trade_type' => $trade,
            'rate' => $buyer_rate,
        ]);

        $notificationData = [
            'item' => 'Giftcard',
            'card' => $card,
            'cost' => 'N'.number_format($rate),
            'amount' => $amount,
            'country' => $country->name,
            'rate' => 'N'.number_format($buyer_rate) . '/$',
            'type' =>'debit',
        ];


        $data = [
            'user' => $user->username,
            'amount' => $amount,
            'rate' => 'N'.number_format($rate),
            'card' => $card,
        ];

        $notification = $user->notify(new TransactionSuccess($notificationData));
        Mail::to('no-reply@truvender.com')->send(new GiftCardPurchase($data));
        session()->flash('success', 'Transaction was successful');
        return \redirect()->route('cards.buy');     
    }
    
    
}
