<?php

namespace App\Http\Controllers\User;

use App\Models\Card;
use App\Models\OtherAsset;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;

class PagesController extends Controller
{
    public $user;
    
    public function index()
    {
        $user = Auth::user();
        
         // Connect to block api
        $apiKey = \config('services.block-io.btc-key');
        $wallet = $user->wallet;
        $btc_wallet_address = $wallet->btc_address;

        $block =  Http::get('https://block.io/api/v2/get_address_balance/?api_key=' . $apiKey .'&addresses='.$btc_wallet_address)->json();
        
        if($block['status'] == 'success'){
            $wallet->update([
                'btc_balance' => $block['data']['available_balance']
            ]);
        }


        if($user->role->name != 'user'){
            return redirect()->route('admin.index');
        }

        $otherasses = OtherAsset::orderBy('created_at', 'desc')->get();
        $transactions = $user->transactions()->orderBy('created_at', 'desc')->get();
        $wallet = $user->wallet;
        return view('users.index', [
            'transactions' => $transactions,
            'wallet' => $wallet,
            'otherassets' => $otherasses,
        ]);
    }

    public function transactions()
    {
        $user = Auth::user();
        $transactions = $user->transactions()->orderBy('created_at', 'desc')->get();
        return \view('users.transactions.index', [
            'transactions' => $transactions
        ]);
    }

    public function wallet()
    {
        $user = Auth::user();
        $wallet = $user->wallet;

        return view('users.wallet.index', [
            'wallet' => $wallet
        ]);
    }

    public function wallet_info($type)
    {

        $user = $this->user;
        $wallet = $user->wallet;

        if(\strtolower($type) == 'bitcoin'){
            
            $wallet_label = $wallet->block_label;

            //Api Call
            $block_root = \config('services.block-io.root_url');
            $api_key = \config('services.block-io.btc-key');
        
            $appeded = $block_root . 'get_address_balance/?api_key='.$api_key . '&labels='.$wallet_label;

            $block = Http::get($appeded)->json();
            $result  = $block['data']['balance'];
            $av_balance = $result['available_balance'];
            $pend_balance = $result['pending_received_balance'];

            
            return view('users.wallet.info', [
                'type' => $type
            ]);

        }elseif (strtolower($type) == 'naira') {
            return view('users.wallet.info', [
                'type' => $type
            ]);
        }
    }

    public function rates()
    {
        return \view('users.rates.index');
    }
    
    public function products()
    {
        $otherassets = OtherAsset::orderBy('created_at', 'desc')->get();
        return view('users.products', [
            'otherassets' => $otherassets
        ]);
    }

    public function settings()
    {
        $user = Auth::user();
        $profile = $user->profile;
        $banking = $user->banking;
        $settings = $user->setting;

        return \view('users.settings.index', [
            'user' => $user,
            'banking' => $banking,
            'profile' => $profile,
            'settings' => $settings
        ]);
    }

    public function restrict()
    {
        $user = Auth::user();
        if($user->in_blacklist == true){
            return \view('auth.block');
        }

        return \redirect()->route('dashboard');
    }

}
