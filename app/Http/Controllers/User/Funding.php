<?php

namespace App\Http\Controllers\User;

use App\Models\User;
use App\Models\Peer2Peer;
use App\Models\Transaction;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Models\Configuration;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use App\Notifications\NewTransaction;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Notifications\Notification;

class Funding extends Controller
{
   
    public function ngn_transfer($to)
    {
        $transfer_to = Crypt::decrypt($to);
        $user = Auth::user();
        $wallet = $user->wallet;
        $trx_limit = Configuration::where('key','max_transaction')->first();
        
        if($transfer_to == 'my_account'){
            if($user->banking == false){
                \session()->flash('error', 'Bank account have not been set');
                return \redirect()->route('wallet');
            }

            return view('users.wallet.transfer-my-account');

        }else{
            return view('users.wallet.transfer', [
                'transfer_to' => $transfer_to
            ]);
        }

    }

    public function my_account(Request $request)
    {
        $this->validate($request, [
            'amount' => 'required|numeric',
            // 'currency' => 'required',
        ]);
        $currency = 'NGN';

        $amount = $request->amount;
        $user = Auth::user();
        $wallet = $user->wallet;
        $banking = $user->banking;
        $trx_limit = Configuration::where('key', 'max_transaction')->first();
        $fee = Configuration::where('key', 'fiat_tx_fee')->first()->value;
        
        if($wallet->ngn_balance < $amount){
            session()->flash('error', 'insufficient fund in wallet');
            return redirect()->route('wallet');
        }

        if($amount >= $trx_limit){
            session()->flash('error', 'Transaction Limit Exceeded');
            return redirect()->route('wallet');
        }

        if($user->banking == false){
            \session()->flash('error', 'Bank account have not been set');
            return \redirect()->route('wallet');
        }

        

        $data = [
            'account_bank' => $banking->bank->code,
            'account_number' => $banking->acc_number,
            'amount' => $amount * 100,
            'naration' => 'Truvender Fund Transfer',
            'currency' => 'NGN',
            'beneficiary_name' => $banking->acc_name,
        ];

       
        $key = config('services.paystack.sk');
        $endpoint = config('services.paystack.url').'bank/resolve?account_number='.$data['account_number'].'&bank_code='.$banking->bank->code;
        $response = Http::withHeaders(['Authorization' => 'Bearer '.$key])->get($endpoint)->json();

        if($response['status'] == true){
            $account_name = $response['data']['account_name'];
            $account_number = $response['data']['account_number'];
            $bank_id = $response['data']['bank_id'];

            $transaction = Transaction::create([
                'user_id' => $user->id,
                'trxn_ref' => 'txtv'.time().Str::random(5),
                'type' => 'debit',
                'amount' => $amount,
                'section' => 'Transfer Fund',
                'wallet' => 'ngn_wallet',
                'currency' => 'ngn',
                'status' => 'unverified',
                'acc_name' => $account_name,
                'acc_number' => $account_number,
                'bank' => $banking->bank->name
            ]);

            $endpoint = config('services.paystack.url').'transferrecipient';
            $response = Http::withHeaders(['Authorization' => 'Bearer '.$key])->post($endpoint, [
                'type' => 'nuban',
                'name' => $account_name,
                'account_number' => $account_number,
                'bank_code' => $banking->bank->code,
                'currency' => $currency
            ])->json();

            if($response['status'] == true){

                $transaction->update([
                    'recipient_code' => $response['data']['recipient_code']
                ]);

                $endpoint = config('services.paystack.url').'transfer';
                $response = Http::withHeaders(['Authorization' => 'Bearer '.$key])->post($endpoint,[
                    'source' => "balance",
                    'amount' => $data['amount'],
                    'recipient' => $transaction->recipient_code,
                    'reason' => 'Transfer from truvender account',
                ])->json();


                if($response['status'] == true){
                    session()->flash('info', 'Your transaction is beign processed');
                    return redirect()->route('wallet');
                }
            }
        }else{

             session()->flash('error', 'Transaction Failed! Please try again someother time');
            return \redirect()->route('wallet');
        }

    }

    public function transfer_process()
    {
        // Retrieve the request's body and parse it as JSON
        $input = @file_get_contents("php://input");
        $event = json_decode($input);

        $reference = $input['reference'];
        $recipient_code = $input['recipient']['recipient_code'];
        $status = $input['status'];
        $amount = $input['data']['amount'];
        $check = $input['event'];

        if($check == 'transfer.success'){
            $transaction = Transaction::where('recipient_code', $recipient_code)->first();

            if($transaction){
                $user = $transaction->user;
                $wallet = $user->wallet;
                $fee = Configuration::where('key', 'fiat_tx_fee')->first()->value;

                $transaction->update([
                    'fee' => $fee,
                    'amount' => $amount,
                    'status' => $status,
                    'recipient_code' => $recipient_code,
                    'tx_ref' => $reference,
                ]);

                $wallet->update([
                    'ngn_balance' => $wallet->ngn_balance - ($amount + $fee)
                ]);


                $data = [
                    'type' => 'debit',
                    'date' => now()->format('d/m/Y H:i:s'),
                    'status' => 'successful',
                    'fee' =>    $fee,
                    'amount' => $amount,
                    'wallet' => 'Naira Wallet',
                    'balance' => $wallet->ngn_balance,
                    'currency' => 'Naira',
                    'username' => $user->username
                ];

                $user->notify(new NewTransaction($data));
            }
        }

        // Do something with $event
        http_response_code(200); // PHP 5.4 or greater
        
    }



    public function my_account_(Request $request)
    {
        $this->validate($request, [
            'amount' => 'required|numeric'
        ]);

        $amount = $request->amount;
        $user = Auth::user();
        $wallet = $user->wallet;
        $banking = $user->banking;
        $trx_limit = Configuration::where('key', 'max_transaction')->first();
        if($wallet->ngn_balance < $amount){
            session()->flash('error', 'insufficient fund in wallet');
            return redirect()->route('wallet');
        }

        if($amount >= $trx_limit){
            session()->flash('error', 'Transaction Limit Exceeded');
            return redirect()->route('wallet');
        }

         if($user->banking == false){
            \session()->flash('error', 'Bank account have not been set');
            return \redirect()->route('wallet');
            }


        $transaction = Transaction::create([
            'user_id' => $user->id,
            'trxn_ref' => 'txtv'.time().Str::random(5),
            'type' => 'debit',
            'amount' => $amount,
            'section' => 'Transfer Fund',
            'wallet' => 'ngn_wallet',
            'currency' => 'ngn',
            'status' => 'unverified',
            'acc_name' => $banking->acc_name,
            'acc_number' => $banking->acc_number,
            'bank' => $banking->bank->name
        ]);


         //Transfer Endpoint
        $flutterwave_endpoint = 'https://api.flutterwave.com/v3/transfers';
        $flutterwave_secrete_key = \config('services.flutterwave.secrete_key');
        $transfer_endpoint = Http::withHeaders(['Authorization' => $flutterwave_secrete_key])->post($flutterwave_endpoint, [
            'account_bank' => $banking->bank->code,
            'account_number' => $banking->acc_number,
            'amount' => $amount,
            'naration' => 'Truvender Fund Transfer',
            'currency' => 'NGN',
            'beneficiary_name' => $banking->acc_name,
            'reference' => $transaction->trxn_ref,
            'callback_url' => \route('wallet.transfer.proccess')
        ])->json();


        if($transfer_endpoint['status'] == 'success' && ($transfer_endpoint['data'] != null && $transfer_endpoint['data']['is_approved'] == true)){
            $transaction->update([
                'fee' => $transfer_endpoint['data']['fee'],
                'status' => 'success'
            ]);
            
            $wallet_balance = $wallet->ngn_balance;
            $debit_amount = $transfer_endpoint['data']['amount'] + $transfer_endpoint['data']['fee'];
            $wallet->update([
                'ngn_balance' => $wallet_balance - $debit_amount
            ]);

            $data = [
                'type' => 'debit',
                'date' => now()->format('d/m/Y H:i:s'),
                'status' => 'successful',
                'fee' =>    $transfer_endpoint['data']['fee'],
                'amount' => $amount,
                'wallet' => 'Naira Wallet',
                'balance' => $wallet->ngn_balance,
                'currency' => 'Naira',
                'username' => $user->username
            ];

            $user->notify(new NewTransaction($data));

            $data = [
                'amount' => $amount,
                'transaction_id' => $transaction->id,
                'transfer_id' => $transfer_endpoint['data']['id']
            ];

            $transaction_data = Crypt::encrypt($data);
            session()->flash('success', 'Transaction was successful.');
            return \redirect()->route('wallet.transfer.proccess', $transaction_data);
        }else{

            session()->flash('error', 'Transaction Failed');
            return \redirect()->route('wallet');
        }

    }

    //Process transfer flutterwave
    public function transfer_process_($transfer_data = null)
    {

        $user  = Auth::user();
        $wallet = $user->wallet;
        
        $data =  Crypt::decrypt($transfer_data);
        $transfer_id = $data['transfer_id'];
        $transaction = Transaction::where('id', $data['transaction_id'])->first();

        $flutter_endpoint = 'https://api.flutterwave.com/v3/transfers/'.$transfer_id;
        $flutter_seckey = config('services.flutterwave.secrete_key');
        $get_transfer = Http::withHeaders(['Authorization' => $flutter_seckey])->get($flutter_endpoint)->json();


        if($get_transfer['status'] == 'success' && ($get_transfer['data'] != null && $get_transfer['data']['is_approved'] == true)){
            $transaction->update([
                'fee' => $get_transfer['data']['fee'],
                'status' => 'success'
            ]);

            $wallet->update([
                'ngn_balance' => $wallet->ngn_balance - $transaction->amount,
            ]);

            $data = [
                'type' => 'debit',
                'date' => now()->format('d/m/Y H:i:s'),
                'status' => 'successful',
                'amount' => $transaction->amount,
                'wallet' => 'Naira Wallet',
                'balance' => $wallet->ngn_balance,
                'fee' =>    $get_transfer['data']['fee'],
                'currency' => 'Naira',
                'username' => $user->username
            ];

            $user->notify(new NewTransaction($data));

            return redirect()->route('transation-success');

        }else{

            $data = [
                'type' => 'debit',
                'date' => now()->format('d/m/Y H:i:s'),
                'status' => 'failed',
                'amount' => $transaction->amount,
                'wallet' => 'Naira Wallet',
                'balance' => $wallet->ngn_balance,
                'fee' =>    $get_transfer['data']['fee'],
                'currency' => 'Naira',
                'username' => $user->username
            ];

            $user->notify(new NewTransaction($data));

            session()->flash('error', 'Transaction Failed');
            return \redirect()->route('wallet');
        }
    }

    public function ngn_fund(Request $request)
    {
        $this->validate($request, [
            'amount' => 'required|numeric',
        ]);

        $user = Auth::user();
        $amount =$request->amount * 100;
        // Create a transaction
        $transaction = Transaction::create([
            'user_id' => $user->id,
            'trxn_ref' => 'tvtx'.time() . Str::random(5),
            'type' => 'credit',
            'section' => 'fund wallet',
            'wallet' => 'ngn_wallet',
            'currency' => 'ngn',
            'amount' => $request->amount,
            'status' =>'unverified',
        ]);

        $key = config('services.paystack.sk');
        $endpoint = config('services.paystack.url').'transaction/initialize';
        $response = Http::withHeaders(['Authorization' => 'Bearer '. $key])->post($endpoint, [
            'cartid' => $transaction->trxn_ref,
            'amount' => $amount,
            'callback_url' => route('wallet.fund.proccess'),
            'email' => $user->email, 
        ]);

        $payment_link = $response['data']['authorization_url'];

        $transaction->update([
            'tx_ref' => $response['data']['reference']
        ]);

        return redirect($payment_link);

    }

    //validating transaction
    public function proccess_ngn_fund()
    {
        $user = Auth::user();
        $wallet = $user->wallet;
        $tx_ref = $_GET['trxref'];
        $ref = $_GET['reference'];
        $fee = 0;
        $transaction = Transaction::where('tx_ref', $tx_ref)->first();

        $key = config('services.paystack.sk');
        $endpoint = config('services.paystack.url').'transaction/verify/'.$ref;
        $response = Http::withHeaders(['Authorization' => 'Bearer '. $key])->get($endpoint);

        if($response['status'] == true){

            $amount = $response['data']['amount'];
            $status = $response['data']['status'];

            if($transaction == true){
                $transaction->update([
                    'status' => 'success',
                    'amount' => $amount
                ]);

                $wallet->update([
                    'ngn_balance' => $wallet->ngn_balance + $amount
                ]);

                
                //Send Email Notification
                $data = [
                    'amount' => $amount,
                    'purpose' => 'Fund Wallet',
                    'currency' => strtoupper($transaction->currency),
                    'status' => $status,
                    'date' => now()->format('d-M-Y H:i:s'),
                    'type' => 'Credit',
                    'username' => $user->username,
                    'wallet' => 'Ngn Wallet',
                    'fee' => $fee,
                    'balance' => $wallet->ngn_balance,
                ];
                $user->notify(new NewTransaction($data));

                
                \session()->flash('success', 'Wallet funding was successful');
                return \redirect()->route('transactions.index');
            }
        }

        \abort(404);
        
    }

    //funding ngn wallet
    public function ngn_fund1(Request $request)
    {
        $this->validate($request, [
            'amount' => 'required|numeric',
        ]);

        $user = Auth::user();
       
        // Create a transaction
        $transaction = Transaction::create([
            'user_id' => $user->id,
            'trxn_ref' => 'tvtx'.time() . Str::random(5),
            'type' => 'credit',
            'section' => 'fund wallet',
            'wallet' => 'ngn_wallet',
            'currency' => 'ngn',
            'amount' => $request->amount,
            'status' =>'unverified',
        ]);

        //Get Flutterwave details
        $flutter_pubkey = \config('services.flutterwave.public_key');
        $flutter_seckey = \config('services.flutterwave.secrete_key');
        $flutter_endpoint = 'https://api.flutterwave.com/v3/payments';

        //Perform Api Call
        $api_call = Http::withHeaders(['Authorization' => $flutter_seckey])->post($flutter_endpoint, [
            'tx_ref' => $transaction->trxn_ref,
            'amount' => $request->amount,
            'currency' => 'NGN',
            'redirect_url' => route('wallet.fund.proccess'),
            'payment_options' => 'card', 
            'customer' => [
                'email' => $user->email, 
                'name' => $user->username,
                'customer_id' => $user->id,
                'transaction_id' => $transaction->id,
            ],
            'customizations'=>[
                'title'=> \config('app.name'),
                'description'=>'Fund your ' . \config('app.name') . ' wallet with NGN '. $request->amount,
                'logo'=> \config('app.url').'images/logo/tru-favicon.png'
            ]
        ])->json();

        
        //Redirect to link
        if($api_call['status'] == 'success' && $api_call['data']['link'] != null){
            $payment_link = $api_call['data']['link'];
             return redirect($payment_link);
        }else{
            session()->flash('error', 'Sorry! this service is currently unavailable');
            return \redirect()->back();
        }
  
    }


    //validating transaction
    public function proccess_ngn_fund1()
    {
        $user = Auth::user();
        $wallet = $user->wallet;
        $tx_ref = $_GET['tx_ref'];
        if(isset($_GET['transaction_id'])){
            $transaction_id = $_GET['transaction_id'];
        }
        $status = $_GET['status'];

        $transaction = Transaction::whereTrxn_ref($tx_ref)->first();

        if(strtolower($status) == 'successful' && $transaction == true){

            $flutter_seckey = \config('services.flutterwave.secrete_key');
            $flutter_endpoint = 'https://api.flutterwave.com/v3/transactions/' . $transaction_id.'/verify';
            $api_call = Http::withHeaders(['Authorization' => $flutter_seckey])->get($flutter_endpoint)->json();

            $trx_status = $api_call['data']['status'];
            $trx_currency = $api_call['data']['currency'];
            $trx_amount = $api_call['data']['amount'];
            $trx_ref = $api_call['data']['tx_ref'];

            if(strtolower($trx_status) == 'successful'){
                if(strtolower($trx_currency) == $transaction->currency){
                    if($trx_amount != $transaction->amount){
                        $transaction->update([
                            'amount' => $trx_amount
                        ]);
                    }else{
                        $transaction->update([
                            'amount' => $trx_amount
                        ]);
                    }

                    
                    if($transaction->return_id != $transaction_id){

                        $wallet->update([
                            'ngn_balance' => $wallet->ngn_balance + $trx_amount
                        ]);

                        
                        //Send Email Notification
                        $data = [
                            'amount' => $trx_amount,
                            'purpose' => 'Fund Wallet',
                            'currency' => $trx_currency,
                            'status' => $trx_status,
                            'date' => now()->format('d-M-Y H:i:s'),
                            'type' => 'Credit',
                            'username' => $user->username,
                            'wallet' => 'Ngn Wallet',
                            'balance' => $wallet->ngn_balance,
                        ];
                        $user->notify(new NewTransaction($data));

                        $transaction->update(['status' => 'success', 'return_id' => $transaction_id]);
                        
                        \session()->flash('success', 'Wallet funding was successful');
                        return \redirect()->route('transactions.index');
                    }else{
                        \abort(404);
                    }


                }
            }else{

                 //Send Email Notification
                $data = [
                        'amount' => $trx_amount,
                        'purpose' => 'Fund Wallet',
                        'currency' => $trx_currency,
                        'status' => $trx_status,
                        'date' => now()->format('d-M-Y H:i:s'),
                        'type' => 'Credit',
                        'username' => $user->username,
                        'wallet' => 'Ngn Wallet',
                        'balance' => $wallet->ngn_balance,
                    ];

                    $user->notify(new NewTransaction($data));
                    
                session()->flash('Transaction failed');
                return \redirect()->route('transactions.index');
            }
        }elseif ($transaction == true && strtolower($status) != 'successful') {

            $transaction->update([
                'status' => 'failed'
            ]);
            \session()->flash('error', 'Transaction Failed');
            return \redirect()->route('transactions.index');
        }
    }


    public function webhook()
    {
        
    }

    public function peer_2_peer()
    {
       $user = Auth::user();
       $profile = $user->profile;
       return view('users.peer.index', [
           'user' => $user,
           'profile' => $profile
       ]);
    }

    public function peer_2_peer_post()
    {
        $request = session('data');
        $user = Auth::user();
        $amount = $request['amount'];
        $account_name = $request['account_name'];
        $account_number = $request['account_number'];
        $bank = $request['bank'];
        $fee = Configuration::where('key', 'fiat_tx_fee')->first()->value;

        $wallet = $user->wallet;
        $wallet->update([
            'ngn_balance' => $wallet->ngn_balance - ($amount + $fee)
        ]);
    
        $transaction = Transaction::create([
            'user_id' => $user->id,
            'trxn_ref' => 'txtv'.time().Str::random(5),
            'type' => 'debit',
            'amount' => $amount,
            'wallet' => 'ngn_wallet',
            'section' => 'Transfer Fund',
            'currency' => 'ngn',
            'status' => 'pending',
        ]);

        $peer = Peer2Peer::create([
            'user_id' => $user->id,
            'transaction_id' => $transaction->id,
            'amount' => $amount,
            'account_number' => $account_number,
            'bank' => $bank['name'],
            'account_name' => $account_name,
            'currency' => 'NGN',
        ]);

        session()->flash('info', 'You have been added to our network and will be funded soon');
        Cache::forget('uri');
        return redirect()->route('wallet');


    }

    public function peer_2_peer_preview($data)
    {
        $peer_data = decrypt($data);
        
        $fee = Configuration::where('key', 'fiat_tx_fee')->first()->value;

        return view('users.peer.perview', [
            'data' => $peer_data,
            'fee' => $fee,
        ]);
    }



    public function transafer_truvender()
    {
        $user = Auth::user();
        $wallet = $user->wallet;
        $request = session('data');
        $username = $request['username'];
        $amount = $request['amount'];
        $fee = 0;

        $truvender = User::where('username', $username)->first();
        $truvenderWallet = $truvender->wallet;

        $truvenderWallet->update([
            'ngn_balance' => $truvenderWallet->ngn_balance + $amount,
        ]);

        $wallet->update([
            'ngn_balance' => $wallet->ngn_balance - $amount
        ]);


        $transaction = Transaction::create([
            'user_id' => $truvender->id,
            'trxn_ref' => 'txtv'.time().Str::random(5),
            'type' => 'credit',
            'amount' => $amount,
            'section' => 'Recieve Fund',
            'wallet' => 'ngn_wallet',
            'currency' => 'ngn',
            'status' => 'success',
        ]);

        $transaction = Transaction::create([
            'user_id' => $user->id,
            'trxn_ref' => 'txtv'.time().Str::random(5),
            'type' => 'debit',
            'amount' => $amount,
            'wallet' => 'ngn_wallet',
            'section' => 'Transfer Fund',
            'currency' => 'ngn',
            'status' => 'success',
        ]);

        $dataUser = [
            'type' => 'debit',
            'date' => now()->format('d/m/Y H:i:s'),
            'status' => 'successful',
            'amount' => $amount,
            'wallet' => 'Naira Wallet',
            'balance' => $wallet->ngn_balance,
            'currency' => 'Naira',
            'username' => $user->username,
            'fee' => $fee
        ];

        $datatru = [
            'type' => 'debit',
            'date' => now()->format('d/m/Y H:i:s'),
            'status' => 'successful',
            'amount' => $amount,
            'wallet' => 'Naira Wallet',
            'balance' => $truvenderWallet->ngn_balance,
            'currency' => 'Naira',
            'username' => $truvender->username,
            'fee' => $fee
        ];


        $user->notify(new NewTransaction($dataUser));
        $truvender->notify(new NewTransaction($datatru));

        Cache::forget('uri');

        return \redirect()->route('transation-success');
    }



    public function transfer_other_bank_account1()
    {
        $request = session('data');
        $user = Auth::user();
        $amount = $request['amount'];
        $account_name = $request['account_name'];
        $account_number = $request['account_number'];
        $bank = $request['bank'];



        $transaction = Transaction::create([
            'user_id' => $user->id,
            'trxn_ref' => 'txtv'.time().Str::random(5),
            'type' => 'credit',
            'amount' => $amount,
            'section' => 'Recieve Fund',
            'wallet' => 'ngn_wallet',
            'currency' => 'ngn',
            'status' => 'unverified',
            'acc_name' => $account_name,
            'acc_number' => $account_number,
            'bank' => $bank->name
        ]);

        //Transfer Endpoint
        $flutterwave_endpoint = 'https://api.flutterwave.com/v3/transfers';
        $flutterwave_secrete_key = \config('services.flutterwave.secrete_key');
        $transfer_endpoint = Http::withHeaders(['Authorization' => $flutterwave_secrete_key])->post($flutterwave_endpoint, [
            'account_bank' => $bank->code,
            'account_number' => $account_number,
            'amount' => $amount,
            'naration' => $note,
            'currency' => 'NGN',
            'beneficiary_name' => $account_name,
            'reference' => $transaction->trxn_ref,
            'callback_url' => route('wallet.transfer.proccess')
        ])->json();


        if($transfer_endpoint['status'] == 'success' && ($transfer_endpoint['data'] != null && $transfer_endpoint['data']['is_approved'] == true)){
            $transaction->update([
                'fee' => $transfer_endpoint['data']['fee'],
                'status' => 'success'
            ]);

            $data = [
                'amount' => $this->amount,
                'transaction_id' => $transaction->id,
                'transfer_id' => $transfer_endpoint['data']['id']
            ];
            

            $transaction_data = Crypt::encrypt($data);
            Cache::forget('uri');
            
            return \redirect()->route('wallet.transfer.proccess', $transaction__data);
        }else{
            session()->flash('error', 'Transaction Failed');
            return \redirect()->route('wallet');
        }
    }

    public function transfer_other_bank_account()
    {
        $request = session('data');
        $user = Auth::user();
        $amount = $request['amount'];
        $account_name = $request['account_name'];
        $account_number = $request['account_number'];
        $bank = $request['bank'];

        $currency = 'NGN';

        $trx_limit = Configuration::where('key', 'max_transaction')->first();
        $fee = Configuration::where('key', 'fiat_tx_fee')->first()->value;

        
        if($wallet->ngn_balance < $amount){
            session()->flash('error', 'insufficient fund in wallet');
            return redirect()->route('wallet');
        }

        if($amount >= $trx_limit){
            session()->flash('error', 'Transaction Limit Exceeded');
            return redirect()->route('wallet');
        }

        $data = [
            'account_number' => $account_number,
            'amount' => $amount * 100,
            'naration' => 'Truvender Fund Transfer',
            'currency' => 'NGN',
            'beneficiary_name' => $banking->acc_name,
            'reference' => $transaction->trxn_ref,
        ];

        $key = config('services.paystack.sk');
        $endpoint = config('services.paystack.url').'bank/resolve?account_number='.$data['account_number'].'bank_code='.$bank->code;
        $response = Http::withHeaders(['Authorization' => 'Bearer '.$key])->get($endpoint)->json();
        if($response['status'] == true){
            $account_name = $response['data']['account_name'];
            $account_number = $response['data']['account_number'];
            $bank_id = $response['data']['bank_id'];

            $transaction = Transaction::create([
                'user_id' => $user->id,
                'trxn_ref' => 'txtv'.time().Str::random(5),
                'type' => 'debit',
                'amount' => $amount,
                'section' => 'Transfer Fund',
                'wallet' => 'ngn_wallet',
                'currency' => 'ngn',
                'status' => 'unverified',
                'acc_name' => $account_name,
                'acc_number' => $account_number,
                'bank' => $bank->name
            ]);

            $endpoint = config('services.paystack.url').'transferrecipient';
            $response = Http::withHeaders(['Authorization' => 'Bearer '.$key])->post($endpoint, [
                'type' => 'nuban',
                'name' => $account_name,
                'account_number' => $account_number,
                'bank_code' => $bank->code,
                'currency' => $currency
            ])->json();

            if($response['status'] == true){

                $transaction->update([
                    'recipient_code' => $response['data']['recipient_code']
                ]);

                $endpoint = config('services.paystack.url').'transfer';
                $response = Http::withHeaders(['Authorization' => 'Bearer '.$key])->post($endpoint,[
                    'source' => "balance",
                    'amount' => $data['amount'],
                    'recipient' => $transaction->recipient_code,
                    'reason' => 'Transfer from truvender account',
                ])->json();


                if($response['status'] == true){
                    session()->flash('info', 'Your transaction is beign processed');
                    return redirect()->route('wallet');
                }
            }
        }
    }
}
