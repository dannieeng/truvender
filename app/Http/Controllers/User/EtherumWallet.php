<?php

namespace App\Http\Controllers\User;

use App\Models\Feature;
use App\Helper\Conversion;
use App\Models\Transaction;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Models\Configuration;
use App\Models\EtherumTransaction;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use App\Notifications\NewTransaction;
use Illuminate\Notifications\Notification;

class EtherumWallet extends Controller
{
    public function index()
    {
        $user = Auth::user();
        $wallet = $user->etherum_wallet;
        
        $update = $wallet->update_address_balance(); //Update Wallet info
        $ether_transactions = $wallet->transactions;

        return view('users.wallet.ether.index', [
            'wallet' => $wallet,
            'transactions' => $ether_transactions
        ]);
    }


    public function create()
    {
        $user = Auth::user();
        $wallet = $user->etherum_wallet;
        if($wallet){
            return redirect()->route('wallet.ether.index');
        }else{
            return view('users.wallet.ether.create');
        }
    }


    public function save(Request $req)
    {
        $user  = Auth::user();
        $ether_wallet = $user->create_ether_wallet();
        if($ether_wallet['status'] == true){
            return redirect()->route('wallet.etherum.index');
        }else{
            return redirect()->route('service.unavalable');
        }

    }


    public function info()
    {
        $user = Auth::user();
        $wallet = $user->lithcoin_wallet;
        $litcoin_transactions = $wallet->transactions;
        $url = config('services.block_cypher.url').'/eth/main/addrs/'.$wallet->address;
        $api_call = Http::get($url)->json();

        $address = $api_call['address'];
        $total_received = $api_call['total_received'];
        $total_sent = $api_call['total_sent'];
        $unconfirmed_balance = $api_call['unconfirmed_balance'];
        $final_balance = $api_call['final_balance'];
        $transactions = $api_call['n_tx'];
        $unconfirmed_transactions = $api_call['unconfirmed_n_tx'];
        $final_transactions = $api_call['final_n_tx'];


        return view('users.wallet.ether.info', [
            'address' => $address,
            'total_received' => $total_received,
            'total_sent' => $total_sent,
            'unconfirmed_balance' => $unconfirmed_balance,
            'final_balance' => $final_balance,
            'transaction_total' => $transactions,
            'unconfirmed_transaction_total' => $unconfirmed_transactions,
            'final_transaction_total' => $final_transactions

        ]);
        
    }

    public function send()
    {
        $req = session('data');
        $user = Auth::user();
        $wallet = $user->etherum_wallet;
        $value = $req['value'];
        $address = $req['address'];
        $ngn_amount = $req['amount_ngn'];
        $usd_amount = $req['amount_usd'];
        $note = $req['note'];
        
        $transfer = $wallet->transfer_to_otherWallet($value, $address);
        if(isset($transfer['data'])){
            $data = $transfer['data'];

            $get_fee = Configuration::where('key', 'crypto_tx_fee')->first()->value; 
            $get_fee2 = Configuration::where('key', 'crypto_tx_fee2')->first()->value; 

            $fee = $usd_amount >= 600 ?
                    ($usd_amount / 100) * $get_fee2 : 
                        ($usd_amount / 100) * $get_fee;

            $wallet->update_address_balance();

            $transaction_fee = Conversion::convert_usd_to_eth($fee);

            $wallet->update([
                'balance' => $wallet->balance - $transaction_fee
            ]);

            $transaction = Transaction::create([
                'user_id' => $user->id,
                'trxn_ref' => 'tvtx'.time().Str::random(5),
                'wallet' => 'Eth wallet',
                'section' => 'Send Eth',
                'type' => 'debit',
                'currency' => 'Eth',
                'fee' => $transaction_fee,
                'amount' => $value,
                'status' => 'success'
            ]);

            $ether_transaction = EtherumTransaction::create([
                'transaction_id' => $transaction->id,
                'ether_wallet_id' => $wallet->id,
                'user_id' => $user->id,
                'tx_hash' => $data['hash'],
                'block_height' => $data['block_height'],
                'value' => $value,
                'double_spend' => $data['double_spend'],
                'confirmation' => $data['confirmation'],
                'created_at' => now()->parse($data['confirmed'])->format('Y-m-d H:i:s'),
                'updated_at' => now()->parse($data['received'])->format('Y-m-d H:i:s'),
            ]);

            $info = [
                'username' => $user->username,
                'status' => 'successful',
                'date' => $ether_transaction->created_at,
                'type' => 'debit',
                'amount' => $value,
                'currency' => 'Eth',
                'fee' => $data['fees'],
                'wallet' => 'Ethereum Wallet',
                'balance' => $wallet->balance,
            ];


            $user->notify(new NewTransaction($info));

            session()->flash('success', 'Transaction was successfull');

            return redirect()->route('wallet.etherum.index');

        }else{
            session()->flash('success', 'Transaction Failed');
            return redirect()->route('wallet.etherum.index');
        }

    }

    public function trade()
    {
        return view('users.trade.eth.index');
    }

    public function buy_post()
    {
        $tradeWith =  decrypt($data);
        $user = Auth::user();
        $wallet = $user->etherum_wallet;
        $main_wallet = $user->wallet;
        $settings =  $user->settings;

        $service = Feature::where('name', 'Buy Ethereum')->first();
        if($service->is_available == false){
                return \redirect()->route('service.unavalable');
        }

        $max_transaction =  Configuration::where('key', 'max_transaction')->first();

        //Get Admin Wallets
        $config_wallet = Configuration::where('key', 'eth_wallet')->first();
        $config_wallet2 = Configuration::where('key', 'eth_wallet2')->first();

        $wallet->update_address_balance();

        if($wallet->balance <= $tradeWith['eth']){
            session()->flash('error', 'Could not Continue Transaction due to Insufficient Ngn Balance');
            return \redirect()->back();
        }else{
            //Create Transactions
            $transaction = Transaction::create([
                'user_id' => $user->id,
                'trxn_ref' => 'tvtx'.time().Str::random(5),
                'type' => 'debit',
                'section' => 'buy eth',
                'wallet' => 'ngn_wallet',
                'currency' => 'ngn',
                'amount' => $tradeWith['rate'],
                'status' => 'unverified',
            ]);


            $transaction2 = Transaction::create([
                'user_id' => $user->id,
                'trxn_ref' => 'tvtx'.time().Str::random(5),
                'type' => 'credit',
                'section' => 'buy_eth',
                'wallet' => 'ether_wallet',
                'amount' => $tradeWith['eth'],
                'status' => 'unverified', 
            ]);

            $transfer = $wallet->receive_from_otherWallet($tradeWith['eth'], $config_wallet);

            if(isset($transfer['data']))
            {
                $data = $transfer['data'];
                $value = $data['value'];

                $main_wallet->update([
                    'ngn_balance' => $main_wallet->ngn_balance - $tradeWith['ngn']
                ]);

                $transaction2->update([
                    'status' => 'success'
                ]);

                $transaction->update([
                    'status' => 'success'
                ]);

                $ether_transaction = EtherumTransaction::create([
                    'transaction_id' => $transaction->id,
                    'ether_wallet_id' => $wallet->id,
                    'user_id' => $user->id,
                    'tx_hash' => $data['hash'],
                    'block_height' => $data['block_height'],
                    'value' => $value,
                    'double_spend' => $data['double_spend'],
                    'confirmation' => $data['confirmation'],
                    'created_at' => now()->parse($data['confirmed'])->format('Y-m-d H:i:s'),
                    'updated_at' => now()->parse($data['received'])->format('Y-m-d H:i:s'),
                ]);

                $info = [
                    'username' => $user->username,
                    'status' => 'successfully',
                    'date' => $ether_transaction->created_at,
                    'type' => 'credit',
                    'amount' => $value,
                    'currency' => 'Eth',
                    'fee' => $data['fees'],
                    'wallet' => 'Ethereum Wallet',
                    'balance' => $wallet->balance,
                ];


                $user->notify(new NewTransaction($info));


                $data2 = [
                    'username' => $user->username,
                    'status' => 'successful',
                    'date' => $transaction->updated_at,
                    'type' => 'debit',
                    'amount' => $tradeWith['ngn'],
                    'wallet' => 'Ngn wallet',
                    'currency' => 'NGN',
                    'balance' => $main_wallet->ngn_balance,
                ];

                $user->notify(new NewTransaction($data2));

                session()->flash('success', 'Transaction was successful');
                return \redirect()->route('wallet.etherum.index');

            }else{
                session()->flash('error', 'Transaction failed');
                return \redirect()->back();
            }

        }

    }


    public function sell_post()
    {
        $tradeWith =  decrypt($data);
        $user = Auth::user();
        $wallet = $user->etherum_wallet;
        $main_wallet = $user->wallet;
        $settings =  $user->settings;

        $service = Feature::where('name', 'Sell Ethereum')->first();
        if($service->is_available == false){
                return \redirect()->route('service.unavalable');
        }
        
        $max_transaction =  Configuration::where('key', 'max_transaction')->first();

       //Check Verification
       if(($user->banking == true && $user->banking->bvn == null ) && $tradeWith['ngn'] >= $max_transaction){
            session()->flash('error', 'Apply verification to make transaction above $' . $max_transaction );
            return redirect()->back();
       }


       //Get Admin Wallets
       $config_wallet = Configuration::where('key', 'eth_wallet')->first();
       $config_wallet2 = Configuration::where('key', 'eth_wallet2')->first();

       $wallet->update_address_balance();

        if($wallet->balance <= $tradeWith['eth']){
            session()->flash('error', 'Could not Continue Transaction due to Insufficient Etherum Balance');
            return \redirect()->back();
        }else{

            $transaction2 = Transaction::create([
                'user_id' => $user->id,
                'trxn_ref' => 'tvtx'.time().Str::random(5),
                'type' => 'debit',
                'section' => 'sell_eth',
                'wallet' => 'eth_wallet',
                'amount' => $tradeWith['eth'],
                'status' => 'unverified', 
            ]);

            $transaction = Transaction::create([
                'user_id' => $user->id,
                'trxn_ref' => 'tvtx'.time().Str::random(5),
                'type' => 'credit',
                'section' => 'sell_eth',
                'wallet' => 'ngn_wallet',
                'currency' => 'ngn',
                'amount' => $tradeWith['rate'],
                'status' => 'unverified',
            ]);

            $transfer = $wallet->transfer_to_otherWallet($tradeWith['eth'], $config_wallet);
            if(isset($transfer['data']))
            {
                $data = $transfer['data'];
                $value = $data['value'];

               
                $transaction2->update([
                    'status' => 'success'
                ]);

                $transaction->update([
                    'status' => 'success'
                ]);


                $ether_transaction = EtherumTransaction::create([
                    'transaction_id' => $transaction2->id,
                    'ether_wallet_id' => $wallet->id,
                    'user_id' => $user->id,
                    'tx_hash' => $data['hash'],
                    'block_height' => $data['block_height'],
                    'value' => $value,
                    'double_spend' => $data['double_spend'],
                    'confirmation' => $data['confirmation'],
                    'created_at' => now()->parse($data['confirmed'])->format('Y-m-d H:i:s'),
                    'updated_at' => now()->parse($data['received'])->format('Y-m-d H:i:s'),
                ]);

                $info = [
                    'username' => $user->username,
                    'status' => 'successful',
                    'date' => $ether_transaction->created_at,
                    'type' => 'debit',
                    'amount' => $value,
                    'currency' => 'ETH',
                    'fee' => $data['fees'],
                    'wallet' => 'Ethereum Wallet',
                    'balance' => $wallet->balance,
                ];


                $user->notify(new NewTransaction($info));

                $main_wallet->update([
                    'ngn_balance' => $main_wallet->ngn_balance + $tradeWith['ngn']
                ]);


                $data2 = [
                    'username' => $user->username,
                    'status' => 'successful',
                    'date' => $transaction->updated_at,
                    'type' => 'credit',
                    'amount' => $tradeWith['ngn'],
                    'wallet' => 'Ngn wallet',
                    'currency' => 'NGN',
                    'balance' => $main_wallet->ngn_balance,
                ];

                $user->notify(new NewTransaction($data2));

                session()->flash('success', 'Transaction was successful');
                return \redirect()->route('wallet.etherum.index');

            }else{
                session()->flash('error', 'Transaction failed');
                return \redirect()->back();
            }

        }

    }
}
