<?php

namespace App\Http\Controllers\User;


use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

class OtpController extends Controller
{
   
    public function showVerifyPage()
    {
        return view('auth.otp');
    }

    public function verify(Request $req)
    {
        $this->validate($req,[
            'code' => 'required'
        ]);

        $user = Auth::user();
        $status = $user->validate_otp($req->code);
        if($status == true){

            $next = Cache::pull('uri');
            $what_next = decrypt($next);
            $data = Cache::pull('data');
            session()->flash('data', $data);
            return redirect($what_next);
            
        }else{
            // $this->addError(['' => 'Invalid verification code!']);
            session()->flash('error', 'Invalid verification code!');
            return redirect()->back();
        }
    }

    

}
