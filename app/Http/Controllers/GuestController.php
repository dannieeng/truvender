<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Support\Str;
use App\Models\PostCategory;
use Illuminate\Http\Request;
use App\Models\Configuration;

class GuestController extends Controller
{
    public function index()
    {
        return \view('guest.index');
    }


    public function rates()
    {
        return \view('guest.rates');
    }

    public function about()
    {
        return \view('guest.about');
    }

    public function career()
    {
        return \view('guest.career');
    }

    public function products()
    {
        return \view('guest.products');
    }

    public function calculator()
    {
        return \view('guest.calculate');
    }

    public function contact()
    {
        return \view('guest.contact');
    }

    public function fq()
    {
        return \view('guest.faq');
    }

    public function send(Request $request)
    {
        
    }

    public function terms()
    {
        $terms = Configuration::where('key', 'term_of_use')->first();

        return \view('guest.terms', [
            'text' => $terms->value,
            'title' => 'Terms of Use'
        ]);
    }

    public function policy()
    {
        $terms = Configuration::where('key', 'policy')->first();
        $atml = Configuration::where('key', 'amlp')->first();

        return \view('guest.terms', [
            'text' => $terms->value ,
            'atml' => $atml->value,
            'title' => 'Our Policies'
        ]);
    }

    public function atml()
    {
        $terms = Configuration::where('key', 'amlp')->first();

        return \view('guest.terms', [
            'text' => $terms->value,
            'title' => 'Anti-Money Laundering Policy'
        ]);
    }
    
    
    public function blog($cat_id = null)
    {
        $id = $cat_id != null ? decrypt($cat_id) : null;
        $category = $id != null ? PostCategory::where('id', $id)->first() : null;

        $posts = $category != null ? $category->posts->orderBy('created_at', 'desc')->get() :
                 Post::orderBy('created_at', 'desc')->paginate(12);

        $latest_posts = Post::orderBy('created_at', 'desc')->inRandomOrder()->take(5);
        $categories = PostCategory::orderBy('name', 'asc')->get();
        return view('guest.blog.index', [
            'posts' => $posts,
            'categories' => $categories,
            'latest_posts' => $latest_posts
        ]);
    }
    
    
    public function show_blog($blog_id)
    {
        $id = decrypt($blog_id);
        $post = Post::where('id', $id)->first();
        $latest_posts = Post::orderBy('created_at', 'desc')->inRandomOrder()->take(5);
        $categories = PostCategory::orderBy('name', 'asc')->get();
        return view('guest.blog.show', [
            'post' => $post,
            'categories' => $categories,
            'related_posts' => $latest_posts
        ]);
    }
}
