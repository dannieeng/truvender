<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class Setting extends Controller
{
    public function password()
    {
        return view('admin.security.password');
    }

    public function update_password(Request $request)
    {
        $this->validate($request, [
            'old_password' => 'required|string',
            'password' => 'required|string|confirmed',
        ]);

        $user = Auth::user();

        $old_password = $request->old_password;
        $new_password = $request->password;

        $check_password = Hash::check($old_password, $user->password);

        if($check_password == true){
            $create_password = Hash::make($new_password);
            $user->update([
                'password' => $create_password
            ]);

            session()->flash('success', 'Password was successfully updated. Please Login to Continue');
            return redirect()->route('admin.index');
        }else{

            session()->flash('error', 'Invalid password');
            return redirect()->route('admin.index');
        }
    }
}
