<?php

namespace App\Http\Controllers\Admin;

use App\Models\OtherAsset;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\OtherAssetTransaction;
use App\Notifications\NewTransaction;

class OtherAssets extends Controller
{
    public function rates($asset_id)
    {
        $id = decrypt($asset_id);

        $asset = OtherAsset::where('id', $id)->first();
        return view('admin.other-asset.rates', [
            'asset' => $asset
        ]);
    }


    public function trades()
    {
        $asset_trx = OtherAssetTransaction::orderBy('created_at', 'desc')->paginate(30);
        return view('admin.other-asset.index', [
            'transactions' => $asset_trx 
        ]);
    }

    public function reject($trx_id)
    {
        $id = decrypt($trx_id);
        $trade = OtherAssetTransaction::where('id', $id)->first();
        $user = $trade->user;
        $wallet = $user->wallet;
        $transaction = $trade->transaction;
        $asset = $trade->asset;
        $amount = $trade->price;

        $fee = 0;
        
        $trade->update([
            'status' => null
        ]);

        $transaction->update([
            'status' => 'success'
        ]);

        session()->flash('success', 'Tranaction was canceled successfully');

        return redirect()->route('admin.other-asset.trades');
    }


    public function accept($trx_id)
    {
        $id = decrypt($trx_id);
        $trade = OtherAssetTransaction::where('id', $id)->first();
        $user = $trade->user;
        $wallet = $user->wallet;
        $transaction = $trade->transaction;
        $asset = $trade->asset;
        $amount = $trade->price;

        $fee = 0;

        $transaction->update([
            'status' => 'success'
        ]);


        $trade->update([
            'status' => true
        ]);

        $wallet->update([
            'ngn_balance' => $wallet->ngn_balance + $trade->price
        ]);

        $data = [
            'type' => 'credit',
            'date' => now()->format('d/m/Y H:i:s'),
            'status' => 'successful',
            'fee' =>    $fee,
            'amount' => $amount,
            'wallet' => 'Naira Wallet',
            'balance' => $wallet->ngn_balance,
            'currency' => 'Naira',
            'username' => $user->username
        ];

        $user->notify(new NewTransaction($data));

        session()->flash('success', 'Tranaction was approved successfully');

        return redirect()->route('admin.other-asset.trades');

    }
}
