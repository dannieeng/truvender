<?php

namespace App\Http\Controllers\Admin;

use App\Models\Post;
use Illuminate\Support\Str;
use App\Models\PostCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;

class Blog extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($cat_id = null)
    {
        $id = $cat_id != null ? Crypt::decrypt($cat_id) : null;
        $category = $id != null ? PostCategory::where('id', $id)->first() : null;

        $posts = $category != null ? $category->posts->orderBy('created_at', 'desc')->get() :
                 Post::orderBy('created_at', 'desc')->paginate(12);

        $latest_posts = Post::orderBy('created_at', 'desc')->inRandomOrder()->take(5);
        $categories = PostCategory::orderBy('name', 'asc')->get();
        return view('admin.blog.index', [
            'posts' => $posts,
            'categories' => $categories,
            'latest_posts' => $latest_posts
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        $categories = PostCategory::orderBy('name', 'asc')->get();
        return \view('admin.blog.create', [
            'categories' => $categories
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'string|required',
            'slug' => 'required|string|unique:posts,slug',
            'image' => 'required|image|mimes:png,jpg,gif,svg,jpeg',
            'category' => 'nullable|integer',
            // 'featured_image' => 'nullable|image|mimes:png,jpg,gif,svg|size:5120',
            'body' => 'required|string',
        ]);

        if($request->category != null){
            $category = PostCategory::where('name', $request->category)->first();
            if($category == true){
                $post_cat = $category->id;
            }else{
                $category = PostCategory::create([
                    'name' => $request->category
                ]);
            }
        }else{
            $category = null;
        }

        $featured_image_path = null;
        // if($request->file('featured_image')){
        //     $featured_image = $request->featured_image;
        //     $featured_image_name = Str::random(20).'-'.time().$featured_image->extension();
        //     $featured_image->move(public_path('uploads/blog/featured-images'), $featured_image_name);
        //     $featured_image_path = 'uploads/blog/featured-images'.$featured_image_name;
        // }

        $post_image = $request->file('image');
        $post_image_name = Str::random(20). '_' .time().'.'.$post_image->extension();  
        $post_image->move(public_path('uploads/blog/images'), $post_image_name);
        $image_path = '/uploads/blog/images/'.$post_image_name;

        $user = Auth::user();

        $post = Post::create([
            'user_id' => $user->id,
            'post_category_id' => $category,
            'title' => $request->title,
            'slug' => $request->slug,
            'featured_image_path' => $featured_image_path != null ? $featured_image_path : null,
            'image_path' => $image_path,
            'body' => $request->body
        ]);

        session()->flash('success', 'Post added successfully');
        return \redirect()->route('admin.blog.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($blog_id)
    {
        $id = Crypt::decrypt($blog_id);
        $post = Post::where('id', $id)->first();
        $latest_posts = Post::orderBy('created_at', 'desc')->inRandomOrder()->take(5);
        $categories = PostCategory::orderBy('name', 'asc')->get();
        return view('admin.blog.show', [
            'post' => $post,
            'categories' => $categories,
            'related_posts' => $latest_posts
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($blog_id)
    {
        $id = Crypt::decrypt($blog_id);
        $post = Post::where('id', $id)->first();
        $user = Auth::user();

        if($post->id == $user->id){
            return view('admin.blog.edit', [
                'post' => $post
            ]);
        }else{
            session()->flash('error', 'You do not have access to this post');
            return redirect()->back();
        }
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $blog_id)
    {
        $this->validate($request, [
            'title' => 'string|required',
            'slug' => 'required|string|unique:posts,slog',
            'image' => 'nullable|image|mimes:png,jpg',
            'category' => 'nullable|integer',
            'featured_image' => 'nullable|image|mimes:png,jpg,gif,svg',
            'body' => 'required|string'
        ]);

        $id = Crypt::decrypt($blog_id);
        $post = Post::where('id', $id)->first();

        if($request->category != null){
            $category = PostCategory::where('name', $request->category)->first();
            if($category == true){
                $post_cat = $category->id;
            }else{
                $category = PostCategory::create([
                    'name' => $request->category
                ]);
            }
        }else{
            $category = null;
        }

        $featured_image_path = null;
        if($request->file('featured_image')){
            $featured_image = $request->file('featured_image');
            $featured_image_name = Str::random(20).'-'.time().$featured_image->extension();
            $featured_image->move(public_path('uploads/blog/featured-images'), $featured_image_name);
            $featured_image_path = '/uploads/blog/featured-images'.$featured_image_name;
        }

        $image_path = null;
        if($request->file('image')){
            $post_image = $request->file('image');
            $post_image_name = Str::random(20). '-' .time().'.'.$post_image->extension();  
            $post_image->move(public_path('uploads/blog/images'), $post_image_name);
            $image_path = '/uploads/blog/images/'.$post_image_name;
        }

        $post = Post::update([
            'post_category_id' => $request->category != null ? $request->category : $post->category,
            'title' => $request->title != null ? $request->title : $post->title,
            'featured_image_path' => $featured_image_path != null ? $featured_image_path : $post->featured_image_path,
            'image_path' => $image_path != null ? $image_path : $post->image_path,
            'body' => $request->body != null ? $request->body : $post->body
        ]);

        session()->flash('success', 'Post was updated successfully');
        return \redirect()->route('admin.blog.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($blog_id)
    {
        $id = Crypt::decrypt($blog_id);
        $post = Post::where('id', $id)->first();
        $user = Auth::user();
        if($post->id == $user->id || $user->role->name == 'admin'){
            $post->delete();
            session()->flash('success', 'Post was deleted successfully');
            return \redirect()->route('admin.blog.index');
        }else{
            session()->flash('error', 'You do not have access to this post');
            return redirect()->back();
        }
    }
}
