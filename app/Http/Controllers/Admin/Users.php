<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use Illuminate\Http\Request;
use App\Models\BlacklistUser;
use App\Notifications\Suspended;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Crypt;
use App\Notifications\SuspensionLifted;
use Illuminate\Notifications\Notification;

class Users extends Controller
{

    public function index()
    {
        return view('admin.users.index');
    }
    
    public function profile($user_id)
    {
        $id = Crypt::decrypt($user_id);
        $user = User::where('id', $id)->first();
        $referrals = User::where('referrer_id', $user->id)->paginate(15);
        
        return \view('admin.users.show', [
            'user' => $user,
            'profile' => $user->profile,
            'banking' => $user->banking,
            'referrals' => $referrals,
        ]);
    }

    public function edit_profile($user_id)
    {
        $id = Crypt::decrypt($user_id);
        $user = User::where('id', $id)->first();

        return \view('admin.index', [
            'user' => $user
        ]);
    }

    // public function update_profile(Request $request, $user_id)
    // {
    //     $request->validate([
    //         ''
    //     ]);

    //     $id = Crypt::decrypt($user_id);
    //     $user = User::where('id', $id)->first();
    // }


    public function add_to_blacklist(Request $request)
    {
        $request->validate([
            'note' => 'required|string|max:300'
        ]);


        $user = User::where('id', $request->id)->first();

        $blacklist = BlacklistUser::withTrashed()->where('user_id', $user->id)->first();

        if($blacklist == true){
            $blacklist->update([
                'cause_of_restriction' => $request->note,
            ]);

            $blacklist->restore();
        }else{
            $blacklist = BlacklistUser::create([
                'user_id' => $user->id,
                'cause_of_restriction'  => $request->note,
            ]);
        }

        $data = [
            'username' => $user->username,
            'note' => $request->note,
            'date' => now()->format('d/m/Y H:i')
        ];

        $user->notify(new Suspended($data));

        session()->flash('success', $user->username . ' was added to blacklist successfully');
        return \redirect()->route('users');
    }


    public function remove_form_blacklist($user_id)
    {
        $id = Crypt::decrypt($user_id);
        $user = User::where('id', $id)->first();

        if($user->in_blacklist == true){
            $blacklist = $user->in_blacklist;
            $blacklist->delete();

            $user->notify(new SuspensionLifted());

            session()->flash('success', $user->username . ' was successfully removed from blacklist.');
            return \redirect()->route('users');

        }else{

            session()->flash('info', $user->username . ' is not in blacklist.');
            return \redirect()->route('users');
        }

    }

    
}
