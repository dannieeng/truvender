<?php

namespace App\Http\Controllers\Admin;

use App\Models\Post;
use App\Models\Role;
use App\Models\User;
use App\Models\Feature;
use App\Models\CryptoRates;
use App\Models\Transaction;
use Illuminate\Http\Request;
use App\Models\Configuration;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class Pages extends Controller
{
    public function index()
    {
        $users = User::where('id', '!=', Auth::user()->id)->get();
        $transactions = Transaction::all();
        $newest_transactions = Transaction::orderBy('created_at', 'desc')->take(20)->get();
        $messages = null;
        $posts = Post::orderBy('created_at', 'desc')->take(12)->get();
        
        return \view('admin.index', [
            'users' => $users,
            'transactions' => $transactions,
            'newest_transactions' => $newest_transactions,
            'messages' => $messages,
            'posts' => $posts
        ]);
    }

    public function services()
    {
        $services = Feature::orderBy('name', 'asc')->get();

        return \view('admin.services.index', [
            'services' => $services
        ]);
    }

    public function update_services($service_id)
    {
       $id = decrypt($service_id);

       $service = Feature::where('id', $id)->first();
       $service->update([
           'is_available' => $service->is_available == true ? false : true
       ]);

       $av = $service->is_available == true ? 'disabled' : 'enabled';
       session()->flash('success', $service->name . ' is now ' . $av);
       return \redirect()->route('admin.services');
    }

    public function btc_rates()
    {
        $btc = CryptoRates::where('symbol', 'BTC')->first();
        return view('admin.crypto.rates' ,[
            'crypto' => $btc
        ]);
    }

    public function ltc_rates()
    {
        $ltc = CryptoRates::where('symbol', 'LTC')->first();
        return view('admin.crypto.rates' ,[
            'crypto' => $ltc
        ]);
    }

    public function eth_rates()
    {
        $eth = CryptoRates::where('symbol', 'ETH')->first();
        return view('admin.crypto.rates' ,[
            'crypto' => $eth
        ]);
    }

    public function priv()
    {
        $user = Auth::user();
        if($user->role->name == 'admin'){
            $users = User::where('id', '!=', Auth::user()->id)->get();
            $priv = Role::orderBy('name', 'asc')->get();
            return \view('admin.privi', [
                'users' =>  $users,
                'privileadges' => $priv
            ]);
        }else{
            session()->flash('error', 'Permission not granted');
            return \redirect()->back();
        }
    }

    public function grant(Request $request)
    {
        $this->validate($request, [
            'user' => 'required|string',
            'privileadge' => 'required|string'
        ]);

        $user = User::where('username', $request->user)->first();
        $priv = Role::where('name', $request->privileadge)->first();

        if($user == true){
            if($priv = true){
                $user->update([
                    'role_id' => $priv->id
                ]);

                session()->flash('success', ucfirst($priv->name) . ' permission granted successfully to ' . $user->username);
                return \redirect()->back();
            }

            session()->flash('error', 'Privileadge does not exist');
            return \redirect()->back();
        }else{
            session()->flash('error', 'User does not exist');
            return \redirect()->back();
        }
    }

    public function revoke($user_id)
    {
        $id = decrypt($user_id);
        $user = User::where('username', $id)->first();
        $priv = Role::where('name', 'user')->first();
        $user->update([
            'role_id' => $priv->id
        ]);

        session()->flash('success',  $user->username . '\'s permission revoked successfully to ' . ucfirst($priv->name));
        return \redirect()->back();
    }

    public function config()
    {
        $policy = Configuration::where('key', 'policy')->first();
        
        $terms = Configuration::where('key', 'term_of_use')->first();

        $disclaim = Configuration::where('key', 'amlp')->first();
        return view('admin.config.index', [
            'terms' => $terms,
            'policy' => $policy,
            'disclaim' => $disclaim
        ]);
    }

    public function tou(Request $request)
    {
        $this->validate($request, [
            'value' => 'required|string'
        ]);

        $terms = Configuration::where('key', 'term_of_use')->first();
        $terms->update([
            'value' => $request->value,
        ]);

        session()->flash('success', 'Terms of use updated successfully');
        return \redirect()->route('admin.config');
    }

    public function policy(Request $request)
    {
        $this->validate($request, [
            'value' => 'required|string'
        ]);

        $policy = Configuration::where('key', 'policy')->first();
        $policy->update([
            'value' => $request->value,
        ]);

        session()->flash('success', 'Policy of use updated successfully');
        return \redirect()->route('admin.config');
    }

    public function amp(Request $request)
    {
        $this->validate($request, [
            'value' => 'required|string'
        ]);

        $amp = Configuration::where('key', 'amlp')->first();
        $amp->update([
            'value' => $request->value,
        ]);

        session()->flash('success', 'Policy of use updated successfully');
        return \redirect()->route('admin.config');
    }
}
