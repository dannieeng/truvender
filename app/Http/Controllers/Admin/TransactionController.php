<?php

namespace App\Http\Controllers\Admin;

use App\Models\Peer2Peer;
use App\Models\Transaction;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Notifications\NewTransaction;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Notifications\Notification;

class TransactionController extends Controller
{
    public function index()
    {
        $transactions = Transaction::orderBy('created_at', 'desc')->paginate(30);
        return view('admin.transactions.index', [
            'transactions' => $transactions
        ]);
    }

    public function details($trx_id)
    {
        $id = Crypt::decrypt($trx_id);
        $transaction = Transaction::where('id', $id)->first();
        return view('admin.transactions.details', [
            'transaction' => $transaction
        ]);
    }

    public function peer2peer()
    {
        $peers = Peer2Peer::orderBy('created_at', 'desc')->paginate(30);
        return view('admin.transactions.peer', [
            'peers' => $peers
        ]);

    }

    public function validate_peer($peer_id)
    {
        $id = decrypt($peer_id);
        $peer = Peer2Peer::where('id', $id)->first();
        $user = $peer->user;
        $wallet = $user->wallet;
        $transaction = $peer->transaction;

        $transaction->update([
            'status' => 'success'
        ]);

      
        $data = [
            'type' => 'debit',
            'date' => now()->format('d/m/Y H:i:s'),
            'status' => 'pending',
            'amount' => $amount,
            'wallet' => 'Naira Wallet',
            'balance' => $truvenderWallet->ngn_balance,
            'currency' => 'Naira',
            'username' => $truvender->username,
            'fee' => $fee
        ];
        $user->notify(new NewTransaction($data));

        return redirect()->back();
        
    }
}
