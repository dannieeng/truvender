<?php

namespace App\Http\Controllers\Admin;

use App\Models\Card;
use App\Models\Country;
use App\Models\CardType;
use App\Models\CardPrice;
use App\Models\CardTrade;
use App\Models\CardCountry;
use App\Models\CardDefaultRate;
use App\Models\Transaction;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Models\CardTradeImage;
use App\Notifications\CardApproved;
use App\Http\Controllers\Controller;
use App\Notifications\CardDisapproved;
use App\Notifications\TransactionSuccess;
use Illuminate\Notifications\Notification;

class CardsController extends Controller
{
    public function index()
    {
        return \view('admin.cards.index');
    }


    public function card_rates()
    {
        $rates = CardCountry::orderBy('created_at', 'desc')->paginate(30);
        return view('admin.cards.rates', [
            'rates' => $rates
        ]);
    }
    public function add_rate($card_id = null)
    {
        $id = $card_id != null ? decrypt($card_id) : null;
        $country = Country::orderBy('name' , 'asc')->get();
        $type = CardType::orderBy('created_at', 'desc')->get();
        $cards = Card::orderBy('name', 'asc')->get();

        return view('admin.cards.add-rate', [
            'countries' => $country,
            'types' => $type,
            'cards' => $cards,
            'card_id' => $id
        ]);
    }


    public function edit_rate($rate_id)
    {
        $id = decrypt($rate_id);
        $rate = CardCountry::where('id', $id)->first();
        $default_rate = CardDefaultRate::where('card_id', $rate->card_id)->first();

        return view('admin.cards.edit-rate', [
            'rate' => $rate,
            'default_rate' => $default_rate == true ? $default_rate->seller_rate : 0
        ]);
    }

    public function update_rate(Request $request, $rate_id)
    {
        $this->validate($request, [
            'buyer_rate' => 'required|numeric',
            'seller_rate' => 'required|numeric',
            'default_rate' => 'required|numeric'
        ]);

        $id = decrypt($rate_id);
        $rate = CardCountry::where('id', $id)->first();

        if($rate == true){
            $rate->update([
                'buyer_rate' => $request->buyer_rate,
                'seller_rate' => $request->seller_rate
            ]);
            
            $default_rate = CardDefaultRate::where('card_id', $rate->card_id)->first();
            if($default_rate == true){
                $default_rate->update([
                    'seller_rate' => $request->default_rate
                ]);
            }else{
                $default_rate = CardDefaultRate::create([
                    'card_id' => $rate->card_id,
                    'buyer_rate' => 0,
                    'seller_rate' => $request->default_rate,
                ]);
            }

            session()->flash('success', 'Card rate was updated successfully');
            return \redirect()->route('admin.cards.index');
        }
    }

    public function save_rate(Request $request)
    {
        $this->validate($request, [
            'card' => 'required|numeric',
            'country' => 'required|numeric',
            'buyer_rate' => 'required|numeric',
            'seller_rate' => 'required|numeric',
            'type' => 'required|string', 
            'amount' => 'required|numeric'
        ]);

        $card = Card::where('id', $request->card)->first();
        $country = Country::where('id', $request->country)->first();
        $card_type = CardType::where('id', $request->type)->first();
        $card_price = CardPrice::where('card_id', $card->id)->where('card_type_id', $card_type->id)->where('amount', $request->amount)->first();

        if($card_price != true){
            $card_price = CardPrice::create([
                'card_id' => $card->id,
                'card_type_id' => $card_type->id,
                'amount' => $request->amount
            ]);
        }

        $cardCountry = CardCountry::create([
            'card_id' => $card->id,
            'country_id' => $country->id,
            'card_price_id' => $card_price->id,
            'card_type_id' => $card_type->id,
            'buyer_rate' => $request->buyer_rate,
            'seller_rate' => $request->seller_rate,
        ]);

        session()->flash('success', 'Card rate successfully added');
        return redirect()->route('admin.cards.index');
    }

    public function get_rates($card_id)
    {
        $id = decrypt($card_id);
        $card = Card::where('id', $id)->first();

        $rates = $card->country_cards;
        $title = 'Rates';
        return view('admin.cards.perif', [
            'collections' => $rates,
            'title' => $title,
            'card_id' => $card->id,
        ]);
    }

    public function get_trades($card_id)
    {
        $id = decrypt($card_id);
        $card = Card::where('id', $id)->first();

        $trades = $card->trades()->orderBy('created_at', 'desc')->paginate(15);
        $title = 'Trades';
        return view('admin.cards.perif', [
            'collections' => $trades,
            'title' => $title,
            'card_id' => $card->id,
        ]);
    }

    public function delete($card_id)
    {
        $id = decrypt($card_id);
        $card = Card::where('id', $id)->first();
        $card_img = $card->image;

        $card->delete();
        Storage::delete($card_img);

        session()->flash('success', ' Card was deleted successfully');
        return redirect()->route('admin.cards.index');
    }


    public function card_trades()
    {
        $trades = CardTrade::orderBy('created_at', 'desc')->paginate(30);
        return  \view('admin.cards.trades', [
            'trades' => $trades
        ]);
    }

    public function card_images($trade_id)
    {
        $id = decrypt($trade_id);
        $card_trade = CardTrade::where('id', $id)->first();
        $card_images = $card_trade->images;
        return view('admin.cards.card-image', [
            'card_images' => $card_images
        ]);
    }

    public function approve_image($image_id)
    {
        $id = decrypt($image_id);
        $card_image = CardTradeImage::where('id', $id)->first();

        $cardUser = $card_image->user;

        $card_trade = $card_image->traded_from;
        $card_rate = $card_trade->card_rate;
        $country = $card_rate->country;

        $total = $card_trade->amount_accepted + $card_trade->amount_rejected;
        $amount = $card_trade->amount;
        $subtracted_amount = $amount - $total;

        $price_per_card = $card_trade->price / $card_trade->amount;
        
        if( $subtracted_amount > 0){

            $card_image->update([
                'is_approved' => true,
            ]);
    
            $card_trade->update([
                'amount_accepted' => $card_trade->amount_accepted + 1,
                'accepted_price' => $card_trade->accepted_price + $price_per_card,
                'status' =>'approved',
            ]);

            $wallet = $cardUser->wallet;
            $wallet->update([
                'ngn_balance' => $wallet->ngn_balance + $price_per_card
            ]);


            //Create Transaction
            // $transaction = Transaction::create([
            //     'user_id' => $cardUser->id,
            //     'trxn_ref' => 'tvtx'.time() . Str::random(5),
            //     'type' => 'credit',
            //     'section' => 'sell-giftcard',
            //     'wallet' => 'ngn_wallet',
            //     'currency' => 'ngn',
            //     'amount' => $price_per_card,
            //     'status' =>'success',
            // ]);
            

            $transaction = $card_trade->transaction;
            $transaction->update([
                'status' => 'success'
            ]);


            $cardTrading =  $card_trade->card;
            
            $notificationData = [
                'item' => 'Giftcard',
                'card' => $cardTrading->name,
                'cost' => 'N'.$price_per_card,
                'amount' => 1,
                'country' => $country->name,
                'rate' => 'N'. number_format($card_trade->buyer_rate,2) . '/$',
                'type' =>'credit',
            ];
            $notification = $cardUser->notify(new TransactionSuccess($notificationData));


            //Notify User About Card
            $data = [
                'user' => $cardUser,
                'cards_submitted' => $card_trade->images->count(),
                'amount_approved' => $card_trade->amount_accepted,
                'amount_disapproved' => $card_trade->amount_rejected,
                'balance' => $wallet->ngn_balance
            ];

            $cardUser->notify(new CardApproved($data));

            $trade_id = encrypt($card_trade->id);
            \session()->flash('success', 'A card image was approved successfully which makes it ' . $card_trade->amount_accepted .  ' cards approved');
            return \redirect()->route('admin.card.trade.images', $trade_id);
        }else{
            session()->flash('info', ' All ' .  $card_trade->amount . ' of this trade was approved');
            return redirect()->back();
        }

    }

    public function disapprove_image(Request $request, $image_id)
    {

        $this->validate($request, [
            'note' => 'nullable|string'
        ]);
        $id = decrypt($image_id);
        $card_image = CardTradeImage::where('id', $id)->first();

        $cardUser = $card_image->user;
        $wallet = $cardUser->wallet;

        $card_trade = $card_image->traded_from;
        $total = $card_trade->amount_accepted + $card_trade->amount_rejected;
        $amount = $card_trade->amount;
        $subtracted_amount = $amount - $total;
        $transaction = $card_trade->transaction;
        
        $card_rate = $card_trade->rate;
        $tx_amount = $transaction->amount - $card_rate;        


        if($subtracted_amount > 0){
            $card_image->update([
                'is_approved' => false,
            ]);

            $card_trade->update([
                'amount_rejected' => $card_trade->amount_rejected + 1,
                'status' => 'disapproved'
            ]);

            if($tx_amount == 0){
                $transaction->update([
                    'status' => 'failed'
                ]);
            }else{
                $total = $amount - ($card_trade->amount_rejected + $card_trade->amount_accepted);
                $transaction->update([
                    'amount' => $tx_amount,
                    'status' => $total == 0 ? 'success' : 'pending'
                ]);

                
            }
            //Notify user about card status
            $data = [
                'user' => $cardUser,
                'cards_submitted' => $card_trade->images->count(),
                'amount_approved' => $card_trade->amount_accepted,
                'amount_disapproved' => $card_trade->amount_rejected,
                'balance' => $wallet->ngn_balance,
                'reason' => $request->note
            ];

            $cardUser->notify(new CardDisapproved($data));

            $trade_id = encrypt($card_trade->id);
            \session()->flash('success', 'card image was rejected which makes it ' . $card_trade->amount_rejected .  ' cards rejected');
            return \redirect()->route('admin.card.trade.images', $trade_id);
        }else{

            session()->flash('info', ' All ' .  $card_trade->amount . ' of this trade was disapproved');
            return redirect()->back();
        }


    }


    public function card_types()
    {
        $types = CardType::orderBy('created_at', 'desc')->paginate(30);
        return \view('admin.cards.types.index', [
            'types' => $types
        ]);
    }

    public function add_card_types()
    {
        return \view('admin.cards.types.add');
    }


    public function save_card_type(Request $request)
    {
        $this->validate($request, [
            'card' => 'required|numeric',
            'type' => 'required|string',
        ]);


        $card = Card::where('id', $request->card)->first();

        $card_type = CardType::create([
            'card_id' => $card->id,
            'type' => $request->type
        ]);
    }


    public function delete_card_type($type_id)
    {
        $id = decrypt($type_id);
        $type = CardType::where('id', $id)->first();

        if($user->role->name == 'admin'){
            $type->delete();
            session()->flash('success', 'Card type was deleted successfully');
            return redirect()->back();
        }

        session()->flash('error', 'no permission to this action');
            return redirect()->back();
    }


    public function card_prices()
    {
        $card_prices = CardPrice::orderBy('created_at', 'desc')->paginate(30);
        return view('admin.cards.card-image');
    }

    public function add_card_price($type_id)
    {
        $id = decrypt($type_id);
        $card_type = CardType::where('id', $id)->first();
        return view('admin.cards.prices.add', [
            'card_type' => $card_type
        ]);
    }

    public function save_card_price(Request $request, $type_id)
    {
        $id = decrypt($type_id);
        
        $this->validate($request, [
            'amount' => 'required|string'
        ]);
        $amounts = explode(',', $request->amount);

        $card_type = CardType::where('id', $id)->first();
        foreach($amounts as $key => $amount){
            $price = CardPrice::create([
                'card_id' => $card_type->card_id,
                'card_type_id' => $card_type->id,
                'amount' => (int)$amount
            ]);
        }

        session()->flash('success', ' Card price was added successfully');
        return \redirect()->route('admin.card.types');
    }
    



}
