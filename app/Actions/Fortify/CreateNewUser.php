<?php

namespace App\Actions\Fortify;

use App\Models\User;
use App\Models\Wallet;
use App\Models\Profile;
use App\Models\Setting;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Validator;
use Laravel\Fortify\Contracts\CreatesNewUsers;

class CreateNewUser implements CreatesNewUsers
{
    use PasswordValidationRules;

    /**
     * Validate and create a newly registered user.
     *
     * @param  array  $input
     * @return \App\Models\User
     */
    public function create(array $input)
    {
        Validator::make($input, [
            'username' => ['required', 'string', 'max:255', 'unique:users,username'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => $this->passwordRules(),
            'referrer_code' => ['nullable', 'string', 'max:20'],
        ])->validate();

        //Check Referrer
        $referrer = null;
        if(session()->pull('referrer') != null){
            $referrer = User::whereReferral_token(session()->pull('referrer'))->first();
        }else{
            $referrer = User::where('referral_token', $input['referrer_code'])->first();
        }
        

        // Create user
        $user = User::create([
            'role_id' => 2,
            'referral_token' => Str::random(12),
            'referrer_id' => $referrer == true ? $referrer->id : null,
            'username' => $input['username'],
            'email' => $input['email'],
            'password' => Hash::make($input['password']),
        ]);

        // Connect to block api
        $apiKey = \config('services.block-io.btc-key');
        $pin = "Dannieeng2021";
        $version = 2;
        $label = $user->username .'-'. Str::random(4);
        $block =  Http::get('https://block.io/api/v2/get_new_address/?api_key=' . $apiKey .'&label='.$label.'&address_type=witness_v0' )->json();

        
        //Generate Wallet info for user
        $wallet = Wallet::create([
            'user_id' => $user->id,
            'ngn_balance' => 0.00,
            'btc_address' => $block['data']['address'],
            'btc_balance' => 0.00000000,
            'btc_pd_balance' => 0.00000000,
            'block_label' => $label,
        ]);

        //Profile
        $profile = Profile::create([
            'user_id' => $user->id
        ]);

        //Settings
        $settings = Setting::create([
            'user_id' => $user->id,
        ]);

        if($referrer == true){
            $referral_bonus = Configuration::where('key', 'referral_comission')->first()->value;
            $referral_wallet = $referrer->wallet;
            $referral_wallet->update([
                'ngn_balance' => $referral_bonus
            ]);
        }

        

        return $user;
    }
}
