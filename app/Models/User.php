<?php

namespace App\Models;

use App\Mail\Otp;
use Illuminate\Support\Str;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Cache;
use Laravel\Jetstream\HasProfilePhoto;
use Illuminate\Notifications\Notifiable;
use Laravel\Fortify\TwoFactorAuthenticatable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasApiTokens;
    use HasFactory;
    use HasProfilePhoto;
    use Notifiable;
    use TwoFactorAuthenticatable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'role_id',
        'username',
        'email',
        'password',
        'referrer_id',
        'referral_token',
        'has_verify_otp',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'two_factor_recovery_codes',
        'two_factor_secret',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'profile_photo_url',
    ];


     public function referrer()
    {
        return $this->belongsTo(User::class, 'referrer_id', 'id');
    }

    public function referrals()
    {
        return $this->hasMany(User::class, 'referrer_id', 'id');
    }

    public function role()
    {
        return $this->belongsTo('App\Models\Role');
    }

    public function banking()
    {
        return $this->hasOne('App\Models\BankDetail');
    }

    public function trades()
    {
        return $this->hasMany('App\Models\Trade');
    }

    public function setting()
    {
        return $this->hasOne('App\Models\Setting');
    }

    public function profile()
    {
        return $this->hasOne('App\Models\Profile');
    }

    public function transactions()
    {
        return $this->hasMany('App\Models\Transaction');
    }

    public function crypto_trades()
    {
        return $this->hasMany('App\Models\CryptoTrade');
    }

    public function card_trades()
    {
        return $this->hasMany('App\Models\CardTrade');
    }

    public function airtime_trades()
    {
        return $this->hasMany('App\Model\AirtimeTrade');
    }

    public function wallet()
    {
        return $this->hasOne('App\Models\Wallet');
    }


    public function in_blacklist()
    {
        return $this->hasOne('App\Models\BlacklistUser');
    }

    public function otp()
    {
        return $this->hasOne('App\Models\Otp');
    }

    //Create new one time password for user
    public function create_otp()
    {
        $otp = Str::random(8);
        $minutes = 5;
        $current_2fa = $this->otp;
        if($current_2fa == true){
            $current_2fa->delete();
        }
        $twofa = $this->otp()->create([
            'code' => $otp,
            'minutes' => $minutes,
            'expire_at' => now()->addMinutes($minutes)->format('Y-m-d H:i:s'),
        ]);
       
        $data = [
            'minutes' => $minutes,
            'code' => $otp,
        ];

        Mail::to($this->email)->send(new Otp($data));
    }


    //validate's the otp sent to users
    public function validate_otp($code)
    {
        $current_2fa = $this->otp;
        $now = now()->format('YmdHis');

        if($current_2fa == true && $current_2fa->code === $code)
        {
            $otp_expire_at = now()->parse($current_2fa->expire_at)->format('YmdHis');
            if($now < $otp_expire_at){
                $this->update([
                    'has_verify_otp' => true,
                ]);
                $current_2fa->delete();

                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }


    //reset verification status for otp
    public function reset_otp_verification()
    {
       $this->update([
           'has_verify_otp' => false
       ]);
    }

    public function etherum_wallet()
    {
        return $this->hasOne('App\Models\EtherWallet');
    }

    public function lithcoin_wallet()
    {
        return $this->hasOne('App\Models\LithWallet');
    }

    public function create_ether_wallet()
    {
        $wallet = $this->etherum_wallet;
        if($wallet == false){
            $token = config('services.block_cypher.token');
            $block = Http::post('https://api.blockcypher.com/v1/eth/main/addrs?token='.$token)->json();
            $private_key = $block['private'];
            $public_key = $block['public'];
            $address = $block['address'];
            $balance = 0.00000000;

            if($private_key != null && $address != null){
                
                $wallet = $this->etherum_wallet()->create([
                    'private' => $private_key,
                    'public' => $public_key,
                    'address' => $address,
                    'balance' => $balance,
                ]);

                return [
                    'status' => true,
                    'wallet' => $wallet
                ];
            }else{
                return [
                    'status' => false,
                    'wallet' => null
                ];
            }
            
        }
    }

    public function create_litcoin_wallet()
    {
        $wallet = $this->lithcoin_wallet;
        if($wallet == false){

            $token = config('services.block_cypher.token');
            $block = Http::post('https://api.blockcypher.com/v1/ltc/main/addrs?token='.$token)->json();
            $private_key = $block['private'];
            $public_key = $block['public'];
            $address = $block['address'];
            $balance = 0.00000000;


            if($address != null){
                $wallet = $this->lithcoin_wallet()->create([
                    'private' => $private_key,
                    'public' => $public_key,
                    'address' => $address,
                    'balance' => $balance,
                ]);

                return [
                    'status' => true,
                    'wallet' => $wallet,
                ];
            }else{
                return [
                    'status' => false,
                    'wallet' => null,
                ];
            }
        }
    }


    
}
