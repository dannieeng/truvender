<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CardDefaultRate extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function card()
    {
        return $this->belongsTo('App\Models\Card');
    }
}
