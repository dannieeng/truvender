<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Card extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function country_cards()
    {
        return $this->hasMany('App\Models\CardCountry');
    }

    public function prices()
    {
        return $this->hasMany('App\Models\CardPrice');
    }

    public function types()
    {
        return $this->hasMany('App\Models\CardType');
    }

    public function trades()
    {
       return $this->hasMany('App\Models\CardTrade');
    }
}
