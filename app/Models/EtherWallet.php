<?php

namespace App\Models;

use Illuminate\Support\Facades\Http;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class EtherWallet extends Model
{
    use HasFactory;
    protected $guarded = ['id'];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function transactions()
    {
        return $this->hasMany('App\Models\EtherumTransaction');
    }


    public function transfer_to_otherWallet($value, $wallet)
    {
        $address = $this->address;
        $receiver_address = $wallet;
        $value = $value;
        $password  = time();

        $token = config('services.block_cypher.token');
        $crypto_api_token = config('services.crypto-api.key');
        $endpoint = config('services.crypto-api.base').'eth/ropsten/address/';

        $get_address = Http::withHeaders(['X-API-Key' => $crypto_api_token])->get($endpoint. $this->address)->json();
        $balance = $get_address['payload']['balance'];

        if($balance > $value){
            $get_hash = 'https://api.cryptoapis.io/v1/bc/eth/ropsten/txs/new';
            $api_call_hash = Http::withHeaders(['X-API-Key' => $crypto_api_token])->post($get_hash, [
                'fromAddress' => $address,
                'toAddress' => $receiver_address,
                'gasPrice' => 21000000000,
                'gasLimit' => 21000,
                'value'  => $value,
                'password' => $password,
                "nonce" =>  0
            ])->json();

            if(isset($api_call_hash['payload']['hex'])){
                $hex = $api_call_hash['payload']['hex'];
                $api_call = Http::post('https://api.blockcypher.com/v1/eth/main/txs/push?token='.$token, [
                    'tx' => $hex
                ]);

                if($api_call['hash']){
                    $response = [
                        'status' => true,
                        'data' => $api_call,
                    ];

                }else{
                    $response = [
                        'status' => false
                    ];
                }

                return $response;
                
            }
        }else{
            return [
                'status' => false
            ];
        }

    }

    public function update_address_balance()
    {
        $endpoint = config('services.block_cypher.url').'/eth/main/addrs/' . $this->address;
        $api_call = Http::get($endpoint)->json();

        if(isset($api_call['balance'])){
            $this->update([
            'balance' => $api_call['balance'],
                // 'unconfirmed_balance' => $api_call['unconfirmed_balance'],
                // 'final_balance' => $api_call['final_balance'],
                // 'total_received' => $api_call['total_received'],
                // 'total_sent' => $api_call['total_sent'],
            ]);

            
        }
        else{
            return [
                'status' => false
            ];
        }


    }

    public function get_transactions()
    {
        $address = $this->address;
        $url = config('services.block_cypher.url').'/eth/main/addrs/'.$this->address;
        $api_call = Http::get($url)->json();
        $response = $api_call;


        $data = $response['txrefs'];
        if($data == true){
            return [
                'status' => true,
                'data' => $data
            ];
        }else{
            return [
                'status' => false,
            ];
        }
    }


    public function receive_from_otherWallet($value, $wallet)
    {
        $address = $wallet;
        $receiver_address = $this->address;
        $value = $value;
        $password  = time();

        $token = config('services.block_cypher.token');
        $crypto_api_token = '31deaaca474e16e230a3a9f20fba25e5976b0ed3';

        $get_address = Http::withHeaders(['X-API-Key' => $crypto_api_token])->get('https://api.cryptoapis.io/v1/bc/eth/ropsten/address/'. $this->address)->json();
        $balance = $get_address['payload']['balance'];
        if($balance > $value){
            $get_hash = 'https://api.cryptoapis.io/v1/bc/eth/ropsten/txs/new';
            $api_call_hash = Http::withHeaders(['X-API-Key' => $crypto_api_token])->post($get_hash, [
                'fromAddress' => $address,
                'toAddress' => $receiver_address,
                'gasPrice' => 21000000000,
                'gasLimit' => 21000,
                'value'  => $value,
                'password' => $password,
                "nonce" =>  0
            ])->json();

            if(isset($api_call_hash['payload']['hex'])){
                $hex = $api_call_hash['payload']['hex'];
                $api_call = Http::post('https://api.blockcypher.com/v1/eth/main/txs/push?token='.$token, [
                    'tx' => $hex
                ]);

                if($api_call['hash']){
                    $response = [
                        'status' => true,
                        'data' => $api_call,
                    ];

                }else{
                    $response = [
                        'status' => false
                    ];
                }

                return $response;
                
            }
        }else{
            return [
                'status' => false
            ];
        }

    }

    

    
}
