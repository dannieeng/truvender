<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LitcoinTransaction extends Model
{
    use HasFactory;
    protected $guarded = ['id'];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function litcoin_wallet()
    {
        return $this->belongsTo('App\Models\LithWallet');
    }

    public function trx()
    {
       return $this->belongsTo('App\Models\Transaction');
    }
}
