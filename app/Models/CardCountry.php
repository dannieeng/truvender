<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CardCountry extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function card()
    {
        return $this->belongsTo('App\Models\Card');
    }

    public function country()
    {
        return $this->belongsTo('App\Models\Country', 'country_id', 'id');
    }

    public function type()
    {
        return $this->belongsTo('App\Models\CardType', 'card_type_id', 'id');
    }

    public function price()
    {
        return $this->belongsTo('App\Models\CardPrice');
    }
}
