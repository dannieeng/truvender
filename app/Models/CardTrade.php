<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CardTrade extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function transaction()
    {
        return $this->belongsTo('App\Models\Transaction', 'transaction_id', 'id');
    }

    public function card()
    {
        return $this->belongsTo('App\Models\Card');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
    
    public function country()
    {
        return $this->belongsTo('App\Models\CardCountry', 'country_id', 'id');
    }
    
    public function card_rate()
    {
        return $this->belongsTo('App\Models\CardCountry', 'card_country_id', 'id');
    }


    public function images()
    {
        return $this->hasMany('App\Models\CardTradeImage');
    }

    
}
