<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CardPrice extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function card()
    {
        return $this->belongsTo('App\Models\Card');
    }

    public function card_type()
    {
        return $this->belongsTo('App\Models\CardType');
    }

    public function rate()
    {
        return $this->belongsTo('App\Models\CardCountry');
    }
}
