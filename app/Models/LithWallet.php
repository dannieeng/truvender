<?php

namespace App\Models;

use Illuminate\Support\Facades\Http;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class LithWallet extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function transactions()
    {
        return $this->hasMany('App\Models\LitcoinTransaction');
    }


    public function update_address_balance()
    {
        $endpoint = config('services.block_cypher.url').'/ltc/main/addrs/' . $this->address;
        $api_call = Http::get($endpoint)->json();

        if(isset($api_call['balance'])){
            $this->update([
            'balance' => $api_call['balance'],
                // 'unconfirmed_balance' => $api_call['unconfirmed_balance'],
                // 'final_balance' => $api_call['final_balance'],
                // 'total_received' => $api_call['total_received'],
                // 'total_sent' => $api_call['total_sent'],
            ]);

            
        }
        else{
            return [
                'status' => false
            ];
        }
        
    }

    public function transfer($address, $value)
    {
        $api_key = config('services.block-io.ltc-key');
        $sender = $this->address;
        $amount = $value;
        $endpoint = 'https://block.io/api/v2/withdraw_from_addresses/?api_key='.$api_key.'&from_addresses='.$sender.'&to_addresses='.$address.'&amounts='.$amount;
        $api_call = Http::post($endpoint)->json();

        if($this->balance > $value && isset($api_call['data'])){
            $key = config('services.block_cypher.token');
            $hex = $api_call['data']['unsigned_tx_hex'];
            $endpoint = config('services.block_cypher.url').'/ltc/main/txs/push?token'.$key;
            $push = Http::post($endpoint, [
                'hex' => $hex
            ])->json();

            if($push['hash']){
                $response = [
                    'status' => true,
                    'data' => $push,
                ];
            }else {
                $response = [
                    'status' => false
                ];
            }

            return $response;
        }else {
            return [
                'status' => false
            ];
        }

    }


    public function receive($address, $value)
    {
        $api_key = config('services.block-io.ltc-key');
        $sender = $address;
        $receiver = $this->address;
        $amount = $value;
        $endpoint = 'https://block.io/api/v2/withdraw_from_addresses/?api_key='.$api_key.'&from_addresses='.$sender.'&to_addresses='.$receiver.'&amounts='.$amount;
        $response = Http::post($endpoint)->json();

        if($this->balance > $value && isset($api_call['data'])){
            $key = config('services.block_cypher.token');
            $hex = $api_call['data']['unsigned_tx_hex'];
            $endpoint = config('services.block_cypher.url').'/ltc/main/txs/push?token'.$key;
            $push = Http::post($endpoint, [
                'hex' => $hex
            ])->json();

            if($push['hash']){
                $response = [
                    'status' => true,
                    'data' => $push,
                ];
            }else {
                $response = [
                    'status' => false
                ];
            }

            return $response;
        }else {
            return [
                'status' => false
            ];
        }
    }

    // public function sign_transaction($data)
    // {
    //     $key = config('services.block-io.ltc-key');
    //     $trx_data = json_encode($data);
    //     $response = Http::post('https://block.io/api/v2/sign_and_finalize_withdrawal/?api_key='.$key.'signature_data=' . $trx_data)->json();

    //     if($response->successful()){
    //         if($response['status'] == 'success'){
    //             return [
    //                 'status' => true,
    //                 'data' => $response['data']
    //             ];
    //         }

    //         return [
    //             'status' => false
    //         ];
    //     }else{
    //         return [
    //             'status' => false
    //         ];
    //     }
    // }

    public function sign_transaction($data)
    {
        $trx_data = json_encode($data);
        $key = config('services.block_cypher.token');
        $endpoint = config('services.crypto-api.base').'bcy/main/txs/push?token'.$key;
        $response = Http::post($endpoint, [
            'hex' => $trx_data['unsigned_tx_hex']
        ])->json();
        
        if($api_call['hash']){
            $response = [
                'status' => true,
                'data' => $api_call,
            ];

        }else{
            $response = [
                'status' => false
            ];
        }
    }


    public function get_transactions()
    {
        $address = $this->address;
        $url = config('services.block_cypher.url').'/ltc/main/addrs/'.$address;
        $api_call = Http::get($url)->json();
        
        $data = $response['txrefs'];
        if($data == true){
            return [
                'status' => true,
                'data' => $data
            ];
        }else{
            return [
                'status' => false,
            ];
        }
    }


    public function get_transaction_received()
    {
        $address = $this->address;
        $key = config('services.block-io.ltc-key');
        $endpoint = 'https://block.io/api/v2/get_transactions/?api_key='.$key.'&type=received&addresses='.$address;
        $api_call = Http::get($endpoint)->json();

        return $api_call['status'] == 'success' ? $api_call['data']['tx'] : null;
    }


    public function get_transaction_sent()
    {
        $address = $this->address;
        $key = config('services.block-io.ltc-key');
        $endpoint = 'https://block.io/api/v2/get_transactions/?api_key='.$key.'&type=sent&addresses='.$address;
        $api_call = Http::get($endpoint)->json();

        return $api_call['status'] == 'success' ? $api_call['data']['tx'] : null;
    }


    

}
