<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Partner extends Model
{
    use HasFactory;

    protected $fillable = [
        'name', 'link', 'logo'
    ];

    //Logo Path images/partners/logo

    public function getLogo($avatar){
        return asset($avatar);
    }
}
