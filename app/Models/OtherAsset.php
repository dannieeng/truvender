<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OtherAsset extends Model
{
    use HasFactory;


    protected $guarded = ['id'];

    public function rate()
    {
        return $this->hasOne('App\Models\OtherAssetRate');
    }

    public function transactions()
    {
        return $this->hasMany('App\Models\OtherAssetTransaction');
    }

    public function currency()
    {
        return $this->belongsTo('App\Models\Country', 'country_id');
    }

    public function emails()
    {
        return $this->hasMany('App\Models\OtherAssetEmail');
    }
    
}
