<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CryptoTrade extends Model
{
    use HasFactory;

    protected $fillable = ['user_id', 
    'transaction_id', 
    'asset', 'amount_fiat', 
    'amount_crypto', 'amount_ngn', 'status'];

    public function transaction()
    {
        return $this->belongsTo('App\Models\Transaction');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

}
