<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function traded_item()
    {
        if($this->section == 'crypto'){
            return $this->hasOne('App\Models\CryptoTrade');
        }elseif($this->section == 'airtime'){
            return $this->hasOne('App\Models\AirtimeTrade');
        }elseif($this->section =='card'){
            return $this->hasOne('App\Models\CardTrade');
        }
    }
}
