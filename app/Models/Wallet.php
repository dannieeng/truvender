<?php

namespace App\Models;

use Illuminate\Support\Facades\Http;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Wallet extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function get_btc_balance()
    {
       
        //Api Call
        $api_key = \config('services.block-io.btc-key');
    
        $appeded = 'https://block.io/api/v2/' . 'get_address_balance/?api_key='.$api_key . '&addresses='.$this->btc_address;
        $block = Http::get($appeded)->json();

        return $block['data'];
    }


    public function update_btc_balance()
    {
        $get_balance = $this->get_btc_balance;
        $this->update([
            'btc_balance' => $get_balance['available_balance'],
            'btc_pd_balance' => $get_balance['pending_balance']
        ]);


        return [
            'available_balance' => $this->btc_balance,
            'pending_balance' => $this->btc_pd_balance
        ];
    }


    public function transfer_btc($address, $value)
    {
        $api_key = config('services.block-io.btc-key');
        $sender = $this->btc_address;
        $amount = $value;
        $endpoint = 'https://block.io/api/v2/withdraw_from_addresses/?api_key='.$api_key.'&from_addresses='.$sender.'&to_addresses='.$address.'&amounts='.$amount;
        $api_call = Http::post($endpoint)->json();

        if($this->balance > $value && isset($api_call['data'])){
            $key = config('services.block_cypher.token');
            $hex = $api_call['data']['unsigned_tx_hex'];
            $endpoint = config('services.block_cypher.url').'/btc/main/txs/push?token'.$key;
            $push = Http::post($endpoint, [
                'hex' => $hex
            ])->json();

            if($push['hash']){
                $response = [
                    'status' => true,
                    'data' => $push,
                ];
            }else {
                $response = [
                    'status' => false
                ];
            }

            return $response;
        }else {
            return [
                'status' => false
            ];
        }
    }


    public function receive_btc($address, $value)
    {
        $api_key = config('services.block-io.btc-key');
        $sender = $address;
        $receiver = $this->btc_address;
        $amount = $value;
        $endpoint = 'https://block.io/api/v2/withdraw_from_addresses/?api_key='.$api_key.'&from_addresses='.$sender.'&to_addresses='.$receiver.'&amounts='.$amount;
        $response = Http::post($endpoint)->json();

        if($this->balance > $value && isset($api_call['data'])){
            $key = config('services.block_cypher.token');
            $hex = $api_call['data']['unsigned_tx_hex'];
            $endpoint = config('services.block_cypher.url').'/btc/main/txs/push?token'.$key;
            $push = Http::post($endpoint, [
                'hex' => $hex
            ])->json();

            if($push['hash']){
                $response = [
                    'status' => true,
                    'data' => $push,
                ];
            }else {
                $response = [
                    'status' => false
                ];
            }

            return $response;
        }else {
            return [
                'status' => false
            ];
        }
    }

    public function update_btc_address()
    {
        $endpoint = config('services.block_cypher.url').'/btc/main/addrs/' . $this->btc_address;
        $api_call = Http::get($endpoint)->json();

        if(isset($api_call['balance'])){
            $this->update([
                'btc_balance' => $api_call['balance'],
            ]);

            return [
                'status' => true
            ];
        }
        else{
            return [
                'status' => false
            ];
        }
        
    }


    


}
