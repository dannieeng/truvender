<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OtherAssetEmail extends Model
{
    use HasFactory;

    protected $guarded = [
        'id'
    ];

    public function asset()
    {
        return $this->belongsTo('App\Models\OtherAsset');
    }


}
