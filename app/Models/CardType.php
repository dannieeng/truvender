<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CardType extends Model
{
    use HasFactory;
    protected $guarded =['id'];
    

    public function card_prices()
    {
        return $this->hasMany('App\Models\CardPrice', 'card_price_id', 'id');
    }

    public function card()
    {
        return $this->belongsTo('App\Models\Card', 'card_id', 'id');
    }
}
