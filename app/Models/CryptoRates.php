<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CryptoRates extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'symbol', 'image', 'buyer_rate', 'seller_rate'];

}
