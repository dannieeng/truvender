<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CardTradeImage extends Model
{
    use HasFactory;
    protected $guarded = ['id'];
    

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function traded_from(){
        return $this->belongsTo('App\Models\CardTrade', 'card_trade_id', 'id');
    }
}
