<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OtherAssetTransaction extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }


    public function asset()
    {
        return $this->belongsTo('App\Models\OtherAsset', 'other_asset_id', 'id');
    }
    

    public function transaction()
    {
        return $this->belongsTo('App\Models\Transaction');
    }
}
