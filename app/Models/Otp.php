<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Otp extends Model
{
    use HasFactory;
    protected $fillable = ['user_id', 'code', 'expire_at', 'minutes'];
    

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }


}
