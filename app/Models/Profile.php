<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    //Avatar Path images/user/avatar/

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    // public function getAvatar($avatar){
    //     return asset($avatar);
    // }
}
