<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BillerCategory extends Model
{
    use HasFactory;

    protected $guarded = ['id'];
    
    public function billers()
    {
        return $this->hasMany('App\Models\Biller');
    }
}
