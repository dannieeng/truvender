<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    use HasFactory;

    protected $fillable = [
        'name', 'flag', 'currency'
    ];


    public function getFlage($value)
    {
        return asset($value);
    }

    public function country_cards()
    {
        return $this->hasMany('App\Models\CardCountry');
    }
}
