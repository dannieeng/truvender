<?php
namespace App\Helper;

use Illuminate\Support\Facades\Http;

class Conversion
{
    public static function convert_ngn_to_ghs($amount)
    {
        $url = 'https://api.exchangerate.host/convert?from=NGN&to=GHS&amount=' . $amount;
        $api_call = Http::get($url)->json();
        $result = $api_call['result'];
        return $result;
    }

    public static function convert_ngn_to_usd($amount)
    {
        $url = 'https://api.exchangerate.host/convert?from=NGN&to=USD&amount=' . $amount;
        $api_call = Http::get($url)->json();
        $result = $api_call['result'];
        return $result;
    }

    public static function convert_usd_to_ngn($amount)
    {
        $url = 'https://api.exchangerate.host/convert?from=USD&to=NGN&amount=' . $amount;
        $api_call = Http::get($url)->json();
        $result = $api_call['result'];
        return $result;
    }

    public static function convert_ghs_to_ngn($amount)
    {
        $url = 'https://api.exchangerate.host/convert?from=GHS&to=NGN&amount=' . $amount;
        $api_call = Http::get($url)->json();
        $result = $api_call['result'];
        return $result;
    }


    public static function convert_usd_to_btc($amount)
    {
        $url = 'https://api.exchangerate.host/convert?from=USD&to=BTC&amount=' . $amount . '&source=crypto';
        $api_call = Http::get($url)->json();
        $result = $api_call['result'];
        return $result;
    }

    public static function convert_usd_to_eth($amount)
    {
        $url = 'https://api.exchangerate.host/convert?from=USD&to=ETH&amount=' . $amount . '&source=crypto';
        $api_call = Http::get($url)->json();
        $result = $api_call['result'];
        return $result;
    }
    


    public static function convert_usd_to_LTC($amount)
    {
        $url = 'https://api.exchangerate.host/convert?from=USD&to=LTC&amount=' . $amount . '&source=crypto';
        $api_call = Http::get($url)->json();
        $result = $api_call['result'];
        return $result;
    }
}