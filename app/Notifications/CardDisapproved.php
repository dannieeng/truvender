<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class CardDisapproved extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public $data;
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        $user = $notifiable;
        $setting = $user->setting;
        return $setting->notify_buy_sell_btc == 'both' ? ['mail'] : ($setting->notify_buy_sell_btc == 'push' ? ['database'] : ['mail']);
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $data = $this->data;
        return (new MailMessage)
                    ->subject('GiftCard Image Disapproved')
                    ->greeting('Hello, '. $data['user']->name)
                    ->line('One of the giftcard images you submitted was disapproved beacuse, ' .$data['reason'])
                    ->line('Total Giftcards Submitted: ' . $data['cards_submitted'])
                    ->line('Approved GiftCards: ' . $data['amount_approved'])
                    ->line('Disaprroved GiftCards: ' . $data['amount_disapproved'])
                    ->line('Wallet Balance Naira: N' . $data['balance'])
                    ->line('Thank you for choosing Truvender!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
