<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class NewBtcTransaction extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */

    public $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $data = $this->data;
        return (new MailMessage)
                    ->subject('New Transaction')
                    ->greeting('Hello, '. \ucfirst($data['username']))
                    ->line('A New. ' . ucfirst($data['status']) . ' Transaction Occured in Your ' . config('app.name') . ' Account.')
                    ->line('See the summary below.')
                    ->line('Data/time: ' . $data['date'])
                    ->line('Credit/Debit: ' . ucfirst($data['type']))
                    ->line('Amount: ' . $data['amount'] .'('. $data['currency'].')' )
                    ->line('Wallet: ' . $data['wallet'])
                    ->line('Current Balance: ' . $data['balance'])
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
